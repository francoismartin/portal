# BioMedIT Portal

[Deploy Portal in production](docs/deploy.md)

[Setup a local environment for development](docs/dev.md).

## Releases

This project follows the [semantic versioning specification](https://semver.org/)
for its [releases](https://gitlab.com/biomedit/portal/-/releases).

**NOTE:** Releases with a version containing `-dev` are not considered stable
and should **NOT** be used in production. Docker images of releases with a
version containing `-dev` are automatically removed after 30 days.

## Usage

The service can be explored through a web browser or accessed directly
through the API; `biomedit_users.py` shows an example of a script that uses
the API. API endpoints are available at `/backend/` and a detailed API schema
at `/backend/schema/`.

**NOTE:** Most actions should be done via the frontend, e.g. adding projects,
managing user roles, adding project resources.

### Adding users

User accounts are automatically created, based on the OIDC account information,
on the first login.

It is also possible to create local user accounts (e.g. service or test
users) using the admin panel (`/backend/admin/auth/user/`).

**NOTE:** When creating a local user account, you must create a new account
with an empty profile first
(see the [issue](https://gitlab.com/biomedit/portal/-/issues/2)).
You can edit the profile information once the new account has been created.

### Permissions

Only authenticated users can see any data. Access to data (both read and write)
is further restricted with specific permission. Global permissions like
"staff" (can read and write all data) can be configured using the Django
admin panel (/backend/admin/).

Custom permissions (model/object-based) can be assigned to groups.
Users can be added to these groups to gain the corresponding group permissions.

Local permission (role) can be assigned per project.

- "Project Leader"
- "Permission Manager"
- "Data Manager"
- "User"

## Customization

### Customize frontend text

To change text used in the frontend, copy the files in `frontend/public/locales`
and mount them into the `frontend` container in your `docker-compose.yml`.

Assuming you have a folder `locales` on the same level as your
`docker-compose.yml` file, your `docker-compose.yml` entry for the `frontend`
container needs to look like this:

```yaml
  frontend:
    ...
    volumes:
      - ./locales:/usr/app/public/locales:ro
    ...
```

### Update supported browsers

When a visitor uses an unsupported browser, they see a message instead of the
website, telling them to use a supported browser along with which browsers are
supported.

Should this change at some point (e.g. because new JavaScript features that are
not polyfilled are used), it will be necessary to update the supported browsers
in the code.

To do this, follow those steps:

1. Adapt the `browserslist` for `production` in `package.json` to the browsers
   that should be newly supported.
   - You can use <https://browserslist.dev/> to check if your `browserslist` is
     correct and which browsers are included.
   - Instead of using the array given by the website directly, join all array
     entries with a comma, followed by a space.
2. Run `npm run supportedBrowsers`
3. Change `unsupportedBrowser` in `public/locales/en/common.json` to explain
   which browsers we now support.
