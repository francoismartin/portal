stages:
  - test
  - prepare-release
  - release
  - release-gitlab

include:
  - template: Workflows/Branch-Pipelines.gitlab-ci.yml # run pipelines for branches and tags
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - project: biomedit/tools/ci
    file:
      - "/template/api-schema-check.yml"
      - "/template/commitlint.yml"
      - "/template/danger-js.yml"
      - "/template/prepare-prerelease.yml"
      - "/template/prepare-release-from-prerelease.yml"
      - "/template/prettier.yml"
      - "/template/python-bandit.yml"
      - "/template/python-black.yml"
      - "/template/python-django-migration-check.yml"
      - "/template/python-pylint.yml"
      - "/template/python-tox-parallel.yml"
      - "/template/release-gitlab-container-registry.yml"
      - "/template/release-gitlab-prerelease.yml"
      - "/template/release-gitlab.yml"
      - "/template/ts-depcheck.yml"
      - "/template/ts-dpdm.yml"
      - "/template/ts-eslint.yml"
      - "/template/ts-jest.yml"
      - "/template/ts-type-check.yml"
      - "/template/ts-type-coverage.yml"

variables:
  ESLINT_MAX_WARNINGS: "17"
  NODE_BASE: frontend
  PYLINT_REQUIREMENTS: >-
    pylint
    pylint-django
    git+https://gitlab.com/biomedit/tools/biomedit-lints.git
    -r requirements.txt
    -r requirements-test.txt
  PYLINT_TARGETS: "portal identities projects"
  PYTHON_BASE: web
  SAST_EXCLUDED_PATHS: "spec,test,tests,tmp,frontend/src/api/generated,*.test.*"
  DPDM_FILE_PATHS_OR_GLOBS: "frontend/src/**/*.ts*"

python-tox-parallel:
  script:
    - tox

python-pylint:
  image: docker.io/library/python

release-gitlab-container-registry:
  before_script:
    - touch .env frontend/.env

# Make sure that config files from docker based deployment and k8s
# based deployment do not diverge:
config-consistency:
  stage: test
  image: docker.io/library/python:alpine
  before_script:
    - pip install pyyaml
  script:
    - diff -u k8s/base/files/nginx.conf services/nginx/default.conf
    - diff -u k8s/base/files/init-db.sh tools/init-db.sh
    # Schema diffs of portal config:
    - |
      ./tools/dict_schema.py k8s/base/files/portal-config.json | sort > k8s.txt \
       && ./tools/dict_schema.py web/.config.json.sample | sort > docker.txt \
       && diff -u k8s.txt docker.txt
    - |
      ./tools/dict_schema.py k8s/overlays/staging/files/portal-config.json | sort > k8s.txt \
       && ./tools/dict_schema.py web/.config.json.sample | sort > docker.txt \
       && diff -u k8s.txt docker.txt
    # diff of env vars:
    - |
      (cat k8s/overlays/staging/files/*.env \
        | grep -o '^[^=]*' ; \
       ./tools/extract_env_vars.py k8s/overlays/staging/kustomization.yml backend-env ;
       ./tools/extract_env_vars.py k8s/base/kustomization.yml backend-env secret-backend-env secret-db-connection-env secret-db-env) \
       | sort -u > k8s.txt \
       && grep -o '^[^=#]*' ./.env.sample | grep -v -e '^REGISTRY' | sort > docker.txt \
       && diff -u k8s.txt docker.txt
    - |
      ./tools/extract_env_vars.py k8s/base/kustomization.yml backend-env secret-backend-env secret-db-connection-env secret-db-env | sort > k8s.txt \
       && grep -o '^[^=#]*' ./.env.sample | grep -v -e '^REGISTRY' -e '^LOGSTASH_HOST' | sort > docker.txt \
       && diff -u k8s.txt docker.txt
    - |
      (./tools/extract_env_vars.py k8s/base/kustomization.yml frontend-env ; \
       ./tools/extract_env_vars.py k8s/overlays/staging/kustomization.yml frontend-env ) \
        | sort -u > k8s.txt \
       && grep -o '^[^=#]*' ./frontend/.env.sample | sort > docker.txt \
       && diff -u k8s.txt docker.txt
  rules:
    - if: $CI_COMMIT_TAG == null # Don't run on tags
