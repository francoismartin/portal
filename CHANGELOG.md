# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [4.7.0](https://gitlab.com/biomedit/portal/compare/4.6.0...4.7.0) (2022-10-12)


### Features

* **backend/DtrNotification:** add ticket mail to dtr creation email body ([3fd7c10](https://gitlab.com/biomedit/portal/commit/3fd7c1019efcea89ac44aa315e7d2de8f0eb0611))
* **backend/DtrNotification:** exclude ticket mail from single approvals emails ([dba648e](https://gitlab.com/biomedit/portal/commit/dba648e742d0fa994de2fe386e3177342e8f5a1e))
* **backend/logging:** log client-side exceptions at the WARNING level ([bf2a1a2](https://gitlab.com/biomedit/portal/commit/bf2a1a2da604835e0878288c036a19cb384012af)), closes [#678](https://gitlab.com/biomedit/portal/issues/678)
* **backend/tasks:** periodically check for revoked keys on the keyserver ([864818d](https://gitlab.com/biomedit/portal/commit/864818d6144c75f09b06d321dcaae672447cabfa)), closes [#672](https://gitlab.com/biomedit/portal/issues/672) [#685](https://gitlab.com/biomedit/portal/issues/685)
* **DataProvider:** add deeplink for DP ([c07aad1](https://gitlab.com/biomedit/portal/commit/c07aad12b6096288c7dcc9fb5f16c3312edf894f)), closes [#689](https://gitlab.com/biomedit/portal/issues/689)
* **ProjectList:** support access for specific projects ([7653af7](https://gitlab.com/biomedit/portal/commit/7653af732849ae3473616ffa67e0063f841bfd35)), closes [#678](https://gitlab.com/biomedit/portal/issues/678)
* **Tab:** add support for pre-selected tab ([4e10e61](https://gitlab.com/biomedit/portal/commit/4e10e619a3906cc890d2cfb36deaba7aaa67d926)), closes [#689](https://gitlab.com/biomedit/portal/issues/689)


### Bug Fixes

* approvals layout when none of them is approved/rejected ([0866188](https://gitlab.com/biomedit/portal/commit/086618897a095df65ed66586e6819db5d0c28502)), closes [#681](https://gitlab.com/biomedit/portal/issues/681)
* **backend/logout:** remove OIDC RP-Initiated Logout incompatible implementation ([a732efa](https://gitlab.com/biomedit/portal/commit/a732efa10a5950a898c170a3a112a2c5b5791fdc)), closes [#688](https://gitlab.com/biomedit/portal/issues/688)
* **DataTransferDetails:** deletion of DTR throws an exception in frontend ([6c4be80](https://gitlab.com/biomedit/portal/commit/6c4be80e24c525234266ce41af1c8ea32592435e)), closes [#693](https://gitlab.com/biomedit/portal/issues/693)
* **DataTransferPathDetail:** deletion of DTR throws an exception ([2bcdd2b](https://gitlab.com/biomedit/portal/commit/2bcdd2bf70d690048778ba0417fc23b8d8ccd176)), closes [#682](https://gitlab.com/biomedit/portal/issues/682)
* **next-redux-wrapper:** warning on legacy implementation ([531f31e](https://gitlab.com/biomedit/portal/commit/531f31e1aae15c48fd8b6a5466576796907a22da))

## [4.6.0](https://gitlab.com/biomedit/portal/compare/4.5.0...4.6.0) (2022-09-20)


### Features

* add public endpoint returning the status of PGP keys ([10b8383](https://gitlab.com/biomedit/portal/commit/10b8383e99e95a4d7cbcf7b81f7db2137c131245)), closes [#645](https://gitlab.com/biomedit/portal/issues/645)
* add support for multiple keys to pgpkey status endpoint ([3d3c267](https://gitlab.com/biomedit/portal/commit/3d3c26735b3cc094aa5f1f4f571575d50c249c0b)), closes [#646](https://gitlab.com/biomedit/portal/issues/646)
* **approval:** add approval log in final approval notification email ([5bd8051](https://gitlab.com/biomedit/portal/commit/5bd8051db5e11ef9f0924690d45955a8ee34c9dc)), closes [#648](https://gitlab.com/biomedit/portal/issues/648)
* **backend/dataPackage:** use pgp_key_info as source of truth instead of signature and keyserver ([193fc71](https://gitlab.com/biomedit/portal/commit/193fc71d7e7af4ad19eefa1983f3d3978aeab2c5)), closes [#662](https://gitlab.com/biomedit/portal/issues/662)
* **backend/PgpKeyInfo:** add lookup filter to endpoint ([256ff24](https://gitlab.com/biomedit/portal/commit/256ff246affc78e63bde4a9581579715f5b9c414)), closes [#643](https://gitlab.com/biomedit/portal/issues/643)
* **backend/pgpKeyInfo:** send automated email when an approval is approved or rejected ([c9b07e6](https://gitlab.com/biomedit/portal/commit/c9b07e625b562cf6b98e8d8189f9a63f19ab4fae)), closes [#637](https://gitlab.com/biomedit/portal/issues/637)
* **backend:** better error handling for short or non-rsa gpg key signing requests ([31ab59b](https://gitlab.com/biomedit/portal/commit/31ab59b44c81ed02e704811aa1c0f0e574c01fe0)), closes [#603](https://gitlab.com/biomedit/portal/issues/603)
* **frontend/Admin:** add keys tab ([db4a4a1](https://gitlab.com/biomedit/portal/commit/db4a4a1bc94604b4dc1a7f188f861631c60476c2)), closes [#605](https://gitlab.com/biomedit/portal/issues/605)
* **frontend/admin:** make codes readonly for nodes, projects, and data providers ([e13ce23](https://gitlab.com/biomedit/portal/commit/e13ce235bf083054d643d052229fb571cfcc92cb)), closes [#632](https://gitlab.com/biomedit/portal/issues/632)
* **frontend/Approvals:** when rejecting an approval, a reason should be given ([35b9bd1](https://gitlab.com/biomedit/portal/commit/35b9bd117b8435680761289db92fed8bc98335c0)), closes [#550](https://gitlab.com/biomedit/portal/issues/550)
* **frontend/DataTransferForm:** use db as source instead of keyserver ([e7ddce9](https://gitlab.com/biomedit/portal/commit/e7ddce9d8529ca4a90f176f7f56973f6744cdb54)), closes [#671](https://gitlab.com/biomedit/portal/issues/671)
* **notification:** show user affiliations in project notifications ([44a0b75](https://gitlab.com/biomedit/portal/commit/44a0b75579bac314821620d693d3b13f2a0b2bdf)), closes [#666](https://gitlab.com/biomedit/portal/issues/666)
* **tasks:** make `user_last_logged_in_notification` configurable via DB ([fac6423](https://gitlab.com/biomedit/portal/commit/fac64233138295b8219d04ce395af54c01ad456d)), closes [#655](https://gitlab.com/biomedit/portal/issues/655)
* turn errors related to non-`AUTHORIZED` DTR into INFO ([6df67c3](https://gitlab.com/biomedit/portal/commit/6df67c3d9b3b1e96283279ada1498636fac413fd)), closes [#661](https://gitlab.com/biomedit/portal/issues/661)
* **UserInfo:** sort groups alphabetically in user form dropdown menu ([016a8f6](https://gitlab.com/biomedit/portal/commit/016a8f6c10e8d915f6ec7038c7acf56fe755388b)), closes [#667](https://gitlab.com/biomedit/portal/issues/667)


### Bug Fixes

* **backend/DataPackageSerializer:** cover missing pgp_key_info scenario in get_sender_pgp_key_info ([4dc8b42](https://gitlab.com/biomedit/portal/commit/4dc8b424fbaa8c062bfba89839cbb612b8044631)), closes [#665](https://gitlab.com/biomedit/portal/issues/665)
* **backend/notifications:** fix typo in dtr confirmation email body ([e958cde](https://gitlab.com/biomedit/portal/commit/e958cde64cf269f6e595851b795bd55ce621a9ed)), closes [#630](https://gitlab.com/biomedit/portal/issues/630)
* **backend:** correct error handling in key metadata download ([734d6b5](https://gitlab.com/biomedit/portal/commit/734d6b55502046aedf6fde365bf8296f8bd6c63e)), closes [#631](https://gitlab.com/biomedit/portal/issues/631)
* **backend:** do not throw error 500 when sending garbage to projects-users backend ([0960401](https://gitlab.com/biomedit/portal/commit/0960401d5199947885d018d31d342fb7f92bc641)), closes [#640](https://gitlab.com/biomedit/portal/issues/640)
* **cimmitlint:** `fixup` should be accepted in local environment ([03af561](https://gitlab.com/biomedit/portal/commit/03af5611586ad992e07baed99232676851b8a218)), closes [#608](https://gitlab.com/biomedit/portal/issues/608)
* **frontend/administration:** show only relevant DPs to DP users ([b75eede](https://gitlab.com/biomedit/portal/commit/b75eede7109e606bf9ee7e54f66cddb8bc244747)), closes [#634](https://gitlab.com/biomedit/portal/issues/634)
* **frontend/app:** restore old color scheme ([e43f445](https://gitlab.com/biomedit/portal/commit/e43f445aaf41a9339338082b01037857330f9a13)), closes [#664](https://gitlab.com/biomedit/portal/issues/664)
* **frontend/DataTransferApprovalForm:** change existingLegalBasis checkbox text ([c4cc163](https://gitlab.com/biomedit/portal/commit/c4cc16365bd00e037aa599b61c7f5b2fc3fd05ba)), closes [#629](https://gitlab.com/biomedit/portal/issues/629)
* revert Next.js to v12.2.5 ([cec5a7e](https://gitlab.com/biomedit/portal/commit/cec5a7e3921f13661269a4d07d0fdb17630da6f4)), closes [#677](https://gitlab.com/biomedit/portal/issues/677)

## [4.5.0](https://gitlab.com/biomedit/portal/compare/4.4.0...4.5.0) (2022-06-30)

### Features

* **app:** add biomedit logo to header ([8f90540](https://gitlab.com/biomedit/portal/commit/8f905409bb63b7e5dd364bf47bba5a92af5d897c)), closes [#610](https://gitlab.com/biomedit/portal/issues/610)
* **backend/dataProvider:** give permissions over new dp groups to dp managers ([52d2a6d](https://gitlab.com/biomedit/portal/commit/52d2a6d0d85a2585cfe12c12e3bfaa34c5d66c4f)), closes [#595](https://gitlab.com/biomedit/portal/issues/595)
* **CI/danger:** notify technical coordinator about UI/UX changes ([98cecb5](https://gitlab.com/biomedit/portal/commit/98cecb536b237acf46d876925ca647f65901ac9a)), closes [#619](https://gitlab.com/biomedit/portal/issues/619)
* **ci:** fixup! should trigger a commit-lint error ([c0eb1bf](https://gitlab.com/biomedit/portal/commit/c0eb1bf4d5b423a50fb2272d921bf35c91eb81d4)), closes [#608](https://gitlab.com/biomedit/portal/issues/608)
* **DataProvider:** show users and their roles on Data Providers ([5cc6624](https://gitlab.com/biomedit/portal/commit/5cc66245225d8aafb5bbd525f0f70828b5198c5d)), closes [#583](https://gitlab.com/biomedit/portal/issues/583)
* **feed:** add a possibility to set a time limit on Feeds ([d0a1010](https://gitlab.com/biomedit/portal/commit/d0a10101c002275a11151e7f5dc50dacd8388155)), closes [#320](https://gitlab.com/biomedit/portal/issues/320)
* **frontend/admin:** show data providers tab to node viewers and admins ([c6b66af](https://gitlab.com/biomedit/portal/commit/c6b66afc33e8b85ce0d099c3b4376b0806211e13)), closes [#580](https://gitlab.com/biomedit/portal/issues/580)
* **frontend/DataTransferDetail:** avoid dialog changing its height between different tabs ([493dce9](https://gitlab.com/biomedit/portal/commit/493dce95ec2ba4b6c46600bfc203dfa3461fface)), closes [#618](https://gitlab.com/biomedit/portal/issues/618)
* **frontend/Keys:** add '+ KEY' button ([4aff66f](https://gitlab.com/biomedit/portal/commit/4aff66f2299defbe76e43a3322b15ae967fa2747))
* **frontend/Keys:** switch to new Keys page depending on env variable ([7134684](https://gitlab.com/biomedit/portal/commit/7134684a81d294feb363400fcfd6b13e0688c006))
* **frontend/PgpKeyInfoList:** add new PgpKeyInfoList component ([2c55e2f](https://gitlab.com/biomedit/portal/commit/2c55e2fc0dc22ffa09257c9b80685bec3be879f2))
* **frontend/PgpKeyInfoList:** add retireKeyButton to Keys page ([6d9b3fc](https://gitlab.com/biomedit/portal/commit/6d9b3fc02234bd1e487eaea49709f325458ae7a8)), closes [#464](https://gitlab.com/biomedit/portal/issues/464)
* **frontend/reducers:** add reducer for PgpKeyInfo model ([f6c8d8c](https://gitlab.com/biomedit/portal/commit/f6c8d8c33421356617822d89227e2dfc0c124e88))
* **Node:** show users and their roles on Nodes ([4df4641](https://gitlab.com/biomedit/portal/commit/4df464186f007fd89d2d4bd502a20742989f4e72)), closes [#432](https://gitlab.com/biomedit/portal/issues/432)
* **pgpKeyInfo/notifications:** send automatic email upon new approval request ([cc820ae](https://gitlab.com/biomedit/portal/commit/cc820aec0dfecc5487687847b3955097f8620e2d))

### Bug Fixes

* **backend/notification:** do not send DTR final confirmation email to node managers ([2db63b9](https://gitlab.com/biomedit/portal/commit/2db63b97267d81627f6746d6b1343c1002aabac4)), closes [#598](https://gitlab.com/biomedit/portal/issues/598)
* **backend:** superusers are automatically DP coordinators ([a98c66c](https://gitlab.com/biomedit/portal/commit/a98c66c9797900ee204a67ff8991e03d4b6a14f4)), closes [#621](https://gitlab.com/biomedit/portal/issues/621)
* correct error handling in key metadata download ([f93016a](https://gitlab.com/biomedit/portal/commit/f93016aa278ab144054e56d401f1728190ac62b8)), closes [#623](https://gitlab.com/biomedit/portal/issues/623)
* **data_package:** improve error message in case of non-existing node ([e223741](https://gitlab.com/biomedit/portal/commit/e223741bbd8c41c7ab19a3a6c02405882e95a728)), closes [#628](https://gitlab.com/biomedit/portal/issues/628)
* **frontend/DataTransferForm:** show form only after keys have been loaded ([40ea9f9](https://gitlab.com/biomedit/portal/commit/40ea9f90780ac8345cd10cda7016ea47438bbdf7)), closes [#589](https://gitlab.com/biomedit/portal/issues/589)
* **frontend/IpRangeList:** fix delete button color ([3f66602](https://gitlab.com/biomedit/portal/commit/3f666029faf478aed9a5d7cb770795fa02a203ab)), closes [#591](https://gitlab.com/biomedit/portal/issues/591) [#557](https://gitlab.com/biomedit/portal/issues/557)
* **frontend/Userinfo:** duplicate entries for groups in userinfo after specifying the username ([69d3214](https://gitlab.com/biomedit/portal/commit/69d32142051d0a76eeacee707574fd00f3574e38)), closes [#563](https://gitlab.com/biomedit/portal/issues/563)
* persistent error field on submit ([c4b1c2b](https://gitlab.com/biomedit/portal/commit/c4b1c2ba6da3448da6d830569e09f7e568031216)), closes [#602](https://gitlab.com/biomedit/portal/issues/602)

## [4.4.0](https://gitlab.com/biomedit/portal/compare/4.4.0-dev.0...4.4.0) (2022-05-03)

### Features

* **frontend/Approval:** setting a DTR to `UNAUTHORIZED` blocks all waiting approvals ([5eaca63](https://gitlab.com/biomedit/portal/commit/5eaca639cd05b602e9cf1c725b5d9146481ae78b))

### Bug Fixes

* **DTR/transferPath:** compute transfer path on backend side ([e73f515](https://gitlab.com/biomedit/portal/commit/e73f5154d5ea0c3e332fcbca1ad3aaf8c527bac4)), closes [#597](https://gitlab.com/biomedit/portal/issues/597)
* **frontend/reducers:** update groups permissionsObject when a group is removed ([5da5441](https://gitlab.com/biomedit/portal/commit/5da544177a55b13a91d8a7e5786000528b61eab9)), closes [#590](https://gitlab.com/biomedit/portal/issues/590)
* **identities/groups:** remove group permissions when an object is removed ([81dc3b4](https://gitlab.com/biomedit/portal/commit/81dc3b4b5b14b4023ffacd11a5b59a4a22157c04))
* **backend/signals/logout:** handle anonymous users at the logout endpoint ([f9cb939](https://gitlab.com/biomedit/portal/commit/f9cb939a55ed4722a4597e888567abb42b7e491b)), closes [#594](https://gitlab.com/biomedit/portal/issues/594)

## [4.3.0](https://gitlab.com/biomedit/portal/compare/4.3.0-dev.22...4.3.0) (2022-04-28)

### Features

* **ProjectList:** add label to archived projects ([4047d74](https://gitlab.com/biomedit/portal/commit/4047d74903ae79a784fd9b3b524af30d81ddf4ff)), closes [#562](https://gitlab.com/biomedit/portal/issues/562)
* **ProjectList:** ask for a safer confirmation text when deleting ([6f04593](https://gitlab.com/biomedit/portal/commit/6f045937046299e47bdb628e430614b43280eea8)), closes [#568](https://gitlab.com/biomedit/portal/issues/568)
* **Config:** add possibility to specify max length for local_username ([4fa0e2e](https://gitlab.com/biomedit/portal/commit/4fa0e2eaea2f0bc884529c1f6c5a3699e99675c6)), closes [#565](https://gitlab.com/biomedit/portal/issues/565)
* **backend/dtr-notifications:** add final approval email notification ([f107a99](https://gitlab.com/biomedit/portal/commit/f107a994c5b88b8ecd4671ce5103a194086694e6)), closes [#569](https://gitlab.com/biomedit/portal/issues/569)
* **backend/Group:** add description field to group ([34c0d82](https://gitlab.com/biomedit/portal/commit/34c0d82e1c9de3fbb762df78a4813432856914c2)), closes [#559](https://gitlab.com/biomedit/portal/issues/559)
* **frontend/Group:** group description editable on frontend side ([11766e6](https://gitlab.com/biomedit/portal/commit/11766e64a5a080e15929512962981b5f19adc851)), closes [#559](https://gitlab.com/biomedit/portal/issues/559)
* **Group:** extend groups generation automation ([9461a14](https://gitlab.com/biomedit/portal/commit/9461a147019f5354b7894d3a542629a68f11bbd8)), closes [#554](https://gitlab.com/biomedit/portal/issues/554)
* **DataTransferApprovals:** add confirmation dialog when someone wants to reject an approval ([ba3b220](https://gitlab.com/biomedit/portal/commit/ba3b220e1f608db1e2aba2a4f9a463d1a9d7dde8)), closes [#555](https://gitlab.com/biomedit/portal/issues/555)
* **backend/dtr:** enable DTR creation for project DM ([d642201](https://gitlab.com/biomedit/portal/commit/d642201bd7d51055126ddacfc33f0ca8a09eb0f9))
* **frontend/dtr:** enable DTR creation for project DM ([c517c1c](https://gitlab.com/biomedit/portal/commit/c517c1c261f2ce645ff306fb45d67ca082d0568d)), closes [#551](https://gitlab.com/biomedit/portal/issues/551)
* add pgp metadata field to model ([e3d3c95](https://gitlab.com/biomedit/portal/commit/e3d3c95cc915d2d0fa000ff0c07cdbaca2a94ffd)), closes [#463](https://gitlab.com/biomedit/portal/issues/463)
* **backend/identities:** log login/logout events ([2fe67b3](https://gitlab.com/biomedit/portal/commit/2fe67b3107e99eb534310ff7148fc989711ecd38)), closes [#552](https://gitlab.com/biomedit/portal/issues/552)
* creation and update fields for approvals and DTR ([0e02a7f](https://gitlab.com/biomedit/portal/commit/0e02a7f4f8dac64f86bc79a52eaca608b7b606e3)), closes [#553](https://gitlab.com/biomedit/portal/issues/553)
* **backend/periodicTask:** add notification for periodic review of user roles ([1751371](https://gitlab.com/biomedit/portal/commit/17513718293d7b13941c6819a7a435410d8acc39)), closes [#549](https://gitlab.com/biomedit/portal/issues/549)
* **Config:** add possibility to specify min length for `local_username` ([a60e755](https://gitlab.com/biomedit/portal/commit/a60e75538bb9900370f11d543cc6bf94f0e59ee9)), closes [#545](https://gitlab.com/biomedit/portal/issues/545)
* **UserProfileDialog:** handle invalid username in frontend ([d0c14fe](https://gitlab.com/biomedit/portal/commit/d0c14fe072d46e859ef0fa0021c8773e1fc2de1a)), closes [#545](https://gitlab.com/biomedit/portal/issues/545)
* **Project:** display destination node ([a44dea6](https://gitlab.com/biomedit/portal/commit/a44dea6127a078a8be4cf1597e158cb9f920c4d4)), closes [#322](https://gitlab.com/biomedit/portal/issues/322)
* **ProjectUsers:** list project users ([b947568](https://gitlab.com/biomedit/portal/commit/b947568888febe81df483dcf0e579eb32bd0ebec)), closes [#322](https://gitlab.com/biomedit/portal/issues/322)
* **frontend/DataTransfer:** add data transfer path to data transfer details ([6eb1246](https://gitlab.com/biomedit/portal/commit/6eb1246d0268cf6684991cc8186dd1213cd9d890)), closes [#547](https://gitlab.com/biomedit/portal/issues/547)
* **frontend/redux:** add retrieve project action ([64c1b55](https://gitlab.com/biomedit/portal/commit/64c1b554161cc789bd1f0094b5dacd7114d513f0))

### Bug Fixes

* **config:** change session timeout to 30 min in .config.json template ([4b14773](https://gitlab.com/biomedit/portal/commit/4b147737a5b33e32e5f338b9926d5b39ca14e0fe)), closes [#544](https://gitlab.com/biomedit/portal/issues/544)
* **next-i18next:** latest version breaks `_app.test.tsx` ([e90d2d0](https://gitlab.com/biomedit/portal/commit/e90d2d0f76d15fa08ce7434e348e07b159d560a6)), closes [#576](https://gitlab.com/biomedit/portal/issues/576)
* **next:** last version v12.1.1 breaks STAGING ([97ccaec](https://gitlab.com/biomedit/portal/commit/97ccaeca6d708c9fbc56b7832e4bfdd9f7e41755))
* **frontend/DataTransfer:** update portal to reflect the fix in next-widgets issue 32 ([7b7f5a6](https://gitlab.com/biomedit/portal/commit/7b7f5a67dbf705f0b0f6120eae4643bdd3188dfa)), closes [#556](https://gitlab.com/biomedit/portal/issues/556)

## [4.2.0](https://gitlab.com/biomedit/portal/compare/4.2.0-dev.34...4.2.0) (2022-03-18)

### Features

* generate emails on approvals as well ([98fd3d7](https://gitlab.com/biomedit/portal/commit/98fd3d720c2c88d984a294b4c596e88dbfd6287d)), closes [#543](https://gitlab.com/biomedit/portal/issues/543)
* **approvals:** do not close dialog on approval/rejection ([9dc8bdc](https://gitlab.com/biomedit/portal/commit/9dc8bdcb9e55c15f7a618e9d49c5c8e582f6a83c)), closes [#546](https://gitlab.com/biomedit/portal/issues/546)
* **DataTransferApprovals:** only refresh/retrieve associated project/DTR on approval ([205f50b](https://gitlab.com/biomedit/portal/commit/205f50b52a9144ec8783a63683152a5a7906509e)), closes [#546](https://gitlab.com/biomedit/portal/issues/546)
* **frontend:** add `DataTransferApprovalForm.tsx` to approve/reject a data transfer request ([1c610d3](https://gitlab.com/biomedit/portal/commit/1c610d371c0bccc7f13feda80c57197f52d94a98)), closes [#507](https://gitlab.com/biomedit/portal/issues/507)
* **NodeApproval:** add `type` ([cf08c7c](https://gitlab.com/biomedit/portal/commit/cf08c7c99301d13dea4a2cb4f38c0724a27e0711)), closes [#507](https://gitlab.com/biomedit/portal/issues/507)
* **serializer/approval:** generate the notifications for approve and reject ([43ab657](https://gitlab.com/biomedit/portal/commit/43ab657618ae9707a59420d3b31bc77b4e83f4be)), closes [#507](https://gitlab.com/biomedit/portal/issues/507)
* **backend/mail:** add project roles to permission update email notification ([6097de4](https://gitlab.com/biomedit/portal/commit/6097de4c6bf173e842026e50762c4e33fffaf8a1)), closes [#376](https://gitlab.com/biomedit/portal/issues/376)
* **backend:** add project roles to users when fetching /projects/id/users ([f022b7f](https://gitlab.com/biomedit/portal/commit/f022b7f199f872e1abd321e74b0b8e1eae9cfaf1)), closes [#530](https://gitlab.com/biomedit/portal/issues/530)
* **backend/DataTransfer:** send notifications on data transfer creation ([dfb134f](https://gitlab.com/biomedit/portal/commit/dfb134f0bc2d8538c73b16fa0d84372089077843)), closes [#498](https://gitlab.com/biomedit/portal/issues/498)
* **frontend/DataTransfer:** add routing to data transfer details ([4c0ee88](https://gitlab.com/biomedit/portal/commit/4c0ee88476c61f3091cf22ca079b9dc428335df4))
* **commitlint:** add commitlint pre-commit hook ([8e15976](https://gitlab.com/biomedit/portal/commit/8e159762aa2ce680a0e885f7bf3973cacb255702))
* **frontend:** move position of "+" button ([6b5ff49](https://gitlab.com/biomedit/portal/commit/6b5ff49e131240ea21f95f364a5163cfb9854c98)), closes [#169](https://gitlab.com/biomedit/portal/issues/169)
* **backend/identities:** make User string representation more informative ([2ef3244](https://gitlab.com/biomedit/portal/commit/2ef32444a0d965ea10cc6f13b1a6e3d21a6ba007))
* **backend/message:** add history to the Message model ([bb3a366](https://gitlab.com/biomedit/portal/commit/bb3a366067df43fd621977fed14e5a2e995b5d76))
* **frontend/message:** change message status to `read` when messages is opened ([ca77147](https://gitlab.com/biomedit/portal/commit/ca77147d551546547fc07d3939624d4649119980)), closes [#521](https://gitlab.com/biomedit/portal/issues/521)
* **frontend/message:** display the beginning of each message in the table view ([14b4753](https://gitlab.com/biomedit/portal/commit/14b47530eeffddf469b903a955642edd2a5e38c2))
* **frontend/message:** show unread messages in bold in the table view ([d644f23](https://gitlab.com/biomedit/portal/commit/d644f231bf11786a7e4a7aa4a1b1c18988c4ff61))
* add possibility to Docker-based development ([133470f](https://gitlab.com/biomedit/portal/commit/133470f0a23a4dfe1b5268f50a073369527853be))

### Bug Fixes

* **CSV:** approvals should be correctly exported on frontend ([b5932c4](https://gitlab.com/biomedit/portal/commit/b5932c41a116a9d639a10249bd0353859e82e01b))
* legal basis not part of TEST DTRs ([60ad1d6](https://gitlab.com/biomedit/portal/commit/60ad1d632054362eb33fcc410dd9f4b021c27815))
* **next-widgets:** newest 'next-widgets' release removed ([4d91404](https://gitlab.com/biomedit/portal/commit/4d914041d575ca9ebcf3647e811166c67f17b6aa))
* **frontend/validations:** fix validations for required fields ([b8fe2da](https://gitlab.com/biomedit/portal/commit/b8fe2da79362a039bba7ebb0a3d3cb12be0d8708)), closes [#541](https://gitlab.com/biomedit/portal/issues/541)
* unnecessary error alert window while setting username ([964386c](https://gitlab.com/biomedit/portal/commit/964386cb057787c04b246d93ad05e7e390a1f53f)), closes [#518](https://gitlab.com/biomedit/portal/issues/518)
* **frontend/project:** fix validation in new project and new data provider form ([87c7fbd](https://gitlab.com/biomedit/portal/commit/87c7fbd43d87a97928b2486fe0078164b23ece58)), closes [#529](https://gitlab.com/biomedit/portal/issues/529)
* **Next.js:** randomly 504 errors on portal STAGING ([8d3c39f](https://gitlab.com/biomedit/portal/commit/8d3c39f6f0d307ea7d2b161eac9a28df51fbe258)), closes [#537](https://gitlab.com/biomedit/portal/issues/537)
* **staging:** a try to fix STAGING deployment ([731f2dc](https://gitlab.com/biomedit/portal/commit/731f2dc8e801a9faaac4dcb1a6041686d5145faf)), closes [#536](https://gitlab.com/biomedit/portal/issues/536)
* **backend/DataProvider:** show data provider coordinators on the API ([8be9cf4](https://gitlab.com/biomedit/portal/commit/8be9cf4045577d747b812b580b4981a906e3bcc9))
* **DataTransfer:** extend data transfer form with new fields ([65878a2](https://gitlab.com/biomedit/portal/commit/65878a2a733124523a135adf718723f26dc1bd91)), closes [#497](https://gitlab.com/biomedit/portal/issues/497)
* **husky:** fix husky to restore pre commit hooks ([14cfdd5](https://gitlab.com/biomedit/portal/commit/14cfdd571337a36ddcc82c88c6e7270cf58eba33))
* **UsersList:** fix TS ignored compiler error ([407592b](https://gitlab.com/biomedit/portal/commit/407592b585616b67b24049369df99797fcb1d325)), closes [#531](https://gitlab.com/biomedit/portal/issues/531)
* **frontend/DataTransfer:** Hide Data Transfers table column 'Actions' if no action possible ([ce320af](https://gitlab.com/biomedit/portal/commit/ce320afd28c2df5f171bea6da567722b8f1709f8)), closes [#517](https://gitlab.com/biomedit/portal/issues/517)
* **backend/models/DataProviderApproval:** use correct permission name ([8685462](https://gitlab.com/biomedit/portal/commit/86854625423253ea05965bdc59b0c636a78b2f24))
* **PermissionsObjectList:** do not fetch permissions for empty choice ([6480a76](https://gitlab.com/biomedit/portal/commit/6480a7637b0d31af28832895156fb8fdb24f281b)), closes [#510](https://gitlab.com/biomedit/portal/issues/510)
* **Project:** 'Invalid user field' when creating/updating a project ([57ef6cf](https://gitlab.com/biomedit/portal/commit/57ef6cfc0fb1a79a736d05c68a6820d063d0ba3a)), closes [#525](https://gitlab.com/biomedit/portal/issues/525)
* **CI:** MRs do no longer run the tests ([6f33962](https://gitlab.com/biomedit/portal/commit/6f339620ea6c10cc1c0de5c415d1bcb6d6f55524))

## [4.1.0](https://gitlab.com/biomedit/portal/compare/4.1.0-dev.0...4.1.0) (2022-01-13)

### Features

* **SWITCH:** switch off no-affiliation-consent emails by default ([abd11cc](https://gitlab.com/biomedit/portal/commit/abd11ccccf9c447fe5de1ea650c127776ecc7add)), closes [#522](https://gitlab.com/biomedit/portal/issues/522)

### Bug Fixes

* **OpenAPI:** reduce the number of warnings on OpenAPI schema generation ([efad404](https://gitlab.com/biomedit/portal/commit/efad40433fa98f30c8e5efe4ee504c63b768da25)), closes [#495](https://gitlab.com/biomedit/portal/issues/495)
* **frontend/Admin:** fix admin page without content after defining a username or giving affiliation consent ([de78897](https://gitlab.com/biomedit/portal/commit/de7889777fdbc760beb501ecbab2dd3cbe005917)), closes [#309](https://gitlab.com/biomedit/portal/issues/309)

## [4.0.0](https://gitlab.com/biomedit/portal/compare/4.0.0-dev.10...4.0.0) (2021-12-21)

### ⚠ BREAKING CHANGES

* **message:** `project-messages` endpoint has been changed to a general-purpose `messages` endpoint.
* **backend/notify:** `notify` endpoint has been removed
* **DataProvider:** Users will be removed from the DP objects.

After the introduction of DP groups, having users directly associated to DPs did not make sense anymore. DP <-> User associations should be covered by Groups, which allows a finer granular authorization pattern and control.

The migration has to be manually performed as it is difficult to foresee which role a previous DP user/manager would have in the new constellation. Following suggestions could be done though:

* If the current user is allowed to generally edit the DP entry (DP groups, DP attributes), then use the DP Manager group
* If the current user should just have read-only permissions, then use the DP Viewer group
* If the current user would be the one who is providing/sending the data to the DMs (Data Managers), the use the DP Technical Admin group
* If the current user is responsible for confirming the DTRs in portal, then use the DP Coordinator group.

### Features

* **backend/switch-notification:** do not make requests to SWITCH SCIM API if user consent is missing ([fbbf20c](https://gitlab.com/biomedit/portal/commit/fbbf20c74091fff137eaea41e55dfa4e1ba98499)), closes [#503](https://gitlab.com/biomedit/portal/issues/503)
* **backend/user:** allow the user to give affiliation consent ([1bd0b47](https://gitlab.com/biomedit/portal/commit/1bd0b4756ada74487b727cebacdc1428c52d5814)), closes [#503](https://gitlab.com/biomedit/portal/issues/503)
* **backend:** add fields `affiliation_consent` and `affiliation_consent_required` to `Profile` ([08edc6c](https://gitlab.com/biomedit/portal/commit/08edc6cf7599d700385ac6b3d72fca23e8439c54)), closes [#503](https://gitlab.com/biomedit/portal/issues/503)
* **frontend:** ask user for affiliation consent if not already given or not required ([61a25c2](https://gitlab.com/biomedit/portal/commit/61a25c2d63468e3325affbed103478d9d975e99f)), closes [#503](https://gitlab.com/biomedit/portal/issues/503)
* **DataProvider:** dissolve DP <-> User one-to-many connection ([51a6f5f](https://gitlab.com/biomedit/portal/commit/51a6f5f81fa195c3e7ffae44bdeca9b7a3424817)), closes [#351](https://gitlab.com/biomedit/portal/issues/351)
* **DataProvider:** use permissions to connect a DP with users ([aa5123e](https://gitlab.com/biomedit/portal/commit/aa5123ea9edbe6f512f3a61a22073483566ba050)), closes [#351](https://gitlab.com/biomedit/portal/issues/351)
* **message:** change messages to a general-purpose messaging system for users ([445d3af](https://gitlab.com/biomedit/portal/commit/445d3af6c8cbf49f2cab1be3aff65d735bce5e5b))
* **backend/notify:** remove unused endpoint ([b57ec7b](https://gitlab.com/biomedit/portal/commit/b57ec7b4c1101acc24ff5a27fa620db835440cf8))
* **backend/affiliation_change:** include flags in email notification about changed affiliation ([bb26430](https://gitlab.com/biomedit/portal/commit/bb26430bd595a54a51e276b298c8a3b04188a47b)), closes [#502](https://gitlab.com/biomedit/portal/issues/502)
* **backend/user_utils:** use a different subject for emails on affiliation changes on login compared to through the switch notification API ([ada35b8](https://gitlab.com/biomedit/portal/commit/ada35b8780a246065b21693f1612f6a2a207a35a)), closes [#502](https://gitlab.com/biomedit/portal/issues/502)
* **backend:** add switch notification API endpoint ([dd42016](https://gitlab.com/biomedit/portal/commit/dd4201655c52dbd0ddcce20005ce96f09311596b)), closes [#502](https://gitlab.com/biomedit/portal/issues/502)

### Bug Fixes

* **frontend/SetUserNameDialog): fix error `validateDOMNesting(...:** <div> cannot appear as a descendant of <p>` ([58a538a](https://gitlab.com/biomedit/portal/commit/58a538aaaf7bd56c20d61b7cd17803fe0b4d39a0)), closes [#512](https://gitlab.com/biomedit/portal/issues/512)
* **k8s:** Add missing quote char ([46b0b78](https://gitlab.com/biomedit/portal/commit/46b0b78da619112e3e4f0ac1fa3ac8d9c973e7b3))

## [3.13.0](https://gitlab.com/biomedit/portal/compare/3.13.0-dev.2...3.13.0) (2021-12-02)

### Features

* **Home/seasonal:** add new seasonal messages ([6f6d182](https://gitlab.com/biomedit/portal/commit/6f6d18203b0b090f9665d3aec468737d2e225064)), closes [#425](https://gitlab.com/biomedit/portal/issues/425)

### Bug Fixes

* **frontend/Projects:** show project filter dropdown when a user previously didn't have any projects but does after updating or adding a project ([2a7e129](https://gitlab.com/biomedit/portal/commit/2a7e12956be0c2474fe5f3f026175b73152caa53)), closes [#488](https://gitlab.com/biomedit/portal/issues/488)

## [3.12.0](https://gitlab.com/biomedit/portal/compare/3.12.0-dev.28...3.12.0) (2021-11-17)

### Features

* add `correlation_id` to requests to trace requests to the backend ([e6f2b6b](https://gitlab.com/biomedit/portal/commit/e6f2b6b109095cfa8cc5b891bd75df287b1a7445)), closes [#472](https://gitlab.com/biomedit/portal/issues/472)
* **backend/API:** add query parameters to /projetcs and /users API ([7663dbf](https://gitlab.com/biomedit/portal/commit/7663dbf5ca8574d3b757a7416c75d34949d872b1)), closes [#392](https://gitlab.com/biomedit/portal/issues/392)
* **models/PeriodicTaskRun:** implement `__str__` method ([f283cba](https://gitlab.com/biomedit/portal/commit/f283cba4ab287411636829f4f71c6c03e8d89a09))
* add service hostname to all logs ([b0bad35](https://gitlab.com/biomedit/portal/commit/b0bad357b541c61137c516372bb3e53242037c99)), closes [#461](https://gitlab.com/biomedit/portal/issues/461)
* **backend/node:** create data provider groups (technical admin, coordinator, manager, viewer) when creating a data provider ([f10cc7d](https://gitlab.com/biomedit/portal/commit/f10cc7ded9edd4d1e394e19517475068d9cc026c)), closes [#453](https://gitlab.com/biomedit/portal/issues/453)
* **backend/node:** create node admin, node viewer and node manager groups when creating a node ([892abd6](https://gitlab.com/biomedit/portal/commit/892abd63d1b591ef626cd6b084625d45c76d10e5)), closes [#453](https://gitlab.com/biomedit/portal/issues/453)
* **backend:** Use python:slim to reduce web container size ([f8cf139](https://gitlab.com/biomedit/portal/commit/f8cf139376524bdadb985bb08d8ceb2192ea6128))
* **backend:** Improve error message triggered by data_package.check_user_permissions ([4e40125](https://gitlab.com/biomedit/portal/commit/4e40125e4752565a7a3a2823928e8c9d702f191f))
* **pl:** data managers AND project leaders should be able to list their data transfers ([46e4876](https://gitlab.com/biomedit/portal/commit/46e4876f955da5dbabb333193603d93f32bc1e02)), closes [#440](https://gitlab.com/biomedit/portal/issues/440)
* **pl:** enable access for project leaders to data transfers menu item ([046d72c](https://gitlab.com/biomedit/portal/commit/046d72c31d8b276d768e5077c7eaee2b873c2acb)), closes [#440](https://gitlab.com/biomedit/portal/issues/440)

### Bug Fixes

* **frontend/Administration/Groups:** fix error when clearing object permission field ([f005e37](https://gitlab.com/biomedit/portal/commit/f005e3752e6c24815bedf8c82a4c43933feeeb6b)), closes [#490](https://gitlab.com/biomedit/portal/issues/490)
* **frontend/Administration/Groups:** fix incompatible objects not being removed when changing permission of a group ([4dd31a9](https://gitlab.com/biomedit/portal/commit/4dd31a9d961d67063f19b07176fab66bd10e0839)), closes [#490](https://gitlab.com/biomedit/portal/issues/490)
* **frontend/DataTransferDetail:** fix data transfer status coloring ([0059e9e](https://gitlab.com/biomedit/portal/commit/0059e9e1b72a0ff7c5b3a5e0da6efcfb356bca7c)), closes [#496](https://gitlab.com/biomedit/portal/issues/496)
* **frontend/ProjectList:** fix unique "key" prop React error in the console ([84e63a4](https://gitlab.com/biomedit/portal/commit/84e63a49ade5daaf58b921a2ba98fbbef838bf8f)), closes [#487](https://gitlab.com/biomedit/portal/issues/487)
* **frontend:** fix unhandled error appearing when editing a group or flag ([ef6573e](https://gitlab.com/biomedit/portal/commit/ef6573ed019a2bc73e8904cc637dd01ed5714bbb)), closes [#489](https://gitlab.com/biomedit/portal/issues/489)
* **frontend/GroupManageForm:** fix objects list being empty when adding a new group object permission ([28367b8](https://gitlab.com/biomedit/portal/commit/28367b888914c5cec90d1c279dc2ac2ee75b8d4e)), closes [#478](https://gitlab.com/biomedit/portal/issues/478)
* **frontend/GroupManageForm:** show different title when creating a group ([d5f01c5](https://gitlab.com/biomedit/portal/commit/d5f01c541efbb5f10ba5cec2f522f519a5090613))
* **frontend/DataTransferDetail:** add a missing field (Data Provider) ([6dc1bcf](https://gitlab.com/biomedit/portal/commit/6dc1bcfc6f9b228f638497b57518b66cd947eba6)), closes [#272](https://gitlab.com/biomedit/portal/issues/272)
* **ProjectList:** remove border on top of `ListPage` component ([7956655](https://gitlab.com/biomedit/portal/commit/79566559b68746c0c1fd2e21a45343a41b9d5dd5)), closes [#480](https://gitlab.com/biomedit/portal/issues/480)
* **backend:** fix broken log file rotation due to concurrency issues when using multiple gunicorn workers ([0525d73](https://gitlab.com/biomedit/portal/commit/0525d7332438caf8e77cfcb27dfc3207becfd942)), closes [#479](https://gitlab.com/biomedit/portal/issues/479)
* **frontend/UsersList:** ensure newly added users don't have any roles ([1fecf82](https://gitlab.com/biomedit/portal/commit/1fecf820c42d212ba157d8fa8949cf485bac6b13)), closes [#458](https://gitlab.com/biomedit/portal/issues/458)
* **celery:** only write logs with at least INFO level ([18df68b](https://gitlab.com/biomedit/portal/commit/18df68b6f7cec91235f5362146dbf6b74d084ea8)), closes [#469](https://gitlab.com/biomedit/portal/issues/469)
* **frontend/GroupManage:** make new groups appear when changing to the group tab after creating a dp or node ([8fbffbe](https://gitlab.com/biomedit/portal/commit/8fbffbe48a0d14576cf62ad4a39ef86238e9f761)), closes [#453](https://gitlab.com/biomedit/portal/issues/453)
* **backend:** remove deprecated fields ([7e36cf4](https://gitlab.com/biomedit/portal/commit/7e36cf44255ecf2ca84e0321b96850de502cf8e3)), closes [#373](https://gitlab.com/biomedit/portal/issues/373)

## [3.11.0](https://gitlab.com/biomedit/portal/compare/3.11.0-dev.6...3.11.0) (2021-10-05)

### Features

* **frontend/Projects:** add tooltip and separator to action buttons in project list items ([e359175](https://gitlab.com/biomedit/portal/commit/e3591752013585e81e15b568632622110668dd25)), closes [#434](https://gitlab.com/biomedit/portal/issues/434)
* k8s deployment ([8db0a7b](https://gitlab.com/biomedit/portal/commit/8db0a7bca931c9b7c2f63c99eb03a292bd6ba5d4))
* show data package sender PGP key info in the transfer log ([7198d66](https://gitlab.com/biomedit/portal/commit/7198d66931d8fa9071bd090dca58a97b4def6c21)), closes [#417](https://gitlab.com/biomedit/portal/issues/417)

### Bug Fixes

* **FeedList:** decrease font size of title and message feed items ([b550cfb](https://gitlab.com/biomedit/portal/commit/b550cfba0fd73f45f2bbdcdf21ab4289567b6110)), closes [#445](https://gitlab.com/biomedit/portal/issues/445)
* remove pylint ignores for consider-using-f-string ([fe3ab83](https://gitlab.com/biomedit/portal/commit/fe3ab83ea8f82d2bde7ae14f53e6c8e22f3c7f0e))
* Remove urls from sample portal config ([c782e7a](https://gitlab.com/biomedit/portal/commit/c782e7af186287ce7554ae4636f5365a4963e96e))
* **backend/user:** return all users when making a `GET` request to `/users` as node admin ([c84759c](https://gitlab.com/biomedit/portal/commit/c84759c0f22a93be1b5e8ef0cb2e5c50f355acf0)), closes [#454](https://gitlab.com/biomedit/portal/issues/454)

## [3.10.0](https://gitlab.com/biomedit/portal/compare/3.10.0-dev.0...3.10.0) (2021-09-15)

### Features

* **backend/data_transfer:** prevent creating or editing data transfers associated to archived projects ([87cae2e](https://gitlab.com/biomedit/portal/commit/87cae2ef6a83daf8edd4f8f35f8f8f9614ce7a5d)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **backend/project:** prevent editing of archived projects ([453beae](https://gitlab.com/biomedit/portal/commit/453beae8866517727182b9eabfc422125b0f9932)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **backend/project:** send notification to node and project users when a project is archived ([b2104dc](https://gitlab.com/biomedit/portal/commit/b2104dc7270fad84d8c80f8611337fd2588d0452)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **backend/project:** set all data transfers related to a project to status `EXPIRED` when the project is archived ([4376d85](https://gitlab.com/biomedit/portal/commit/4376d85e2739208760edbc6402c430b43ec09ea9)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend/DataTransferOverviewTable:** prevent editing DTRs of archived projects ([d260ba4](https://gitlab.com/biomedit/portal/commit/d260ba417168eac61ca1c8dbf1b1825e948e2d16)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend/ProjectList:** add filter for archived projects and hide archived projects from "All Projects" and "My Projects" ([3b65842](https://gitlab.com/biomedit/portal/commit/3b658429417ad92707f8c41be72f61da2a13a5b2)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend/ProjectList:** make archived projects read only except for deletion of data transfers and projects for staff ([910fac0](https://gitlab.com/biomedit/portal/commit/910fac04fd12cb834e49a4f9444bf3f7f0561615)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend/ProjectList:** show confirmation dialog upon archival of a project ([438c8cb](https://gitlab.com/biomedit/portal/commit/438c8cbd5323e9cd51b9cb30a8d543b966981524)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend:** require typing the item name in the deletion dialog ([9a0f9e0](https://gitlab.com/biomedit/portal/commit/9a0f9e0db023e977f9d9cb27ce3a4f3ff8bfaa7e)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **projects:** allow PA's and NA's to archive and unarchive projects ([59ad86a](https://gitlab.com/biomedit/portal/commit/59ad86a9c6e227236515c82ef14575a74ac1def4)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)

### Bug Fixes

* **frontend/Home:** replace deprecated justify prop with its newer equivalent ([a3d6315](https://gitlab.com/biomedit/portal/commit/a3d6315fea3fffa9ed482d2d08898bc6e5b81289))
* **frontend/Home:** use GridItem special key prop properly. ([3d6fc59](https://gitlab.com/biomedit/portal/commit/3d6fc592c321e7e93f9d053f29d35b4a64537c6c))
* **frontend/Administration:** sort columns of admin table correctly ([2bac0cb](https://gitlab.com/biomedit/portal/commit/2bac0cb4078e0aad931bfadf88a2334b3024b488))
* **frontend/DataTransfer:** sort requestor column of data-transfers correctly ([a9f0946](https://gitlab.com/biomedit/portal/commit/a9f094685a65dd0c4a0598da9210d5c3a47c196a))

## [3.9.0](https://gitlab.com/biomedit/portal/compare/3.9.0-dev.1...3.9.0) (2021-08-20)

### Features

* add new Data Provider (object-based) permissions ([6550b1b](https://gitlab.com/biomedit/portal/commit/6550b1b68e52466eaec56b28341367f350087246)), closes [#368](https://gitlab.com/biomedit/portal/issues/368)

### Bug Fixes

* **filebeat:** decode the message object in logs ([6f84fff](https://gitlab.com/biomedit/portal/commit/6f84fff38e00198a59489007bc560edd64196dfc)), closes [#442](https://gitlab.com/biomedit/portal/issues/442)

## [3.8.0](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.16...3.8.0) (2021-08-20)

### Features

* Work around docker.sock in filebeat service ([073f5ea](https://gitlab.com/biomedit/portal/commit/073f5ea8ac7889760cf4992b659f1220d5f28f4c))
* **frontend:** show message instead of website if visitor is using an unsupported browser ([4d2ca25](https://gitlab.com/biomedit/portal/commit/4d2ca251328d1d0e64eb4df5a15edce03227d695)), closes [#189](https://gitlab.com/biomedit/portal/issues/189)
* podman deployment ([52d759b](https://gitlab.com/biomedit/portal/commit/52d759b8d106289eda0bd8be0697c2c7b75fc1d1))
* **security:** Avoid docker.sock in traefik service ([a204728](https://gitlab.com/biomedit/portal/commit/a20472869b3b8ea8771e17ccb45811c157f3ca2b))
* **backend:** add history for changes on `User` ([18d5a05](https://gitlab.com/biomedit/portal/commit/18d5a059b92431227b172f01786c83f6c57a4377)), closes [#422](https://gitlab.com/biomedit/portal/issues/422)
* **backend/projects:** allow node admins to edit and create projects associated with their node ([9579f31](https://gitlab.com/biomedit/portal/commit/9579f3133e8252f4eba7625cf6f4f6408f5c097a)), closes [#406](https://gitlab.com/biomedit/portal/issues/406)
* **frontend/ProjectForm:** allow node admins to only select nodes they are associated with when creating a new project ([d053ed0](https://gitlab.com/biomedit/portal/commit/d053ed0f30de082970073aa8e8042629355cf749)), closes [#406](https://gitlab.com/biomedit/portal/issues/406)
* **frontend/ProjectForm:** prevent node admins from changing the node of an existing project ([dcfd973](https://gitlab.com/biomedit/portal/commit/dcfd973636bdf1bf9dbd45d68179c7d33ded5b20)), closes [#406](https://gitlab.com/biomedit/portal/issues/406)
* **frontend/Projects:** allow node admins to edit and create projects associated with their node ([2591ae4](https://gitlab.com/biomedit/portal/commit/2591ae4b45ff10aefea6925429b6664248de0175)), closes [#406](https://gitlab.com/biomedit/portal/issues/406)
* **frontend:** display groups that a user belongs to in userinfo box. ([c1a11ce](https://gitlab.com/biomedit/portal/commit/c1a11ced6eaaa47b0e60fd4c27ee1166a1b592ae)), closes [#370](https://gitlab.com/biomedit/portal/issues/370)
* **frontend/admin/user:** display user flags in username tooltip ([d886aa3](https://gitlab.com/biomedit/portal/commit/d886aa3aab342ee95830f513abcd998918c65dab)), closes [#414](https://gitlab.com/biomedit/portal/issues/414)

### Bug Fixes

* **docker-compose:** run filebeat with `root` user ([e19eaff](https://gitlab.com/biomedit/portal/commit/e19eafff6b0b655f538013708f410cbfa6586ee4))
* **docker-compose:** prevent filebeat from running in a restart loop ([ffba1c9](https://gitlab.com/biomedit/portal/commit/ffba1c9518449bb47923ae1d30ca98b85a04b18b))
* **backend/unique_check:** log `is not unique` responses from a unique check as `warn` instead of `error` ([4f615d4](https://gitlab.com/biomedit/portal/commit/4f615d4dc9f8eb9df8d3dc40f3fff80d357e8b83)), closes [#395](https://gitlab.com/biomedit/portal/issues/395)

## [3.7.0](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.17...3.7.0) (2021-07-13)

### Features

* **backend/User:** add possibility to change/update 'uid', 'flags' and 'local_username' through the REST API ([37b8f33](https://gitlab.com/biomedit/portal/commit/37b8f33bb3557088fcc67acf6f0a026e4ebba9f9)), closes [#13](https://gitlab.com/biomedit/portal/issues/13)
* **frontend/Home:** make longer titles possible in quick access tiles ([26a7df6](https://gitlab.com/biomedit/portal/commit/26a7df62d35438c2c224a093170bc1ff0207b6d8)), closes [#412](https://gitlab.com/biomedit/portal/issues/412)
* **frontend/Home:** display quick access tiles specified in the database ([9d987e7](https://gitlab.com/biomedit/portal/commit/9d987e7524b95e4fc5cfad16f3d49906b240dd27)), closes [#393](https://gitlab.com/biomedit/portal/issues/393)
* **quick accesses:** move quick access tiles administration to database ([52e0560](https://gitlab.com/biomedit/portal/commit/52e05604a332599b49592265d59f627de2848936)), closes [#393](https://gitlab.com/biomedit/portal/issues/393)
* **backend/DataPackageLogViewSet:** accept model permissions on the view ([16fa701](https://gitlab.com/biomedit/portal/commit/16fa701695ef34a1d7a2d45f4012763001d90f68)), closes [#405](https://gitlab.com/biomedit/portal/issues/405)
* **backend/logging:** add app name to request logging ([e91449f](https://gitlab.com/biomedit/portal/commit/e91449f931a1d8c36f94fad574a641d4b0fa365b)), closes [#397](https://gitlab.com/biomedit/portal/issues/397)
* **frontend/project:** add 'contact' to project resource ([e34cbce](https://gitlab.com/biomedit/portal/commit/e34cbce3545b6504a1a9277a9acfe9c8f026aa5b)), closes [#321](https://gitlab.com/biomedit/portal/issues/321)
* **frontend/project:** change resources management in project space ([105dd9a](https://gitlab.com/biomedit/portal/commit/105dd9a5b84f3a5237aafe2328f4da24b1ae44d0)), closes [#321](https://gitlab.com/biomedit/portal/issues/321)

### Bug Fixes

* **frontend/Administration/Users:** fix issue where sorting by username would result in an exception ([0cc2030](https://gitlab.com/biomedit/portal/commit/0cc20302259c1849ef0712d946b79a6e504bf65d)), closes [#418](https://gitlab.com/biomedit/portal/issues/418)
* **frontend:** add `UserMenu` back ([9fb7fee](https://gitlab.com/biomedit/portal/commit/9fb7fee6fade425b91c0f746ee2d4980da90cb41)), closes [#411](https://gitlab.com/biomedit/portal/issues/411)
* **frontend:** fix weird styling in frontend production builds ([09ef4e6](https://gitlab.com/biomedit/portal/commit/09ef4e65c2e9afc64215b98808775df1b5553381)), closes [#411](https://gitlab.com/biomedit/portal/issues/411)

## [3.6.0](https://gitlab.com/biomedit/portal/compare/3.6.0-dev.6...3.6.0) (2021-06-15)

### Features

* **backend/permission:** allow node viewer/manager more read permissions ([2d63281](https://gitlab.com/biomedit/portal/commit/2d63281f6be66d3a07f9f1f079099dbbec81d8c9))
* **frontend/UserManage:** show affiliations tooltip on username ([ba68082](https://gitlab.com/biomedit/portal/commit/ba6808207adbbe2a618e2d66f374b0951d661c92)), closes [#361](https://gitlab.com/biomedit/portal/issues/361)

### Bug Fixes

* **backend/logging:** log requests in `identities` app ([4f9f337](https://gitlab.com/biomedit/portal/commit/4f9f3378907f9daba1c0f9ed48735e735681c3d2)), closes [#394](https://gitlab.com/biomedit/portal/issues/394)
* **frontend/admin/users:** label IS the code for flags on users panel ([bdc97db](https://gitlab.com/biomedit/portal/commit/bdc97dbbb4d372103f761f5b75f8333fa9845a4a)), closes [#399](https://gitlab.com/biomedit/portal/issues/399)
* **backend/tests:** fix tests not being executed which are in a class whose name is not prefixed by `Test` ([49d4de6](https://gitlab.com/biomedit/portal/commit/49d4de63dd26b8266ac7547150f157112244928e))

## [3.5.0](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.12...3.5.0) (2021-05-25)

### Features

* **backend/user:** projects API view should display 'affiliation_id' of users as well ([2382c94](https://gitlab.com/biomedit/portal/commit/2382c94d1fedfbdd6e6e5a68331bc02ce195b629))
* **backend/user:** implement 'affiliation_id' based on 'linkedAffiliationUniqueID' ([3a7956b](https://gitlab.com/biomedit/portal/commit/3a7956b5425ea99f3507e63db7214fcc9b6e3b6b)), closes [#337](https://gitlab.com/biomedit/portal/issues/337)
* **backend:** remove LDAP support ([a893875](https://gitlab.com/biomedit/portal/commit/a893875dd1da6c3b8ba1d6c2df10e3725617608f)), closes [#388](https://gitlab.com/biomedit/portal/issues/388)
* **frontend:** show redux dev tools outside of local development if the environment variable `NEXT_USE_REDUX_DEV_TOOLS` is set to `true` ([a9fa207](https://gitlab.com/biomedit/portal/commit/a9fa207080314c508bde7723b6e9ed6721309179))
* **contact:** add contact form ([0682a64](https://gitlab.com/biomedit/portal/commit/0682a644cd99031931a5e37be635e24401c96702)), closes [#247](https://gitlab.com/biomedit/portal/issues/247)
* **backend/permissions:** add custom permission `admin_node` ([23b03bc](https://gitlab.com/biomedit/portal/commit/23b03bc96cfa51f094771cbe3583c9f495541d34)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **backend/permissions:** allow creating groups with permissions to access or modify permissions or groups ([28f7dbd](https://gitlab.com/biomedit/portal/commit/28f7dbdccfb62d2721b6f5529d4a9bae6214fad0)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **frontend/manage nodes:** make nodes editable by node admins ([0e41945](https://gitlab.com/biomedit/portal/commit/0e41945578a642eefbefb09464a9e1306eda0d53)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **frontend/NodeManagerForm:** make node id only editable by staff ([1a26f0a](https://gitlab.com/biomedit/portal/commit/1a26f0a3bd7d507b75b060f7a2cb34fd33d0ae65)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **group management:** allow Node Managers to edit `users` on groups ([8192569](https://gitlab.com/biomedit/portal/commit/81925696afd17df7d819def1eb41d2d4937ceee2)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **node:** remove `users` from `Node` ([aab1628](https://gitlab.com/biomedit/portal/commit/aab1628dabbff941d1c8e149ad7453b7667ee769)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **node permissions:** show `Administration` menu option and `Node` tabs for Node Admins and Node Viewers ([d16b155](https://gitlab.com/biomedit/portal/commit/d16b1550aceef0b2537a2eed796c61a04a7f90d5)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)

### Bug Fixes

* **frontend/GroupManageForm:** fix a bug where in some cases the objects field in the object permissions was empty on edit ([c89080a](https://gitlab.com/biomedit/portal/commit/c89080a4aa36896b6b5b6a6238902ad2d09f8005)), closes [#381](https://gitlab.com/biomedit/portal/issues/381), relates to [/github.com/react-hook-form/react-hook-form/issues/1558#issuecomment-623196472](https://github.com/react-hook-form/react-hook-form/issues/1558/issues/issuecomment-623196472)
* **frontend/NodeManage:** fix bug where navigating from `Projects` to the node administration will always show all nodes ([3145f1a](https://gitlab.com/biomedit/portal/commit/3145f1ac7880894ef78016d03ad86684de1c0d8b)), closes [#381](https://gitlab.com/biomedit/portal/issues/381)
* **frontend/Admin:** make 'isActive' property of user filterable and sortable ([e4f0caa](https://gitlab.com/biomedit/portal/commit/e4f0caa0bcdab5bb0c2639e8efa4ceed0b62cf68)), closes [#385](https://gitlab.com/biomedit/portal/issues/385)
* **frontend/LoadingButton:** make sure spinner in form submit buttons always stays within the boundaries of the button ([85ce44b](https://gitlab.com/biomedit/portal/commit/85ce44b2960ce9c1dc20a2427706fb9f61e930bd))

### [3.4.1](https://gitlab.com/biomedit/portal/compare/3.4.1-dev.0...3.4.1) (2021-05-11)

### Bug Fixes

* **backend/notification:** empty affiliation triggers an error ([05c0492](https://gitlab.com/biomedit/portal/commit/05c049290b91592357d8f66dac4da5153fd4aab0))
* **frontend/TextField:** labelled fields do not get properly disabled ([fdbc0cf](https://gitlab.com/biomedit/portal/commit/fdbc0cfcd38cd8dd749bf42cd6746cda7ddb2e6d))

## [3.4.0](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.13...3.4.0) (2021-05-11)

### Features

* **frontend/user:** make sorting case- and language-insensitive ([25c4d4f](https://gitlab.com/biomedit/portal/commit/25c4d4f9499935d365aff662d31067500dc102fd)), closes [#357](https://gitlab.com/biomedit/portal/issues/357)
* **backend/data_transfer:** prevent data transfers from being created with a requestor that is not a data manager of the project ([1bfbb8d](https://gitlab.com/biomedit/portal/commit/1bfbb8d2e0b8ae5457f5b5d16c4b7be0d61952ca)), closes [#346](https://gitlab.com/biomedit/portal/issues/346)
* **frontend/DataTransferForm:** only show requestors in the dropdown which are data managers of the project ([7b442a9](https://gitlab.com/biomedit/portal/commit/7b442a9e93eeb8dd383b532852e50459e766c898)), closes [#346](https://gitlab.com/biomedit/portal/issues/346)
* **backend/scheduled job:** send a notification email if a user hasn't logged in for 60 days ([307866e](https://gitlab.com/biomedit/portal/commit/307866e946240853b33d65ba25ddc065f86f72bf)), closes [#333](https://gitlab.com/biomedit/portal/issues/333)
* **backend/data_package:** prevent a package from getting transferred using sett if it has been transferred before ([8b2a4c9](https://gitlab.com/biomedit/portal/commit/8b2a4c9b7e840e4b47cc96e1d7ad6a3f23e5a240)), closes [#350](https://gitlab.com/biomedit/portal/issues/350)
* **backend:** add object-level permission backend ([84f1c89](https://gitlab.com/biomedit/portal/commit/84f1c899dffda66848c145f28d2c7d1c5e070d36))
* **backend:** add object-level permission filter and permission verification class ([ecea135](https://gitlab.com/biomedit/portal/commit/ecea1354c9e4181a66fbe5ec302ea5c744e12170))
* **backend:** add object-level permission management endpoints ([b30b868](https://gitlab.com/biomedit/portal/commit/b30b868b83f0a7ea5c6dc9ad79c42a64012c1d91))
* **backend:** allow group management in user endpoint ([f6a39cf](https://gitlab.com/biomedit/portal/commit/f6a39cf56b890fc5571f5177b241a76abbcaeec4))
* **backend/DataProvider:** check object-level permissions ([ca74ab3](https://gitlab.com/biomedit/portal/commit/ca74ab381bda730c1067f589ccf275fc4f1fa927))
* **backend/Node:** check object-level permissions ([8a6bd11](https://gitlab.com/biomedit/portal/commit/8a6bd1144842fa2ce57b0bbada0609eb0c4e51aa))
* **frontend/admin:** add group management tab ([3281914](https://gitlab.com/biomedit/portal/commit/3281914cefa64b2efbc4a4f6ac9678f561710f62))
* **frontend/admin/user:** add group assignment field ([b71b621](https://gitlab.com/biomedit/portal/commit/b71b621b7a23a79ce6bbb870bccc86955318a204))
* **backend:** make the lowest and highest `uid` and `gid` of users and `gid` of projects configurable in `config.json` ([37f6fc1](https://gitlab.com/biomedit/portal/commit/37f6fc18d640373382447f0245cbb7c3bdf58adf)), closes [#11](https://gitlab.com/biomedit/portal/issues/11)

### Bug Fixes

* **backend/notification:** be smarter while checking user affiliations ([6d74528](https://gitlab.com/biomedit/portal/commit/6d74528a1ee536ebb1f01d3b48cfc598670f49c2))
* **frontend/AutocompleteField:** attach a hidden field in case a 'AutocompleteField' is disabled ([8b6d840](https://gitlab.com/biomedit/portal/commit/8b6d8408cfb106478f4febcdc795ed8cd160d0e4)), closes [#369](https://gitlab.com/biomedit/portal/issues/369)
* **frontend/LabelledField:** attach a hidden field in case a 'LabelledField' is disabled ([cd4420e](https://gitlab.com/biomedit/portal/commit/cd4420e415c9d1bde07c04570c7eb5808a5fa47f)), closes [#369](https://gitlab.com/biomedit/portal/issues/369)
* **frontend/project:** pass project role via hidden field and remove 'ref' when field is disabled ([a8f4850](https://gitlab.com/biomedit/portal/commit/a8f485006eef0e2f9a8e3ea463e6944ca1610112)), closes [#369](https://gitlab.com/biomedit/portal/issues/369)
* **frontend/logger:** import 'setimmediate' polyfill for winston logger ([5e1ead9](https://gitlab.com/biomedit/portal/commit/5e1ead940d4c1f0607e98c8756c083c4763691af)), closes [#377](https://gitlab.com/biomedit/portal/issues/377)
* **frontend/validation:** fix 'max' vs. 'maxLength' ([8270b91](https://gitlab.com/biomedit/portal/commit/8270b915f7fccdc176730746dace8c86997338f0)), closes [#360](https://gitlab.com/biomedit/portal/issues/360)
* **frontend/validation:** specify a key to each 'validate' definition and use 'deepmerge' for merging the validators ([7c91c2d](https://gitlab.com/biomedit/portal/commit/7c91c2d6d5738587fb2eb8395315f896e01fbaf7)), closes [#360](https://gitlab.com/biomedit/portal/issues/360)
* **frontend/user:** overwrite 'datetime' as it can't cope with 'null' value ([3b61117](https://gitlab.com/biomedit/portal/commit/3b611177c90ae0073d611b8c131ccf93ffb6eefb))
* **config:** revert configuration ([c71b369](https://gitlab.com/biomedit/portal/commit/c71b3698c6243d1c7182dab9419eae1502162d17))
* **frontend/user:** use 'datetime' as sort type for 'lastlogin' column ([c93ee2c](https://gitlab.com/biomedit/portal/commit/c93ee2cb9de92ebe63f8ecab4578926f761b59c2)), closes [#357](https://gitlab.com/biomedit/portal/issues/357)
* **backend/project:** make gid field read-only ([add778f](https://gitlab.com/biomedit/portal/commit/add778f099cdfd3ddaa53898b49c2cc760f9daed))
* **backend/model:** make model str representations more verbose ([3813c3b](https://gitlab.com/biomedit/portal/commit/3813c3b8575bb54c4390596f66d603c1ee197be2))
* **frontend/UserManageForm:** make flags field optional ([d98a768](https://gitlab.com/biomedit/portal/commit/d98a768425f97df662bb28335de6ba5ddb7d5ca5))

## [3.3.0](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.12...3.3.0) (2021-04-20)

### Features

* **frontend/home:** add link to registry and specify 'target="_blank"' for each grid tile ([4370d4f](https://gitlab.com/biomedit/portal/commit/4370d4fd611038e6d79153eb3abcffa60d4bed2f)), closes [#336](https://gitlab.com/biomedit/portal/issues/336) [#343](https://gitlab.com/biomedit/portal/issues/343)
* **backend/user:** send notification ticket on user affiliation change ([3af7c70](https://gitlab.com/biomedit/portal/commit/3af7c70b14dc476fa204db8cc5e34cbd57ef244e)), closes [#332](https://gitlab.com/biomedit/portal/issues/332)
* **forms:** add `SelectArrayField` ([db01ca7](https://gitlab.com/biomedit/portal/commit/db01ca71d7b492f0a20e28d941d4f0ff273d04f3)), closes [#338](https://gitlab.com/biomedit/portal/issues/338)
* **frontend/forms:** add `AutocompleteArrayField` ([5d142f4](https://gitlab.com/biomedit/portal/commit/5d142f4aec13ee321d33c06bdcc8f63c8d0fd45e)), closes [#338](https://gitlab.com/biomedit/portal/issues/338)
* **frontend/DataTransferTables:** only search for numbers in `id` and `maxPackages` ([4f134db](https://gitlab.com/biomedit/portal/commit/4f134dbf1b1a980b48008495660d1629943535a9)), closes [#329](https://gitlab.com/biomedit/portal/issues/329)
* **user:** display 'local_username' for project users ([ac84f11](https://gitlab.com/biomedit/portal/commit/ac84f1196d321788692358a28b537ee082fbbda7))
* **frontend/administration:** prevent the currently logged in user from inactivating themself ([bbee346](https://gitlab.com/biomedit/portal/commit/bbee346cd036447809fd07c49163d679f7c43564)), closes [#328](https://gitlab.com/biomedit/portal/issues/328)
* **frontend/tests:** log all requests on fails associated with `RequestVerifier` ([09a0751](https://gitlab.com/biomedit/portal/commit/09a07514a2822092c167ea2cfd39a3a37f558da3))

### Bug Fixes

* **backend/data_package:** Propagate metadata to the DB ([871254e](https://gitlab.com/biomedit/portal/commit/871254e9d1f235e699a74cd0756c2af3bc3d7ab3))
* **backend/login:** redirect users trying to log in using a link with an expired session to the login page ([7437526](https://gitlab.com/biomedit/portal/commit/74375262fcfbba08824d7828f0a59f82f1397d38)), closes [#341](https://gitlab.com/biomedit/portal/issues/341)
* **frontend/DataTransfers:** fix a bug where newly added data transfers in `Projects` would not appear in `Data Transfers` ([21ae295](https://gitlab.com/biomedit/portal/commit/21ae2954d2a859c7d82b8ab0572b4c35602755e0))
* **frontend/renderers:** fix error when rendering a `LabelledControl` with `multiple` and an empty value ([de4d50b](https://gitlab.com/biomedit/portal/commit/de4d50b46e8caf412f629b8236978b23c760bfb1))

## [3.2.0](https://gitlab.com/biomedit/portal/compare/3.2.0-dev.11...3.2.0) (2021-03-22)

### Features

* **frontend/navigation:** highlight current route ([df1b46e](https://gitlab.com/biomedit/portal/commit/df1b46e944293b058553eb64883552e7461ef3f8))
* **frontend/Home:** add easter egg during easter ([1b5ffd2](https://gitlab.com/biomedit/portal/commit/1b5ffd2c8e5d66cb0777d2b3f07358087dd21765)), closes [#304](https://gitlab.com/biomedit/portal/issues/304)
* **frontend/Home:** show easter background during easter ([5c8d48a](https://gitlab.com/biomedit/portal/commit/5c8d48af3fc431ec96caf67f8acb82a3866f8dde)), closes [#304](https://gitlab.com/biomedit/portal/issues/304)
* **Administration/Users Table:** add column showing if user is active or not ([f162c09](https://gitlab.com/biomedit/portal/commit/f162c09386b8129c791eaa128ae90c8b80edb8d3)), closes [#268](https://gitlab.com/biomedit/portal/issues/268)
* **backend/user:** remove objects related to a user which was made inactive ([8ce40a6](https://gitlab.com/biomedit/portal/commit/8ce40a69b79dd223bec3edf565320e4fc1699fbf)), closes [#268](https://gitlab.com/biomedit/portal/issues/268)
* **user management:** include inactive users in the list of users ([fbf10bd](https://gitlab.com/biomedit/portal/commit/fbf10bd9d3115d8eb9e2700f07f2e8532f07781c)), closes [#268](https://gitlab.com/biomedit/portal/issues/268)
* **User Management:** allow changing the active status of a user from the frontend ([ee87471](https://gitlab.com/biomedit/portal/commit/ee874713d65d6a57e71e7f9de573bdbe81f6920e)), closes [#268](https://gitlab.com/biomedit/portal/issues/268)
* **Feed:** add basic markdown parsing for `title` and `message` ([9ba40ce](https://gitlab.com/biomedit/portal/commit/9ba40ce8ee50e8604a16f8c32616d849b4f1010a)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **Feed:** allow specifying a `title` separately from a `message` for `Feed` ([a6d742d](https://gitlab.com/biomedit/portal/commit/a6d742d6914eb8ed05f2d24fc21a2adf1bfcf469)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **frontend/Feed:** change icon for `Feed` label `WARN` to a warning symbol ([02f8eb0](https://gitlab.com/biomedit/portal/commit/02f8eb0ebfdb0b27ff2f8f8ccfb8b45d2b890463)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **frontend/FeedList:** show empty message when no feeds are present ([ce8c8f9](https://gitlab.com/biomedit/portal/commit/ce8c8f9c9ed810cae3457dc0d6d7d59f9815e4d1)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)

### Bug Fixes

* **frontend:** catch 'undefined' on value and default value ([a7c293a](https://gitlab.com/biomedit/portal/commit/a7c293a803c063930ccbb6cd54b6630e053811a9)), closes [#324](https://gitlab.com/biomedit/portal/issues/324)
* **frontend:** fix console errors when specifying users in a new project ([de109be](https://gitlab.com/biomedit/portal/commit/de109be9b7b10a1d60c1662a8c2b216716c8ef89)), closes [#324](https://gitlab.com/biomedit/portal/issues/324)
* **frontend/Home:** fix layout issue where quick access links would separate too far apart ([0eb6b62](https://gitlab.com/biomedit/portal/commit/0eb6b62ab1956254e45443adc9d22036e4a8f4a5))
* **backend/signals:** remove deprecation warning ([586a458](https://gitlab.com/biomedit/portal/commit/586a4582b2ac20ff25b2397cf598b192b33bb0cd))
* **frontend/DataTransferTable:** fix console error related to updating a component while rendering a different component ([d21374a](https://gitlab.com/biomedit/portal/commit/d21374a8839ecacb2b7d4d3e1a706ada412a8afb)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Home:** fix console error about missing `ref` ([740a572](https://gitlab.com/biomedit/portal/commit/740a57223f2331b64e71f39073b0a4a18ae126f8)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Home:** fix console error related to invalid DOM nesting ([8ffc005](https://gitlab.com/biomedit/portal/commit/8ffc005900837efb25b7febc7ff56de3fa63d1cc)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Projects:** fix console error about `Description` not having unique keys ([c810e66](https://gitlab.com/biomedit/portal/commit/c810e661fcd8d1bd10db7c089de1c0902385da5f)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Projects:** fix console error related to duplicate keys ([013b2ca](https://gitlab.com/biomedit/portal/commit/013b2ca351b2b30420cd0e32f54cf3910a2d9ae1)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Tab:** fix console error related to invalid DOM nesting ([de77b2f](https://gitlab.com/biomedit/portal/commit/de77b2f0ea7972af6225cace7d5eb906cfc9daa4)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Tables:** fix console error related to invalid DOM nesting ([c6738fd](https://gitlab.com/biomedit/portal/commit/c6738fdc2dd434fb8a4c15dc853702d5c1f0291f)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/UserInfoBox:** fix console error when opening the `UserInfoBox` due to a missing forwarded ref ([a5b5b42](https://gitlab.com/biomedit/portal/commit/a5b5b421a1c1b8a1ede57cfd5eeabd85047057e2)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/UserMenu:** fix console error when opening the Messages box ([3e401c4](https://gitlab.com/biomedit/portal/commit/3e401c48b5f9405d7f881efdaed82a458c263434)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Feed:** fix a bug where a feed with label `Warning` would not show an icon ([af417f7](https://gitlab.com/biomedit/portal/commit/af417f74e8b9981e309257e7edb03cb18391efad)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **frontend/Feed:** fix a bug where line breaks in Feed messages were ignored ([5e5bf7f](https://gitlab.com/biomedit/portal/commit/5e5bf7fd838b34ee5b70962a72d1cbc0f9432b24)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **frontend/Home:** fix strange layout with longer text in `Feed` ([6e49be1](https://gitlab.com/biomedit/portal/commit/6e49be168b2f2cce3234b81bcbb93a45ba5973e9)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)

## [3.1.0](https://gitlab.com/biomedit/portal/compare/3.1.0-dev.9...3.1.0) (2021-03-09)

### Features

* **frontedn/UserInfoBox:** show user flags ([d0a12f9](https://gitlab.com/biomedit/portal/commit/d0a12f93bdae2cb08e3df21d2cb291c30c2bd887)), closes [#250](https://gitlab.com/biomedit/portal/issues/250)
* **backend/user:** add optional flags specification on user ([21d9e98](https://gitlab.com/biomedit/portal/commit/21d9e980fad2a2c388b447d18214a789f20e6425)), closes [#288](https://gitlab.com/biomedit/portal/issues/288)
* **frontend/flags:** add a new tab for flags administration ([c050f87](https://gitlab.com/biomedit/portal/commit/c050f8706aa91fa11517068b193d4327352561a3)), closes [#288](https://gitlab.com/biomedit/portal/issues/288)
* **frontend/reducers:** introduce reducer for flags ([1f142c4](https://gitlab.com/biomedit/portal/commit/1f142c4f953682860cfe0ac8e1e49e171c739fbd)), closes [#288](https://gitlab.com/biomedit/portal/issues/288)
* **frontend/user form:** display the flags in the user form ([9d677b6](https://gitlab.com/biomedit/portal/commit/9d677b6a72398d9cda0a40bc2b430fddf4b0f300)), closes [#288](https://gitlab.com/biomedit/portal/issues/288)
* **frontend:** make docs url configurable in env vars ([4ed892c](https://gitlab.com/biomedit/portal/commit/4ed892c48c5273bb95a8b306b07effcd86166de2)), closes [#299](https://gitlab.com/biomedit/portal/issues/299)
* **maxPackages:** enforce 'maxPackages' specification (no longer optional) ([95d0bb7](https://gitlab.com/biomedit/portal/commit/95d0bb7783f0a5aa01d684fa340ef0d0f108627a))
* **backend/migrations:** generate migration script ([8d6c9bc](https://gitlab.com/biomedit/portal/commit/8d6c9bcf14dbfdb3eadaf9b623b7489819fb6a30)), closes [#283](https://gitlab.com/biomedit/portal/issues/283)
* **backend/models:** replace user 'status' with flags ([76bb9a1](https://gitlab.com/biomedit/portal/commit/76bb9a1a5d0d5ea4ba65c4fb584e87ca45ad813a)), closes [#283](https://gitlab.com/biomedit/portal/issues/283)
* **frontend/api:** re-generate the API ([6a7350c](https://gitlab.com/biomedit/portal/commit/6a7350c91f632be693c5564aa4d75cb022a29415)), closes [#283](https://gitlab.com/biomedit/portal/issues/283)

### Bug Fixes

* **frontend/FlagManageForm:** use right backend API for flag uniqueness ([d42c71d](https://gitlab.com/biomedit/portal/commit/d42c71d1f32c5bf180225b14681c88b32772c5d6))
* **frontend/forms:** fix delay when clicking on the `submit` button in forms ([f94df38](https://gitlab.com/biomedit/portal/commit/f94df38d1b2f09f87bac4a8e6483328017a99bb4)), closes [#280](https://gitlab.com/biomedit/portal/issues/280)
* **frontend/affiliations:** nicely display the affiliations ([dedc85d](https://gitlab.com/biomedit/portal/commit/dedc85dc033fa14a20034625f826f3caa203a56e))
* **backend/exception_handler:** log exceptions where a user is not logged in as `INFO` instead of `ERROR` ([6fc62b5](https://gitlab.com/biomedit/portal/commit/6fc62b5f3fe70a419ddd9ee3e67424e492628f30))
* **frontend/maxPackages:** show unlimited DTR's when filtering data transfer tables for 'unlimited' ([eee6856](https://gitlab.com/biomedit/portal/commit/eee685643cd1eef277086e7c825b68d75edc0e3b)), closes [#249](https://gitlab.com/biomedit/portal/issues/249)
* **frontend/user administration:** remove displaying/management of user status ([aacf895](https://gitlab.com/biomedit/portal/commit/aacf895b33b13a4360e726190c1189e0f2429bd0)), closes [#283](https://gitlab.com/biomedit/portal/issues/283)

## [3.0.0](https://gitlab.com/biomedit/portal/compare/3.0.0-dev.6...3.0.0) (2021-02-22)

### ⚠ BREAKING CHANGES

* **frontend:** The restriction of only having environment variables exposed to the frontend
with keys prefixed by `REACT_APP_` came from `create-react-app`.
With the migration to Next.js, we have to use the Next.js specific prefix `NEXT_PUBLIC_`.

Migration Steps: Rename all environment variable key names starting with `REACT_APP_` to start with `NEXT_PUBLIC_` instead.

### Features

* **logging:** unify different logging streams into the console output ([3be84e1](https://gitlab.com/biomedit/portal/commit/3be84e1bb77776278a9d40a653966d5ec6ec8aca))
* **frontend:** conditional adding of empty entries for IP ranges and resources ([e15f7f7](https://gitlab.com/biomedit/portal/commit/e15f7f78d559a1de20d7318c951dabb9fba86244))
* **backend/test_node:** ensure that we have the right number of registered node users returned ([b473ee5](https://gitlab.com/biomedit/portal/commit/b473ee518928ff196e01027da8c95a29d149c662))

### Bug Fixes

* **frontend/resources:** improve layout of resources block ([e3c303f](https://gitlab.com/biomedit/portal/commit/e3c303fc9481fbf2005b0e28d3bd2894747b0de1)), closes [#290](https://gitlab.com/biomedit/portal/issues/290)
* **backend/user status:** remove all consequences linked to user status changes ([42acd87](https://gitlab.com/biomedit/portal/commit/42acd87b9d0ed0725fea7fc3ba2b210e81e1758c)), closes [#219](https://gitlab.com/biomedit/portal/issues/219) [#217](https://gitlab.com/biomedit/portal/issues/217) [#284](https://gitlab.com/biomedit/portal/issues/284)
* **frontend/UsersList:** remove warning icon when user is 'INITIAL' and get rid of 'excludeUnauthorized' ([6692199](https://gitlab.com/biomedit/portal/commit/6692199d3794750330de3b9d63d547ec27fd39cf)), closes [#284](https://gitlab.com/biomedit/portal/issues/284)
* **frontend/UsersList:** make sure that we are comparing numbers ([7cd17e6](https://gitlab.com/biomedit/portal/commit/7cd17e6d2205a6c125866795b4120258c8c3348e)), closes [#295](https://gitlab.com/biomedit/portal/issues/295) [#279](https://gitlab.com/biomedit/portal/issues/279)
* **frontend:** prevent errors when accessing `window` / `document` during SSR ([ea821d1](https://gitlab.com/biomedit/portal/commit/ea821d1cffe502bb77c324b5bbdcf950c4d3a724))
* **NotFound:** fix error `h6 cannot appear as a descendant of p` ([f95774a](https://gitlab.com/biomedit/portal/commit/f95774a76f4741146de69db2e97feff660be15cf))
* **frontend:** replace `react-env` mechanism for loading environment variables with Next.js built-in features ([46dcf8d](https://gitlab.com/biomedit/portal/commit/46dcf8d5c7fe3f5892abbd6522dd9f3b83cf25b1))
* **backend/user:** return all active users from '/backend/users' to node managers ([b4a722f](https://gitlab.com/biomedit/portal/commit/b4a722feb2986366b23ed545ab544c7216069439)), closes [#291](https://gitlab.com/biomedit/portal/issues/291)

## [2.2.0](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.12...2.2.0) (2021-02-08)

### Features

* **backend/data_transfer:** completely remove RT ticket generation/email notification on DTR creation ([b8af12e](https://gitlab.com/biomedit/portal/commit/b8af12e6e0ad65e304b276b1ec12fdc3a42fa600)), closes [#276](https://gitlab.com/biomedit/portal/issues/276)
* **frontend:** DM are no longer able to create new DTRs ([260c860](https://gitlab.com/biomedit/portal/commit/260c8606e1b934a360b17690ec5b56c144c291e0)), closes [#276](https://gitlab.com/biomedit/portal/issues/276)
* **frontend/forms:** make all dialogs that can be cancelled closeable by pressing the escape key on the keyboard or clicking outside of the dialog ([0d19c8b](https://gitlab.com/biomedit/portal/commit/0d19c8bdb76d68a6b6eec1dd77d91e9a6a9cb2dd))
* **backend:** a node manager can edit his own nodes ([801e859](https://gitlab.com/biomedit/portal/commit/801e859b5029045afc88d64fd885b8f674875740)), closes [#269](https://gitlab.com/biomedit/portal/issues/269)
* **backend/ReadOnly:** 'ReadOnly' should apply to authenticated users only ([10462a5](https://gitlab.com/biomedit/portal/commit/10462a55cc4bd7240c6af2b8a8c8b4b9a743af77))
* **frontend:** polish node management by NM ([ce12192](https://gitlab.com/biomedit/portal/commit/ce1219219607f2b76f2d9063c4f3b27ca1757ed8)), closes [#269](https://gitlab.com/biomedit/portal/issues/269)
* **frontend/navigation:** rename 'Users & Groups' to 'Administration' ([a6a7651](https://gitlab.com/biomedit/portal/commit/a6a76511b3cc58d818291388130f4e13c519245f))
* **frontend/permmissions:** make 'Nodes' tab item accessible to node managers ([d813e85](https://gitlab.com/biomedit/portal/commit/d813e85b66a340136c1c660b4a8a97ea3e3c97c4)), closes [#269](https://gitlab.com/biomedit/portal/issues/269)
* **DataTransfer:** prevent changing purpose on DTR's once a data package was created ([6a1edbf](https://gitlab.com/biomedit/portal/commit/6a1edbf07fb1b8ce8c3cd4be433cf95406a4a23a))
* **users:** validate `email` on `User` to be unique ([2968a4d](https://gitlab.com/biomedit/portal/commit/2968a4d2d0477687189c9adfa16fecd7544baf35)), closes [#273](https://gitlab.com/biomedit/portal/issues/273)

### Bug Fixes

* **frontend/ListHooks:** fix repetitive effect calls ([87496b3](https://gitlab.com/biomedit/portal/commit/87496b396589d8bfd017ffbee39a831a8a00f17d))
* **frontend/UsersList:** fix add user not working ([87aa1bb](https://gitlab.com/biomedit/portal/commit/87aa1bbb842a2245b43f2e8328ba00ad20cb19d6))
* **frontend:** revert cleanup "missing dependencies" linter warnings ([cf42297](https://gitlab.com/biomedit/portal/commit/cf422971267853ab4a50dceb8e94b72e9e727361))
* **frontend/ProjectList:** only show edit button on projects the user has the permissions to edit ([2141216](https://gitlab.com/biomedit/portal/commit/21412161ba66bef32488ba8e10d161f9f6295111)), closes [#281](https://gitlab.com/biomedit/portal/issues/281)
* **backend/Userinfo:** make Userinfo ID required as we're only using GET ([dda607d](https://gitlab.com/biomedit/portal/commit/dda607d4e0032b0c4020cb0c556e3db0a9103333))
* **frontend:** make sure that NMs are not editing node IDs ([fcc6a63](https://gitlab.com/biomedit/portal/commit/fcc6a632501b950625c305c57c103e6e2a7927c4)), closes [#269](https://gitlab.com/biomedit/portal/issues/269)
* **frontend/empty tables:** fix add button not being visible when table is empty ([5f8ac65](https://gitlab.com/biomedit/portal/commit/5f8ac654960fc986efa1a9898efbf8f324befed5))

## [2.1.0](https://gitlab.com/biomedit/portal/compare/2.1.0-dev.0...2.1.0) (2021-01-26)

### Features

* **user:** enforce `email` of users to be unique ([2a51934](https://gitlab.com/biomedit/portal/commit/2a519344f05f998630baf0ea2632a3bad896441b))

## [2.0.0](https://gitlab.com/biomedit/portal/compare/2.0.0-dev.1...2.0.0) (2021-01-26)

### ⚠ BREAKING CHANGES

* **backend:** If you are upgrading an existing `portal` installation, you MUST first deploy this version
and follow the migration steps BEFORE deploying any later version!

Django requires the first migration to be the one that initializes a custom user model
(see <https://docs.djangoproject.com/en/3.1/topics/auth/customizing/#changing-to-a-custom-user-model-mid-project>)
which is why all existing migrations had to be removed, which requires manual intervention during deployment.

Migration Steps:

* Stop the running docker containers, except for the database container
* Make a backup of the database
* Run the following query on the database: `TRUNCATE django_migrations`
* Deploy the image of EXACTLY this version (don't run the backend server yet)
* Run `./manage.py migrate --fake` on the backend container
* Run the backend server as usual
* From this point on, you can deploy any version later than this one,
using `./manage.py migrate` (without `--fake`) and running the server like usual

* **backend:** use custom user model for authentication ([875c98f](https://gitlab.com/biomedit/portal/commit/875c98f27b7bfd27cfd76a06eb76cbfc01b6b398))

### Features

* **frontend/UserManageForm:** add cancel button to user edit form ([f118e18](https://gitlab.com/biomedit/portal/commit/f118e18d6cea66e4761fd56856d95b228a259614))
* **user management:** enable creating local users in the user management in the frontend ([0c7cef7](https://gitlab.com/biomedit/portal/commit/0c7cef78cf4f87f5aee0b5664435196f44786f4a))

### Bug Fixes

* **user_init:** use `get_user_model` to retrieve the `User` model as defined in `settings.py` ([cd7a6dc](https://gitlab.com/biomedit/portal/commit/cd7a6dc513536887d6e9e5c626bf8aa68f54d9ac))

## [1.4.0](https://gitlab.com/biomedit/portal/compare/1.4.0-dev.8...1.4.0) (2021-01-25)

### Features

* **backend/unauthorized users:** prevent the backend from accepting users in `UNAUTHORIZED` status in modifying operations on projects ([0b973cd](https://gitlab.com/biomedit/portal/commit/0b973cd6ec3cad498ffeeb863bfbc90fbe694bb5)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)
* **frontend/ProjectForm:** show warning icon next to users with `INITIAL` state ([8662bfc](https://gitlab.com/biomedit/portal/commit/8662bfcf8e1fb36d57aa1fee091fe35d341477c5)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)
* **frontend/user selection:** hide users in `UNAUTHORIZED` status from user selection in the project form ([7adc655](https://gitlab.com/biomedit/portal/commit/7adc655d4dacc80bba518a484d54828a5a7ff98e)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)
* **Project Resources:** disable resources for users with `INITIAL` status ([c33a55b](https://gitlab.com/biomedit/portal/commit/c33a55b317fc9d11f316185b7434a1cf33e29cea)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)
* **users:** hide users which are not active ([3b494a9](https://gitlab.com/biomedit/portal/commit/3b494a99fe5b56818f9314122d8f968210414ff3)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)
* **backend/project_user_notification:** for each user added/removed, specify the status ([064eb1c](https://gitlab.com/biomedit/portal/commit/064eb1cc4eff99b4cd611d0807e36daffa0e9c35))
* **data package:** Add 'ordering' to '/backend/data-transfer/' ([a569dbc](https://gitlab.com/biomedit/portal/commit/a569dbcd663b65930ccff4b1e7ae38f6189910e8))
* **fontend/DataTransferDetail:** Use file name instead of 'Package X' ([fc78b35](https://gitlab.com/biomedit/portal/commit/fc78b358242404e4c7498eeaecad4f9c2943baf2))
* **serializers/data_transfer:** Sort data packages inside a data transfer by file name ([ac964c1](https://gitlab.com/biomedit/portal/commit/ac964c1482ada3feb7967c8e51871ccf3a2e0618))
* **backend:** add name to projects app ([157aa50](https://gitlab.com/biomedit/portal/commit/157aa50dcdd21eacfffecd3733637e189ce8f4f1))
* **logging:** add request logging ([e8302ae](https://gitlab.com/biomedit/portal/commit/e8302ae0af4bfce4a9a5b16b9d4e031e71af9785)), closes [#242](https://gitlab.com/biomedit/portal/issues/242)
* **test:** Improve error message on failing ([499a25c](https://gitlab.com/biomedit/portal/commit/499a25ce3615101d7a3084caa04943637f6ff3b4))
* **frontend/ProjectForm:** Add more space between project metadata fields ([5f214ab](https://gitlab.com/biomedit/portal/commit/5f214aba6bb2f75fa2b4465f26ed77de7e961d83))
* **frontend/SetUsernameDialog:** Use 'fullWidth' for input field ([46d73cb](https://gitlab.com/biomedit/portal/commit/46d73cb4142944cfe56d7d0128d365026282c2bb))

### Bug Fixes

* **backend/update_permissions:** add user status to PL mail ([30518bd](https://gitlab.com/biomedit/portal/commit/30518bd86a0795b5d5bdfaae03d06fccd1a7392a))
* **frontend/forms:** support nested array fields in forms ([12f41ec](https://gitlab.com/biomedit/portal/commit/12f41eced27af14221945ffb24237d445eecc293))
* **UserDataPermission:** handle requests without profile field ([ffdf7df](https://gitlab.com/biomedit/portal/commit/ffdf7df3ce6e306290ac1c8ce91277bdb21f4b67))

## [1.3.0](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.12...1.3.0) (2021-01-11)

### Features

* **projects:** only send email notification to ticketing system when AUTHORIZED users are added/removed to/from a project ([df56f4d](https://gitlab.com/biomedit/portal/commit/df56f4d1e28f25e4d77bf7d8180b90c989468e23)), closes [#219](https://gitlab.com/biomedit/portal/issues/219)
* **projects:** remove all project permission from users when their status is changed from AUTHORIZED to INITIAL or UNAUTHORIZED and send email notification ([519d3ba](https://gitlab.com/biomedit/portal/commit/519d3babdf7534c09f208983367d1c684a00d2ea)), closes [#219](https://gitlab.com/biomedit/portal/issues/219)
* **projects:** send email notification to ticketing system when status of user is set to AUTHORIZED ([39a4a9f](https://gitlab.com/biomedit/portal/commit/39a4a9fe3885ceb33f9c246d49d3bb160010e571)), closes [#219](https://gitlab.com/biomedit/portal/issues/219)
* **projects/users:** only return users with AUTHORIZED status when users of a project are retrieved by permission ([c48b8c3](https://gitlab.com/biomedit/portal/commit/c48b8c3eccdd5dd98d4fc6c560aede21c333b18f)), closes [#219](https://gitlab.com/biomedit/portal/issues/219)
* **backend:** Add key_length to PgpKey model ([b1dd5d6](https://gitlab.com/biomedit/portal/commit/b1dd5d6157f10c0b37866398bf9ba2b2627087ef))
* **backend:** key sign request: check for minimal key length ([7da8401](https://gitlab.com/biomedit/portal/commit/7da8401c801972489b43b81075dc4c60ed81bb43))
* **backend/OIDCAuth:** Adapt 'OIDCAuth' to support local -> federated user migration ([5e5d727](https://gitlab.com/biomedit/portal/commit/5e5d7270e0137247b4fb6eec4911719ea10fbc80))
* **frontend/ProjectForm:** Check whether at least one PL has been assigned to the project ([2f31f01](https://gitlab.com/biomedit/portal/commit/2f31f01600243b530e81fc1c79bea472c09f8258))
* **data providers:** show unique validation errors for name and id ([cdd9831](https://gitlab.com/biomedit/portal/commit/cdd983188431ede5dbde6f2edb86aec4ae24ee5b))
* **frontend:** prevent repeated calls to the backend during uniqueness check if keystrokes are less than 750ms apart ([b626fcc](https://gitlab.com/biomedit/portal/commit/b626fcc6d129dbebaccfaeec46c3c0551a511567))
* **nodes:** show unique validation errors for name and id ([20b6bc1](https://gitlab.com/biomedit/portal/commit/20b6bc1d9ea882217c4775e6e1798a5f97fb5cc7))
* **projects:** show unique validation errors for name and id ([a6f06f6](https://gitlab.com/biomedit/portal/commit/a6f06f63cbfd9c33de768c1e0687a4fa8a0117a6))
* **backend:** send all logs to ELK ([2f59b3a](https://gitlab.com/biomedit/portal/commit/2f59b3a4efa2976739fbc4d0340ee4e8d27f4867))

### Bug Fixes

* **tables:** navigate to first page when search query is changed ([c49d1e3](https://gitlab.com/biomedit/portal/commit/c49d1e366a8d17b5578c6fa18fcb8a955296327c)), closes [#256](https://gitlab.com/biomedit/portal/issues/256)
* **username form:** correct validation error to mention that the username must start with a letter ([93e06fc](https://gitlab.com/biomedit/portal/commit/93e06fc155966f3238fc28fa4298fb3871229c78)), closes [#259](https://gitlab.com/biomedit/portal/issues/259)
* **backend/project:** `send_user_emails` should be invoked on project creation/deletion as well ([4748a9f](https://gitlab.com/biomedit/portal/commit/4748a9fe0bd51f66532d4c3cab905c108387c0bb))
* **node:** fix unique check for nodes ([ed07c0a](https://gitlab.com/biomedit/portal/commit/ed07c0adab6eb4078e58b07231784fa21ae37605))
* **username validation:** fix incorrect validation error message for usernames ([32c2210](https://gitlab.com/biomedit/portal/commit/32c2210f7341aaf5841a4598d5e842ba8a35939e))
* **frontend:** fix unique check not working in production builds ([891c6bc](https://gitlab.com/biomedit/portal/commit/891c6bcece7733c5ea2a9c3cd6110f74ef759ef8)), relates to [/github.com/facebook/create-react-app/blob/master/packages/react-scripts/config/webpack.config.js#L272](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/config/webpack.config.js/issues/L272)
* **frontend:** when editing an object, changing a text field's value back to its initial value will not show a unique validation error ([d961e50](https://gitlab.com/biomedit/portal/commit/d961e50ccb781f384ffcff761ca882986b36ebbf))
* **frontend/projects:** Revert Display dedicated "empty project" message for initial users ([d71a91a](https://gitlab.com/biomedit/portal/commit/d71a91a1bfcbfdc9affe12c0477a24e33b5723b2))
* **frontend:** remove leading dollar sign in correlationId's ([eac12c6](https://gitlab.com/biomedit/portal/commit/eac12c64b02f89a74a16ce3a2d0fb003df129f43))
* **frontend:** fix toasts not appearing on errors ([bc9bd71](https://gitlab.com/biomedit/portal/commit/bc9bd7163629a8ad2ec80fb99bcff7e70b726357))

## [1.2.0](https://gitlab.com/biomedit/portal/compare/1.2.0-dev.4...1.2.0) (2020-12-14)

### Features

* **frontend/projects:** Display dedicated "empty project" message for initial users ([7410716](https://gitlab.com/biomedit/portal/commit/74107162e891cad4249937df6fa8c6d730a93e5c))
* **frontend/users:** show user status in table under `Users & Groups` ([ca93594](https://gitlab.com/biomedit/portal/commit/ca935941722721e48560fcf5f02c862fd9086f28)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)
* **user:** add new property `status` to user profile ([c4fa04a](https://gitlab.com/biomedit/portal/commit/c4fa04a1b675112e1da6e87c4e3ba37f8d56cb67)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)
* **users:** allow changing user status on the frontend ([0d22e13](https://gitlab.com/biomedit/portal/commit/0d22e13fc79278b7f96d6176b541bdda901c8796)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)
* **frontend/users:** show user status in table under `Users & Groups` ([ca93594](https://gitlab.com/biomedit/portal/commit/ca935941722721e48560fcf5f02c862fd9086f28)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)
* **user:** add new property `status` to user profile ([c4fa04a](https://gitlab.com/biomedit/portal/commit/c4fa04a1b675112e1da6e87c4e3ba37f8d56cb67)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)
* **users:** allow changing user status on the frontend ([b16d34c](https://gitlab.com/biomedit/portal/commit/b16d34cfcc05c627c68ce856224a1384b178cdd8)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)

### Bug Fixes

* **frontend/fields:** support retrieving initial values of fields which are nested ([2dbaaa8](https://gitlab.com/biomedit/portal/commit/2dbaaa82e72e27acf180e26f879b073dd7c4344c))
* **frontend/fields:** support retrieving initial values of fields which are nested ([2dbaaa8](https://gitlab.com/biomedit/portal/commit/2dbaaa82e72e27acf180e26f879b073dd7c4344c))

## [1.1.0](https://gitlab.com/biomedit/portal/compare/1.1.0-dev.10...1.1.0) (2020-12-08)

### Features

* allow "'" in names ([69704d7](https://gitlab.com/biomedit/portal/commit/69704d79486ab9c5949685552e6dc28f0c6f2b04))
* **frontend/Home:** show christmas wishes during christmas ([f34a530](https://gitlab.com/biomedit/portal/commit/f34a5300c70710fee93d3d803bdfe4c16aec11cd))
* **frontend/UserMenu:** show christmas hat on user icon during christmas ([659bf33](https://gitlab.com/biomedit/portal/commit/659bf33db993fe927812e558b8521cbb3e3778ce))
* **frontend/UserMenu:** show snow background during christmas ([dc9d081](https://gitlab.com/biomedit/portal/commit/dc9d08162e36ad27aab2bf62513917dfae57bbf0))
* **frontend/nav:** show current portal version ([ecda227](https://gitlab.com/biomedit/portal/commit/ecda227091a603b657cafd3875d58abd3d03c6de))

### Bug Fixes

* **backend/dtr_creation:** only create child tickets if we have a 'complete' ticket body ([f21a021](https://gitlab.com/biomedit/portal/commit/f21a0216609d1ead2bfac786607bee8b09c9f1ea))
* **frontend/ProjectForm:** make project name wider ([de50406](https://gitlab.com/biomedit/portal/commit/de50406a5b998a5845bb2ad1de236d48bb6b91ff))
* **backend/dtr_creation:** remove unused method ([aed9901](https://gitlab.com/biomedit/portal/commit/aed9901ef200b0f1e17d4e87179de1fefc8110c7))
* **backend/dtr_creation:** switch log level from info to debug ([d8d54ad](https://gitlab.com/biomedit/portal/commit/d8d54ada88a59fe1bc8e153c4477b44e3b6dfba8))
* **backend/dtr_creation:** wrong salutation for node managers ([0a1837a](https://gitlab.com/biomedit/portal/commit/0a1837acf6aec233bad8f17e5cdb5cec210821ac))

## 1.0.0 (2020-12-02)

### Bug Fixes

* make ip_address_ranges consistent in /backend/projects/ ([f87bd93](https://gitlab.com/biomedit/portal/commit/f87bd93d99bd6eece96968f0209c1dedadcb8393))
