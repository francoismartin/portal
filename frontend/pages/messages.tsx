import React, { ReactElement } from 'react';
import {
  MessageTable,
  MessageTableRefresh,
} from '../src/components/message/MessageTable';
import { PageBase } from '@biomedit/next-widgets';
import { getPageTitle } from '../src/utils';
import { FrontendGetStaticProps } from '@biomedit/next-widgets';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { I18nNamespace } from '../src/i18n';

export const MessagePage = (): ReactElement => {
  return (
    <PageBase
      title={getPageTitle('Messages')}
      content={MessageTable}
      toolbar={MessageTableRefresh}
      props={{}}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.USER_INFO,
      I18nNamespace.MESSAGE,
    ])),
  },
});

export default MessagePage;
