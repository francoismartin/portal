import React, { ReactElement } from 'react';
import { PageBase } from '@biomedit/next-widgets';
import { getPageTitle } from '../src/utils';
import { FrontendGetStaticProps } from '@biomedit/next-widgets';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { I18nNamespace } from '../src/i18n';
import { PgpKeyInfoList } from '../src/components/pgpKeyInfo/PgpKeyInfoList';

export const KeysPage = (): ReactElement => {
  return (
    <PageBase
      title={getPageTitle('My Keys')}
      content={PgpKeyInfoList}
      props={{}}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.USER_INFO,
      I18nNamespace.PGP_KEY_INFO_LIST,
      I18nNamespace.PGP_KEY_INFO_MANAGE_FORM,
      I18nNamespace.LIST,
    ])),
  },
});

export default KeysPage;
