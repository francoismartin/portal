import React, { ReactElement } from 'react';
import { useRouter } from 'next/router';
import {
  DataTransferOverviewTable,
  DataTransferOverviewTableProps,
} from '../../src/components/dataTransfer/DataTransferOverviewTable';
import { DataTransferExport } from '../../src/components/dataTransfer/DataTransferExport';
import { PageBase } from '@biomedit/next-widgets';
import { getPageTitle } from '../../src/utils';

export { getStaticProps } from './index';

export const DataTransferPageWithSelection = (): ReactElement => {
  const router = useRouter();
  const props: DataTransferOverviewTableProps = {};
  const selected = router.query.id;

  if (typeof selected === 'string') props.selected = selected;

  return (
    <PageBase
      title={getPageTitle('Data Transfers')}
      content={DataTransferOverviewTable}
      toolbar={DataTransferExport}
      props={props}
    />
  );
};

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export default DataTransferPageWithSelection;
