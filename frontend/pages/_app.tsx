import type { AppProps } from 'next/app';
import React, { ReactElement, useEffect, useMemo, useState } from 'react';
import { Provider, useDispatch, useSelector } from 'react-redux';
import { CHECK_AUTH } from '../src/actions/actionTypes';
import { UserInfoBox } from '../src/components/nav/UserInfoBox';
import { UserProfileDialog } from '../src/components/UserProfileDialog';
import { contactEmail } from '../src/config';
import { wrapper } from '../src/store';
import { appWithTranslation, Trans } from 'next-i18next';
import { useRouter } from 'next/router';
import nextI18NextConfig from '../next-i18next.config.js';
import {
  Container,
  EmailLink,
  ErrorBoundary,
  Header,
  isClient,
  ListItemAction,
  logger,
  LogListener,
  LogoutMenuItem,
  Nav,
  NavItem,
  offLog,
  onLog,
  requestAction,
  ThemeProvider,
  ToastBar,
  UserMenuItemModel,
} from '@biomedit/next-widgets';
import { winstonLogger } from '../src/logger';
import { createMenuItems, userMenu } from '../src/structure';
import { UserMenu } from '../src/components/nav/UserMenu';
import { logoutUrl } from '../src/api/api';
import { supportedBrowsers } from '../src/supportedBrowsers';
import isbot from 'isbot';
import { I18nNamespace } from '../src/i18n';
import { Box, Divider, createTheme } from '@mui/material';
import { Footer } from '../src/components/Footer';
import Head from 'next/head';

export const unsupportedBrowserLogMessage = 'Browser is not supported!';

export function App({ children }: { children? }): ReactElement {
  // See https://github.com/vercel/next.js/discussions/17443
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
  }, []);

  const isSupportedBrowser: boolean | undefined = useMemo(() => {
    if (isClient()) {
      const userAgent = (window.navigator || navigator).userAgent;
      if (isbot(userAgent)) {
        // prevent bots from crawling contact email address
        return true;
      }
      return supportedBrowsers.test(userAgent);
    } else {
      return undefined;
    }
  }, []);

  const dispatch = useDispatch();

  const isFetching = useSelector((state) => state.auth.isFetching);
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);
  const permissionGroups = useSelector((state) => state.auth.pagePermissions);
  // see https://github.com/vercel/next.js/discussions/31152#discussioncomment-1689577
  const { pathname } = useRouter() || { pathname: '/' };
  useEffect(() => {
    if (isSupportedBrowser && !isAuthenticated) {
      dispatch(requestAction(CHECK_AUTH));
    }
  }, [dispatch, isAuthenticated, isSupportedBrowser]);

  useEffect(() => {
    const log = winstonLogger();
    const listener: LogListener = (data) => {
      log.log(data);
    };
    onLog(listener);
    return () => {
      offLog(listener);
    };
  }, []);

  const log = logger('App');

  if (mounted && !isSupportedBrowser) {
    log.warn({ message: unsupportedBrowserLogMessage });
    return (
      <Trans
        i18nKey={`${I18nNamespace.COMMON}:unsupportedBrowser`}
        values={{ contactEmail }}
        components={{
          email: <EmailLink email={contactEmail ?? ''} />,
          ulist: <ul />,
          litem: <li />,
          break: <br />,
        }}
      />
    );
  }

  const subjectPrefix = 'Portal: ';

  const theme = createTheme({
    palette: {
      primary: {
        main: '#010101',
        light: '#646363',
      },
      secondary: {
        main: '#e30613',
        light: '#ee7559',
      },
      background: { default: 'white' },
    },
  });

  return (
    <ErrorBoundary
      subjectPrefix={subjectPrefix}
      contactEmail={contactEmail ?? ''}
    >
      <ThemeProvider theme={theme}>
        <div>
          <ToastBar
            subjectPrefix={subjectPrefix}
            contactEmail={contactEmail ?? ''}
          />
          {isAuthenticated && !isFetching && (
            <Box display="flex">
              <UserProfileDialog />
              <Nav>
                <UserMenu>
                  <UserInfoBox />
                  <Divider />
                  <LogoutMenuItem logoutUrl={logoutUrl} />
                  {userMenu.map((menuItem: UserMenuItemModel) => (
                    <ListItemAction
                      action={menuItem.action}
                      key={menuItem.title}
                      primary={menuItem.title}
                      icon={menuItem.icon}
                    />
                  ))}
                </UserMenu>
                {createMenuItems(permissionGroups).map((menuItem) => (
                  <NavItem
                    selected={menuItem.link == pathname}
                    href={menuItem.link ?? '#'}
                    key={'MenuItem-' + menuItem.title}
                    primary={menuItem.title}
                    icon={menuItem.icon}
                  />
                ))}
                <Footer />
              </Nav>
              <Container maxWidth={false}>
                <>
                  <Header
                    logoSrc="/biomedit_logo.png"
                    logoWidth="313"
                    logoHeight="80"
                    logoText="BioMedIT Logo"
                    logoHref="https://www.biomedit.ch/"
                  />
                  {children}
                </>
              </Container>
            </Box>
          )}
        </div>
      </ThemeProvider>
    </ErrorBoundary>
  );
}

const AppWrapper = ({ Component, ...rest }: AppProps): ReactElement => {
  const { store, props } = wrapper.useWrappedStore(rest);
  return (
    <>
      <Head>
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>
      <Provider store={store}>
        <App>
          <Component {...props.pageProps} />
        </App>
      </Provider>
    </>
  );
};

export default appWithTranslation(AppWrapper, nextI18NextConfig);
