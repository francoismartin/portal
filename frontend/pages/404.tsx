import React, { ReactElement } from 'react';
import { FrontendGetStaticProps, NotFound } from '@biomedit/next-widgets';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { contactEmail } from '../src/config';
import { I18nNamespace } from '../src/i18n';

export const NotFoundPage = (): ReactElement => (
  <NotFound contactEmail={contactEmail ?? ''} />
);

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.USER_INFO,
    ])),
  },
});

export default NotFoundPage;
