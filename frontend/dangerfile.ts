import {
  GitMatchResult,
  MatchResult,
  danger,
  fail,
  message,
  schedule,
  warn,
} from 'danger';

async function praiseDependencyReduction() {
  const pkg = 'frontend/package.json';
  if (danger.git.modified_files.includes(pkg)) {
    const displayMsg = (depList, isDev: boolean) => {
      if (depList && depList.length > 0) {
        message(
          ':tada: Congratulations! You have reduced the ' +
            (isDev ? 'development ' : '') +
            'dependency tree by ' +
            `${depList.length} ` +
            (depList.length == 1 ? 'package' : 'packages') +
            ` (${depList})`
        );
      }
    };
    const result = await danger.git.JSONDiffForFile(pkg);
    displayMsg(result['dependencies']?.removed, false);
    displayMsg(result['devDependencies']?.removed, true);
  }
}

function warnNoTests(
  src: MatchResult<GitMatchResult>,
  test: MatchResult<GitMatchResult>,
  where: string
) {
  if (src.edited && !test.edited) {
    warn(
      `(${where}): There are changes in the application code but not in tests. ` +
        "That's OK as long as you're refactoring existing code."
    );
  }
}

function notifyTechnicalCoordinators() {
  const labelName =
    process.env.TECHNICAL_COORDINATOR_NOTIFICATION_LABEL ?? 'UX/UI';
  const technicalCoordinator = process.env.TECHNICAL_COORDINATOR;

  if (technicalCoordinator && danger.gitlab.mr.labels.includes(labelName)) {
    message(
      `Tagging ${technicalCoordinator} because this MR was labelled with ${labelName}.`
    );
  }
}

function checkBreakingChangeFooter() {
  const labelName = process.env.BREAKING_CHANGE_LABEL ?? 'breaking-change';
  const footer = process.env.BREAKING_CHANGE_FOOTER_LABEL ?? 'BREAKING CHANGE:';
  const breakingCommits = danger.git.commits.filter((commit) =>
    commit.message.includes(footer)
  );

  if (
    danger.gitlab.mr.labels.includes(labelName) &&
    !!danger.git.commits.length &&
    !breakingCommits.length
  ) {
    fail(
      `This MR was labelled with ${labelName}, but none of its commits have the \`${footer}\` footer.`
    );
  }
}

// Frontend: warn if there are changes in the application code but not in tests
warnNoTests(
  danger.git.fileMatch('frontend/**/*.ts*'),
  danger.git.fileMatch('frontend/**/*.test.*', 'frontend/test-data/**/*.*'),
  'frontend'
);

// Backend: warn if there are changes in the application code but not in tests
warnNoTests(
  danger.git.fileMatch('web/**/*.py*'),
  danger.git.fileMatch('web/**/test_*.py'),
  'backend'
);

// Praise the author
schedule(praiseDependencyReduction);

// Notify technical coordinators about UX/UI changes
notifyTechnicalCoordinators();

// Warn if breaking change footer is missing
checkBreakingChangeFooter();
