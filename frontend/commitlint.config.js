// NOTE: this file is only used locally for pre-commit hooks

module.exports = {
  extends: ['@commitlint/config-conventional'],
  // We accept the default `ignores` in local environment (i.e., `fixup`)
  // but we reject them on CI/CD
  defaultIgnores: true,
  // see https://github.com/conventional-changelog/commitlint/issues/3206
};
