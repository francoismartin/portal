import React, { ReactElement } from 'react';
import { render, screen } from '@testing-library/react';
import { Description } from '@biomedit/next-widgets';
import { I18nNamespace } from '../i18n';
import { useUserRoleEntries } from './UserRoleEntries';

describe('UserRoleEntries', () => {
  it('should return an empty table if nothing is passed in', () => {
    const MockComponent = (): ReactElement => {
      const entries = useUserRoleEntries({}, I18nNamespace.COMMON);
      return <Description entries={entries} labelWidth={300} />;
    };
    render(<MockComponent />);
    expect(screen.queryAllByRole('row')).toHaveLength(0);
  });

  it('should return table with `n` rows when `n` entries are passed', async () => {
    const roles = {
      foo: [
        {
          username: 'brian',
          firstName: 'Brian',
          lastName: 'Cohen',
          email: 'brian@example.org',
        },
      ],
      bar: [
        {
          username: 'chuck',
          firstName: 'Chuck',
          lastName: 'Norris',
          email: 'chuck@example.org',
        },
      ],
      baz: [],
    };
    const MockComponent = (): ReactElement => {
      const entries = useUserRoleEntries(roles, I18nNamespace.COMMON);
      return <Description entries={entries} labelWidth={300} />;
    };

    render(<MockComponent />);
    expect(await screen.findAllByRole('row')).toHaveLength(2);
  });
});
