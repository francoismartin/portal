// eslint-disable-rule no-unused-vars
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { AnyAction } from 'redux';
import { generatedBackendApi, loginRedirect } from '../api/api';
import { StatusCodes } from 'http-status-codes';
import {
  ADD_CONTACT,
  ADD_DATA_PROVIDER,
  ADD_DATA_TRANSFER,
  ADD_FLAG,
  ADD_GROUP,
  ADD_LOCAL_USER,
  ADD_NODE,
  ADD_PROJECT,
  ARCHIVE_PROJECT,
  CALL,
  UPDATE_USER_PROFILE,
  CHECK_AUTH,
  DELETE_DATA_TRANSFER,
  DELETE_GROUP,
  DELETE_MESSAGE,
  DELETE_PROJECT,
  LOAD_DATA_PROVIDERS,
  LOAD_DATA_TRANSFERS,
  LOAD_FEED,
  LOAD_FLAGS,
  LOAD_GROUPS,
  LOAD_MESSAGES,
  LOAD_NODES,
  LOAD_PERMISSIONS,
  LOAD_PROJECTS,
  LOAD_QUICK_ACCESSES,
  LOAD_USERS,
  loginAction,
  REDIRECT,
  UNARCHIVE_PROJECT,
  UPDATE_APPROVAL,
  UPDATE_DATA_PROVIDER,
  UPDATE_DATA_TRANSFER,
  UPDATE_FLAG,
  UPDATE_GROUP,
  UPDATE_MESSAGE,
  UPDATE_NODE,
  UPDATE_PROJECT,
  UPDATE_USER,
  LOAD_PGP_KEY_INFOS,
  UPDATE_PGP_KEY_INFO,
  ADD_PGP_KEY_INFO,
  RETIRE_PGP_KEY_INFO,
} from './actionTypes';
import { redirect, takeApiFactory } from '@biomedit/next-widgets';

export function* rootSaga() {
  const takeApi = takeApiFactory(generatedBackendApi, loginRedirect);
  yield all([
    yield takeLatest(CHECK_AUTH.success, login),
    yield takeLatest(REDIRECT, redirect),
    yield takeLatest(CALL, function* ({ callback }: AnyAction) {
      yield call(callback);
    }),
    yield takeApi.latest(LOAD_PROJECTS),
    yield takeApi.latest(UPDATE_PROJECT),
    yield takeApi.latest(ADD_PROJECT),
    yield takeApi.latest(DELETE_PROJECT),
    yield takeApi.latest(ARCHIVE_PROJECT),
    yield takeApi.latest(UNARCHIVE_PROJECT),
    yield takeApi.latest(LOAD_MESSAGES),
    yield takeApi.latest(DELETE_MESSAGE),
    yield takeApi.latest(UPDATE_MESSAGE),
    yield takeApi.latest(LOAD_FEED),
    yield takeApi.latest(LOAD_USERS),
    yield takeApi.latest(CHECK_AUTH),
    yield takeApi.latest(UPDATE_USER_PROFILE, [
      StatusCodes.CONFLICT,
      StatusCodes.UNPROCESSABLE_ENTITY,
    ]),
    yield takeApi.latest(ADD_LOCAL_USER),
    yield takeApi.latest(UPDATE_USER),
    yield takeApi.latest(LOAD_NODES),
    yield takeApi.latest(ADD_NODE),
    yield takeApi.latest(UPDATE_NODE),
    yield takeApi.latest(LOAD_FLAGS),
    yield takeApi.latest(ADD_FLAG),
    yield takeApi.latest(UPDATE_FLAG),
    yield takeApi.latest(LOAD_GROUPS),
    yield takeApi.latest(ADD_GROUP),
    yield takeApi.latest(UPDATE_GROUP),
    yield takeApi.latest(DELETE_GROUP),
    yield takeApi.latest(LOAD_PERMISSIONS),
    yield takeApi.leading(LOAD_DATA_PROVIDERS),
    yield takeApi.latest(ADD_DATA_PROVIDER),
    yield takeApi.latest(UPDATE_DATA_PROVIDER),
    yield takeApi.latest(ADD_DATA_TRANSFER),
    yield takeApi.latest(DELETE_DATA_TRANSFER),
    yield takeApi.latest(UPDATE_DATA_TRANSFER),
    yield takeApi.latest(LOAD_DATA_TRANSFERS),
    yield takeApi.latest(ADD_CONTACT),
    yield takeApi.latest(LOAD_QUICK_ACCESSES),
    yield takeApi.latest(UPDATE_APPROVAL),
    yield takeApi.latest(LOAD_PGP_KEY_INFOS),
    yield takeApi.latest(ADD_PGP_KEY_INFO),
    yield takeApi.latest(UPDATE_PGP_KEY_INFO),
    yield takeApi.latest(RETIRE_PGP_KEY_INFO),
  ]);
}

function* login(res: AnyAction) {
  const { response } = res;
  if (response !== undefined) {
    yield put(loginAction(response));
  }
}
