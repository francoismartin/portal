import { declareAction, requestAction } from '@biomedit/next-widgets';
import { Project, UpdateProjectRequest, Userinfo } from '../api/generated';
import { generatedBackendApi } from '../api/api';
import { Action } from 'redux';

export const CHECK_AUTH = declareAction(
  'CHECK_AUTH' as const,
  generatedBackendApi.retrieveUserinfo
);
export const LOGIN = 'LOGIN' as const;
export type LoginAction = Action<typeof LOGIN> & Record<'response', Userinfo>;
export const loginAction = (response: Userinfo): LoginAction => ({
  type: LOGIN,
  response,
});

//---------------------------------------------------------------------

export const UPDATE_USER_PROFILE = declareAction(
  'UPDATE_USER_PROFILE' as const,
  generatedBackendApi.partialUpdateUser
);

//---------------------------------------------------------------------

export const ADD_CONTACT = declareAction(
  'ADD_CONTACT' as const,
  generatedBackendApi.createContact
);

//---------------------------------------------------------------------

export const ADD_LOCAL_USER = declareAction(
  'ADD_LOCAL_USER' as const,
  generatedBackendApi.createUser
);
export const LOAD_USERS = declareAction(
  'LOAD_USERS' as const,
  generatedBackendApi.listUsers
);
export const UPDATE_USER = declareAction(
  'UPDATE_USER' as const,
  generatedBackendApi.partialUpdateUser
);

//---------------------------------------------------------------------

export const LOAD_PROJECTS = declareAction(
  'LOAD_PROJECTS' as const,
  generatedBackendApi.listProjects
);
export const CLEAR_PROJECTS = 'CLEAR_PROJECTS' as const;
export type ClearProjects = Action<typeof CLEAR_PROJECTS>;
export const clearProjectsAction: ClearProjects = { type: CLEAR_PROJECTS };
export const UPDATE_PROJECT = declareAction(
  'UPDATE_PROJECT' as const,
  generatedBackendApi.updateProject
);

export const updateProjectAction = <OnSuccessAction extends Action>(
  project: Project,
  onSuccess?: OnSuccessAction
): Action<typeof UPDATE_PROJECT.request> & UpdateProjectRequest =>
  requestAction(UPDATE_PROJECT, { id: String(project.id), project }, onSuccess);

export const ADD_PROJECT = declareAction(
  'ADD_PROJECT' as const,
  generatedBackendApi.createProject
);
export const DELETE_PROJECT = declareAction(
  'DELETE_PROJECT' as const,
  generatedBackendApi.destroyProject
);
export const ARCHIVE_PROJECT = declareAction(
  'ARCHIVE_PROJECT' as const,
  generatedBackendApi.archiveProject
);
export const UNARCHIVE_PROJECT = declareAction(
  'UNARCHIVE_PROJECT' as const,
  generatedBackendApi.unarchiveProject
);

//---------------------------------------------------------------------

export const LOAD_MESSAGES = declareAction(
  'LOAD_MESSAGES' as const,
  generatedBackendApi.listMessages
);

export const UPDATE_MESSAGE = declareAction(
  'UPDATE_MESSAGE' as const,
  generatedBackendApi.partialUpdateMessage
);

export const DELETE_MESSAGE = declareAction(
  'DELETE_MESSAGE' as const,
  generatedBackendApi.destroyMessage
);

//---------------------------------------------------------------------

export const LOAD_FEED = declareAction(
  'LOAD_FEED' as const,
  generatedBackendApi.listFeeds
);

//---------------------------------------------------------------------
export const LOAD_NODES = declareAction(
  'LOAD_NODES' as const,
  generatedBackendApi.listNodes
);
export const ADD_NODE = declareAction(
  'ADD_NODE' as const,
  generatedBackendApi.createNode
);
export const UPDATE_NODE = declareAction(
  'UPDATE_NODE' as const,
  generatedBackendApi.updateNode
);

//---------------------------------------------------------------------
export const LOAD_FLAGS = declareAction(
  'LOAD_FLAGS' as const,
  generatedBackendApi.listFlags
);
export const ADD_FLAG = declareAction(
  'ADD_FLAG' as const,
  generatedBackendApi.createFlag
);
export const UPDATE_FLAG = declareAction(
  'UPDATE_FLAG' as const,
  generatedBackendApi.updateFlag
);

//---------------------------------------------------------------------
export const LOAD_GROUPS = declareAction(
  'LOAD_GROUPS' as const,
  generatedBackendApi.listGroups
);
export const ADD_GROUP = declareAction(
  'ADD_GROUP' as const,
  generatedBackendApi.createGroup
);
export const UPDATE_GROUP = declareAction(
  'UPDATE_GROUP' as const,
  generatedBackendApi.updateGroup
);
export const DELETE_GROUP = declareAction(
  'DELETE_GROUP' as const,
  generatedBackendApi.destroyGroup
);

//---------------------------------------------------------------------
export const LOAD_PERMISSIONS = declareAction(
  'LOAD_PERMISSIONS' as const,
  generatedBackendApi.listPermissions
);

//---------------------------------------------------------------------
export const LOAD_DATA_PROVIDERS = declareAction(
  'LOAD_DATA_PROVIDERS' as const,
  generatedBackendApi.listDataProviders
);
export const ADD_DATA_PROVIDER = declareAction(
  'ADD_DATA_PROVIDER' as const,
  generatedBackendApi.createDataProvider
);
export const UPDATE_DATA_PROVIDER = declareAction(
  'UPDATE_DATA_PROVIDER' as const,
  generatedBackendApi.updateDataProvider
);

//---------------------------------------------------------------------
export const LOAD_DATA_TRANSFERS = declareAction(
  'LOAD_DATA_TRANSFERS' as const,
  generatedBackendApi.listDataTransfers
);
export const ADD_DATA_TRANSFER = declareAction(
  'ADD_DATA_TRANSFER' as const,
  generatedBackendApi.createDataTransfer
);
export const DELETE_DATA_TRANSFER = declareAction(
  'DELETE_DATA_TRANSFER' as const,
  generatedBackendApi.destroyDataTransfer
);
export const UPDATE_DATA_TRANSFER = declareAction(
  'UPDATE_DATA_TRANSFER' as const,
  generatedBackendApi.updateDataTransfer
);

//---------------------------------------------------------------------
export const LOAD_QUICK_ACCESSES = declareAction(
  'LOAD_QUICK_ACCESSES' as const,
  generatedBackendApi.listQuickAccessTiles
);

//---------------------------------------------------------------------
export const UPDATE_APPROVAL = declareAction(
  'UPDATE_APPROVAL' as const,
  generatedBackendApi.partialUpdateApproval
);

//---------------------------------------------------------------------
export const LOAD_PGP_KEY_INFOS = declareAction(
  'LOAD_PGP_KEY_INFOS' as const,
  generatedBackendApi.listPgpKeyInfos
);
export const ADD_PGP_KEY_INFO = declareAction(
  'ADD_PGP_KEY_INFO' as const,
  generatedBackendApi.createPgpKeyInfo
);
export const UPDATE_PGP_KEY_INFO = declareAction(
  'UPDATE_PGP_KEY_INFO' as const,
  generatedBackendApi.partialUpdatePgpKeyInfo
);
export const RETIRE_PGP_KEY_INFO = declareAction(
  'RETIRE_PGP_KEY_INFO' as const,
  generatedBackendApi.retirePgpKeyInfo
);
export const CLEAR_PGP_KEY_INFOS = 'CLEAR_PGP_KEY_INFOS' as const;
export type ClearPgpKeyInfos = Action<typeof CLEAR_PGP_KEY_INFOS>;

//---------------------------------------------------------------------
export const REDIRECT = 'REDIRECT' as const;
export const CALL = 'CALL' as const;
