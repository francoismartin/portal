export type IdRequired<T> = Required<T, 'id'>;

/**
 * EnumValuesToUnion
 * @desc Takes the values from an enum `E` and turns them into a union type.
 *       To get a union of all the keys of an enum, use `typeof keyof E` instead.
 * @example
 *    enum Color {
 *      RED = 'red',
 *      GREEN = 'green',
 *      BLUE = 'blue',
 *    };
 *
 *    // Expect: type ColorValuesUnion = 'red' | 'green' | 'blue';
 *    type ColorValuesUnion = EnumValuesToUnion<Color>;
 */
export type EnumValuesToUnion<E> = `${E}`;
