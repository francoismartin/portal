export type PagePermission = Record<string, boolean>;

export interface PagePermissions extends Record<string, PagePermission> {
  project: ProjectPermissions;
  dataTransfer: DataTransferPermissions;
  userManage: UserManagePermissions;
}

export interface ProjectPermissions extends PagePermission {
  del: boolean;
  add: boolean;
  edit: boolean;
}

export interface DataTransferPermissions extends PagePermission {
  dataManager: boolean;
  projectLeader: boolean;
  staff: boolean; // Portal Admins
  dataProviderAdmin: boolean;
  dataProviderViewer: boolean;
  nodeViewer: boolean;
  nodeAdmin: boolean;
}

export interface UserManagePermissions extends PagePermission {
  staff: boolean; // Portal Admins
  nodeAdmin: boolean;
  nodeViewer: boolean;
  dataProviderAdmin: boolean;
  dataProviderViewer: boolean;
  groupManager: boolean;
}

export const noProjectPermissions: ProjectPermissions = {
  del: false,
  add: false,
  edit: false,
};
export const noDataTransferPermissions: DataTransferPermissions = {
  dataManager: false,
  projectLeader: false,
  staff: false,
  dataProviderAdmin: false,
  dataProviderViewer: false,
  nodeViewer: false,
  nodeAdmin: false,
};
export const noUserManagePermissions: UserManagePermissions = {
  staff: false,
  nodeAdmin: false,
  nodeViewer: false,
  dataProviderAdmin: false,
  dataProviderViewer: false,
  groupManager: false,
};
export const noPermissions: PagePermissions = {
  project: noProjectPermissions,
  dataTransfer: noDataTransferPermissions,
  userManage: noUserManagePermissions,
};

export function any(pagePermission: PagePermission): boolean {
  if (!pagePermission) {
    return false;
  }
  const permissions: boolean[] = Object.values(pagePermission);
  return permissions.some((permission) => permission);
}

// TODO: merge with generated api model ProjectUsersRolesEnum
/* eslint-disable typescript-sort-keys/string-enum */
export enum userRoles {
  PL = 'Project Leader',
  PM = 'Permissions Manager',
  DM = 'Data Manager',
  USER = 'User',
}
