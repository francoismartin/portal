import React, { ReactElement } from 'react';
import { version } from '../config';
import { Typography } from '@mui/material';

export const Version = (): ReactElement => (
  <Typography
    variant="body2"
    title="Portal Version"
    sx={{
      textAlign: 'center',
      color: 'primary.contrastText',
    }}
  >
    {version}
  </Typography>
);
