import React, { ReactElement, useEffect } from 'react';
import { DeepRequired } from 'utility-types';
import { Group, User } from '../../api/generated';
import { FormProvider } from 'react-hook-form';
import {
  AutocompleteField,
  ConfirmLabel,
  FormDialog,
  HiddenField,
  LabelledField,
  nameValidations,
  requestAction,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { batch, useDispatch, useSelector } from 'react-redux';
import {
  ADD_GROUP,
  CALL,
  LOAD_PERMISSIONS,
  LOAD_USERS,
  UPDATE_GROUP,
} from '../../actions/actionTypes';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { usePermissionChoices, useUserChoices } from '../choice';
import { PermissionsObjectList } from './PermissionsObjectList';
import { generatedBackendApi } from '../../api/api';
import { isStaff } from '../selectors';
import { FormFieldsContainer } from '../FormFieldsContainer';

type GroupFormProps = {
  group: Partial<Group>;
  onClose: () => void;
};

export const GroupForm = ({ group, onClose }: GroupFormProps): ReactElement => {
  const dispatch = useDispatch();
  const { t } = useTranslation([
    I18nNamespace.GROUP_LIST,
    I18nNamespace.GROUP_MANAGE_FORM,
  ]);

  const isEdit = !!group.id;
  const isFetchingUsers = useSelector((state) => state.users.isFetching);
  const staff = useSelector(isStaff);
  const isFetchingPermissions = useSelector(
    (state) => state.permissions.isFetching
  );
  const isSubmitting = useSelector((state) => state.groups.isSubmitting);

  const users = useSelector(
    (state) => state.users.itemList
  ) as DeepRequired<User>[];
  const permissions = useSelector((state) => state.permissions.itemList);
  const permissionChoices = usePermissionChoices(permissions);

  const form = useEnhancedForm<Group>({ defaultValues: group });

  function submit(activeGroup: Group) {
    const onSuccessAction = { type: CALL, callback: onClose };
    if (isEdit) {
      dispatch(
        requestAction(
          UPDATE_GROUP,
          {
            id: String(activeGroup.id),
            group: activeGroup,
          },
          onSuccessAction
        )
      );
    } else {
      dispatch(
        requestAction(ADD_GROUP, { group: activeGroup }, onSuccessAction)
      );
    }
  }

  useEffect(() => {
    batch(() => {
      dispatch(requestAction(LOAD_USERS));
      dispatch(requestAction(LOAD_PERMISSIONS));
    });
  }, [dispatch]);

  const uniqueCheck = async (value) =>
    generatedBackendApi.uniqueGroup({ uniqueGroup: value });

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={t(
          `${I18nNamespace.GROUP_MANAGE_FORM}:title.${
            isEdit ? 'edit' : 'create'
          }`
        )}
        open={!!group}
        confirmLabel={ConfirmLabel.SAVE}
        onSubmit={submit}
        onClose={onClose}
        isSubmitting={isSubmitting}
      >
        <HiddenField name="id" initialValues={{ id: group.id }} />
        <FormFieldsContainer>
          <LabelledField
            name="name"
            type="text"
            label={t(`${I18nNamespace.GROUP_MANAGE_FORM}:captions.name`)}
            validations={nameValidations()}
            unique={uniqueCheck}
            initialValues={group}
            fullWidth
            disabled={!staff}
          />
          <LabelledField
            name="profile.description"
            type="text"
            label={t(`${I18nNamespace.GROUP_MANAGE_FORM}:captions.description`)}
            initialValues={group}
            fullWidth
            disabled={!staff}
            multiline
            rows={3}
            sx={{ marginBottom: 5 }}
          />
          <AutocompleteField
            name="users"
            label={t(`${I18nNamespace.GROUP_MANAGE_FORM}:captions.users`)}
            initialValues={group}
            choices={useUserChoices(users)}
            isLoading={isFetchingUsers}
            multiple
            width="100%"
          />
          <AutocompleteField
            name="permissions"
            label={t(
              `${I18nNamespace.GROUP_MANAGE_FORM}:captions.modelPermissions`
            )}
            initialValues={group}
            choices={permissionChoices}
            isLoading={isFetchingPermissions}
            multiple
            width="100%"
            disabled={!staff}
          />
        </FormFieldsContainer>
        <h4>
          {t(`${I18nNamespace.GROUP_MANAGE_FORM}:captions.objectPermissions`)}
        </h4>
        <PermissionsObjectList />
      </FormDialog>
    </FormProvider>
  );
};
