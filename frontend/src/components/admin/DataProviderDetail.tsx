import React, { ReactElement, useEffect, useMemo, useState } from 'react';
import {
  CancelLabel,
  Description,
  Dialog,
  Field,
  formatItemFields,
  FormattedItemField,
  ListModel,
} from '@biomedit/next-widgets';
import { useTranslation } from 'next-i18next';
import { CircularProgress } from '@mui/material';
import { DataProvider, DataProviderRole } from '../../api/generated';
import { I18nNamespace } from '../../i18n';
import { generatedBackendApi } from '../../api/api';
import {
  RoleMap,
  UserType,
  useUserRoleEntries,
} from '../../widgets/UserRoleEntries';
import { getDataProviderColoredStatus } from './AdministrationHooks';

export type DataProviderDetailProps = {
  dp: DataProvider;
  open: boolean;
  onClose: () => void;
};

type DataProviderType = keyof DataProviderRole;

export const DataProviderDetail = ({
  dp,
  open,
  onClose,
}: DataProviderDetailProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.DATA_PROVIDER]);
  const [roles, setRoles] = useState<RoleMap>({});
  const [isFetchingRoles, setIsFetchingRoles] = useState<boolean>(false);

  useEffect(() => {
    if (dp?.id) {
      setIsFetchingRoles(true);
      generatedBackendApi
        .rolesDataProviderRole({ id: dp?.id.toString() })
        .then((result) => {
          const roleDict: Record<DataProviderType, UserType[]> = {
            dataEngineer: result.dataEngineer || [],
            coordinator: result.coordinator || [],
            manager: result.manager || [],
            securityOfficer: result.securityOfficer || [],
            technicalAdmin: result.technicalAdmin || [],
            viewer: result.viewer || [],
          };

          setRoles(roleDict);
        })
        .finally(() => {
          setIsFetchingRoles(false);
        });
    }
  }, [dp]);

  const detailEntries: FormattedItemField[] | null = useMemo(() => {
    const model: ListModel<DataProvider> = {
      fields: [
        Field({
          caption: t(`${I18nNamespace.DATA_PROVIDER}:columns.code`),
          getProperty: (dp: DataProvider) => dp.code,
          key: 'code',
        }),
        Field({
          caption: t(`${I18nNamespace.DATA_PROVIDER}:columns.name`),
          getProperty: (dp: DataProvider) => dp.name,
          key: 'name',
        }),
        Field({
          caption: t(`${I18nNamespace.DATA_PROVIDER}:columns.node`),
          getProperty: (dp: DataProvider) => dp.node,
          key: 'node',
        }),
        Field({
          caption: t(`${I18nNamespace.DATA_PROVIDER}:columns.enabled`),
          getProperty: (dp: DataProvider) =>
            getDataProviderColoredStatus(dp.enabled),
          key: 'enabled',
        }),
      ],
    };
    return formatItemFields<DataProvider>(model, dp);
  }, [t, dp]);

  const userRoleEntries = useUserRoleEntries(
    roles,
    I18nNamespace.DATA_PROVIDER
  );

  return (
    <Dialog
      open={open}
      isSubmitting={false}
      fullWidth={true}
      maxWidth={'lg'}
      title={dp.name}
      cancelLabel={CancelLabel.CLOSE}
      onClose={onClose}
    >
      <Description entries={detailEntries} labelWidth={300} />
      {isFetchingRoles ? (
        <CircularProgress />
      ) : (
        <Description entries={userRoleEntries} labelWidth={300} />
      )}
    </Dialog>
  );
};
