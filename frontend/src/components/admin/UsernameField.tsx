import React, { ReactElement, useMemo } from 'react';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { Tooltip } from '@biomedit/next-widgets';
import { Typography } from '@mui/material';
import { affiliationFieldCaption, flagFieldCaption } from '../nav/UserInfoBox';

export function UsernameField(
  username: string,
  affiliation: string | undefined,
  flags: Array<string> | undefined
): ReactElement {
  const { t } = useTranslation(I18nNamespace.USER_INFO);
  const name = useMemo(() => username, [username]);
  const userAffiliation = useMemo(
    () => (affiliation || '-').replace(',', ', '),
    [affiliation]
  );
  const userFlags = useMemo(
    () => (flags && flags.length ? flags.join(', ') : '-'),
    [flags]
  );
  return (
    <Tooltip
      title={
        <>
          <Typography key="affiliations" variant="caption" display="block">
            {t(affiliationFieldCaption)}: {userAffiliation}
          </Typography>
          <Typography key="flags" variant="caption" display="block">
            {t(flagFieldCaption)}: {userFlags}
          </Typography>
        </>
      }
    >
      <span>{name}</span>
    </Tooltip>
  );
}
