import { configure, render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { getInitialState, makeStore } from '../../store';
import React from 'react';
import staffState from '../../../test-data/staffState.json';
import { Group } from '../../api/generated';
import {
  expectQueryParameter,
  MockServer,
  RequestVerifier,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { rest } from 'msw';
import { backend } from '../../api/api';
import listPermissionsResponse from '../../../test-data/listPermissionsResponse.json';
import listObjectByPermissionResponse from '../../../test-data/listObjectByPermissionResponse.json';
import listUsersResponse from '../../../test-data/listUsersResponse.json';
import { GroupForm } from './GroupManageForm';

describe('GroupManageForm', () => {
  const listObjectByPermissionParam = 'perm_id';
  const viewNodePermissionId = 44;
  const listPermissions = `${backend}identity/permission/`;
  const listUsers = `${backend}users/`;
  let listObjectByPermission;

  const group: Group = {
    id: 1,
    name: 'Node 1 Node Viewer',
    profile: {
      description: 'hello',
    },
    users: [1, 5],
    permissions: [],
    permissionsObject: [
      {
        permission: viewNodePermissionId,
        objects: [1],
      },
    ],
  };

  const server: MockServer = setupMockApi();

  const createServer = (
    delayListUsers: number,
    delayListPerm: number,
    delayListObjectByPerm: number
  ): void => {
    listObjectByPermission = `${backend}identity/object_by_permission/`;

    server.use(
      rest.get(listUsers, (req, res, ctx) =>
        res(
          ctx.delay(delayListUsers),
          resJson(ctx),
          resBody(ctx, listUsersResponse)
        )
      ),
      rest.get(listPermissions, (req, res, ctx) =>
        res(
          ctx.delay(delayListPerm),
          resJson(ctx),
          resBody(ctx, listPermissionsResponse)
        )
      ),
      rest.get(listObjectByPermission, (req, res, ctx) => {
        expectQueryParameter(
          req,
          listObjectByPermissionParam,
          String(viewNodePermissionId)
        );
        return res(
          ctx.delay(delayListObjectByPerm),
          resJson(ctx),
          resBody(ctx, listObjectByPermissionResponse)
        );
      })
    );

    // msw only needs the path
    listObjectByPermission += `?${listObjectByPermissionParam}=${viewNodePermissionId}`;
  };

  beforeAll(() => {
    configure({
      asyncUtilTimeout: 10000,
    });
  });

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  it.each`
    delayListUsers | delayListPerm | delayListObjectByPerm
    ${0}           | ${0}          | ${0}
    ${500}         | ${500}        | ${500}
    ${2000}        | ${2000}       | ${2000}
    ${2000}        | ${0}          | ${0}
    ${0}           | ${2000}       | ${0}
    ${0}           | ${0}          | ${2000}
    ${2000}        | ${2000}       | ${0}
    ${0}           | ${2000}       | ${2000}
    ${2000}        | ${0}          | ${2000}
  `(
    'should contain both permission and object when ' +
      'call to list users is delayed by $delayListUsers ms and ' +
      'call to list permissions is delayed by $delayListPerm ms and ' +
      'call to list object by permission is delayed by $delayListObjectByPerm ms',
    async ({ delayListUsers, delayListPerm, delayListObjectByPerm }) => {
      createServer(delayListUsers, delayListPerm, delayListObjectByPerm);
      const verifier = new RequestVerifier(server);

      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            ...staffState,
          })}
        >
          <GroupForm group={group} onClose={jest.fn()} />
        </Provider>
      );

      // wait for all API calls to be finished (no more spinners are visible, text-/combobox all visible)
      const comboboxes = await waitFor(
        () => {
          const boxes = screen.getAllByRole('combobox');
          expect(boxes).toHaveLength(4);
          expect(screen.getAllByRole('textbox')).toHaveLength(2);
          return boxes;
        },
        { timeout: 5000 }
      );

      expect(comboboxes[2]).toHaveValue('Can view node');
      await screen.findByText('Node 1 (node_1)');

      verifier.assertCount(3);
      verifier.assertCalled(listUsers, 1);
      verifier.assertCalled(listPermissions, 1);
      verifier.assertCalled(listObjectByPermission, 1);
    }
  );
});
