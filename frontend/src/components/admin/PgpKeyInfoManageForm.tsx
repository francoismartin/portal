import React, { ReactElement, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormProvider } from 'react-hook-form';
import { PgpKeyInfo, PgpKeyInfoStatusEnum } from '../../api/generated';
import { CALL, UPDATE_PGP_KEY_INFO } from '../../actions/actionTypes';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import {
  AnyObject,
  ConfirmLabel,
  FormDialog,
  HiddenField,
  requestAction,
  SelectField,
  useEnhancedForm,
  useEnumChoices,
} from '@biomedit/next-widgets';
import { FormFieldsContainer } from '../FormFieldsContainer';

enum PendingPgpKeyInfoStatusEnum {
  APPROVED = PgpKeyInfoStatusEnum.APPROVED,
  REJECTED = PgpKeyInfoStatusEnum.REJECTED,
}

enum ApprovedPgpKeyInfoStatusEnum {
  APPROVAL_REVOKED = PgpKeyInfoStatusEnum.APPROVAL_REVOKED,
}

export const allowedPgpKeyInfoStatusEnumSubset = (
  currentStatus: PgpKeyInfoStatusEnum
) => {
  switch (currentStatus) {
    case PgpKeyInfoStatusEnum.PENDING:
      return PendingPgpKeyInfoStatusEnum;
    case PgpKeyInfoStatusEnum.APPROVED:
      return ApprovedPgpKeyInfoStatusEnum;
    default:
      return undefined;
  }
};

type PgpKeyInfoManageFormProps = {
  pgpKeyInfo: Partial<PgpKeyInfo>;
  onClose: () => void;
};

export const PgpKeyInfoManageForm = ({
  pgpKeyInfo,
  onClose,
}: PgpKeyInfoManageFormProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.PGP_KEY_INFO_LIST]);
  const isSubmitting = useSelector((state) => state.pgpKeyInfo.isSubmitting);
  const dispatch = useDispatch();
  const form = useEnhancedForm<PgpKeyInfo>();
  const pgpKeyInfoStatusEnumSubset = useMemo(
    () =>
      pgpKeyInfo.status
        ? allowedPgpKeyInfoStatusEnumSubset(pgpKeyInfo.status)
        : [],
    [pgpKeyInfo.status]
  );

  function submit(formPgpKeyInfo: PgpKeyInfo) {
    const onSuccessAction = {
      type: CALL,
      callback: onClose,
    };
    dispatch(
      requestAction(
        UPDATE_PGP_KEY_INFO,
        {
          id: String(pgpKeyInfo.id),
          pgpKeyInfo: formPgpKeyInfo,
        },
        onSuccessAction
      )
    );
  }

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'md'}
        title={`Key: ${pgpKeyInfo?.fingerprint}`}
        open={!!pgpKeyInfo}
        onSubmit={submit}
        onClose={onClose}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
      >
        <FormFieldsContainer>
          <HiddenField name="id" initialValues={{ id: pgpKeyInfo.id }} />
          <SelectField
            name="status"
            label={t(`${I18nNamespace.PGP_KEY_INFO_LIST}:columns.status`)}
            required
            choices={useEnumChoices(pgpKeyInfoStatusEnumSubset as AnyObject)}
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
