import React, { useMemo } from 'react';
import { Column } from 'react-table';
import {
  DataProvider,
  Flag,
  Group,
  Node,
  NodeNodeStatusEnum,
  PgpKeyInfo,
  PgpKeyInfoStatusEnum,
  User,
} from '../../api/generated';
import { DataProviderForm } from './DataProviderManageForm';
import { NodeForm } from './NodeManageForm';
import { FlagManageForm } from './FlagManageForm';
import truncate from 'lodash/truncate';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { GroupForm } from './GroupManageForm';
import { Required } from 'utility-types';
import {
  ColoredStatus,
  formatDate,
  Status,
  useFormDialog,
} from '@biomedit/next-widgets';
import { PgpKeyInfoManageForm } from './PgpKeyInfoManageForm';
import { UsernameField } from './UsernameField';

export const useUserColumns = (): Array<Column<Required<User>>> => {
  const { t } = useTranslation([
    I18nNamespace.USER_LIST,
    I18nNamespace.BOOLEAN,
  ]);
  return useMemo(() => {
    const statuses: Status<boolean>[] = [
      {
        color: 'success',
        value: true,
        text: t<string>(`${I18nNamespace.BOOLEAN}:true`),
      },
      {
        color: 'error',
        value: false,
        text: t<string>(`${I18nNamespace.BOOLEAN}:false`),
      },
    ];
    return [
      {
        id: 'firstName',
        Header: t<string>('columns.firstName'),
        accessor: 'firstName',
        sortType: 'caseInsensitive',
      },
      {
        id: 'lastName',
        Header: t<string>('columns.lastName'),
        accessor: 'lastName',
        sortType: 'caseInsensitive',
      },
      {
        id: 'email',
        Header: t<string>('columns.email'),
        accessor: 'email',
        sortType: 'caseInsensitive',
      },
      {
        id: 'username',
        Header: t<string>('columns.username'),
        accessor: (row: User) => [
          row.username,
          row.profile?.affiliation,
          row.flags,
        ],
        Cell: (row) => UsernameField(row.value[0], row.value[1], row.value[2]),
        sortType: 'caseInsensitive',
      },
      {
        id: 'lastLogin',
        Header: t<string>('columns.lastLogin'),
        accessor: 'lastLogin',
        sortType: 'datetime',
        Cell: (row) => formatDate(row.value, true) ?? '',
      },
      {
        id: 'isActive',
        Header: t<string>('columns.isActive'),
        accessor: (value) => {
          return t<string>(`${I18nNamespace.BOOLEAN}:${value.isActive}`);
        },
        sortType: 'caseInsensitive',
        Cell: (row) => {
          return ColoredStatus<boolean>({
            value: row.row.original.isActive,
            statuses,
          });
        },
      },
    ];
  }, [t]);
};

export const useFlagColumns = (): Array<Column<Required<Flag>>> => {
  const { t } = useTranslation(I18nNamespace.FLAG_LIST);
  return useMemo(() => {
    return [
      {
        id: 'code',
        Header: t<string>('columns.code'),
        accessor: 'code',
      },
      {
        id: 'description',
        Header: t<string>('columns.description'),
        accessor: 'description',
        Cell: (row) =>
          truncate(row.value, {
            length: 100,
            separator: ' ',
          }),
      },
      {
        id: 'users',
        Header: t<string>('columns.users'),
        accessor: 'users',
        Cell: (row) => row.value.length,
      },
    ];
  }, [t]);
};

export const useGroupColumns = (): Array<Column<Required<Group>>> => {
  const { t } = useTranslation(I18nNamespace.GROUP_LIST);
  return useMemo(() => {
    return [
      {
        id: 'name',
        Header: t<string>('columns.name'),
        accessor: 'name',
        sortType: 'caseInsensitive',
      },
      {
        id: 'users',
        Header: t<string>('columns.users'),
        accessor: (row) => row.users.length,
      },
    ];
  }, [t]);
};

export const useNodeColumns = (): Array<Column<Required<Node>>> => {
  const { t } = useTranslation(I18nNamespace.NODE);
  return useMemo(() => {
    const statuses: Status<NodeNodeStatusEnum>[] = [
      {
        color: 'success',
        value: NodeNodeStatusEnum.ON,
      },
      {
        color: 'error',
        value: NodeNodeStatusEnum.OFF,
      },
    ];
    return [
      {
        id: 'code',
        Header: t<string>('columns.code'),
        accessor: 'code',
      },
      {
        id: 'name',
        Header: t<string>('columns.name'),
        accessor: 'name',
        sortType: 'caseInsensitive',
      },
      {
        id: 'nodeStatus',
        Header: t<string>('columns.nodeStatus'),
        accessor: 'nodeStatus',
        Cell: (row) =>
          ColoredStatus<NodeNodeStatusEnum>({ value: row.value, statuses }),
      },
    ];
  }, [t]);
};

enum DataProviderStatusEnum {
  DISABLED = 'DISABLED',
  ENABLED = 'ENABLED',
}

export function getDataProviderColoredStatus(enabled: boolean | undefined) {
  const statuses: Status<DataProviderStatusEnum>[] = [
    {
      color: 'success',
      value: DataProviderStatusEnum.ENABLED,
    },
    {
      color: 'error',
      value: DataProviderStatusEnum.DISABLED,
    },
  ];
  return ColoredStatus<DataProviderStatusEnum>({
    value: enabled
      ? DataProviderStatusEnum.ENABLED
      : DataProviderStatusEnum.DISABLED,
    statuses,
  });
}

export const useDataProviderColumns = (): Array<
  Column<Required<DataProvider>>
> => {
  const { t } = useTranslation(I18nNamespace.DATA_PROVIDER);
  return useMemo(() => {
    return [
      {
        id: 'code',
        Header: t<string>('columns.code'),
        accessor: 'code',
      },
      {
        id: 'name',
        Header: t<string>('columns.name'),
        accessor: 'name',
        sortType: 'caseInsensitive',
      },
      {
        id: 'node',
        Header: t<string>('columns.node'),
        accessor: 'node',
      },
      {
        id: 'enabled',
        Header: t<string>('columns.enabled'),
        accessor: (row) => +!!row.enabled,
        Cell: (row) => getDataProviderColoredStatus(row.value),
      },
    ];
  }, [t]);
};

export const useFlagForm = () => {
  const newFlag = {
    code: undefined,
    description: undefined,
    users: [],
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<Flag>>(newFlag);
  const flagForm = data && (
    <FlagManageForm flag={data} onClose={closeFormDialog} />
  );
  return {
    flagForm,
    ...rest,
  };
};

export function useNodeForm() {
  const newNode = {
    code: undefined,
    name: undefined,
    nodeStatus: NodeNodeStatusEnum.ON,
    users: [],
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<Node>>(newNode);
  const nodeForm = data && <NodeForm node={data} onClose={closeFormDialog} />;
  return {
    nodeForm,
    ...rest,
  };
}

export function useDataProviderForm() {
  const newDataProvider = {
    code: undefined,
    name: undefined,
    description: '',
    enabled: false,
    node: '',
    users: [],
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<DataProvider>>(newDataProvider);
  const dataProviderForm = data && (
    <DataProviderForm dataProvider={data} onClose={closeFormDialog} />
  );
  return {
    dataProviderForm,
    ...rest,
  };
}

export function useGroupForm() {
  const newGroup = {
    name: undefined,
    users: [],
    permissions: [],
    permissionsObject: [],
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<Group>>(newGroup);
  const groupForm = data && (
    <GroupForm group={data} onClose={closeFormDialog} />
  );
  return {
    groupForm,
    ...rest,
  };
}

export const usePgpKeyInfoForm = () => {
  const newPgpKeyInfo = {
    id: undefined,
    status: PgpKeyInfoStatusEnum.PENDING,
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<PgpKeyInfo>>(newPgpKeyInfo);
  const pgpKeyInfoForm = data && (
    <PgpKeyInfoManageForm pgpKeyInfo={data} onClose={closeFormDialog} />
  );
  return {
    pgpKeyInfoForm,
    ...rest,
  };
};
