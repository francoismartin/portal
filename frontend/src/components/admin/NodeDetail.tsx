import React, { ReactElement, useEffect, useMemo, useState } from 'react';
import {
  CancelLabel,
  Description,
  Dialog,
  Field,
  formatItemFields,
  FormattedItemField,
  ListModel,
} from '@biomedit/next-widgets';
import { useTranslation } from 'next-i18next';
import { CircularProgress } from '@mui/material';
import { Node, NodeRole } from '../../api/generated';
import { I18nNamespace } from '../../i18n';
import { generatedBackendApi } from '../../api/api';
import {
  RoleMap,
  UserType,
  useUserRoleEntries,
} from '../../widgets/UserRoleEntries';

export type NodeDetailProps = {
  node: Node;
  open: boolean;
  onClose: () => void;
};

type NodeRoleType = keyof NodeRole;

export const NodeDetail = ({
  node,
  open,
  onClose,
}: NodeDetailProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.NODE]);
  const [roles, setRoles] = useState<RoleMap>({});
  const [isFetchingRoles, setIsFetchingRoles] = useState<boolean>(false);

  useEffect(() => {
    if (node?.id) {
      setIsFetchingRoles(true);
      generatedBackendApi
        .rolesNode({ id: node?.id.toString() })
        .then((result) => {
          const roleDict: Record<NodeRoleType, UserType[]> = {
            manager: result.manager || [],
            admin: result.admin || [],
            viewer: result.viewer || [],
          };

          setRoles(roleDict);
        })
        .finally(() => {
          setIsFetchingRoles(false);
        });
    }
  }, [node]);

  const detailEntries: FormattedItemField[] | null = useMemo(() => {
    const model: ListModel<Node> = {
      fields: [
        Field({
          caption: t(`${I18nNamespace.NODE}:columns.code`),
          getProperty: (node: Node) => node.code,
          key: 'code',
        }),
        Field({
          caption: t(`${I18nNamespace.NODE}:columns.name`),
          getProperty: (node: Node) => node.name,
          key: 'name',
        }),
        Field({
          caption: t(`${I18nNamespace.NODE}:columns.nodeStatus`),
          getProperty: (node: Node) => node.nodeStatus,
          key: 'nodeStatus',
        }),
        Field({
          caption: t(`${I18nNamespace.NODE}:columns.ticketingSystemEmail`),
          getProperty: (node: Node) => node.ticketingSystemEmail,
          key: 'ticketingSystemEmail',
        }),
      ],
    };
    return formatItemFields<Node>(model, node);
  }, [t, node]);

  const userRoleEntries = useUserRoleEntries(roles, I18nNamespace.NODE);

  return (
    <Dialog
      open={open}
      isSubmitting={false}
      fullWidth={true}
      maxWidth={'lg'}
      title={node.name}
      cancelLabel={CancelLabel.CLOSE}
      onClose={onClose}
    >
      <Description entries={detailEntries} labelWidth={300} />
      {isFetchingRoles ? (
        <CircularProgress />
      ) : (
        <Description entries={userRoleEntries} labelWidth={300} />
      )}
    </Dialog>
  );
};
