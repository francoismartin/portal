import React, { ReactElement, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DELETE_GROUP, LOAD_GROUPS } from '../../actions/actionTypes';
import { Group } from '../../api/generated';
import { IdRequired } from '../../global';
import { useGroupColumns, useGroupForm } from './AdministrationHooks';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { isGroupManager, isStaff } from '../selectors';
import { EnhancedTable, requestAction } from '@biomedit/next-widgets';

export const GroupList = (): ReactElement => {
  const { t } = useTranslation([I18nNamespace.GROUP_LIST, I18nNamespace.LIST]);

  const isFetching = useSelector((state) => state.groups.isFetching);
  const isSubmitting = useSelector((state) => state.groups.isSubmitting);
  const groups = useSelector(
    (state) => state.groups.itemList as IdRequired<Group>
  );
  const groupManager = useSelector(isGroupManager);
  const staff = useSelector(isStaff);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_GROUPS));
  }, [dispatch]);

  const deleteItem = useCallback(
    (id) => {
      dispatch(requestAction(DELETE_GROUP, { id }));
    },
    [dispatch]
  );

  const { openFormDialog, groupForm } = useGroupForm();

  const getDeleteConfirmationText = useCallback(
    (item: Required<Group>) => 'Delete ' + item.name,
    []
  );

  const columns = useGroupColumns();
  return (
    <EnhancedTable<Required<Group>>
      itemList={groups}
      columns={columns}
      canAdd={staff}
      canEdit={staff || groupManager}
      canDelete={staff}
      onAdd={openFormDialog}
      onEdit={openFormDialog}
      onDelete={deleteItem}
      form={groupForm}
      isFetching={isFetching}
      getDeleteConfirmationText={getDeleteConfirmationText}
      isSubmitting={isSubmitting}
      emptyMessage={t(`${I18nNamespace.LIST}:emptyMessage`, {
        model: t(`${I18nNamespace.COMMON}:models.group_plural`),
      })}
      addButtonLabel={t(`${I18nNamespace.GROUP_LIST}:addButton`)}
      inline
    />
  );
};
