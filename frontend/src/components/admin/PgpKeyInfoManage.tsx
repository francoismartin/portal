import React, { ReactElement, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LOAD_PGP_KEY_INFOS } from '../../actions/actionTypes';
import { PgpKeyInfo } from '../../api/generated';
import { isStaff } from '../selectors';
import { I18nNamespace } from '../../i18n';
import { useTranslation } from 'next-i18next';
import { EnhancedTable, requestAction } from '@biomedit/next-widgets';
import { usePgpKeyInfoColumns } from '../pgpKeyInfo/PgpKeyInfoHooks';
import { usePgpKeyInfoForm } from './AdministrationHooks';
import { allowedPgpKeyInfoStatusEnumSubset } from './PgpKeyInfoManageForm';

export const PgpKeyInfoList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.PGP_KEY_INFO_LIST,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
  ]);
  const { isFetching, isSubmitting, itemList } = useSelector(
    (state) => state.pgpKeyInfo
  );
  const staff = useSelector(isStaff);
  const canEdit = useMemo(
    () => (item?: Required<PgpKeyInfo>) =>
      (item && staff && !!allowedPgpKeyInfoStatusEnumSubset(item.status)) ||
      false,
    [staff]
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_PGP_KEY_INFOS));
  }, [dispatch]);

  const { openFormDialog, pgpKeyInfoForm } = usePgpKeyInfoForm();

  const columns = usePgpKeyInfoColumns();
  return (
    <EnhancedTable<Required<PgpKeyInfo>>
      itemList={itemList}
      columns={columns}
      canAdd={false} // it's forbidden to add keys
      canEdit={canEdit}
      canDelete={false} // it's forbidden to delete keys
      onEdit={openFormDialog}
      onAdd={openFormDialog}
      form={pgpKeyInfoForm}
      isFetching={isFetching}
      isSubmitting={isSubmitting}
      emptyMessage={t(`${I18nNamespace.LIST}:emptyMessage`, {
        model: t(`${I18nNamespace.COMMON}:models.key_plural`),
      })}
      inline
    />
  );
};
