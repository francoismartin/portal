import {
  MockServer,
  mockTimeZone,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { backend } from '../../api/api';
import { fireEvent, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { makeStore } from '../../store';
import React from 'react';
import { UserList } from './UserManage';
import listUsersResponse from '../../../test-data/listUsersResponse.json';
import { rest } from 'msw';

const assertHasTextContents = async (
  textContents: string[],
  identifier: string,
  order: number[]
) => {
  // cell_${i}_${identifier}
  const cells = await screen.findAllByTestId(`_${identifier}`, {
    exact: false,
  });
  for (let i = 0; i < order.length; i++) {
    // Can not use 'toHaveTextContent' with empty value
    expect(cells[i].textContent).toBe(textContents[order[i]]);
  }
};

describe('UserList.fetchUsers', () => {
  const server: MockServer = setupMockApi();

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  it('should display empty list', async () => {
    server.use(
      rest.get(`${backend}users/`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, []))
      )
    );
    render(
      <Provider store={makeStore()}>
        <UserList />
      </Provider>
    );
    // Promise is rejected if NOT found. Empty message (NOT i18n'ed) expected.
    await screen.findByText('list:emptyMessage');
  });

  it('should list the users', async () => {
    server.use(
      rest.get(`${backend}users/`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listUsersResponse))
      )
    );
    render(
      <Provider store={makeStore()}>
        <UserList />
      </Provider>
    );
    // Table header (1x), plus user rows (6x), plus table footer (1x)
    expect(await screen.findAllByRole('row')).toHaveLength(8);
  });

  it('should correctly sort the last login values', async () => {
    mockTimeZone('UTC');
    server.use(
      rest.get(`${backend}users/`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listUsersResponse))
      )
    );
    render(
      <Provider store={makeStore()}>
        <UserList />
      </Provider>
    );
    const expected = [
      '2019-05-02 14:46',
      '2020-07-09 12:22',
      '2021-01-03 08:20',
      '2019-12-03 10:20',
      '',
      '',
    ];
    const identifier = 'lastLogin';
    const sorted = [4, 5, 0, 3, 1, 2];
    await assertHasTextContents(expected, identifier, [0, 1, 2, 3, 4, 5]);
    // Sort asc
    const lastLoginHeader = await screen.findByText('columns.lastLogin');
    fireEvent.click(lastLoginHeader);
    await assertHasTextContents(expected, identifier, sorted);
    // Sort desc
    fireEvent.click(lastLoginHeader);
    await assertHasTextContents(expected, identifier, sorted.reverse());
  });

  it('should correctly sort the last names', async () => {
    server.use(
      rest.get(`${backend}users/`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listUsersResponse))
      )
    );
    render(
      <Provider store={makeStore()}>
        <UserList />
      </Provider>
    );
    const expected = [
      'kerluke',
      'Rowan',
      'norris',
      'österle',
      'schulz',
      'lagaffe',
    ];
    const identifier = 'lastName';
    const sorted = [0, 5, 2, 3, 1, 4];
    await assertHasTextContents(expected, identifier, [0, 1, 2, 3, 4, 5]);
    // Sort asc
    const lastNameHeader = await screen.findByText('columns.lastName');
    fireEvent.click(lastNameHeader);
    await assertHasTextContents(expected, identifier, sorted);
    // Sort desc
    fireEvent.click(lastNameHeader);
    await assertHasTextContents(expected, identifier, sorted.reverse());
  });
});
