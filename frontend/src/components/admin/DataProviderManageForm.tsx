import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormProvider } from 'react-hook-form';
import { DataProvider } from '../../api/generated';
import {
  ADD_DATA_PROVIDER,
  CALL,
  LOAD_NODES,
  UPDATE_DATA_PROVIDER,
} from '../../actions/actionTypes';
import { generatedBackendApi } from '../../api/api';
import { useNodeChoices } from '../choice';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import {
  CheckboxField,
  ConfirmLabel,
  FormDialog,
  HiddenField,
  idValidations,
  LabelledField,
  nameValidations,
  requestAction,
  SelectField,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { isStaff } from '../selectors';
import { FormFieldsContainer } from '../FormFieldsContainer';

type DataProviderFormProps = {
  dataProvider: Partial<DataProvider>;
  onClose: () => void;
};

export const DataProviderForm = ({
  dataProvider,
  onClose,
}: DataProviderFormProps): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.COMMON,
    I18nNamespace.DATA_PROVIDER,
  ]);
  const staff = useSelector(isStaff);
  const isEdit = !!dataProvider.id;

  const dispatch = useDispatch();

  const nodes = useSelector((state) => state.nodes.itemList);
  const isFetchingNodes = useSelector((state) => state.nodes.isFetching);
  const isSubmitting = useSelector((state) => state.dataProvider.isSubmitting);

  const form = useEnhancedForm<DataProvider>();

  function submit(dataProvider: DataProvider) {
    const onSuccessAction = { type: CALL, callback: onClose };
    if (isEdit) {
      dispatch(
        requestAction(
          UPDATE_DATA_PROVIDER,
          {
            id: String(dataProvider.id),
            dataProvider,
          },
          onSuccessAction
        )
      );
    } else {
      dispatch(
        requestAction(
          ADD_DATA_PROVIDER,
          {
            dataProvider,
          },
          onSuccessAction
        )
      );
    }
  }

  useEffect(() => {
    dispatch(requestAction(LOAD_NODES));
  }, [dispatch]);

  const uniqueCheck = async (value) =>
    generatedBackendApi.uniqueDataProvider({ uniqueDataProvider: value });

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={
          isEdit ? `Data Provider: ${dataProvider?.name}` : 'New Data Provider'
        }
        open={!!dataProvider}
        onSubmit={submit}
        onClose={onClose}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
        noValidate
      >
        <HiddenField name="id" initialValues={{ id: dataProvider.id }} />
        <FormFieldsContainer>
          <LabelledField
            name="code"
            type="text"
            label={t(`${I18nNamespace.DATA_PROVIDER}:columns.code`)}
            validations={idValidations()}
            unique={uniqueCheck}
            initialValues={dataProvider}
            fullWidth
            disabled={isEdit}
          />
          <LabelledField
            name="name"
            type="text"
            label={t(`${I18nNamespace.DATA_PROVIDER}:columns.name`)}
            initialValues={dataProvider}
            validations={nameValidations()}
            unique={uniqueCheck}
            fullWidth
          />
          <CheckboxField
            name="enabled"
            label="Enabled"
            initialValues={dataProvider}
          />
          <SelectField
            name="node"
            label={t(`${I18nNamespace.COMMON}:models.node`)}
            initialValues={dataProvider}
            required
            choices={useNodeChoices(nodes)}
            isLoading={isFetchingNodes}
            disabled={!staff}
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
