import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormProvider } from 'react-hook-form';
import { DeepRequired } from 'utility-types';
import { Flag, User } from '../../api/generated';
import {
  ADD_FLAG,
  CALL,
  LOAD_USERS,
  UPDATE_FLAG,
} from '../../actions/actionTypes';
import { generatedBackendApi } from '../../api/api';
import { useUserChoices } from '../choice';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import {
  AutocompleteField,
  ConfirmLabel,
  FormDialog,
  HiddenField,
  idValidations,
  LabelledField,
  requestAction,
  required,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { FormFieldsContainer } from '../FormFieldsContainer';

type FlagFormProps = {
  flag: Partial<Flag>;
  onClose: () => void;
};

export const FlagManageForm = ({
  flag,
  onClose,
}: FlagFormProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.FLAG_LIST]);
  const dispatch = useDispatch();

  const users = useSelector(
    (state) => state.users.itemList
  ) as DeepRequired<User>[];
  const isFetchingUsers = useSelector((state) => state.users.isFetching);
  const isSubmitting = useSelector((state) => state.nodes.isSubmitting);
  const isEdit = !!flag.id;

  const form = useEnhancedForm<Flag>();

  function submit(formFlag: Flag) {
    const onSuccessAction = {
      type: CALL,
      callback: onClose,
    };
    if (isEdit) {
      dispatch(
        requestAction(
          UPDATE_FLAG,
          {
            id: String(flag.id),
            flag: formFlag,
          },
          onSuccessAction
        )
      );
    } else {
      dispatch(requestAction(ADD_FLAG, { flag: formFlag }, onSuccessAction));
    }
  }

  useEffect(() => {
    dispatch(requestAction(LOAD_USERS));
  }, [dispatch]);

  const uniqueCheck = async (value) =>
    generatedBackendApi.uniqueFlag({ uniqueFlag: value });

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={isEdit ? `Flag: ${flag?.code}` : 'New Flag'}
        open={!!flag}
        onSubmit={submit}
        onClose={onClose}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
      >
        <HiddenField name="id" initialValues={{ id: flag.id }} />
        <FormFieldsContainer>
          <LabelledField
            name="code"
            type="text"
            label={t(`${I18nNamespace.FLAG_LIST}:columns.code`)}
            validations={idValidations()}
            unique={uniqueCheck}
            initialValues={flag}
            fullWidth
          />
          <LabelledField
            name="description"
            type="text"
            multiline
            validations={required}
            label={t(`${I18nNamespace.FLAG_LIST}:columns.description`)}
            initialValues={flag}
            fullWidth
            style={{ marginBottom: 15 }}
          />
          <AutocompleteField
            name="users"
            label={t(`${I18nNamespace.FLAG_LIST}:columns.users`)}
            initialValues={flag}
            choices={useUserChoices(users)}
            isLoading={isFetchingUsers}
            multiple
            width="100%"
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
