import React, { ReactElement, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LOAD_FEED } from '../actions/actionTypes';
import {
  Markdown,
  requestAction,
  Timeline,
  TimelineElement,
} from '@biomedit/next-widgets';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../i18n';
import { Typography } from '@mui/material';

export const FeedList = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.FEED_LIST);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_FEED));
  }, [dispatch]);
  const feed = useSelector((state) => state.feed.itemList);

  const elements: TimelineElement[] = useMemo(
    () =>
      feed.map((feed) => ({
        icon: feed.label,
        title: feed.title ? <Markdown text={feed.title} /> : undefined,
        message: feed.message ? <Markdown text={feed.message} /> : undefined,
        date: feed.created,
        titleProps: { variant: 'body1' },
        messageProps: { color: 'textSecondary', variant: 'body2' },
      })),
    [feed]
  );

  if (!feed.length) {
    return (
      <Typography variant="body2" sx={{ textAlign: 'center' }}>
        {t('emptyMessage')}
      </Typography>
    );
  }

  return (
    <Timeline
      elements={elements}
      sx={{
        padding: 0,
        margin: 0,
      }}
    />
  );
};
