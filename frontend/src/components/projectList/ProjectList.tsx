import React, {
  Fragment,
  ReactElement,
  ReactNode,
  useCallback,
  useMemo,
  useState,
} from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import {
  AddIcon,
  AdditionalActionButtonFactory,
  DeleteDialog,
  FabButton,
  ListPage,
  requestAction,
  Select,
} from '@biomedit/next-widgets';

import { Box, Button, Toolbar } from '@mui/material';
import {
  ARCHIVE_PROJECT,
  CALL,
  DELETE_PROJECT,
  LOAD_DATA_PROVIDERS,
  LOAD_DATA_TRANSFERS,
  LOAD_NODES,
  LOAD_PROJECTS,
  LOAD_USERS,
  UNARCHIVE_PROJECT,
} from '../../actions/actionTypes';
import { useProjectForm } from '../projectForm/ProjectForm';
import { IdRequired } from '../../global';
import { Project } from '../../api/generated';
import { canAddProjects } from '../selectors';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { useFilterProjects, useProjectModel } from './ProjectListHooks';

export const projectListItemName = 'project';
export const allProjectsKey = 'filter.allProjects';
export const archivedProjectsKey = 'filter.archivedProjects';

export declare type ProjectListProps = {
  selected?: string; // Project code (NOT ID)
  children?: ReactNode;
};

export const ProjectList = (props: ProjectListProps): ReactElement => {
  const { t } = useTranslation(I18nNamespace.PROJECT_LIST);

  const dispatch = useDispatch();
  const isFetching = useSelector((state) => state.project.isFetching);
  const isSubmitting = useSelector((state) => state.project.isSubmitting);
  const canAdd = useSelector(canAddProjects);
  const canDelete = useSelector(
    (state) => state.auth.pagePermissions.project.del
  );

  const loadItems = useCallback(() => {
    batch(() => {
      dispatch(requestAction(LOAD_DATA_PROVIDERS));
      dispatch(requestAction(LOAD_NODES));
      dispatch(requestAction(LOAD_USERS));
      dispatch(requestAction(LOAD_DATA_TRANSFERS));
      dispatch(requestAction(LOAD_PROJECTS, { ordering: 'name' }));
    });
  }, [dispatch]);

  const deleteItem = useCallback(
    (id) => {
      dispatch(requestAction(DELETE_PROJECT, { id }));
    },
    [dispatch]
  );

  const projectModel = useProjectModel();
  const { openFormDialog, projectForm } = useProjectForm();
  const openAddForm = openFormDialog && (() => openFormDialog());

  const { filteredProjects, selectProps } = useFilterProjects(props.selected);

  const canEdit = useCallback((item: IdRequired<Project>) => {
    if (item.archived) {
      return false;
    }
    const edit = item?.permissions?.edit;
    for (const key in edit) {
      if (Object.prototype.hasOwnProperty.call(edit, key)) {
        const value = edit[key];
        if (Array.isArray(value)) {
          if (value.length) {
            return true;
          }
        } else {
          if (value) {
            return true;
          }
        }
      }
    }
    return false;
  }, []);

  const getDeleteConfirmationText = useCallback(
    (item: IdRequired<Project>) => 'Delete ' + item.name,
    []
  );

  const [archiveDialogItem, setArchiveDialogItem] = useState<
    string | undefined
  >();

  const onArchiveDialogClose = useCallback(
    () => setArchiveDialogItem(undefined),
    []
  );

  const additionalActionButtons = useMemo<
    Array<AdditionalActionButtonFactory<Project>>
  >(
    () => [
      (item) => {
        if (item?.permissions?.archive && !item?.archived) {
          const archiveLabel = t('actionButtons.archive');
          return (
            <Fragment key={'archive-dialog-wrapper-' + item?.code}>
              <DeleteDialog
                key={'archive-dialog-' + item?.code}
                title={t('archiveDialog.title')}
                text={t('archiveDialog.text')}
                open={archiveDialogItem == item?.name}
                itemName={t(projectListItemName)}
                confirmationText={archiveDialogItem}
                isSubmitting={isSubmitting}
                onDelete={(isConfirmed) => {
                  if (isConfirmed) {
                    dispatch(
                      requestAction(
                        ARCHIVE_PROJECT,
                        {
                          id: String(item?.id),
                        },
                        {
                          type: CALL,
                          callback: onArchiveDialogClose,
                        }
                      )
                    );
                  } else {
                    onArchiveDialogClose();
                  }
                }}
              />
              <FabButton
                key={'archive-button-' + item?.code}
                aria-label={archiveLabel}
                title={archiveLabel}
                size="small"
                icon="archive"
                onClick={() => setArchiveDialogItem(item?.name)}
              />
            </Fragment>
          );
        } else {
          return null;
        }
      },
      (item) => {
        if (item?.permissions?.archive && item?.archived) {
          const unarchiveLabel = t('actionButtons.unarchive');
          return (
            <FabButton
              key={'unarchive-button-' + item?.code}
              aria-label={unarchiveLabel}
              title={unarchiveLabel}
              size="small"
              icon="unarchive"
              onClick={() =>
                dispatch(
                  requestAction(UNARCHIVE_PROJECT, { id: String(item?.id) })
                )
              }
            />
          );
        } else {
          return null;
        }
      },
    ],
    [dispatch, t, archiveDialogItem, isSubmitting, onArchiveDialogClose]
  );

  return (
    <>
      {(selectProps || canAdd) && (
        <Toolbar sx={{ marginBottom: 2 }} disableGutters={true}>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'flex-start',
              columnGap: 1,
            }}
          >
            {selectProps && <Select {...selectProps} />}
            {canAdd && (
              <Button
                variant="outlined"
                color="primary"
                startIcon={<AddIcon />}
                onClick={openAddForm}
                aria-label={t('common:models.project')}
              >
                {t('common:models.project')}
              </Button>
            )}
          </Box>
        </Toolbar>
      )}
      <div>
        <ListPage<IdRequired<Project>>
          model={projectModel}
          emptyMessage={t('emptyMessage')}
          itemList={filteredProjects}
          itemName={t(projectListItemName)}
          loadItems={loadItems}
          isFetching={isFetching}
          form={projectForm}
          canDelete={canDelete}
          canEdit={canEdit}
          deleteItem={deleteItem}
          getDeleteConfirmationText={getDeleteConfirmationText}
          isSubmitting={isSubmitting}
          openForm={openFormDialog}
          additionalActionButtons={additionalActionButtons}
        />
      </div>
    </>
  );
};
