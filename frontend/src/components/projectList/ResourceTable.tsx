import { IdRequired } from '../../global';
import { Project, ProjectResources } from '../../api/generated';
import React, { ReactElement, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateProjectAction } from '../../actions/actionTypes';
import { EnhancedTable } from '@biomedit/next-widgets';
import { Required } from 'utility-types';
import { useResourceColumns, useResourceForm } from './ResourceHooks';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import produce from 'immer';

export const resourceTableTitle = `${I18nNamespace.COMMON}:models.resource_plural`;
export const resourceTableAddButtonLabel = `${I18nNamespace.PROJECT_RESOURCE_LIST}:addButton`;

export const ResourceTable = (project: IdRequired<Project>): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.PROJECT_RESOURCE_LIST,
    I18nNamespace.COMMON,
  ]);
  const isFetching = useSelector((state) => state.project.isFetching);
  const isSubmitting = useSelector((state) => state.project.isSubmitting);
  const canAddResource = project?.permissions?.edit?.resources;

  const dispatch = useDispatch();
  const deleteItem = useCallback(
    (id) => {
      const newProject: Project = produce<Project>(project, (draft) => {
        const index = project.resources!.findIndex(
          (resource) => resource.id == id
        );
        draft.resources!.splice(index, 1);
      });
      dispatch(updateProjectAction(newProject));
    },
    [project, dispatch]
  );

  const { openFormDialog, resourceForm } = useResourceForm(project);
  const columns = useResourceColumns();

  const getDeleteConfirmationText = useCallback(
    (item: Required<ProjectResources>) => 'Delete ' + item.name,
    []
  );

  return (
    <>
      <EnhancedTable<Required<ProjectResources>>
        itemList={project?.resources}
        columns={columns}
        title={t(resourceTableTitle)}
        canAdd={canAddResource}
        canEdit={canAddResource}
        canDelete={canAddResource}
        onAdd={openFormDialog}
        onEdit={openFormDialog}
        form={resourceForm}
        onDelete={deleteItem}
        addButtonLabel={t(resourceTableAddButtonLabel)}
        isFetching={isFetching}
        isSubmitting={isSubmitting}
        canFilter={false}
        pagination={false}
        getDeleteConfirmationText={getDeleteConfirmationText}
        inline
      />
    </>
  );
};
