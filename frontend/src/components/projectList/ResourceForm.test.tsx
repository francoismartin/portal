import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { makeStore } from '../../store';
import React from 'react';
import { ResourceForm } from './ResourceForm';
import { Project, ProjectResources } from '../../api/generated';

describe('ResourceForm', () => {
  const getResource = (): ProjectResources => {
    return {
      id: 1,
      name: 'Test Resource',
      location: 'http://test.com',
      description: 'Test Website',
      contact: 'chuck@norris.gov',
    };
  };

  const getProject = (): Project => {
    return {
      name: 'Test Project',
      code: 'TSTPROJ',
      users: [],
      resources: [getResource()],
    };
  };

  it('should display the resource for update', async () => {
    const project = getProject();
    const resource: ProjectResources = project.resources![0]!;
    render(
      <Provider store={makeStore()}>
        <ResourceForm
          project={project}
          onClose={() => {
            return;
          }}
          resource={resource}
        />
      </Provider>
    );
    // All resource properties are correctly displayed in the form
    await screen.findByText('columns.name: Test Resource');
    await screen.findByDisplayValue('http://test.com', { exact: true });
    await screen.findByDisplayValue('Test Website', { exact: true });
    await screen.findByDisplayValue('chuck@norris.gov', { exact: true });
  });

  it('should display form for a new resource', async () => {
    const project = getProject();
    const newResource = {
      name: undefined,
      location: undefined,
      description: undefined,
      contact: undefined,
    };
    render(
      <Provider store={makeStore()}>
        <ResourceForm
          project={project}
          onClose={() => {
            return;
          }}
          resource={newResource}
        />
      </Provider>
    );
    await screen.findByText('New');
  });
});
