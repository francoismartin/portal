import { configure, fireEvent, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { getInitialState, makeStore, RootState } from '../../store';
import { AuthState } from '../../reducers/auth';
import React from 'react';
import authStateNodeAdmin from '../../../test-data/authStateNodeAdmin.json';
import authStateUser from '../../../test-data/authStateUser.json';
import listDataProviderResponse from '../../../test-data/listDataProviderResponse.json';
import listUsersResponse from '../../../test-data/listUsersResponse.json';
import listNodesResponse from '../../../test-data/listNodesResponse.json';
import staffState from '../../../test-data/staffState.json';
import {
  allProjectsKey,
  archivedProjectsKey,
  ProjectList,
  projectListItemName,
  ProjectListProps,
} from './ProjectList';
import {
  DataTransfer,
  DataTransferPurposeEnum,
  DataTransferStatusEnum,
  Project,
  ProjectIpAddressRanges,
  ProjectPermissionsEdit,
  ProjectPermissionsEditUsersEnum,
  ProjectResources,
  ProjectUsersRolesEnum,
} from '../../api/generated';
import {
  addIconButtonLabelPrefix,
  DeepWriteable,
  deleteIconButtonLabelPrefix,
  editIconButtonLabelPrefix,
  expectToBeInTheDocument,
  MockServer,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import {
  resourceTableAddButtonLabel,
  resourceTableTitle,
} from './ResourceTable';
import {
  dtrTableAddButtonLabel,
  dtrTableTitle,
} from '../dataTransfer/DataTransferTable';
import { rest } from 'msw';
import { backend, generatedBackendApi } from '../../api/api';

const server: MockServer = setupMockApi();

beforeAll(() => {
  configure({
    asyncUtilTimeout: 10000,
  });
});

afterEach(() => {
  // Reset any runtime handlers tests may use.
  server.resetHandlers();
});

afterAll(() => {
  server.close();
});

const dtrs: Array<DataTransfer> = [
  {
    id: 1,
    packages: [],
    requestorName:
      'Chuck Norris (ID: 63457666153) (chuck.norris@roundhouse.kick)',
    requestorDisplayId: 'ID: 63457666153',
    requestorFirstName: 'Chuck',
    requestorLastName: 'Norris',
    requestorEmail: 'chuck.norris@roundhouse.kick',
    projectName: 'Test Project 1',
    project: 1,
    maxPackages: 1,
    status: DataTransferStatusEnum.INITIAL,
    dataProvider: 'area51',
    requestor: 2,
    purpose: DataTransferPurposeEnum.TEST,
    requestorPgpKeyFp: '1100000000000000000000000000000000000000',
  },
];

function createProject(
  withDtr: boolean,
  withResource: boolean,
  roles: ProjectUsersRolesEnum[], // The roles current user has in the project
  edit: ProjectPermissionsEdit,
  archived: boolean,
  id = 1
): {
  archived: boolean;
  gid: number;
  code: string;
  permissions: { edit: ProjectPermissionsEdit };
  ipAddressRanges: ProjectIpAddressRanges[];
  name: string;
  destination: string;
  resources: ProjectResources[];
  id: number;
  dataTransferCount: number;
  users: {
    firstName: string;
    lastName: string;
    affiliation: string;
    displayName: string;
    roles: ProjectUsersRolesEnum[];
    affiliationId: string;
    id: number;
    email: string;
    username: string;
  }[];
} {
  const resource: Array<ProjectResources> = [
    {
      id: 1,
      name: 'Croissants',
      location: 'http://croissant.ch',
      description: 'Fresh Croissants!',
      contact: '',
    },
  ];

  return {
    id,
    gid: 1500000,
    permissions: {
      edit,
    },
    dataTransferCount: withDtr ? 1 : 0,
    name: `Test Project ${id}`,
    code: `test_project_${id}`,
    destination: 'sis',
    ipAddressRanges: [],
    resources: withResource ? resource : [],
    users: roles.length
      ? [
          {
            username: '63457666153@eduid.ch',
            email: 'chuck.norris@roundhouse.kick',
            firstName: 'Chuck',
            lastName: 'Norris',
            displayName:
              'Chuck Norris (ID: 63457666153) (chuck.norris@roundhouse.kick)',
            affiliation: '',
            affiliationId: '',
            id: 2,
            roles,
          },
        ]
      : [],
    archived,
  };
}

// A project user can NOT do anything on project level. Read-only mode.
const editUser = {
  name: false,
  code: false,
  destination: false,
  users: [],
  ipAddressRanges: false,
  resources: false,
};

// A Project User can NOT do anything on project level. Read-only mode.
const editNodeAdmin = {
  name: true,
  code: true,
  destination: false,
  users: [
    ProjectPermissionsEditUsersEnum.PM,
    ProjectPermissionsEditUsersEnum.DM,
    ProjectPermissionsEditUsersEnum.PL,
    ProjectPermissionsEditUsersEnum.USER,
  ],
  ipAddressRanges: true,
  resources: true,
};

// A Project Leader can NOT edit the project but can manage his people
const editPl = {
  name: false,
  code: false,
  destination: false,
  users: [
    ProjectPermissionsEditUsersEnum.PM,
    ProjectPermissionsEditUsersEnum.DM,
    ProjectPermissionsEditUsersEnum.USER,
  ],
  ipAddressRanges: false,
  resources: false,
};

// Staff users are super-users and can do what they want with the project
const editStaff = {
  name: true,
  code: true,
  destination: true,
  users: [
    ProjectPermissionsEditUsersEnum.PL,
    ProjectPermissionsEditUsersEnum.PM,
    ProjectPermissionsEditUsersEnum.DM,
    ProjectPermissionsEditUsersEnum.USER,
  ],
  ipAddressRanges: true,
  resources: true,
};

type WriteableProject = DeepWriteable<Project>;

const itemList: Array<WriteableProject> = [
  createProject(false, true, [ProjectUsersRolesEnum.USER], editUser, false),
  createProject(true, false, [ProjectUsersRolesEnum.USER], editUser, false),
  createProject(false, false, [], editNodeAdmin, false),
  createProject(false, false, [ProjectUsersRolesEnum.PL], editPl, false),
  createProject(false, true, [ProjectUsersRolesEnum.PL], editPl, false),
  createProject(false, true, [ProjectUsersRolesEnum.USER], editUser, true),
  createProject(true, false, [ProjectUsersRolesEnum.USER], editUser, true),
  createProject(false, false, [], editUser, true),
  createProject(false, false, [ProjectUsersRolesEnum.PL], editUser, true),
  createProject(false, true, [ProjectUsersRolesEnum.PL], editUser, true),
  createProject(true, false, [ProjectUsersRolesEnum.USER], editStaff, false),
  createProject(false, true, [ProjectUsersRolesEnum.PL], editStaff, false),
];

const userStates = {
  staff: staffState, // Staff
  nodeAdmin: authStateNodeAdmin, // Node admin
  user: authStateUser, // Simple project user
};

describe('ProjectList', () => {
  describe('Component', () => {
    const renderProjectList = (
      authState: AuthState,
      projects?: Array<WriteableProject>,
      props?: ProjectListProps
    ) => {
      // mocked since project is parametrized as an object, for `msw` the backend representation would be necessary
      jest
        .spyOn(generatedBackendApi, 'listProjectsRaw')
        .mockImplementation(() => {
          return new Promise((resolve) => {
            // @ts-expect-error not relevant for logic to be tested
            resolve({
              value: () =>
                new Promise((resolve2) => {
                  // @ts-expect-error not relevant for logic to be tested
                  resolve2([project]);
                }),
            });
          });
        });

      server.use(
        rest.get(`${backend}users/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, listUsersResponse))
        ),
        rest.get(`${backend}data-provider/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, listDataProviderResponse))
        ),
        rest.get(`${backend}nodes/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, listNodesResponse))
        ),
        rest.get(`${backend}data-transfer/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, dtrs))
        )
      );

      const state: DeepWriteable<RootState> = {
        ...getInitialState(),
        ...{
          project: {
            isFetching: false,
            isSubmitting: false,
            itemList: projects ? projects : [itemList[0]],
            ipAddress: '127.0.0.1',
          },
          ...{
            dataProvider: {
              isFetching: false,
              isSubmitting: false,
              itemList: listDataProviderResponse,
            },
          },
          ...{
            auth: authState,
          },
        },
      };
      render(
        <Provider
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore project role is expected to be an enum, which it can't be in a json file
          store={makeStore(undefined, state)}
        >
          <ProjectList {...props} />
        </Provider>
      );
    };
    it('should allow adding a new project as node admin', async () => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore node status is expected to be an enum, which it can't be in a json file
      renderProjectList(userStates['nodeAdmin'].auth, undefined);
      await screen.findByRole('button', {
        name: 'common:models.project',
      });
    });

    it.each`
      projectIndex | role           | showResources | canAddResource | showDtrs | canAddDtr | canEditProject | canDeleteProject
      ${0}         | ${'nodeAdmin'} | ${true}       | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${0}         | ${'user'}      | ${true}       | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${1}         | ${'nodeAdmin'} | ${false}      | ${false}       | ${true}  | ${false}  | ${false}       | ${false}
      ${2}         | ${'nodeAdmin'} | ${false}      | ${true}        | ${false} | ${false}  | ${true}        | ${false}
      ${3}         | ${'nodeAdmin'} | ${false}      | ${false}       | ${false} | ${false}  | ${true}        | ${false}
      ${4}         | ${'nodeAdmin'} | ${true}       | ${false}       | ${false} | ${false}  | ${true}        | ${false}
      ${5}         | ${'nodeAdmin'} | ${true}       | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${6}         | ${'nodeAdmin'} | ${false}      | ${false}       | ${true}  | ${false}  | ${false}       | ${false}
      ${6}         | ${'staff'}     | ${false}      | ${false}       | ${true}  | ${false}  | ${false}       | ${true}
      ${6}         | ${'user'}      | ${false}      | ${false}       | ${true}  | ${false}  | ${false}       | ${false}
      ${7}         | ${'nodeAdmin'} | ${false}      | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${8}         | ${'nodeAdmin'} | ${false}      | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${9}         | ${'nodeAdmin'} | ${true}       | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${9}         | ${'staff'}     | ${true}       | ${false}       | ${false} | ${false}  | ${false}       | ${true}
      ${10}        | ${'staff'}     | ${false}      | ${true}        | ${true}  | ${true}   | ${true}        | ${true}
      ${11}        | ${'staff'}     | ${true}       | ${true}        | ${true}  | ${true}   | ${true}        | ${true}
    `(
      'should show resources ($showResources), ' +
        'can add resource ($canAddResource), ' +
        'show DTRs ($showDtrs), ' +
        'can add DTR ($canAddDtr), ' +
        'can edit the project ($canEditProject), ' +
        'can delete the project ($canDeleteProject), ' +
        'on project with index $projectIndex, ' +
        'if user role is $role',
      async ({
        projectIndex,
        role,
        showResources,
        canAddResource,
        showDtrs,
        canAddDtr,
        canEditProject,
        canDeleteProject,
      }) => {
        const authState = userStates[role].auth;
        const project = itemList[projectIndex];
        const { hasProjects, staff, nodeAdmin, nodeViewer } =
          authState.user.permissions;
        const canSeeFilter = hasProjects && (staff || nodeAdmin || nodeViewer);
        renderProjectList(authState, [project]);

        expectToBeInTheDocument(
          screen.queryByText('projectList:archived'),
          (project.archived ?? false) && !canSeeFilter
        );

        if (project.archived && canSeeFilter) {
          // for staff and node admins, archived projects are only visible under "Archived Projects"
          fireEvent.mouseDown(await screen.findByText(allProjectsKey));
          fireEvent.click(await screen.findByText(archivedProjectsKey));
          expect(
            screen.queryByText('projectList:archived')
          ).toBeInTheDocument();
        }

        const accordion = await screen.findByText(project.name);
        accordion.click();
        await screen.findByText('captions.code'); // wait for project to be expanded

        const resources = await screen.queryByText(resourceTableTitle);
        expectToBeInTheDocument(resources, showResources || canAddResource);

        if (showResources) {
          const resource = project?.resources?.[0].name;
          expect(resource).toBeDefined();
          await screen.getByText(resource as string); // safe cast as we verified it to be defined
        } else {
          expect(project.resources).toHaveLength(0);
        }

        const addResourceBtn = screen.queryByRole('button', {
          name: addIconButtonLabelPrefix + resourceTableAddButtonLabel,
        });
        expectToBeInTheDocument(addResourceBtn, canAddResource);

        const dtrs = screen.queryByText(dtrTableTitle);
        expectToBeInTheDocument(dtrs, showDtrs);

        const addDtrBtn = screen.queryByRole('button', {
          name: addIconButtonLabelPrefix + dtrTableAddButtonLabel,
        });
        expectToBeInTheDocument(addDtrBtn, canAddDtr);

        const editProjectBtn = screen.queryByRole('button', {
          name: editIconButtonLabelPrefix + projectListItemName,
        });
        expectToBeInTheDocument(editProjectBtn, canEditProject);

        const deleteProjectBtn = screen.queryByRole('button', {
          name: deleteIconButtonLabelPrefix + projectListItemName,
        });
        expectToBeInTheDocument(deleteProjectBtn, canDeleteProject);
      }
    );

    it.each`
      ids       | selected
      ${[2, 3]} | ${'test_project_2'}
      ${[2, 3]} | ${undefined}
    `(
      'should display the selected project if any',
      async ({ ids, selected }) => {
        const projects = ids.map((id) =>
          createProject(
            false,
            false,
            [ProjectUsersRolesEnum.USER],
            editUser,
            false,
            id
          )
        );
        renderProjectList(userStates['staff'].auth, projects, { selected });
        // Add button is always there
        await screen.findByRole('button', {
          name: 'common:models.project',
        });
        expect(
          await screen.findAllByText('test_project', { exact: false })
        ).toHaveLength(selected ? 1 : ids.length);
      }
    );
  });
});
