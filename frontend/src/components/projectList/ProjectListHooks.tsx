import { Field, ListModel, SelectProps, Warning } from '@biomedit/next-widgets';
import { IdRequired } from '../../global';
import { Project } from '../../api/generated';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { useSelector } from 'react-redux';
import { canAddDataTransfer, canSeeNodeTab } from '../selectors';
import React, { useMemo, useState } from 'react';
import { Box, Chip } from '@mui/material';
import { ProjectUserTable } from './ProjectUserTable';
import { ResourceTable } from './ResourceTable';
import { DataTransferTable } from '../dataTransfer/DataTransferTable';
import { allProjectsKey, archivedProjectsKey } from './ProjectList';

type ProjectFilterChoice = {
  label: string;
  filter: (p: Project, userId: number) => boolean;
};

export const useProjectModel = (): ListModel<IdRequired<Project>> => {
  const { t } = useTranslation(I18nNamespace.PROJECT_FORM);
  const canAddDtr = useSelector(canAddDataTransfer);
  const nodes = useSelector((state) => state.nodes.itemList);

  return useMemo(
    () => ({
      getCaption: (prj: Project) => (
        <Box sx={{ display: 'flex' }}>
          {prj.name ?? t(`${I18nNamespace.COMMON}:models.project`)}
          {prj.archived && (
            <Chip
              label={t(`${I18nNamespace.PROJECT_LIST}:archived`)}
              sx={{ marginLeft: 2 }}
            />
          )}
        </Box>
      ),
      fields: [
        Field({
          caption: t('captions.code'),
          getProperty: (prj: Project) => prj.code,
          key: 'code',
        }),
        Field({
          caption: t('captions.destination'),
          getProperty: (prj: Project) => {
            const node = nodes.find(
              (prjNode) => prjNode.code === prj.destination
            );
            return node ? `${node.name} (${node.code})` : prj.destination;
          },
          key: 'destination',
        }),
        Field({
          getProperty: (prj: Project) => prj.users,
          key: 'users',
          render: ProjectUserTable,
        }),
        Field({
          getProperty: formatWarning,
          key: 'ipAddressInRange',
          render: renderWarning,
        }),
        Field({
          getProperty: (prj: Project) => prj,
          key: 'resources',
          render: ResourceTable,
          hideIf: (item) =>
            !(item?.resources?.length || item?.permissions?.edit?.resources),
        }),
        Field({
          getProperty: (prj: Project) => prj,
          key: 'dataTransfers',
          render: DataTransferTable,
          hideIf: (item) =>
            !item?.dataTransferCount && (item?.archived || !canAddDtr),
        }),
      ],
    }),
    [t, canAddDtr, nodes]
  );
};

function renderWarning(data: string | null) {
  return <Warning tooltip={data} />;
}

function formatWarning(prj: Project): string | null {
  if (prj['ipAddressInRange']) {
    // TODO: move postProcessProjects (ipAddressInRange) to here
    return null;
  }
  return 'Your IP address is not within a range of the project';
}

type FilterProjectsFunction = {
  filteredProjects: Project[];
  selectProps: undefined | SelectProps<number>;
};

export const useFilterProjects = (
  selected?: string
): FilterProjectsFunction => {
  const itemList: Project[] = useSelector((state) => state.project.itemList);
  const [filterIndex, setFilterIndex] = useState<number>(0);
  const user = useSelector((state) => state.auth.user);
  const { t } = useTranslation(I18nNamespace.PROJECT_LIST);
  const hasProjects = useSelector(
    (state) => !!state.auth.user?.permissions?.hasProjects
  );
  const canSeeNodeProjects = useSelector(canSeeNodeTab);
  const selectedProject = useMemo(() => {
    return itemList.find((x) => x.code === selected);
  }, [selected, itemList]);
  if (selectedProject) {
    return {
      filteredProjects: [selectedProject],
      selectProps: undefined,
    };
  }
  const showFilter = hasProjects && canSeeNodeProjects;
  if (!showFilter) {
    return {
      filteredProjects: itemList,
      selectProps: undefined,
    };
  }
  const userId = user?.id;
  const projectFilterChoices: ProjectFilterChoice[] = [
    { label: t(allProjectsKey), filter: (prj) => !prj.archived },
    {
      label: t('filter.myProjects'),
      filter: (prj, usrId) =>
        !prj.archived && prj.users.some((prjUser) => prjUser.id === usrId),
    },
    {
      label: t(archivedProjectsKey),
      filter: (p) => !!p.archived,
    },
  ];
  const { filter, label: filterLabel } = projectFilterChoices[filterIndex];
  const dropDownEntries = projectFilterChoices.map((choice, index) => ({
    label: choice.label,
    value: index,
  }));
  return {
    filteredProjects:
      userId !== undefined ? itemList.filter((p) => filter(p, userId)) : [],
    selectProps: {
      choices: dropDownEntries,
      value: filterLabel,
      setValue: setFilterIndex,
    },
  };
};
