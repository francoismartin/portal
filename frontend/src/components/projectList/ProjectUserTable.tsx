import React, { ReactElement, useMemo } from 'react';
import { ProjectUsers, ProjectUsersRolesEnum } from '../../api/generated';
import { userRoles } from '../../permissions';
import { Description } from '@biomedit/next-widgets';
import { useUserRoleEntries } from '../../widgets/UserRoleEntries';
import { I18nNamespace } from '../../i18n';

export const ProjectUserTable = (
  projectUsers: Array<ProjectUsers>
): ReactElement => {
  const usersByRole = useMemo(() => {
    const roleDict: Partial<Record<ProjectUsersRolesEnum, ProjectUsers[]>> = {};
    // Preserve the order in which the users will be listed
    for (const role in userRoles) {
      roleDict[role] = [];
    }
    projectUsers.forEach((user) => {
      user.roles.forEach((role) => {
        roleDict[role] = (roleDict[role] ?? []).concat([user]);
      });
    });
    return roleDict;
  }, [projectUsers]);

  const userRoleEntries = useUserRoleEntries(
    usersByRole,
    I18nNamespace.PROJECT_LIST
  );

  return <Description entries={userRoleEntries} labelWidth="12%" />;
};
