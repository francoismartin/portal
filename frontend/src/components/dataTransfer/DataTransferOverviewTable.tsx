import React, { ReactElement, ReactNode, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  DELETE_DATA_TRANSFER,
  LOAD_DATA_TRANSFERS,
} from '../../actions/actionTypes';
import { DataTransfer } from '../../api/generated';
import { useDataTransferForm } from './DataTransferForm';
import {
  EnhancedTable,
  requestAction,
  useDialogState,
} from '@biomedit/next-widgets';
import {
  useDataTransferColumns,
  useGlobalDataTransferFilter,
} from './DataTransferHooks';
import { DataTransferDetail } from './DataTransferDetail';
import { isStaff } from '../selectors';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';

export declare type DataTransferOverviewTableProps = {
  children?: ReactNode;
  selected?: string;
};

export const DataTransferOverviewTable = (
  props: DataTransferOverviewTableProps
): ReactElement => {
  const { t } = useTranslation([I18nNamespace.LIST, I18nNamespace.COMMON]);

  const isFetching = useSelector((state) => state.dataTransfers.isFetching);
  const isSubmitting = useSelector((state) => state.dataTransfers.isSubmitting);
  const dataTransfers = useSelector((state) => state.dataTransfers.itemList);
  const staff = useSelector(isStaff);
  const canEdit = useCallback(
    (item?: Required<DataTransfer>) => staff && !item?.projectArchived,
    [staff]
  );

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_DATA_TRANSFERS));
  }, [dispatch]);

  const deleteItem = useCallback(
    (id) => {
      dispatch(requestAction(DELETE_DATA_TRANSFER, { id }));
    },
    [dispatch]
  );

  const { openFormDialog, dtrForm } = useDataTransferForm();

  const columns = useDataTransferColumns(true);

  const { item, setItem, onClose, open } = useDialogState<DataTransfer>();

  useEffect(() => {
    if (props.selected) {
      // Passing `props.selected` causes a TypeScript error
      const selected = props.selected;
      const dataTransferSelected = dataTransfers.find(
        (x) => x.id === parseInt(selected)
      );
      if (dataTransferSelected) {
        setItem(dataTransferSelected);
      }
    }
  }, [props.selected, setItem, dataTransfers]);

  const globalFilter = useGlobalDataTransferFilter();

  const getDeleteConfirmationText = useCallback(
    (item: Required<DataTransfer>) => 'Delete ' + String(item.id),
    []
  );

  return (
    <>
      {item && item.id && (
        <DataTransferDetail dtrId={item.id} onClose={onClose} open={open} />
      )}
      <EnhancedTable<Required<DataTransfer>>
        itemList={dataTransfers}
        columns={columns}
        canAdd={false} // it should only be possible to add data transfers from the projects tabs
        canEdit={canEdit}
        canDelete={staff}
        onEdit={openFormDialog}
        onRowClick={setItem}
        form={dtrForm}
        onDelete={deleteItem}
        isFetching={isFetching}
        getDeleteConfirmationText={getDeleteConfirmationText}
        isSubmitting={isSubmitting}
        emptyMessage={t(`${I18nNamespace.LIST}:emptyMessage`, {
          model: t(`${I18nNamespace.COMMON}:models.dataTransfer_plural`),
        })}
        globalFilter={globalFilter}
      />
    </>
  );
};
