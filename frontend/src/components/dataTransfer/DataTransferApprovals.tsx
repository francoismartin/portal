import React, { ReactElement } from 'react';
import {
  ApprovalInstitutionEnum,
  DataTransfer,
  DataTransferDataProviderApprovalsStatusEnum,
  DataTransferGroupApprovalsStatusEnum,
  DataTransferNodeApprovalsStatusEnum,
} from '../../api/generated';
import { Stack } from '@mui/material';
import { DataTransferApprovalForm } from './DataTransferApprovalForm';
import {
  DataTransferApproval,
  DataTransferApprovalsStack,
} from './DataTransferApprovalsStack';
import { useDialogState } from '@biomedit/next-widgets';

type DataTransferApprovalsProps = {
  dtr: DataTransfer;
};

export type ApprovalDialogState = {
  approval: DataTransferApproval;
  approvalInstitution: ApprovalInstitutionEnum;
  action: ApprovalAction;
};

export type DataTransferStatus =
  | DataTransferNodeApprovalsStatusEnum
  | DataTransferDataProviderApprovalsStatusEnum
  | DataTransferGroupApprovalsStatusEnum;

export enum ApprovalAction {
  ACCEPT,
  REJECT,
}

export const DataTransferApprovals = ({
  dtr,
}: DataTransferApprovalsProps): ReactElement => {
  const { item, setItem, open, onClose } =
    useDialogState<ApprovalDialogState>();

  return (
    <>
      {dtr && item && (
        <DataTransferApprovalForm
          dtr={dtr}
          approval={item.approval}
          approvalInstitution={item.approvalInstitution}
          onClose={onClose}
          open={open}
          isRejectionForm={item.action === ApprovalAction.REJECT}
        />
      )}

      <Stack direction="column" spacing={5}>
        {!!dtr.nodeApprovals?.length && (
          <DataTransferApprovalsStack
            approvals={dtr.nodeApprovals}
            approvalInstitution={ApprovalInstitutionEnum.node}
            setApprovalState={setItem}
            waitingStatus={DataTransferNodeApprovalsStatusEnum.W}
          />
        )}
        {!!dtr.dataProviderApprovals?.length && (
          <DataTransferApprovalsStack
            approvals={dtr.dataProviderApprovals}
            approvalInstitution={ApprovalInstitutionEnum.data_provider}
            setApprovalState={setItem}
            waitingStatus={DataTransferDataProviderApprovalsStatusEnum.W}
          />
        )}
        {!!dtr.groupApprovals?.length && (
          <DataTransferApprovalsStack
            approvals={dtr.groupApprovals}
            approvalInstitution={ApprovalInstitutionEnum.group}
            setApprovalState={setItem}
            waitingStatus={DataTransferGroupApprovalsStatusEnum.W}
          />
        )}
      </Stack>
    </>
  );
};
