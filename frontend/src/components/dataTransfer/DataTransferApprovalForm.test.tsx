import React from 'react';
import {
  ApprovalInstitutionEnum,
  DataTransfer,
  DataTransferDataProviderApprovals,
  DataTransferDataProviderApprovalsStatusEnum,
  DataTransferGroupApprovals,
  DataTransferNode,
  DataTransferNodeApprovals,
  DataTransferNodeApprovalsStatusEnum,
  DataTransferPurposeEnum,
} from '../../api/generated';
import listNodeResponse from '../../../test-data/listNodesResponse.json';
import listDataTransfers from '../../../test-data/listDataTransfers.json';
import listDataProviderResponse from '../../../test-data/listDataProviderResponse.json';
import { DataTransferApprovalForm, Title } from './DataTransferApprovalForm';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { getInitialState, makeStore } from '../../store';
import { DataTransferApproval } from './DataTransferApprovalsStack';
import camelCase from 'lodash/camelCase';

const approval: DataTransferApproval = {
  id: 1,
  canApprove: true,
  created: new Date(2022, 1, 1),
  changeDate: new Date(2022, 2, 1),
};
const node = listNodeResponse[0] as DataTransferNode;
const dp = listDataProviderResponse[0];
const groupApproval = (
  listDataTransfers[2].groupApprovals as Array<DataTransferGroupApprovals>
)[0];

const nodeApproval: DataTransferNodeApprovals = {
  ...approval,
  status: DataTransferNodeApprovalsStatusEnum.W,
  node: node,
};

const dpApproval: DataTransferDataProviderApprovals = {
  ...approval,
  status: DataTransferDataProviderApprovalsStatusEnum.W,
  dataProvider: dp,
};

const dtr = listDataTransfers[0] as DataTransfer;

describe('Title', function () {
  it.each`
    approval        | institution                      | approvalInstitution
    ${nodeApproval} | ${'SIS (sis)'}                   | ${ApprovalInstitutionEnum.node}
    ${dpApproval}   | ${'Kinderspital Zürich (kispi)'} | ${ApprovalInstitutionEnum.data_provider}
  `(
    'Should show correct information when approval is $approval',
    async function ({ approval, institution, approvalInstitution }) {
      const dtrInformation = '44 - Project Number 1';

      render(
        <Title
          dtr={dtr}
          approval={approval}
          approvalInstitution={approvalInstitution}
        />
      );

      expect(await screen.findByText(dtrInformation)).toBeInTheDocument();
      expect(await screen.findByText(institution)).toBeInTheDocument();
    }
  );

  describe('DataTransferApprovalForm', function () {
    it.each`
      approval         | approvalInstitution
      ${nodeApproval}  | ${ApprovalInstitutionEnum.node}
      ${dpApproval}    | ${ApprovalInstitutionEnum.data_provider}
      ${groupApproval} | ${ApprovalInstitutionEnum.group}
    `(
      'Should show "technicalMeasuresInPlace" checkbox for non-group approval (current: $approvalInstitution)',
      async function ({ approval, approvalInstitution }) {
        const technicalMeasuresInPlace = `dataTransferApprovals:declarations.${camelCase(
          approvalInstitution
        )}.technicalMeasuresInPlace`;

        render(
          <Provider store={makeStore()}>
            <DataTransferApprovalForm
              approval={approval}
              approvalInstitution={approvalInstitution}
              dtr={dtr}
              onClose={() => {
                return;
              }}
              open={true}
              isRejectionForm={false}
            />
          </Provider>
        );

        if (approvalInstitution === 'group') {
          expect(
            screen.queryByRole(technicalMeasuresInPlace)
          ).not.toBeInTheDocument();
        } else {
          expect(
            await screen.findByText(technicalMeasuresInPlace)
          ).toBeInTheDocument();
        }
      }
    );

    it.each`
      dtrPurpose      | expected
      ${'PRODUCTION'} | ${true}
      ${'TEST'}       | ${false}
    `(
      'Should show "existingLegalBasis" checkbox $expected if purpose is $dtrPurpose',
      async function ({ dtrPurpose, expected }) {
        const existingLegalBasis =
          'dataTransferApprovals:declarations.node.existingLegalBasis';

        render(
          <Provider store={makeStore()}>
            <DataTransferApprovalForm
              approval={nodeApproval}
              approvalInstitution={ApprovalInstitutionEnum.node}
              dtr={{ ...dtr, purpose: dtrPurpose }}
              onClose={() => {
                return;
              }}
              open={true}
              isRejectionForm={false}
            />
          </Provider>
        );

        if (expected) {
          expect(
            await screen.findByText(existingLegalBasis)
          ).toBeInTheDocument();
        } else {
          expect(
            screen.queryByText(existingLegalBasis)
          ).not.toBeInTheDocument();
        }
      }
    );

    it.each`
      approval                          | expected
      ${{ ...nodeApproval, type: 'H' }} | ${true}
      ${{ ...nodeApproval, type: 'T' }} | ${false}
    `(
      'Should show "projectSpaceReady" checkbox $expected if node type is $approval.type',
      async function ({ approval, expected }) {
        const projectSpaceReady =
          'dataTransferApprovals:declarations.node.projectSpaceReady';

        render(
          <Provider store={makeStore()}>
            <DataTransferApprovalForm
              approval={approval}
              approvalInstitution={ApprovalInstitutionEnum.node}
              dtr={dtr}
              onClose={() => {
                return;
              }}
              open={true}
              isRejectionForm={false}
            />
          </Provider>
        );

        if (expected) {
          expect(
            await screen.findByText(projectSpaceReady)
          ).toBeInTheDocument();
        } else {
          expect(screen.queryByText(projectSpaceReady)).not.toBeInTheDocument();
        }
      }
    );
  });
});

describe('RejectionForm', function () {
  it.each`
    isRejectionForm | approval         | approvalInstitution                      | expected                                           | len
    ${false}        | ${dpApproval}    | ${ApprovalInstitutionEnum.data_provider} | ${'dataTransfer:actionButtons.approve'}            | ${2}
    ${false}        | ${nodeApproval}  | ${ApprovalInstitutionEnum.node}          | ${'dataTransfer:actionButtons.approve'}            | ${2}
    ${false}        | ${groupApproval} | ${ApprovalInstitutionEnum.group}         | ${'dataTransfer:actionButtons.approve'}            | ${1}
    ${true}         | ${dpApproval}    | ${ApprovalInstitutionEnum.data_provider} | ${'dataTransferApprovals:rejectDataTransfer.text'} | ${0}
    ${true}         | ${nodeApproval}  | ${ApprovalInstitutionEnum.node}          | ${'dataTransferApprovals:rejectDataTransfer.text'} | ${0}
    ${true}         | ${groupApproval} | ${ApprovalInstitutionEnum.group}         | ${'dataTransferApprovals:rejectDataTransfer.text'} | ${0}
  `(
    'Should show the form (rejection=$isRejectionForm) for $approvalType',
    async function ({
      isRejectionForm,
      approval,
      approvalInstitution,
      expected,
      len,
    }) {
      render(
        <Provider store={makeStore(undefined, getInitialState())}>
          <DataTransferApprovalForm
            dtr={{ ...dtr, purpose: DataTransferPurposeEnum.PRODUCTION }}
            approval={approval}
            onClose={() => undefined}
            open={true}
            approvalInstitution={approvalInstitution}
            isRejectionForm={isRejectionForm}
          />
        </Provider>
      );
      if (len > 0) {
        const checkboxes = await screen.findAllByRole('checkbox');
        expect(checkboxes).toHaveLength(len);
      } else {
        expect(screen.queryByRole('checkbox')).not.toBeInTheDocument();
        const textboxes = await screen.findAllByRole('textbox');
        expect(textboxes).toHaveLength(1);
      }
      expect(await screen.findByText(expected)).toBeInTheDocument();
    }
  );
});
