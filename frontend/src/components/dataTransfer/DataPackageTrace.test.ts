import {
  DataPackageTrace,
  DataPackageTraceStatusEnum,
} from '../../api/generated';
import {
  DataPackageTraceStep,
  DataPackageTraceSteps,
  makeTraceSteps,
} from './DataPackageTrace';
import { mockConsoleError } from '@biomedit/next-widgets';

describe('DataTransferTrace', () => {
  describe('makeTraceSteps', () => {
    const dataProvider = 'insel';
    const localNode = 'scicore';
    const transitNode = 'sis';
    const projectSpace = 'Project Space';

    const timestamp1Local = new Date('2020-08-19T07:30:11.031399Z');
    const timestamp2Local = new Date('2020-08-19T08:30:11.031399Z');
    const timestamp3Transit = new Date('2020-08-19T09:30:11.031399Z');
    const timestamp4Transit = new Date('2020-08-19T10:30:11.031399Z');
    const timestamp5Local = new Date('2020-08-19T11:30:11.031399Z');
    const timestamp6Transit = new Date('2020-08-19T12:30:11.031399Z');
    const timestamp7Transit = new Date('2020-08-19T13:30:11.031399Z');

    // Trace Items
    const localNodeEntered = {
      node: localNode,
      timestamp: timestamp1Local,
      status: DataPackageTraceStatusEnum.ENTER,
    };
    const localNodeProcessed: DataPackageTrace = {
      node: localNode,
      timestamp: timestamp2Local,
      status: DataPackageTraceStatusEnum.PROCESSED,
    };
    const localNodeError: DataPackageTrace = {
      node: localNode,
      timestamp: timestamp5Local,
      status: DataPackageTraceStatusEnum.ERROR,
    };

    const transitNodeEntered: DataPackageTrace = {
      node: transitNode,
      timestamp: timestamp3Transit,
      status: DataPackageTraceStatusEnum.ENTER,
    };
    const transitNodeProcessed: DataPackageTrace = {
      node: transitNode,
      timestamp: timestamp4Transit,
      status: DataPackageTraceStatusEnum.PROCESSED,
    };
    const transitNodeError: DataPackageTrace = {
      node: transitNode,
      timestamp: timestamp6Transit,
      status: DataPackageTraceStatusEnum.ERROR,
    };
    const transitNodeError2: DataPackageTrace = {
      node: transitNode,
      timestamp: timestamp7Transit,
      status: DataPackageTraceStatusEnum.ERROR,
    };

    const traceInitial: DataPackageTrace[] = [localNodeEntered];

    const traceLocalProjectSpace: DataPackageTrace[] = [
      ...traceInitial,
      localNodeProcessed,
    ];

    const traceBeforeProjectSpaceTransit: DataPackageTrace[] = [
      ...traceLocalProjectSpace,
      transitNodeEntered,
    ];

    const traceTransitNode: DataPackageTrace[] = [
      ...traceBeforeProjectSpaceTransit,
      transitNodeProcessed,
    ];

    const traceErrorLocal: DataPackageTrace[] = [
      ...traceInitial,
      localNodeError,
    ];

    const traceErrorTransitInTransit: DataPackageTrace[] = [
      ...traceBeforeProjectSpaceTransit,
      transitNodeError,
    ];

    const traceTransferredWithErrors: DataPackageTrace[] = [
      ...traceInitial,
      localNodeError,
      localNodeError,
      localNodeProcessed,
      transitNodeEntered,
      transitNodeError,
      transitNodeError2,
      transitNodeProcessed,
    ];

    const traceWithoutNode: Omit<DataPackageTrace, 'node'>[] = [
      ...traceInitial,
      {
        timestamp: timestamp7Transit,
        status: DataPackageTraceStatusEnum.ERROR,
      },
    ];

    const traceWithoutTimestamp: DataPackageTrace[] = [
      ...traceInitial,
      {
        node: localNode,
        status: DataPackageTraceStatusEnum.ENTER,
      },
    ];

    const dataProviderStep: DataPackageTraceStep = {
      location: dataProvider,
      processed: true,
    };

    const projectSpaceStep: DataPackageTraceStep = {
      location: projectSpace,
    };

    const projectSpaceEnterStep: DataPackageTraceStep = {
      ...projectSpaceStep,
      enter: true,
    };

    const outputEmpty: DataPackageTraceSteps = {
      steps: [],
      activeStepIndex: -1,
    };

    const outputLocalProjectSpace: DataPackageTraceSteps = {
      steps: [
        dataProviderStep,
        {
          location: localNode,
          enter: timestamp1Local,
          processed: timestamp2Local,
        },
        projectSpaceEnterStep,
      ],
      activeStepIndex: 2,
    };

    const outputTransitNode: DataPackageTraceSteps = {
      steps: [
        dataProviderStep,
        {
          location: localNode,
          enter: timestamp1Local,
          processed: timestamp2Local,
        },
        {
          location: transitNode,
          enter: timestamp3Transit,
          processed: timestamp4Transit,
        },
        projectSpaceEnterStep,
      ],
      activeStepIndex: 3,
    };

    const outputInitialLocal: DataPackageTraceSteps = {
      steps: [
        dataProviderStep,
        {
          location: localNode,
          enter: timestamp1Local,
        },
        projectSpaceStep,
      ],
      activeStepIndex: 1,
    };

    const outputInitialTransit: DataPackageTraceSteps = {
      steps: [
        dataProviderStep,
        {
          location: localNode,
          enter: timestamp1Local,
        },
        {
          location: transitNode,
        },
        projectSpaceStep,
      ],
      activeStepIndex: 1,
    };

    const outputBeforeProjectSpaceTransit: DataPackageTraceSteps = {
      steps: [
        dataProviderStep,
        {
          location: localNode,
          enter: timestamp1Local,
          processed: timestamp2Local,
        },
        {
          location: transitNode,
          enter: timestamp3Transit,
        },
        projectSpaceStep,
      ],
      activeStepIndex: 2,
    };

    const outputErrorLocal: DataPackageTraceSteps = {
      steps: [
        dataProviderStep,
        {
          location: localNode,
          enter: timestamp1Local,
          error: [timestamp5Local],
        },
        projectSpaceStep,
      ],
      activeStepIndex: 1,
    };

    const outputErrorLocalInTransit: DataPackageTraceSteps = {
      steps: [
        dataProviderStep,
        {
          location: localNode,
          enter: timestamp1Local,
          error: [timestamp5Local],
        },
        {
          location: transitNode,
        },
        projectSpaceStep,
      ],
      activeStepIndex: 1,
    };

    const outputErrorTransitInTransit: DataPackageTraceSteps = {
      steps: [
        dataProviderStep,
        {
          location: localNode,
          enter: timestamp1Local,
          processed: timestamp2Local,
        },
        {
          location: transitNode,
          enter: timestamp3Transit,
          error: [timestamp6Transit],
        },
        projectSpaceStep,
      ],
      activeStepIndex: 2,
    };

    const outputTransferredWithErrors: DataPackageTraceSteps = {
      steps: [
        dataProviderStep,
        {
          location: localNode,
          enter: timestamp1Local,
          error: [timestamp5Local, timestamp5Local],
          processed: timestamp2Local,
        },
        {
          location: transitNode,
          enter: timestamp3Transit,
          error: [timestamp6Transit, timestamp7Transit],
          processed: timestamp4Transit,
        },
        projectSpaceEnterStep,
      ],
      activeStepIndex: 3,
    };

    const outputWithoutTimestamp: DataPackageTraceSteps = {
      steps: [
        dataProviderStep,
        {
          location: localNode,
          enter: true,
        },
        projectSpaceStep,
      ],
      activeStepIndex: 1,
    };

    // not relevant for testing - only needed for initialization of `ProjectPackage`
    const fileName = 'abcd_DTR00001_mock_data.tar';
    const metadata = 'someMetadata';

    const descriptionWithoutNode =
      'traces without node should be ignored - an error will be logged';

    test.each`
      trace                             | destinationNode | output                             | description
      ${undefined}                      | ${undefined}    | ${outputEmpty}                     | ${'empty parameters'}
      ${[]}                             | ${undefined}    | ${outputEmpty}                     | ${'empty trace array'}
      ${traceLocalProjectSpace}         | ${localNode}    | ${outputLocalProjectSpace}         | ${'fully transferred pkg from data provider to project space (local node)'}
      ${traceTransitNode}               | ${transitNode}  | ${outputTransitNode}               | ${'fully transferred pkg from data provider to project space (transit node)'}
      ${traceInitial}                   | ${localNode}    | ${outputInitialLocal}              | ${'initial state after creation of data package by local node, with project space = local node and last state before the pkg was sent to the project space'}
      ${traceInitial}                   | ${transitNode}  | ${outputInitialTransit}            | ${'initial state after creation of data package by local node, with project space = transit node'}
      ${traceBeforeProjectSpaceTransit} | ${transitNode}  | ${outputBeforeProjectSpaceTransit} | ${'last state before pkg was sent to the project space, with project space = transit node'}
      ${traceErrorLocal}                | ${localNode}    | ${outputErrorLocal}                | ${'error while processing on local node, with project space = local node'}
      ${traceErrorLocal}                | ${transitNode}  | ${outputErrorLocalInTransit}       | ${'error while processing on local node, with project space = transit node'}
      ${traceErrorTransitInTransit}     | ${transitNode}  | ${outputErrorTransitInTransit}     | ${'error while processing on transit node, with project space = transit node'}
      ${traceTransferredWithErrors}     | ${transitNode}  | ${outputTransferredWithErrors}     | ${'errors while processing on local and transit node, but finally transferred, with project space = transit node'}
      ${traceWithoutNode}               | ${localNode}    | ${outputInitialLocal}              | ${descriptionWithoutNode}
      ${traceWithoutTimestamp}          | ${localNode}    | ${outputWithoutTimestamp}          | ${'traces without timestamp should just result in `true` being used instead.'}
    `('$description', ({ trace, destinationNode, output, description }) => {
      let spy;
      if (description === descriptionWithoutNode) {
        spy = mockConsoleError();
      }

      expect(
        makeTraceSteps({
          trace,
          destinationNode,
          dataProvider,
          fileName,
          metadata,
        })
      ).toEqual(output);

      if (spy) {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy.mock.calls[0][0]).toContain("doesn't contain a node");
        spy.mockRestore();
      }
    });

    it('should return an empty steps object if nothing is passed in', () => {
      expect(makeTraceSteps(undefined)).toEqual(outputEmpty);
    });
  });
});
