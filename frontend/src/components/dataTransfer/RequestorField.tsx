import { DataTransfer } from '../../api/generated';
import React, { ReactElement, useMemo } from 'react';
import { Tooltip } from '@biomedit/next-widgets';
import { Typography } from '@mui/material';

export function RequestorField({
  requestorFirstName,
  requestorLastName,
  requestorEmail,
  requestorDisplayId,
  requestorPgpKeyFp,
}: Pick<
  DataTransfer,
  | 'requestorFirstName'
  | 'requestorLastName'
  | 'requestorEmail'
  | 'requestorDisplayId'
  | 'requestorPgpKeyFp'
>): ReactElement {
  const name = useMemo(
    () => `${requestorFirstName} ${requestorLastName}`,
    [requestorFirstName, requestorLastName]
  );
  const fingerprint = requestorPgpKeyFp || 'No PGP fingerprint';

  const requestorRest: Array<string | undefined> = useMemo(
    () => [requestorDisplayId, requestorEmail],
    [requestorDisplayId, requestorEmail]
  );

  return (
    <Tooltip
      title={
        <>
          {requestorRest.map((part) => (
            <Typography
              key={'requestor-rest-' + part}
              variant="caption"
              display="block"
            >
              {part}
            </Typography>
          ))}
          <Typography key="fingerprint" variant="caption" display="block">
            {fingerprint}
          </Typography>
        </>
      }
    >
      <span>{name}</span>
    </Tooltip>
  );
}
