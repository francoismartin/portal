import React, { ReactElement, useMemo } from 'react';
import {
  CancelLabel,
  ColoredStatus,
  Description,
  Dialog,
  Field,
  formatItemFields,
  FormattedItemField,
  ListModel,
  ListPage,
  TabItem,
  TabPane,
} from '@biomedit/next-widgets';
import { DataPackage, DataTransfer } from '../../api/generated';
import { IdRequired } from '../../global';
import { DataPackageTrace } from './DataPackageTrace';
import { useDataTransfer, useDataTransferStatus } from './DataTransferHooks';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { styled } from '@mui/material';
import { DataTransferApprovals } from './DataTransferApprovals';
import { DataTransferPath } from './DataTransferPath';
import { RequestorField } from './RequestorField';
import { MaxPackagesField } from './MaxPackagesField';

const StyledColoredStatus = styled(ColoredStatus)(({ theme }) => ({
  paddingLeft: theme.spacing(5),
  display: 'inline',
}));

export type DataTransferDetailProps = {
  dtrId: number;
  open: boolean;
  onClose: () => void;
};

export const DataTransferDetail = ({
  dtrId,
  open,
  onClose,
}: DataTransferDetailProps): ReactElement => {
  const { t } = useTranslation(I18nNamespace.DATA_TRANSFER);

  // We fetch the DTR from Redux store by ID
  const dataTransfer = useDataTransfer(dtrId);
  const statuses = useDataTransferStatus();

  const detailEntries: FormattedItemField[] | null = useMemo(() => {
    const model: ListModel<DataTransfer> = {
      fields: [
        Field({
          caption: t<string>('columns.projectName'),
          getProperty: (dtr: DataTransfer) => dtr.projectName,
          key: 'projectName',
        }),
        Field({
          caption: t<string>('columns.transferId'),
          getProperty: (dtr: DataTransfer) => dtr.id,
          key: 'transferId',
        }),
        Field({
          caption: t<string>('columns.dataProvider'),
          getProperty: (dtr: DataTransfer) => dtr.dataProvider,
          key: 'dataProvider',
        }),
        Field({
          caption: t<string>('columns.status'),
          getProperty: (dtr: DataTransfer) => (
            <StyledColoredStatus value={dtr.status} statuses={statuses} />
          ),
          key: 'status',
        }),
        Field({
          caption: t<string>('columns.maxPackages'),
          getProperty: (dtr: DataTransfer) => dtr.maxPackages,
          render: MaxPackagesField,
          key: 'maxPackages',
        }),
        Field({
          caption: t<string>('columns.transferredPackages'),
          getProperty: (dtr: DataTransfer) => dtr.packages?.length,
          key: 'packageCount',
        }),
        Field({
          caption: t<string>('columns.requestor'),
          getProperty: (dtr: DataTransfer) => dtr,
          render: RequestorField,
          key: 'requestor',
        }),
        Field({
          caption: t<string>('columns.purpose'),
          getProperty: (dtr: DataTransfer) => dtr.purpose,
          key: 'purpose',
        }),
      ],
    };
    return formatItemFields<DataTransfer>(model, dataTransfer);
  }, [t, dataTransfer, statuses]);

  const logModel: ListModel<DataPackage> = useMemo(() => {
    return {
      getCaption: (pkg) => pkg.fileName,
      fields: [
        Field({
          getProperty: (pkg: DataPackage) => pkg,
          key: 'packages',
          render: DataPackageTrace,
        }),
      ],
    };
  }, []);

  const tabModel: TabItem[] = useMemo(() => {
    const tabItems = [
      {
        id: 'details',
        label: 'Details',
        content: (
          <>
            <Description entries={detailEntries} labelWidth={300} />
            {dataTransfer?.transferPath && (
              <DataTransferPath transferPath={dataTransfer.transferPath} />
            )}
          </>
        ),
      },
      {
        id: 'log',
        label: 'Log',
        content: (
          <>
            {dataTransfer?.packages && (
              <ListPage<IdRequired<DataPackage>>
                model={logModel}
                emptyMessage={
                  "This data transfer doesn't have any data packages yet."
                }
                itemList={dataTransfer.packages as DataPackage[]}
                canAdd={false}
                canDelete={false}
                canEdit={false}
                isFetching={false}
                isSubmitting={false}
              />
            )}
          </>
        ),
      },
    ];
    if (
      dataTransfer?.nodeApprovals?.length ||
      dataTransfer?.dataProviderApprovals?.length
    ) {
      tabItems.push({
        id: 'approvals',
        label: t(`${I18nNamespace.COMMON}:models.approval_plural`),
        content: <DataTransferApprovals dtr={dataTransfer} />,
      });
    }
    return tabItems;
  }, [t, detailEntries, dataTransfer, logModel]);

  return (
    <Dialog
      open={open}
      isSubmitting={false} // We do NOT have any submit button
      fullWidth={true}
      maxWidth={'lg'}
      title={`${dataTransfer.id} - ${dataTransfer.projectName}`}
      cancelLabel={CancelLabel.CLOSE}
      onClose={onClose}
    >
      <TabPane
        id="DataTransferDetail"
        label="Switch between details of the data transfer and the log of data packages."
        model={tabModel}
        panelBoxProps={{ sx: { height: '50vh' } }}
      />
    </Dialog>
  );
};
