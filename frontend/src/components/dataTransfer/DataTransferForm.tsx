import React, { ReactElement, useCallback, useEffect, useState } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import { Required } from 'utility-types';
import {
  AutocompleteField,
  Button,
  CheckboxField,
  Choice,
  ConfirmLabel,
  FixedChildrenHeight,
  FormDialog,
  grey,
  HiddenField,
  LabelledField,
  requestAction,
  SelectField,
  StyleMap,
  ToggleButton,
  ToggleButtonGroup,
  useEnhancedForm,
  useEnumChoices,
  useFormDialog,
} from '@biomedit/next-widgets';
import {
  ADD_DATA_TRANSFER,
  CALL,
  LOAD_DATA_PROVIDERS,
  LOAD_USERS,
  UPDATE_DATA_TRANSFER,
  LOAD_PGP_KEY_INFOS,
  CLEAR_PGP_KEY_INFOS,
} from '../../actions/actionTypes';
import {
  DataProvider,
  DataTransfer,
  DataTransferPurposeEnum,
  DataTransferStatusEnum,
  ListUsersRoleEnum,
  PgpKeyInfoStatusEnum,
} from '../../api/generated';
import { FormProvider } from 'react-hook-form';
import { TestId } from '../../testId';
import { isStaff } from '../selectors';
import { useDataProviderChoices, useUserChoices } from '../choice';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { CircularProgress, Link, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';

export enum MaxPackagesToggleState {
  UNLIMITED = -1,
  ONE_PACKAGE = 1,
}

export type UiDataTransfer = Required<
  Partial<DataTransfer>,
  'maxPackages' | 'purpose' | 'project'
>;

export type DataTransferFormProps = {
  transfer: UiDataTransfer;
  close: () => void;
};

export function useDataTransferForm(projectId: number | undefined = undefined) {
  const newDtr =
    projectId !== undefined
      ? {
          maxPackages: MaxPackagesToggleState.ONE_PACKAGE,
          project: +projectId,
          purpose: DataTransferPurposeEnum.PRODUCTION,
        }
      : undefined;
  const { closeFormDialog, data, ...rest } =
    useFormDialog<UiDataTransfer>(newDtr);
  const dtrForm = data && (
    <DataTransferForm transfer={data} close={closeFormDialog} />
  );
  return {
    dtrForm,
    ...rest,
  };
}

export const DataTransferForm = ({
  transfer,
  close,
}: DataTransferFormProps): ReactElement => {
  const { t } = useTranslation(I18nNamespace.DATA_TRANSFER);

  const isEdit = !!transfer.id;

  const dispatch = useDispatch();

  const [requestorDataLoaded, setRequestorDataLoaded] =
    useState<boolean>(false);
  const isFetchingDataProvider = useSelector(
    (state) => state.dataProvider.isFetching
  );
  const isSubmitting = useSelector((state) => state.project.isSubmitting);
  const { isFetching: isFetchingUsers, itemList: users } = useSelector(
    (state) => state.users
  );
  const dataProviders: DataProvider[] = useSelector(
    (state) => state.dataProvider.itemList
  );
  const { isFetching: isFetchingUserKeys, itemList: keys } = useSelector(
    (state) => state.pgpKeyInfo
  );
  const requestorFieldVisible = useSelector(isStaff);
  const userChoices = useUserChoices(users);
  const dataProviderChoices = useDataProviderChoices(
    dataProviders.filter((dp) => dp.coordinators && dp.coordinators.length > 0)
  );
  const dataTransferPurposeChoices = useEnumChoices(DataTransferPurposeEnum);
  const dataTransferStatusChoices = useEnumChoices(DataTransferStatusEnum);

  const form = useEnhancedForm<DataTransfer>({ defaultValues: transfer });

  const { reset, getValues, watch } = form;

  function submit(dataTransfer: DataTransfer) {
    const onSuccessAction = { type: CALL, callback: close };
    if (isEdit) {
      dispatch(
        requestAction(
          UPDATE_DATA_TRANSFER,
          {
            id: String(dataTransfer.id),
            dataTransfer,
          },
          onSuccessAction
        )
      );
    } else {
      dispatch(
        requestAction(
          ADD_DATA_TRANSFER,
          {
            dataTransfer,
          },
          onSuccessAction
        )
      );
    }
  }

  const watchRequestor = watch('requestor');

  useEffect(() => {
    batch(() => {
      if (requestorFieldVisible) {
        dispatch(
          requestAction(
            LOAD_USERS,
            {
              projectId: transfer.project,
              role: ListUsersRoleEnum.DM,
            },
            { type: CALL, callback: () => setRequestorDataLoaded(true) }
          )
        );
      } else {
        dispatch(
          requestAction(
            LOAD_PGP_KEY_INFOS,
            {
              status: PgpKeyInfoStatusEnum.APPROVED,
            },
            { type: CALL, callback: () => setRequestorDataLoaded(true) }
          )
        );
      }
      dispatch(requestAction(LOAD_DATA_PROVIDERS));
    });
  }, [
    transfer.project,
    dispatch,
    requestorFieldVisible,
    setRequestorDataLoaded,
  ]);

  useEffect(() => {
    const requestorUser = users.find((user) => user.id === watchRequestor);

    if (requestorFieldVisible && watchRequestor && requestorUser) {
      dispatch(
        requestAction(LOAD_PGP_KEY_INFOS, {
          status: PgpKeyInfoStatusEnum.APPROVED,
          username: requestorUser.username,
        })
      );
    } else {
      dispatch({ type: CLEAR_PGP_KEY_INFOS });
    }
  }, [dispatch, watchRequestor, users, requestorFieldVisible]);

  const watchDataProvider = watch('dataProvider');
  const [dataProviderCoordinators, setDataTransferCoordinators] =
    useState<string>('');
  useEffect(() => {
    setDataTransferCoordinators(
      (
        dataProviders.find((dp) => dp.code === watchDataProvider)
          ?.coordinators || []
      )
        .map((x) => `${x.firstName} ${x.lastName}`)
        .join(', ')
    );
  }, [dataProviders, watchDataProvider]);

  const watchPurpose = watch('purpose');
  const [legalBasisRequired, setLegalBasisRequired] = useState<boolean>(
    transfer.purpose === DataTransferPurposeEnum.PRODUCTION
  );
  useEffect(() => {
    setLegalBasisRequired(watchPurpose === DataTransferPurposeEnum.PRODUCTION);
  }, [watchPurpose]);

  const [maxPackagesToggleState, setMaxPackagesToggleState] =
    useState<MaxPackagesToggleState>(transfer.maxPackages);

  const handleMaxPackageStateToggled = useCallback(
    (_event, newToggleState) => {
      if (newToggleState !== null) {
        // update max packages on switching between "unlimited" and "one package"
        const formValues = getValues();
        formValues.maxPackages = newToggleState;
        reset(formValues);

        // update toggle
        setMaxPackagesToggleState(newToggleState);
      }
    },
    [setMaxPackagesToggleState, getValues, reset]
  );

  const checkForKeys = useCallback(() => {
    dispatch(requestAction(LOAD_PGP_KEY_INFOS));
  }, [dispatch]);

  if (!users) {
    return <CircularProgress />;
  }

  const keysChoices: Choice[] = keys.map((key) => ({
    key: key.fingerprint as string,
    value: key.fingerprint as string,
    label: `${key.keyUserId} <${key.keyEmail}> (${key.fingerprint})`,
  }));
  const styles: StyleMap = {
    buttons: {
      marginTop: 30,
      float: 'right',
    },
    saveButton: {
      marginLeft: 5,
    },
    firstRow: {
      marginTop: '1.5rem',
      marginBottom: '1.5rem',
    },
  };

  // in edit mode, requestor's keys are shown instead
  if (requestorDataLoaded && !keys.length && !requestorFieldVisible) {
    const docsLink = (link: string, text: string): ReactElement => {
      return (
        <Link href={link} target={'_blank'} rel={'noopener noreferrer'}>
          {text}
        </Link>
      );
    };

    const StyledTypography = styled(Typography)(({ theme }) => ({
      marginBottom: theme.spacing(1),
    }));

    return (
      <>
        <StyledTypography>{t('noKeys.intro')}</StyledTypography>
        <StyledTypography>
          {t('noKeys.upload.helper')}
          {docsLink(t('noKeys.upload.link'), t('noKeys.upload.text'))}
        </StyledTypography>
        <StyledTypography>
          {t('noKeys.create.helper')}
          {docsLink(t('noKeys.create.link'), t('noKeys.create.text'))}
        </StyledTypography>
        <Button color="primary" variant="outlined" onClick={checkForKeys}>
          {t('noKeys.button')}
        </Button>
      </>
    );
  }

  return (
    <FormProvider {...form}>
      <FormDialog
        title={isEdit ? `Data Transfer #${transfer?.id}` : 'New Data Transfer'}
        open={!!transfer && requestorDataLoaded}
        isSubmitting={isSubmitting}
        onSubmit={submit}
        onClose={close}
        confirmLabel={ConfirmLabel.SAVE}
      >
        <HiddenField
          name="project"
          initialValues={{ project: transfer.project }}
        />
        <HiddenField name="id" initialValues={{ id: transfer.id }} />
        <div style={styles.firstRow}>
          <ToggleButtonGroup
            value={maxPackagesToggleState}
            exclusive
            onChange={handleMaxPackageStateToggled}
          >
            {[
              MaxPackagesToggleState.UNLIMITED,
              MaxPackagesToggleState.ONE_PACKAGE,
            ].map((enumItem) => {
              return (
                <ToggleButton key={enumItem} value={enumItem}>
                  {t<string>('maxPackages', { context: String(enumItem) })}
                </ToggleButton>
              );
            })}
          </ToggleButtonGroup>
          <HiddenField
            name="maxPackages"
            initialValues={{
              maxPackages: maxPackagesToggleState,
            }}
          />
        </div>
        <FixedChildrenHeight>
          <SelectField
            name="dataProvider"
            label="Data Provider"
            initialValues={transfer}
            required
            isLoading={isFetchingDataProvider}
            choices={dataProviderChoices}
            fullWidth={true}
          />
          <Typography variant="inherit">
            {t<string>('coordinators')}: {dataProviderCoordinators}
          </Typography>
          <SelectField
            name="purpose"
            label="Purpose"
            initialValues={transfer}
            required
            choices={dataTransferPurposeChoices}
            disabled={!!transfer?.packages?.length}
          />
          {isEdit && (
            <SelectField
              name="status"
              label="Status"
              initialValues={transfer}
              required
              choices={dataTransferStatusChoices}
            />
          )}
          {requestorFieldVisible && (
            <AutocompleteField
              name="requestor"
              label="Requestor"
              initialValues={transfer}
              required
              testId={TestId.REQUESTOR}
              choices={userChoices}
              isLoading={isFetchingUsers}
              width="100%"
            />
          )}
          <AutocompleteField
            name="requestorPgpKeyFp"
            label="Key"
            testId={TestId.FINGERPRINT}
            initialValues={transfer}
            required
            choices={keysChoices}
            isLoading={isFetchingUserKeys}
            width="100%"
          />
          {!isEdit && (
            <CheckboxField
              name="dataSelectionConfirmation"
              label={t<string>('dataSelectionConfirmation')}
              initialValues={transfer}
              required
            />
          )}
          <Typography
            variant={'inherit'}
            sx={legalBasisRequired ? {} : { color: grey[400] }}
          >
            {t<string>('legalBasisHelp')}
          </Typography>
          <LabelledField
            name="legalBasis"
            type="text"
            label={t<string>('legalBasis')}
            initialValues={transfer}
            required={legalBasisRequired}
            disabled={!legalBasisRequired}
          />
        </FixedChildrenHeight>
      </FormDialog>
    </FormProvider>
  );
};
