import { getInitialState, makeStore } from '../../store';
import staffState from '../../../test-data/staffState.json';
import { fireEvent, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import React from 'react';
import { DataTransferDetail } from './DataTransferDetail';
import {
  DataTransfer,
  DataTransferDataProviderApprovals,
  DataTransferNodeApprovals,
  DataTransferPurposeEnum,
  DataTransferStatusEnum,
} from '../../api/generated';

const userEmail = 'one.pgp.key@sib.swiss';
const project = 1;
const projectName = 'Test';
const id = 2;
const requestorName = `One PGP Key (ID: 1) (${userEmail})`;
const requestorPgpKeyFp = '1100000000000000000000000000000000000000';
const dataProvider = 'kispi';
const node = 'sis';

const dataTransferWithoutApprovals = {
  project,
  maxPackages: -1,
  status: DataTransferStatusEnum.INITIAL,
  dataProvider,
  requestor: 1,
  packages: [],
  requestorName,
  purpose: DataTransferPurposeEnum.PRODUCTION,
  projectName,
  requestorPgpKeyFp,
  id,
  nodeApprovals: [],
  dataProviderApprovals: [],
};

const dataTransfer = Object.assign({}, dataTransferWithoutApprovals, {
  nodeApprovals: [
    {
      id: 7,
      status: 'W',
      canApprove: true,
      node: {
        id: 1,
        code: node,
        name: node,
        nodeStatus: 'ON',
        ticketingSystemEmail: 'sis@sis.ch',
      },
      type: 'H',
    } as DataTransferNodeApprovals,
  ],
  dataProviderApprovals: [
    {
      id: 7,
      status: 'W',
      canApprove: true,
      dataProvider: {
        id: 1,
        code: dataProvider,
        name: dataProvider,
        enabled: true,
        node,
      },
    } as DataTransferDataProviderApprovals,
  ],
});

const createInitialState = (dtr: DataTransfer) => {
  return {
    ...getInitialState(),
    ...staffState,
    ...{
      dataTransfers: {
        isFetching: false,
        isSubmitting: false,
        itemList: [dtr],
      },
    },
  };
};

describe('DataTransferDetail', () => {
  describe('DataTransferDetail', () => {
    it('should display details for a given DTR', async () => {
      const store = makeStore(undefined, createInitialState(dataTransfer));
      render(
        <Provider store={store}>
          <DataTransferDetail
            open={true}
            dtrId={dataTransfer.id}
            onClose={() => {
              return;
            }}
          />
        </Provider>
      );

      // Title
      await screen.findByText(`${id} - ${projectName}`);
      const tabs = await screen.findAllByRole('tab');
      expect(tabs).toHaveLength(3);

      // First tab is selected
      await screen.findByText('columns.projectName');
      await screen.findByText('columns.dataProvider');
      await screen.findByText(dataProvider);
      await screen.findByText('columns.transferredPackages');

      expect(
        screen.queryByText('common:models.node common:models.approval_plural')
      ).toBeNull();

      // Approvals tab
      fireEvent.click(tabs[2]);
      await screen.findByText(
        'common:models.node common:models.approval_plural'
      );
      // Two buttons to reject
      expect(await screen.findAllByText('actionButtons.reject')).toHaveLength(
        2
      );
      // Two buttons to approve
      expect(await screen.findAllByText('actionButtons.approve')).toHaveLength(
        2
      );
    });

    it('should NOT display approvals for DTRs (without approvals)', async () => {
      const store = makeStore(
        undefined,
        createInitialState(dataTransferWithoutApprovals)
      );
      render(
        <Provider store={store}>
          <DataTransferDetail
            open={true}
            dtrId={dataTransferWithoutApprovals.id}
            onClose={() => {
              return;
            }}
          />
        </Provider>
      );

      // Title
      await screen.findByText(`${id} - ${projectName}`);
      const tabs = await screen.findAllByRole('tab');
      // No approvals tab
      expect(tabs).toHaveLength(2);
    });
  });
});
