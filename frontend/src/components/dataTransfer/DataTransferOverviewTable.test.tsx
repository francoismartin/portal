import React from 'react';
import { Provider } from 'react-redux';
import { render, screen, waitFor } from '@testing-library/react';
import { rest } from 'msw';
import {
  MockServer,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { DataTransferDetail } from './DataTransferDetail';
import { DataTransferOverviewTable } from './DataTransferOverviewTable';
import { backend } from '../../api/api';
import { makeStore } from '../../store';
import listDataProviderResponse from '../../../test-data/listDataProviderResponse.json';
import listDataTransferResponse from '../../../test-data/listDataTransfers.json';

jest.mock('./DataTransferDetail', () => {
  const originalModule = jest.requireActual('./DataTransferDetail');

  return {
    __esModule: true,
    ...originalModule,
    DataTransferDetail: jest.fn((props) => `${props?.dtrId}`),
  };
});

describe('DataTransferOverviewTable', () => {
  const server: MockServer = setupMockApi();

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  describe('No data transfers', () => {
    beforeAll(() => {
      server.use(
        rest.get(`${backend}data-transfer/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, []))
        )
      );
    });

    it('should display empty list', async () => {
      render(
        <Provider store={makeStore()}>
          <DataTransferOverviewTable />
        </Provider>
      );
      // Empty message (NOT i18n'ed)
      await screen.findByText('list:emptyMessage');
    });
  });

  describe('Some data transfers', () => {
    beforeEach(() => {
      server.use(
        rest.get(`${backend}data-transfer/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, listDataTransferResponse))
        ),
        rest.get(`${backend}data-provider/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, listDataProviderResponse))
        )
      );
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test.each`
      props                 | output       | description
      ${{ selected: '44' }} | ${'44'}      | ${'existing object'}
      ${{ selected: '42' }} | ${undefined} | ${'non-existing object'}
      ${{}}                 | ${undefined} | ${'nothing selected'}
    `('$description', async ({ props, output }) => {
      render(
        <Provider store={makeStore()}>
          <DataTransferOverviewTable {...props} />
        </Provider>
      );
      await waitFor(() => {
        if (output) {
          expect(DataTransferDetail).toHaveReturnedWith(output);
        } else {
          expect(DataTransferDetail).not.toHaveReturned();
        }
      });
    });
  });
});
