import React, { ReactElement } from 'react';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { MaxPackagesToggleState } from './DataTransferForm';

export function MaxPackagesField(maxPackages: number): ReactElement {
  const { t } = useTranslation(I18nNamespace.DATA_TRANSFER);
  return (
    <>
      {maxPackages === MaxPackagesToggleState.UNLIMITED
        ? t<string>('maxPackages', { context: String(maxPackages) })
        : maxPackages}
    </>
  );
}
