import { Column } from 'react-table';
import { Required } from 'utility-types';
import {
  DataTransfer,
  DataTransferStatusEnum,
  Project,
} from '../../api/generated';
import { useCallback, useMemo } from 'react';
import { MaxPackagesToggleState, UiDataTransfer } from './DataTransferForm';
import {
  ColoredStatus,
  formatDate,
  GlobalFilterFunction,
  isInteger,
  Status,
  text,
} from '@biomedit/next-widgets';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { RootState } from '../../store';
import { createSelector } from 'reselect';
import { useParamSelector } from '../selectors';
import { DataProviderField } from './DataProviderField';
import { RequestorField } from './RequestorField';

/**
 * '-1' (standing for 'unlimited'/'infinity') gets converted into 'Number.MAX_SAFE_INTEGER'
 * for sorting purposes.
 * We can not use 'infinity'. Otherwise addition (in sorting) will not work.
 */
const maxPackagesValueOrInfinity = (value: number): number =>
  value === MaxPackagesToggleState.UNLIMITED ? Number.MAX_SAFE_INTEGER : value;

export const useApprovalStatus = () => {
  const { t } = useTranslation([I18nNamespace.DATA_TRANSFER]);

  return useMemo<Status<string>[]>(
    () => [
      {
        color: 'info',
        text: t(`actionButtons.W`).toUpperCase(),
        value: 'W',
      },
      {
        color: 'success',
        text: t(`actionButtons.A`).toUpperCase(),
        value: 'A',
      },
      {
        color: 'error',
        text: t(`actionButtons.R`).toUpperCase(),
        value: 'R',
      },
      {
        text: t(`actionButtons.B`).toUpperCase(),
        value: 'B',
      },
    ],
    [t]
  );
};

export const useDataTransferStatus = () =>
  useMemo<Status<DataTransferStatusEnum>[]>(
    () => [
      {
        color: 'info',
        value: DataTransferStatusEnum.INITIAL,
      },
      {
        color: 'success',
        value: DataTransferStatusEnum.AUTHORIZED,
      },
      {
        value: DataTransferStatusEnum.EXPIRED,
      },
      {
        color: 'error',
        value: DataTransferStatusEnum.UNAUTHORIZED,
      },
    ],
    []
  );

export const useDataTransferColumns = (
  withProjectName: boolean
): Array<Column<Required<DataTransfer>>> => {
  const { t } = useTranslation(I18nNamespace.DATA_TRANSFER);
  return useMemo(() => {
    const columns: Array<Column<Required<DataTransfer>>> = [
      {
        id: 'id',
        Header: t<string>('columns.transferId'),
        accessor: 'id',
      },
      {
        id: 'dataProvider',
        Header: t<string>('columns.dataProvider'),
        accessor: 'dataProvider',
        Cell: (row) => DataProviderField({ code: row.value }),
        sortType: 'caseInsensitive',
      },
      {
        id: 'maxPackages',
        Header: t<string>('columns.maxPackages'),
        accessor: (value: UiDataTransfer) => {
          const maxPackages = value.maxPackages;
          return maxPackages === MaxPackagesToggleState.UNLIMITED
            ? t<string>('maxPackages', { context: String(maxPackages) })
            : maxPackages;
        },
        sortType: (rowA, rowB) => {
          const a = maxPackagesValueOrInfinity(rowA.original.maxPackages);
          const b = maxPackagesValueOrInfinity(rowB.original.maxPackages);
          return a - b;
        },
      },
      {
        id: 'requestorName',
        Header: t<string>('columns.requestor'),
        // value returned by accessor is used for sorting and filtering and represents the array `row.value` in `Cell`
        accessor: (data: UiDataTransfer) => [
          data.requestorFirstName,
          data.requestorLastName,
          data.requestorEmail,
          data.requestorDisplayId,
          data.requestorPgpKeyFp,
        ],
        Cell: (row) =>
          RequestorField({
            requestorFirstName: row.value[0],
            requestorLastName: row.value[1],
            requestorEmail: row.value[2],
            requestorDisplayId: row.value[3],
            requestorPgpKeyFp: row.value[4],
          }),
        sortType: (rowA, rowB) => {
          const fullName = (row) =>
            (
              row.original.requestorFirstName + row.original.requestorLastName
            ).toLowerCase();
          return fullName(rowA).localeCompare(fullName(rowB));
        },
      },
      {
        id: 'purpose',
        Header: t<string>('columns.purpose'),
        accessor: 'purpose',
      },
      {
        id: 'changeDate',
        Header: t<string>('columns.changeDate'),
        accessor: 'changeDate',
        Cell: (row) => formatDate(row.value, true) ?? null,
        sortType: 'datetime',
      },
      {
        id: 'status',
        Header: t<string>('columns.status'),
        accessor: 'status',
        Cell: (row) =>
          ColoredStatus({
            value: row.value,
            statuses: useDataTransferStatus(),
          }),
      },
    ];

    if (withProjectName) {
      columns.unshift({
        id: 'projectName',
        Header: t<string>('columns.projectName'),
        accessor: 'projectName',
        sortType: 'caseInsensitive',
      });
    }

    return columns;
  }, [t, withProjectName]);
};
export const useGlobalDataTransferFilter = (): GlobalFilterFunction<
  Required<DataTransfer>
> =>
  useCallback<GlobalFilterFunction<Required<DataTransfer>>>(
    (rows, columnIds, filterValue) => {
      if (isInteger(filterValue)) {
        return rows.filter(
          (row) =>
            row.values['id'] == filterValue ||
            row.values['maxPackages'] == filterValue
        );
      } else {
        return text(rows, columnIds, filterValue);
      }
    },
    []
  );

const getDataTransfers = (state: RootState) => state.dataTransfers.itemList;
const getDtrIdParam = (_, dtrId): number => dtrId;
const getProjectParam = (_, prj): Project => prj;
const findDataTransferById = createSelector(
  getDataTransfers,
  getDtrIdParam,
  (dataTransfers: DataTransfer[], dtrId: number) =>
    dataTransfers.find((dataTransfer) => dataTransfer.id === dtrId)
);
const filterDataTransfersByProject = createSelector(
  getDataTransfers,
  getProjectParam,
  (dataTransfers: DataTransfer[], prj: Project) =>
    dataTransfers.filter((dtr) => dtr.project === prj.id)
);

export const useDataTransfer = (dtrId: number): DataTransfer => {
  return useParamSelector(findDataTransferById, dtrId);
};

export const useProjectDataTransfers = (project: Project) => {
  return useParamSelector(filterDataTransfersByProject, project);
};
