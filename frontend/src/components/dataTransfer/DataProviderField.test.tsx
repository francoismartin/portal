import { backend } from '../../api/api';
import {
  MockServer,
  RequestVerifier,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { Provider } from 'react-redux';
import { makeStore } from '../../store';
import React from 'react';
import listDataProviderResponse from '../../../test-data/listDataProviderResponse.json';
import { rest } from 'msw';
import { render, waitFor } from '@testing-library/react';
import { DataProviderField } from './DataProviderField';

describe('DataProviderField', () => {
  const server: MockServer = setupMockApi();
  const listDataProviders = `${backend}data-provider/`;

  beforeEach(() => {
    server.use(
      rest.get(listDataProviders, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listDataProviderResponse))
      )
    );
  });

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  describe('fetchDataProviders', () => {
    it('should only perform one API call even if DataProviderField is rendered multiple times', async () => {
      const verifier = new RequestVerifier(server);
      verifier.assertCount(0);
      // The additional '<DataProviderField />' is intentional!
      render(
        <Provider store={makeStore()}>
          <DataProviderField />
          <DataProviderField />
        </Provider>
      );
      await waitFor(() => {
        verifier.assertCount(1);
        verifier.assertCalled(listDataProviders, 1);
      });
    });
  });
});
