import React from 'react';
import {
  configure,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { DataTransferForm } from './DataTransferForm';
import {
  expectQueryParameter,
  mockConsoleWarn,
  MockServer,
  openAutocompleteDropdown,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { Provider } from 'react-redux';
import { backend } from '../../api/api';
import {
  DataTransferPurposeEnum,
  DataTransferStatusEnum,
  ListUsersRoleEnum,
  PgpKeyInfoStatusEnum,
} from '../../api/generated';
import { getInitialState, makeStore, RootState } from '../../store';
import { TestId } from '../../testId';
import listDataProviderResponse from '../../../test-data/listDataProviderResponse.json';
import staffState from '../../../test-data/staffState.json';
import { rest } from 'msw';

const requestorLabel = 'Requestor';
const keyLabel = 'Key';

const projectId = 1;
const transferId = 2;

const user1Email = 'one.pgp.key@sib.swiss';
const user2Email = 'two.pgp.keys@sib.swiss';

const user1Username = '1@eduid.ch';
const user2Username = '2@eduid.ch';

const user1Key1 = '1100000000000000000000000000000000000000';
const user2Key1 = '2100000000000000000000000000000000000000';
const user2Key2 = '2200000000000000000000000000000000000000';

const user1Key1Name = `One PGP Key 1 <${user1Email}>`;
const user2Key1Name = `Two PGP Keys 1 <${user2Email}>`;

const user1Key1Label = `${user1Key1Name} (${user1Key1})`;
const user2Key1Label = `${user2Key1Name} (${user2Key1})`;

const requestor1Name = `One PGP Key (ID: 1) (${user1Email})`;
const requestor2Name = `Two PGP Keys (ID: 2) (${user2Email})`;

const dataTransfer = {
  project: projectId,
  maxPackages: -1,
  status: DataTransferStatusEnum.INITIAL,
  dataProvider: 'kispi',
  requestor: 1,
  packages: [],
  requestorName: requestor1Name,
  purpose: DataTransferPurposeEnum.PRODUCTION,
  projectName: 'Test',
  requestorPgpKeyFp: user1Key1,
  id: transferId,
};

const usersResponse = [
  {
    url: 'http://localhost:8000/backend/users/1/',
    id: 1,
    username: user1Username,
    email: user1Email,
    first_name: 'One',
    last_name: 'PGP Key',
    profile: {
      affiliation: 'affiliate@sib.swiss',
      local_username: 'onekey',
      display_name: requestor1Name,
      uid: 1000001,
      gid: 1000001,
      namespace: 'ch',
      display_local_username: 'ch_onekey',
    },
  },
  {
    url: 'http://localhost:8000/backend/users/2/',
    id: 2,
    username: user2Username,
    email: user2Email,
    first_name: 'Two',
    last_name: 'PGP Keys',
    profile: {
      affiliation: 'affiliate@sib.swiss',
      local_username: 'twokeys',
      display_name: requestor2Name,
      uid: 1000002,
      gid: 1000002,
      namespace: 'ch',
      display_local_username: 'ch_twokeys',
    },
  },
];

const pgpKeyResponse1 = [
  {
    fingerprint: user1Key1,
    key_user_id: 'One PGP Key 1',
    key_email: user1Email,
    status: PgpKeyInfoStatusEnum.APPROVED,
  },
];

const pgpKeyResponse2 = [
  {
    fingerprint: user2Key1,
    key_user_id: 'Two PGP Keys 1',
    key_email: user2Email,
    status: PgpKeyInfoStatusEnum.APPROVED,
  },
  {
    fingerprint: user2Key2,
    key_user_id: 'Two PGP Keys 2',
    key_email: user2Email,
    status: PgpKeyInfoStatusEnum.APPROVED,
  },
];

describe('DataTransferForm', () => {
  let store;
  const server: MockServer = setupMockApi();

  beforeAll(() => {
    server.use(
      rest.get(`${backend}users/`, (req, res, ctx) => {
        const expectedRole = ListUsersRoleEnum.DM;
        const actualRole = req.url.searchParams.get('role');
        const expectedProjectId = String(projectId);
        const actualProjectId = req.url.searchParams.get('project_id');
        if (
          actualRole === expectedRole &&
          actualProjectId === expectedProjectId
        ) {
          return res(resJson(ctx), resBody(ctx, usersResponse));
        } else {
          console.error(
            `MockAPI: Expected project_id to be '${expectedProjectId}' and role to be '${expectedRole}', but instead got project_id '${actualProjectId}' and role ${actualRole}`
          );
          throw req;
        }
      }),
      rest.get(`${backend}data-provider/`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listDataProviderResponse))
      ),
      rest.get(`${backend}pgpkey`, (req, res, ctx) => {
        expectQueryParameter(req, 'status', 'APPROVED');

        const username = req.url.searchParams.get('username');
        if (username === user1Username) {
          return res(resJson(ctx), resBody(ctx, pgpKeyResponse1));
        } else if (username === user2Username) {
          return res(resJson(ctx), resBody(ctx, pgpKeyResponse2));
        }
      })
    );

    configure({
      asyncUtilTimeout: 2000,
    });
  });

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  const initialState: RootState = {
    ...getInitialState(),
    ...staffState,
  };

  beforeEach(async () => {
    store = makeStore(undefined, initialState);
  });

  describe('Edit', () => {
    it('should clear the key field when requestor is changed', async () => {
      const spy = mockConsoleWarn();

      render(
        <Provider store={store}>
          <DataTransferForm
            transfer={dataTransfer}
            close={() => {
              return;
            }}
          />
        </Provider>
      );

      // default (from API call): Requestor 1 and Key 1
      await screen.findByDisplayValue(user1Key1Label);
      await screen.findByDisplayValue(requestor1Name);

      // change requestor to 2
      openAutocompleteDropdown(requestorLabel);
      fireEvent.click(await screen.findByText(requestor2Name));
      await screen.findByDisplayValue(requestor2Name);

      // expect requestor to be 2, and key to be cleared
      await waitFor(() => {
        expect(screen.getByTestId(TestId.REQUESTOR)).toHaveValue(
          requestor2Name
        );
        expect(screen.getByTestId(TestId.FINGERPRINT)).toHaveValue('');
      });

      // set key to 1 (of requestor 2)
      openAutocompleteDropdown(keyLabel);
      fireEvent.click(await screen.findByText(user2Key1Label));

      // expect key to be 1 (of requestor 2)
      await screen.findByDisplayValue(user2Key1Label);

      // change requestor to 1
      openAutocompleteDropdown(requestorLabel);
      fireEvent.click(await screen.findByText(requestor1Name));

      // expect requestor to be 1, and key to be cleared
      await waitFor(() => {
        expect(screen.getByTestId(TestId.REQUESTOR)).toHaveValue(
          requestor1Name
        );
        expect(screen.getByTestId(TestId.FINGERPRINT)).toHaveValue('');
      });

      expect(spy).toHaveBeenCalledTimes(2);
      const calls = spy.mock.calls;
      const warn0 = calls[0][0];
      const warn1 = calls[1][0];
      expect(warn0).toContain(
        `{"key":"${user1Key1}","value":"${user1Key1}","label":"${user1Key1Label}"}`
      );
      expect(warn1).toContain(
        `{"key":"${user2Key1}","value":"${user2Key1}","label":"${user2Key1Label}"}`
      );

      spy.mockRestore();
    }, 10000);
  });
});
