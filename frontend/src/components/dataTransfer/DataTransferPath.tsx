import React, { ReactElement } from 'react';
import { Step, Stepper } from '@biomedit/next-widgets';
import { Typography } from '@mui/material';

export type DataTransferPathProps = {
  transferPath: Array<string>;
};

export const DataTransferPath = ({
  transferPath,
}: DataTransferPathProps): ReactElement => {
  return (
    <Stepper activeStep={-1}>
      {transferPath.map((step) => (
        <Step key={'step-' + step} active>
          <Typography key={'label-' + step} variant={'subtitle1'}>
            {step}
          </Typography>
        </Step>
      ))}
    </Stepper>
  );
};
