import {
  addExcelSeparatorHeader,
  dataTransfersToCsv,
} from './DataTransferExport';
import listDataTransfers from '../../../test-data/listDataTransfers.json';
import { DataTransfer } from '../../api/generated';
import { getTestFileAsString } from '../../testUtils';
import cloneDeep from 'lodash/cloneDeep';

describe('DataTransferExport', () => {
  describe('dataTransfersToCsv', () => {
    it('should correctly transform DataTransfers into csv format', () => {
      const copy = cloneDeep(listDataTransfers);
      const expectedCsv = getTestFileAsString('listDataTransfers.csv');
      const actual = dataTransfersToCsv(listDataTransfers as DataTransfer[]);
      expect(actual).toEqual(expectedCsv);
      expect(listDataTransfers).toEqual(copy); // passed in dtr's are unchanged
    });
  });

  describe('addExcelSeparatorHeader', () => {
    it('should add line with excel separator header before actual csv content', () => {
      expect(addExcelSeparatorHeader('Some Text\non a new line.')).toBe(
        'sep=,\nSome Text\non a new line.'
      );
    });
  });
});
