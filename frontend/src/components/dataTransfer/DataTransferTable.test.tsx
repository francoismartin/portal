import { DataTransferTable } from './DataTransferTable';
import {
  DataTransfer,
  DataTransferPurposeEnum,
  DataTransferStatusEnum,
  Project,
} from '../../api/generated';
import { DeepWriteable, setInputValue } from '@biomedit/next-widgets';
import { Provider } from 'react-redux';
import { getInitialState, makeStore } from '../../store';
import React from 'react';
import { render, screen } from '@testing-library/react';

const getDtr = (
  id: number,
  maxPackages: number
): DeepWriteable<DataTransfer> => ({
  project: 1,
  maxPackages: maxPackages,
  status: DataTransferStatusEnum.INITIAL,
  dataProvider: 'uzh',
  requestor: 2,
  purpose: DataTransferPurposeEnum.TEST,
  requestorPgpKeyFp: '9E10FE436DA14366B6254861E75A30416DE01D17',
  id,
  packages: [],
  requestorName:
    'Chuck Norris (ID: 63457666153) (chuck.norris@roundhouse.kick)',
  requestorDisplayId: 'ID: 63457666153',
  requestorFirstName: 'Chuck',
  requestorLastName: 'Norris',
  requestorEmail: 'chuck.norris@roundhouse.kick',
  projectName: 'Test Project 1',
  nodeApprovals: [],
  dataProviderApprovals: [],
});

const dataTransfers = [getDtr(1, -1), getDtr(5, 1), getDtr(15, -1)];

describe('DataTransferTable', () => {
  describe('Component', () => {
    describe('Global Filter', () => {
      const project: Project = {
        code: 'test_project_1',
        name: 'Test Project 1',
        id: 1,
        users: [],
      };

      const assertDtrVisible = async (
        accessibleName: string,
        isVisible: boolean
      ) => {
        const row = await screen.queryByRole('row', {
          name: accessibleName,
        });
        if (isVisible) {
          expect(row).toBeInTheDocument();
        } else {
          expect(row).not.toBeInTheDocument();
        }
      };

      const state = {
        ...getInitialState(),
        ...{
          dataTransfers: {
            isFetching: false,
            isSubmitting: false,
            itemList: dataTransfers,
          },
        },
      };
      beforeEach(() => {
        render(
          <Provider store={makeStore(undefined, state)}>
            <DataTransferTable {...project} />
          </Provider>
        );
      });

      const displayId = dataTransfers[0].requestorDisplayId;
      const pgpKeyFp = dataTransfers[0].requestorPgpKeyFp;

      it.each`
        searchQuery  | dtr1Visible | dtr5Visible | dtr15Visible | description
        ${undefined} | ${true}     | ${true}     | ${true}      | ${'show all DTRs by default'}
        ${'1'}       | ${true}     | ${true}     | ${false}     | ${'match DTR 1 (by id) and DTR 5 (maxPackages)'}
        ${'5'}       | ${false}    | ${true}     | ${false}     | ${'only match DTR with id 5'}
        ${'15'}      | ${false}    | ${false}    | ${true}      | ${'only match DTR with id 15'}
        ${displayId} | ${true}     | ${true}     | ${true}      | ${'show all DTRs (as all are by the same requestor)'}
        ${pgpKeyFp}  | ${true}     | ${true}     | ${true}      | ${'show all DTRs (as all are by the same requestor)'}
      `(
        'should $description when searching for $searchQuery',
        async ({ searchQuery, dtr1Visible, dtr5Visible, dtr15Visible }) => {
          const search = screen.getByRole('textbox', { name: 'search' });

          if (searchQuery) {
            setInputValue(search, searchQuery);
          }

          await assertDtrVisible(
            '1 uzh maxPackages Chuck Norris TEST INITIAL',
            dtr1Visible
          );
          await assertDtrVisible(
            '5 uzh 1 Chuck Norris TEST INITIAL',
            dtr5Visible
          );
          await assertDtrVisible(
            '15 uzh maxPackages Chuck Norris TEST INITIAL',
            dtr15Visible
          );
        }
      );
    });
  });
});
