import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import listDataTransfers from '../../../test-data/listDataTransfers.json';
import { Provider } from 'react-redux';
import { getInitialState, makeStore } from '../../store';
import { DataTransferApprovals } from './DataTransferApprovals';
import { DataPackagePurposeEnum } from '../../api/generated';

describe('DataTransferApprovals', function () {
  it.each`
    isRejectionForm | dtr                     | buttonLabel                | expected
    ${false}        | ${listDataTransfers[2]} | ${'actionButtons.approve'} | ${'dataTransferApprovals:declarations.title'}
    ${true}         | ${listDataTransfers[2]} | ${'actionButtons.reject'}  | ${'dataTransferApprovals:rejectDataTransfer.text'}
  `(
    'Should show the form for approving data transfer',
    async function ({ dtr, buttonLabel, expected }) {
      render(
        <Provider store={makeStore(undefined, getInitialState())}>
          <DataTransferApprovals dtr={dtr} />
        </Provider>
      );
      expect(dtr.purpose).toBe(DataPackagePurposeEnum.PRODUCTION);

      const buttons = screen.getAllByText(buttonLabel);
      expect(buttons).toHaveLength(3);

      fireEvent.click(buttons[0]);
      expect(await screen.findByText(expected)).toBeInTheDocument();
    }
  );
});
