import { expectAllToBeInTheDocument } from '@biomedit/next-widgets';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import React from 'react';
import { getInitialState, makeStore } from '../store';
import staffState from '../../test-data/staffState.json';
import {
  affiliationConsentLabel,
  dialogTitle,
  usernameLabel,
  UserProfileDialog,
} from './UserProfileDialog';
import produce from 'immer';

describe('UserProfileDialog', () => {
  describe('Component', () => {
    const username = 'chucknorris';

    it.each`
      localUsername | affiliationConsent | affiliationConsentRequired | isDialogOpen | isPromptedUsername | isPromptedAffiliation
      ${undefined}  | ${false}           | ${false}                   | ${true}      | ${true}            | ${false}
      ${undefined}  | ${true}            | ${false}                   | ${true}      | ${true}            | ${false}
      ${username}   | ${false}           | ${false}                   | ${false}     | ${false}           | ${false}
      ${username}   | ${true}            | ${false}                   | ${false}     | ${false}           | ${false}
      ${undefined}  | ${false}           | ${true}                    | ${true}      | ${true}            | ${true}
      ${undefined}  | ${true}            | ${true}                    | ${true}      | ${true}            | ${false}
      ${username}   | ${false}           | ${true}                    | ${true}      | ${false}           | ${true}
      ${username}   | ${true}            | ${true}                    | ${false}     | ${false}           | ${false}
    `(
      'should show user profile dialog ($isDialogOpen), ' +
        'include a field to enter a username ($isPromptedUsername) and ' +
        'ask for affiliation consent ($isPromptedAffiliation),' +
        'when localUsername is $localUsername, ' +
        'affiliationConsent is $affiliationConsent and ' +
        'affiliationConsentRequired is $affiliationConsentRequired',
      async ({
        localUsername,
        affiliationConsent,
        affiliationConsentRequired,
        isDialogOpen,
        isPromptedUsername,
        isPromptedAffiliation,
      }) => {
        const preloadedState = produce(
          {
            ...getInitialState(),
            ...staffState,
          },
          (draft) => {
            draft.auth.user.profile.localUsername = localUsername;
            draft.auth.user.profile.affiliationConsent = affiliationConsent;
            draft.auth.user.profile.affiliationConsentRequired =
              affiliationConsentRequired;
          }
        );

        render(
          <Provider store={makeStore(undefined, preloadedState)}>
            <UserProfileDialog />
          </Provider>
        );

        expectAllToBeInTheDocument(
          await screen.queryAllByText(dialogTitle),
          isDialogOpen
        );
        expectAllToBeInTheDocument(
          await screen.queryAllByText(usernameLabel),
          isPromptedUsername
        );
        expectAllToBeInTheDocument(
          await screen.queryAllByText(affiliationConsentLabel),
          isPromptedAffiliation
        );
      }
    );
  });
});
