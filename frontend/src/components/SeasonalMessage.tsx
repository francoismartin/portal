import React, { ReactElement, useMemo, useRef, useCallback } from 'react';
import {
  isChristmas,
  isEaster,
  isNewYear,
  isNationalDay,
  isHalloween,
} from '@biomedit/next-widgets';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../i18n';
import { EffectRef, EffectsOverlay } from '../widgets/effects/EffectsOverlay';
import { styled } from '@mui/material/styles';
import { EffectCommand } from '../widgets/effects';

const EasterEgg = styled('span')(() => ({
  cursor: 'pointer',
}));

declare type MessageProps = {
  message: string;
  easterEggEmoji: string;
  easterEggEffect: EffectCommand;
};

function Message({
  message,
  easterEggEmoji,
  easterEggEffect,
}: MessageProps): ReactElement {
  const { t } = useTranslation(I18nNamespace.HOME);
  const effectRef = useRef<EffectRef>(null);

  const onEasterEggClick = useCallback(() => {
    effectRef.current?.start(easterEggEffect);
  }, [easterEggEffect]);

  return (
    <>
      <EffectsOverlay ref={effectRef} />
      <h3>
        {t(message)}
        <>
          {' '}
          <EasterEgg onClick={onEasterEggClick}>{t(easterEggEmoji)}</EasterEgg>
        </>
      </h3>
    </>
  );
}

export const SeasonalMessage = (): ReactElement => {
  const christmas = useMemo(() => {
    return isChristmas();
  }, []);

  const easter = useMemo(() => {
    return isEaster();
  }, []);

  const newYear = useMemo(() => {
    return isNewYear();
  }, []);

  const nationalDay = useMemo(() => {
    return isNationalDay();
  }, []);

  const halloween = useMemo(() => {
    return isHalloween();
  }, []);

  return (
    <>
      {easter && (
        <Message
          message={'seasonal.easter'}
          easterEggEmoji={'seasonal.easterEgg'}
          easterEggEffect={EffectCommand.CONFETTI}
        />
      )}
      {nationalDay && (
        <Message
          message={'seasonal.nationalDay'}
          easterEggEmoji={'seasonal.fireworks'}
          easterEggEffect={EffectCommand.FIREWORKS}
        />
      )}
      {halloween && (
        <Message
          message={'seasonal.halloween'}
          easterEggEmoji={'seasonal.ghost'}
          easterEggEffect={EffectCommand.SPACEINVADERS}
        />
      )}
      {christmas && (
        <Message
          message={'seasonal.christmas'}
          easterEggEmoji={'seasonal.gift'}
          easterEggEffect={EffectCommand.SNOWFALL}
        />
      )}
      {newYear && (
        <Message
          message={'seasonal.newYear'}
          easterEggEmoji={'seasonal.fireworks'}
          easterEggEffect={EffectCommand.FIREWORKS}
        />
      )}
    </>
  );
};
