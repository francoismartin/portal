import React, { ForwardedRef, ReactElement } from 'react';
import { useSelector } from 'react-redux';
import {
  Description,
  FormattedItemField,
  PaddingBox,
} from '@biomedit/next-widgets';
import { User, Userinfo } from '../../api/generated';
import {
  getAffiliation,
  getEduidUsername,
  getFullName,
  getLocalUsername,
} from '../../utils';
import { I18nNamespace } from '../../i18n';
import { useTranslation } from 'next-i18next';
import { addMultiValueField } from '../../widgets/UserRoleEntries';

function isUserInfo(userOrInfo: User | Userinfo): userOrInfo is Userinfo {
  return (
    !!userOrInfo &&
    ('manages' in userOrInfo ||
      'permissions' in userOrInfo ||
      'ipAddress' in userOrInfo)
  );
}

export const emailFieldCaption = `${I18nNamespace.COMMON}:email`;
export const flagFieldCaption = `${I18nNamespace.COMMON}:models.flag_plural`;
export const groupFieldCaption = `${I18nNamespace.COMMON}:models.group_plural`;
export const affiliationFieldCaption = `${I18nNamespace.USER_INFO}:affiliation_plural`;
export const displayNameFieldCaption = `${I18nNamespace.USER_INFO}:displayName`;
export const eduidUsernameFieldCaption = `${I18nNamespace.USER_INFO}:eduidUsername`;
export const ipAddressFieldCaption = `${I18nNamespace.USER_INFO}:ipAddress`;
export const localUsernameFieldCaption = `${I18nNamespace.USER_INFO}:localUsername`;

export function useUserInfoFields(
  userInfo: User | Userinfo | null
): FormattedItemField[] {
  const { t } = useTranslation([I18nNamespace.COMMON, I18nNamespace.USER_LIST]);
  if (!userInfo) {
    return [];
  }
  const { email } = userInfo;
  const localUsername = getLocalUsername(userInfo);
  const eduidUsername = getEduidUsername(userInfo);
  const affiliation = getAffiliation(userInfo);
  const displayName = getFullName(userInfo);

  const fields: FormattedItemField[] = [
    { caption: t(displayNameFieldCaption), component: displayName },
    { caption: t(eduidUsernameFieldCaption), component: eduidUsername },
    { caption: t(localUsernameFieldCaption), component: localUsername ?? '' },
    { caption: t(emailFieldCaption), component: email },
    { caption: t(affiliationFieldCaption), component: affiliation },
  ];

  if (isUserInfo(userInfo)) {
    fields.push({
      caption: t(ipAddressFieldCaption),
      component: userInfo.ipAddress,
    });

    // Add "Groups" and "Flags" fields to the Userinfo box that lists all the
    // groups a user belongs to, and all flags that are active for the user.
    addMultiValueField(t(groupFieldCaption), userInfo.groups, fields);
    addMultiValueField(t(flagFieldCaption), userInfo.flags, fields);
  }

  return fields.filter(({ component }) => component !== undefined);
}

export const UserInfoBox = React.forwardRef(
  (props, ref: ForwardedRef<HTMLDivElement>): ReactElement => {
    const userInfo = useSelector((state) => state.auth.user);
    const userInfoEntries = useUserInfoFields(userInfo);

    return (
      <PaddingBox ref={ref}>
        <Description
          title="Userinfo"
          entries={userInfoEntries}
          labelWidth="50%"
        />
      </PaddingBox>
    );
  }
);
UserInfoBox.displayName = 'UserInfoBox';
