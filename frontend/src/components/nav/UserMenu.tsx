import React, { ReactElement, useEffect, useMemo, useState } from 'react';
import { styled } from '@mui/material/styles';
import Link from 'next/link';
import { useDispatch, useSelector } from 'react-redux';
import { LOAD_MESSAGES } from '../../actions/actionTypes';
import { Message, MessageStatusEnum } from '../../api/generated';
import {
  isChristmas,
  isEaster,
  Markdown,
  PaddingBox,
  requestAction,
} from '@biomedit/next-widgets';
import AccountCircle from '@mui/icons-material/AccountCircle';
import Announcement from '@mui/icons-material/Announcement';
import ChatBubble from '@mui/icons-material/ChatBubble';
import IconButton from '@mui/material/IconButton';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import MaterialUiList from '@mui/material/List';
import Menu from '@mui/material/Menu';
import Popover from '@mui/material/Popover';
import { Divider, Typography } from '@mui/material';

const UserMenuContainer = styled('div')(({ theme }) => ({
  ...{
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    paddingLeft: theme.spacing(0.5),
    backgroundColor: theme.palette.grey['200'],
  },
  ...(isChristmas() && {
    backgroundImage: `url(/bg_snow.png);`,
  }),
  ...(isEaster() && {
    background:
      'linear-gradient(#e3e7e8b8, #e3e7e8b8), url(/bg_easter.jpg) no-repeat top right;',
  }),
}));

const ChristmasHat = styled('img')(() => ({
  position: 'absolute',
  top: -13,
  bottom: 0,
  left: 11,
  right: 0,
  zIndex: 1,
  pointerEvents: 'none',
}));

const UserIconContainer = styled('span')(() => ({
  position: 'relative',
}));

type UserMenuProps = {
  children: React.ReactNode;
};

type UserMenuIconProps = {
  children: React.ReactNode;
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
  anchorEl: HTMLElement | null;
  anchorElTarget: string;
};

const UserMenuIcon = ({
  children,
  onClick,
  anchorEl,
  anchorElTarget,
}: UserMenuIconProps) => {
  return (
    <IconButton
      sx={{
        zIndex: 0,
        marginRight: 1,
        color: 'primary.dark',
      }}
      aria-owns={anchorEl ? anchorElTarget : undefined}
      aria-haspopup={'true'}
      onClick={onClick}
      size={'large'}
    >
      {children}
    </IconButton>
  );
};

export const UserMenu = React.forwardRef<HTMLDivElement, UserMenuProps>(
  ({ children, ...other }, ref): ReactElement => {
    const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
    const handleClick = (event: React.MouseEvent<HTMLElement>) =>
      setAnchorEl(event.currentTarget);
    const handleClose = () => setAnchorEl(null);
    const [anchorElMsg, setAnchorElMsg] = useState<HTMLElement | null>(null);
    const handleClickMsg = (event: React.MouseEvent<HTMLElement>) =>
      setAnchorElMsg(event.currentTarget);
    const handleCloseMsg = () => setAnchorElMsg(null);
    const dispatch = useDispatch();
    const messages: Message[] = useSelector((state) =>
      state.messages.itemList
        .filter((x: Message) => x.status === MessageStatusEnum.U)
        .slice(-5)
    );

    useEffect(() => {
      dispatch(requestAction(LOAD_MESSAGES));
    }, [dispatch]);
    const christmas = useMemo(() => {
      return isChristmas();
    }, []);

    return (
      <UserMenuContainer>
        <UserIconContainer>
          <UserMenuIcon
            onClick={handleClick}
            anchorEl={anchorEl}
            anchorElTarget={'user-menu'}
          >
            <AccountCircle />
          </UserMenuIcon>
          {christmas && (
            <ChristmasHat
              src="/christmas_hat.png"
              width="30"
              height="22"
              alt="Christmas hat on top of user icon"
            />
          )}
        </UserIconContainer>
        {messages && messages.length > 0 && (
          <UserMenuIcon
            onClick={handleClickMsg}
            anchorEl={anchorElMsg}
            anchorElTarget={'msg-menu'}
          >
            <Announcement />
          </UserMenuIcon>
        )}
        <Menu
          id="user-menu"
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={Boolean(anchorEl)}
          onClose={handleClose}
          ref={ref}
          {...other}
        >
          {children}
        </Menu>
        {messages && messages.length > 0 && (
          <Popover
            id="msg-menu"
            anchorEl={anchorElMsg}
            open={Boolean(anchorElMsg)}
            onClose={handleCloseMsg}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
          >
            <PaddingBox>
              <Typography variant="h5">Messages</Typography>
              <MaterialUiList>
                {messages.map((msg) => (
                  <ListItem key={msg.title}>
                    <ListItemIcon>
                      <ChatBubble />
                    </ListItemIcon>
                    {msg.title ? <Markdown text={msg.title} /> : ''}
                  </ListItem>
                ))}
              </MaterialUiList>
            </PaddingBox>
            <Divider />
            <PaddingBox>
              <Typography variant="body2">
                <Link href="/messages">See all messages</Link>
              </Typography>
            </PaddingBox>
          </Popover>
        )}
      </UserMenuContainer>
    );
  }
);
UserMenu.displayName = 'UserMenu';
