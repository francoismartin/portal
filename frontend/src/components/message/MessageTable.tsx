import React, { ReactElement, useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DELETE_MESSAGE, LOAD_MESSAGES } from '../../actions/actionTypes';
import { Message, MessageStatusEnum } from '../../api/generated';
import { IdRequired } from '../../global';
import { Column } from 'react-table';
import {
  Button,
  ColoredStatus,
  EnhancedTable,
  formatDate,
  requestAction,
  Status,
  useDialogState,
} from '@biomedit/next-widgets';
import { MessageDetail } from './MessageDetail';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import AutorenewIcon from '@mui/icons-material/Autorenew';
import truncate from 'lodash/truncate';
import { Typography } from '@mui/material';

function fmtMsgByStatus(
  text: string | null | undefined,
  status: MessageStatusEnum
) {
  if (!text) {
    return;
  }
  if (status === MessageStatusEnum.R) {
    return text;
  }
  return (
    <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
      {text}
    </Typography>
  );
}

export const MessageTableRefresh = (): ReactElement => {
  const { t } = useTranslation([I18nNamespace.COMMON, I18nNamespace.MESSAGE]);
  const dispatch = useDispatch();
  return (
    <Button
      onClick={() => {
        dispatch(requestAction(LOAD_MESSAGES));
      }}
      variant="outlined"
      color="primary"
      startIcon={<AutorenewIcon />}
      aria-label={
        t(`${I18nNamespace.MESSAGE}:actionButtons.refresh`) +
        ' ' +
        t(`${I18nNamespace.COMMON}:models.message_plural`)
      }
    >
      {t(`${I18nNamespace.MESSAGE}:actionButtons.refresh`)}
    </Button>
  );
};
export const MessageTable = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.MESSAGE);
  const dispatch = useDispatch();

  const itemList = useSelector((state) => state.messages.itemList);
  const isFetching = useSelector((state) => state.messages.isFetching);
  const isSubmitting = useSelector((state) => state.messages.isSubmitting);

  const loadItems = useCallback(() => {
    dispatch(requestAction(LOAD_MESSAGES));
  }, [dispatch]);

  const columns: Array<Column<IdRequired<Message>>> = useMemo(() => {
    const statuses: Status<MessageStatusEnum>[] = [
      {
        text: t('status.read'),
        value: MessageStatusEnum.R,
      },
      {
        color: 'info',
        text: t('status.unread'),
        value: MessageStatusEnum.U,
      },
    ];
    return [
      {
        id: 'title',
        Header: t('columns.title'),
        accessor: 'title',
        Cell: (data) => fmtMsgByStatus(data.value, data.row.original.status),
        sortType: 'caseInsensitive',
      },
      {
        id: 'body',
        Header: t('columns.body'),
        accessor: 'body',
        Cell: (data) =>
          fmtMsgByStatus(
            truncate(data.value, { length: 100, separator: ' ' }),
            data.row.original.status
          ),
        sortType: 'caseInsensitive',
      },
      {
        id: 'date',
        Header: t('columns.created'),
        accessor: 'created',
        Cell: (data) =>
          fmtMsgByStatus(
            formatDate(data.value, true),
            data.row.original.status
          ),
        sortType: 'datetime',
      },
      {
        id: 'status',
        Header: t('columns.status'),
        accessor: 'status',
        Cell: (data) =>
          ColoredStatus<MessageStatusEnum>({
            value: data.value,
            statuses,
          }),
      },
    ];
  }, [t]);

  const deleteItem = useCallback(
    (id) => {
      dispatch(requestAction(DELETE_MESSAGE, { id }));
    },
    [dispatch]
  );

  const { item, setItem, onClose, open } = useDialogState<Message>();

  return (
    <>
      <MessageDetail message={item} onClose={onClose} open={open} />
      <EnhancedTable<Required<Message>>
        emptyMessage={t('emptyMessage')}
        columns={columns}
        itemList={itemList}
        loadItems={loadItems}
        isFetching={isFetching}
        isSubmitting={isSubmitting}
        onRowClick={setItem}
        canEdit={true}
        onEdit={setItem}
        canDelete={true}
        onDelete={deleteItem}
      />
    </>
  );
};
