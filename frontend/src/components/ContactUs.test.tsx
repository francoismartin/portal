import {
  fillTextboxes,
  MockServer,
  RequestVerifier,
  resBody,
  resJson,
  setupMockApi,
  TextboxField,
} from '@biomedit/next-widgets';
import { fireEvent, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import React from 'react';
import { rest } from 'msw';
import { getInitialState, makeStore } from '../store';
import staffState from '../../test-data/staffState.json';
import { ContactUs } from './ContactUs';
import { backend } from '../api/api';
import { $Keys } from 'utility-types';
import { Contact } from '../api/generated';

describe('ContactUs', () => {
  describe('Component', () => {
    const server: MockServer = setupMockApi();

    const endpoint = `${backend}contact/`;

    const firstName = staffState.auth.user.firstName;
    const lastName = staffState.auth.user.lastName;
    const email = staffState.auth.user.email;

    const subject = 'Test Subject';
    const message =
      'Chuck Norris is not a test subject.\nChuck Norris is the test.';

    const getExpectedBody = (authenticated: boolean, sendCopy: boolean) => {
      const body = {
        subject,
        message,
        send_copy: sendCopy,
      };
      if (!authenticated) {
        body['first_name'] = firstName;
        body['last_name'] = lastName;
        body['email'] = email;
      }
      return body;
    };

    type Field = {
      name: $Keys<Contact>;
      value: string;
    };

    afterEach(() => {
      // Reset any runtime handlers tests may use.
      server.resetHandlers();
    });

    afterAll(() => {
      server.close();
    });

    async function assertSubmitSuccess(verifier: RequestVerifier) {
      const submitBtn = await screen.findByText('submit');
      fireEvent.click(submitBtn);

      await screen.findByText('success.title');
      await screen.findByText('success.message');

      verifier.assertCalled(`${endpoint}`, 1);
    }

    describe('Authenticated', () => {
      beforeEach(() => {
        render(
          <Provider
            store={makeStore(undefined, {
              ...getInitialState(),
              ...staffState,
            })}
          >
            <ContactUs />
          </Provider>
        );
      });

      it('should submit form and see a success confirmation', async () => {
        server.use(
          rest.post(endpoint, (req, res, ctx) =>
            res(resJson(ctx), resBody(ctx, getExpectedBody(true, true)))
          )
        );
        const verifier = new RequestVerifier(server);

        const textboxes = await screen.findAllByRole('textbox');
        expect(textboxes).toHaveLength(2);

        // fill out text fields
        const fields: Field[] = [
          { name: 'subject', value: subject },
          { name: 'message', value: message },
        ];

        fillTextboxes<Contact>(textboxes, fields);

        // check sendCopy checkbox
        const sendCopy = await screen.findByRole('checkbox');
        expect(sendCopy).toHaveAttribute('name', 'sendCopy');
        fireEvent.click(sendCopy);

        await assertSubmitSuccess(verifier);
      });
    });

    describe('Unauthenticated', () => {
      beforeEach(() => {
        render(
          <Provider store={makeStore()}>
            <ContactUs />
          </Provider>
        );
      });

      it('should submit form and see a success confirmation', async () => {
        server.use(
          rest.post(endpoint, (req, res, ctx) =>
            res(resJson(ctx), resBody(ctx, getExpectedBody(false, false)))
          )
        );
        const verifier = new RequestVerifier(server);

        const textboxes = await screen.findAllByRole('textbox');
        expect(textboxes).toHaveLength(5);

        // fill out text fields
        const fields: TextboxField<Contact>[] = [
          { name: 'firstName', value: firstName },
          { name: 'lastName', value: lastName },
          { name: 'email', value: email },
          { name: 'subject', value: subject },
          { name: 'message', value: message },
        ];

        fillTextboxes<Contact>(textboxes, fields);

        await assertSubmitSuccess(verifier);
      });
    });
  });
});
