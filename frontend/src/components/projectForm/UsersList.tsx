import { Controller, useFieldArray, useFormContext } from 'react-hook-form';
import { useSelector } from 'react-redux';
import React, { ReactElement, useState } from 'react';
import { userRoles } from '../../permissions';
import {
  AutoSelectBase,
  CheckboxArrayField,
  HiddenArrayField,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TooltipText,
  UserChip,
} from '@biomedit/next-widgets';
import {
  ProjectPermissionsEditUsersEnum,
  ProjectUsers,
  ProjectUsersRolesEnum,
} from '../../api/generated';
import { I18nNamespace } from '../../i18n';
import { useTranslation } from 'next-i18next';
import { Overwrite } from 'utility-types';

// TODO: refactor in #101
type ConvertedProjectUserFields = {
  roles: Partial<Record<ProjectUsersRolesEnum, boolean>>;
};
export type ConvertedProjectUser = Overwrite<
  ProjectUsers,
  ConvertedProjectUserFields
>;

interface FormValues {
  users: Array<ConvertedProjectUser>;
}

type UserListProps = {
  permissions?: Array<ProjectPermissionsEditUsersEnum>;
};

const isRoleSelected = (
  field: ProjectUsers,
  role: ProjectUsersRolesEnum
): boolean => {
  return field?.roles?.[role];
};

export const UsersList = ({ permissions }: UserListProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.PROJECT_USER_LIST]);
  const arrayName = 'users';

  const {
    control,
    clearErrors,
    getValues,
    formState: { errors },
  } = useFormContext<FormValues>();

  const { fields, append, remove } = useFieldArray({
    control,
    name: arrayName,
    keyName: 'key',
  });

  const users = useSelector((state) => state.users.itemList);
  const [inputValue, setInputValue] = useState<string | undefined>();

  const choices = users.map((user) => ({
    label: user?.profile?.displayName,
    displayName: user?.profile?.displayName,
    affiliation: user?.profile?.affiliation,
    id: user.id,
    ...user,
  }));

  const choicesFields = ['firstName', 'lastName', 'username', 'affiliation'];

  const addUser = (projectUser: ConvertedProjectUser) => {
    if (!fields.some((field) => String(field.id) === String(projectUser.id))) {
      // initialize checkboxes to be empty
      projectUser.roles = {
        DM: false,
        PL: false,
        PM: false,
        USER: false,
      };
      append(projectUser);
    }
    setInputValue('');
  };

  function hasRole(
    role: ProjectUsersRolesEnum | ProjectPermissionsEditUsersEnum
  ): boolean {
    return !!permissions?.includes(role as ProjectPermissionsEditUsersEnum);
  }

  const onChange = () => {
    const projectLeader = getValues()[arrayName].find(
      (user) => user.roles?.[ProjectUsersRolesEnum.PL]
    );
    if (projectLeader) {
      clearErrors(arrayName);
    }
  };

  const tooltips: Record<userRoles, string> = {
    [userRoles.PL]: t('roles.PL'),
    [userRoles.PM]: t('roles.PM'),
    [userRoles.DM]: t('roles.DM'),
    [userRoles.USER]: t('roles.USER'),
  };

  return (
    <div>
      {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore // TODO
        <AutoSelectBase
          onSelect={addUser}
          choices={choices}
          fields={choicesFields}
          placeholder="Add users here"
          inputValue={inputValue || ''}
          error={errors?.[arrayName]}
          onChange={(event) => {
            setInputValue(event.target.value);
          }}
        />
      }
      {fields.length > 0 && (
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>User</TableCell>
              {Object.values<string>(userRoles).map((permission) => (
                <TableCell key={permission}>
                  <TooltipText text={permission} title={tooltips[permission]} />
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {fields.map((field, index) => {
              const displayName = field?.['displayName'];
              const roles = Object.keys(
                userRoles
              ) as Array<ProjectUsersRolesEnum>;

              /*
               * Make sure only users can be deleted which only have selected roles the current user is allowed to change.
               *
               * For example: If the current user is permissions manager, they cannot change whether or not users are project leaders.
               *              This means they should also not be able to delete any users from the list which are project leaders,
               *              else they would also remove the project leader's roles by doing so.
               */
              const canDelete = roles.every((r) => {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore TODO
                if (isRoleSelected(field as ProjectUsers, r)) {
                  return hasRole(r);
                }
                return true;
              });

              const onDelete = canDelete
                ? (): void => remove(index)
                : undefined;

              return (
                <TableRow key={field.key}>
                  <TableCell>
                    <Controller
                      control={control}
                      name={`${arrayName}.${index}.displayName`}
                      defaultValue={displayName}
                      render={() => (
                        <UserChip user={displayName} onDelete={onDelete} />
                      )}
                    />
                  </TableCell>
                  {roles.map((r) => {
                    const readOnlyPermission = !hasRole(r);
                    return (
                      <TableCell key={r}>
                        <HiddenArrayField
                          arrayName={arrayName}
                          fieldName="id"
                          index={index}
                          field={field}
                        />
                        <CheckboxArrayField
                          arrayName={arrayName}
                          fieldName={`roles.${r}`}
                          index={index}
                          field={field}
                          disabled={readOnlyPermission}
                          disabledTitle={t('disabledTitle')}
                          onChange={onChange}
                        />
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      )}
    </div>
  );
};
