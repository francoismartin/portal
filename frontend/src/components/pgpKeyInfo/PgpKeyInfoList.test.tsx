import React from 'react';
import { PgpKeyInfoList, hasActiveKey, isActive } from './PgpKeyInfoList';
import listPgpKeyInfoResponse from '../../../test-data/listPgpKeyInfoResponse.json';
import {
  MockServer,
  resBody,
  resJson,
  setupMockApi,
  Unpacked,
} from '@biomedit/next-widgets';
import { rest } from 'msw';
import { backend } from '../../api/api';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { getInitialState, makeStore } from '../../store';
import { PgpKeyInfoStatusEnum } from '../../api/generated';
import authStateUser from '../../../test-data/authStateUser.json';
import userEvent from '@testing-library/user-event';

const prettify = (key: Unpacked<typeof listPgpKeyInfoResponse>) => key.id;

describe('isActive', function () {
  it.each`
    status                                   | expected
    ${PgpKeyInfoStatusEnum.APPROVAL_REVOKED} | ${false}
    ${PgpKeyInfoStatusEnum.KEY_REVOKED}      | ${false}
    ${PgpKeyInfoStatusEnum.APPROVED}         | ${true}
    ${PgpKeyInfoStatusEnum.REJECTED}         | ${false}
    ${PgpKeyInfoStatusEnum.PENDING}          | ${true}
    ${PgpKeyInfoStatusEnum.DELETED}          | ${false}
  `('Should evaluate status $status to $expected', ({ status, expected }) => {
    expect(isActive(status)).toBe(expected);
  });
});

describe('hasActiveKey', function () {
  it.each`
    itemList                           | expected | humanReadableItemList
    ${[]}                              | ${false} | ${[]}
    ${listPgpKeyInfoResponse}          | ${true}  | ${listPgpKeyInfoResponse.map(prettify)}
    ${listPgpKeyInfoResponse.slice(1)} | ${false} | ${listPgpKeyInfoResponse.slice(1).map(prettify)}
  `(
    'Should evaluate itemList $humanReadableItemList to $expected',
    ({ itemList, expected }) => {
      expect(hasActiveKey(itemList)).toBe(expected);
    }
  );
});

describe('PgpKeyInfoList', function () {
  const server: MockServer = setupMockApi();
  const retireKeyButtonLabel = 'retireKeyButton';

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  it.each`
    itemList                           | expected | humanReadableItemList
    ${[]}                              | ${true}  | ${[]}
    ${listPgpKeyInfoResponse}          | ${false} | ${listPgpKeyInfoResponse.map(prettify)}
    ${listPgpKeyInfoResponse.slice(1)} | ${true}  | ${listPgpKeyInfoResponse.slice(1).map(prettify)}
  `(
    'Should show ($expected) the add button when itemList is $humanReadableItemList',
    async ({ itemList, expected }) => {
      // given
      server.use(
        rest.get(`${backend}pgpkey/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, itemList))
        )
      );
      const addButtonLabel = 'Add common:models.key';

      // when
      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            ...authStateUser,
          })}
        >
          <PgpKeyInfoList />
        </Provider>
      );

      // then
      if (expected) {
        expect(
          await screen.findByLabelText(addButtonLabel)
        ).toBeInTheDocument();
      } else {
        expect(screen.queryByLabelText(addButtonLabel)).not.toBeInTheDocument();
      }
    }
  );

  it.each`
    itemList                           | expected | humanReadableItemList
    ${listPgpKeyInfoResponse}          | ${true}  | ${listPgpKeyInfoResponse.map(prettify)}
    ${listPgpKeyInfoResponse.slice(1)} | ${false} | ${listPgpKeyInfoResponse.slice(1).map(prettify)}
  `(
    'Should show ($expected) the retireKeyButton when itemList is $humanReadableItemList',
    async function ({ itemList, expected }) {
      // given
      server.use(
        rest.get(`${backend}pgpkey/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, itemList))
        )
      );

      // when
      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            ...authStateUser,
          })}
        >
          <PgpKeyInfoList />
        </Provider>
      );

      // then
      expected
        ? expect(
            await screen.findByLabelText(retireKeyButtonLabel)
          ).toBeInTheDocument()
        : expect(
            screen.queryByLabelText(retireKeyButtonLabel)
          ).not.toBeInTheDocument();
    }
  );

  it('Should should show the dialog when clicking the retireKeyButton', async function () {
    // given
    server.use(
      rest.get(`${backend}pgpkey/`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listPgpKeyInfoResponse))
      )
    );
    const user = userEvent.setup();

    // when
    render(
      <Provider
        store={makeStore(undefined, {
          ...getInitialState(),
          ...authStateUser,
        })}
      >
        <PgpKeyInfoList />
      </Provider>
    );
    const retireKeyButton = await screen.findByLabelText(retireKeyButtonLabel);
    await user.click(retireKeyButton);

    // then
    expect(
      await screen.findByText('pgpKeyInfoList:dialogs.retireKey.text')
    ).toBeInTheDocument();
  });
});
