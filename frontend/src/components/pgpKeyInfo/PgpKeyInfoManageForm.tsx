import React, { ReactElement, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  FabButton,
  FormDialog,
  LabelledField,
  mustBe,
  pgpFingerprint,
  requestAction,
  required,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { PgpKeyInfo, PgpKeyMetadata } from '../../api/generated';
import { Trans, useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { FormProvider } from 'react-hook-form';
import { ADD_PGP_KEY_INFO, CALL } from '../../actions/actionTypes';
import { generatedBackendApi } from '../../api/api';
import { Alert, AlertColor, Box, Link } from '@mui/material';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { styled } from '@mui/material/styles';
import { keyServer } from '../../config';

type KeyMetadata = {
  keyUserId?: string;
  keyEmail?: string;
};

export type PgpKeyInfoManageFormProps = {
  pgpKeyInfo: Partial<PgpKeyInfo>;
  onClose: () => void;
};

enum HelperTextEnum {
  INITIAL,
  VERIFIED,
  NOT_VERIFIED,
  NOT_FOUND,
}

type HelperText = {
  text: string | ReactElement;
  severity?: AlertColor;
};

function useHelperTexts(): Record<keyof typeof HelperTextEnum, HelperText> {
  const { t } = useTranslation(I18nNamespace.PGP_KEY_INFO_MANAGE_FORM);

  return {
    INITIAL: {
      text: t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.initial`),
      severity: 'info',
    },
    VERIFIED: {
      text: t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.keyVerified`),
      severity: 'warning',
    },
    NOT_VERIFIED: {
      text: (
        <Trans
          i18nKey={`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.keyNotVerified`}
          components={{
            keyServer: (
              <Link
                href={keyServer}
                target={'_blank'}
                rel={'noopener noreferrer'}
              />
            ),
          }}
        />
      ),
      severity: 'error',
    },
    NOT_FOUND: {
      text: t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.keyNotFound`),
      severity: 'error',
    },
  };
}

const StyledFabButton = styled(FabButton)(({ theme }) => ({
  marginLeft: theme.spacing(1),
}));

export const PgpKeyInfoManageForm = ({
  pgpKeyInfo,
  onClose,
}: PgpKeyInfoManageFormProps): ReactElement => {
  const { t } = useTranslation(I18nNamespace.PGP_KEY_INFO_MANAGE_FORM);
  const form = useEnhancedForm<PgpKeyInfo>({ mode: 'onChange' });
  const isSubmitting = useSelector((state) => state.pgpKeyInfo.isSubmitting);
  const dispatch = useDispatch();
  const [keyMetadata, setKeyMetadata] = useState<KeyMetadata>();
  const userEmail = useSelector((state) => state.auth.user?.email);
  const fingerprintFieldName = 'fingerprint';
  const helperTexts = useHelperTexts();
  const [helperText, setHelperText] = useState<HelperText>(helperTexts.INITIAL);
  const watchFingerprint = form.watch(fingerprintFieldName);
  const [isFetchingKeyMetadata, setIsFetchingKeyMetadata] =
    useState<boolean>(false);
  const uniqueCheck = async (value) =>
    generatedBackendApi.uniquePgpKeyInfo({
      uniquePgpKeyInfo: { fingerprint: value.fingerprint },
    });

  function submit(pgpKeyInfo: PgpKeyInfo) {
    const onSuccessAction = {
      type: CALL,
      callback: onClose,
    };
    dispatch(
      requestAction(
        ADD_PGP_KEY_INFO,
        {
          pgpKeyInfo,
        },
        onSuccessAction
      )
    );
  }

  async function onSearchClick() {
    form.clearErrors('keyEmail');
    const { fingerprint: formFingerprint } = form.getValues();
    setIsFetchingKeyMetadata(true);
    generatedBackendApi
      .metadataPgpKeyMetadata({
        pgpKeyMetadata: { fingerprint: formFingerprint },
      })
      .then(({ fingerprint, email, userId }: PgpKeyMetadata) => {
        const keyMetadata: KeyMetadata = {
          keyEmail: email,
          keyUserId: userId,
        };
        setKeyMetadata(keyMetadata);
        if (fingerprint) {
          if (email && userId) {
            setHelperText(helperTexts.VERIFIED);
          } else {
            setHelperText(helperTexts.NOT_VERIFIED);
          }
        } else {
          setHelperText(helperTexts.NOT_FOUND);
        }
      })
      .finally(() => setIsFetchingKeyMetadata(false));
  }

  return (
    <FormProvider {...form}>
      <FormDialog
        open={!!pgpKeyInfo}
        title={t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:title`)}
        onSubmit={submit}
        isSubmitting={isSubmitting || isFetchingKeyMetadata}
        onClose={onClose}
        confirmButtonProps={{ disabled: !keyMetadata }}
        fullWidth={true}
      >
        <FormFieldsContainer>
          <Alert
            severity={helperText.severity}
            sx={{ marginBottom: 3, marginRight: 0 }}
          >
            {helperText.text}
          </Alert>
          <Box sx={{ display: 'flex', marginRight: 0 }}>
            <LabelledField
              name={fingerprintFieldName}
              type="text"
              label={t(
                `${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:captions.${fingerprintFieldName}`
              )}
              fullWidth
              validations={[required, pgpFingerprint]}
              unique={uniqueCheck}
              autofocus={true}
            />
            <StyledFabButton
              icon={'search'}
              onClick={onSearchClick}
              size={'small'}
              aria-label={t(
                `${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:buttons.search`
              )}
              disabled={
                !watchFingerprint ||
                watchFingerprint.length === 0 ||
                !!form.getFieldState(fingerprintFieldName).error ||
                isFetchingKeyMetadata
              }
            />
          </Box>
          <LabelledField
            name="keyUserId"
            type="text"
            label={t(
              `${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:captions.keyUserId`
            )}
            fullWidth
            disabled
            // Both needed as `initialValues` wouldn't show up in the UI and
            // value wouldn't be submitted when confirming
            initialValues={keyMetadata}
            value={keyMetadata?.keyUserId ?? ''}
          />
          <LabelledField
            name="keyEmail"
            type="text"
            label={t(
              `${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:captions.keyEmail`
            )}
            fullWidth
            disabled
            // Both needed as `initialValues` wouldn't show up in the UI and
            // value wouldn't be submitted when confirming
            initialValues={keyMetadata}
            value={keyMetadata?.keyEmail ?? ''}
            validations={mustBe(userEmail)}
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
