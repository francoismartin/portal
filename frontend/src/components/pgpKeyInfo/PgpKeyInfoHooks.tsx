import React, { useMemo } from 'react';
import { ColoredStatus, Status, useFormDialog } from '@biomedit/next-widgets';
import { PgpKeyInfo, PgpKeyInfoStatusEnum } from '../../api/generated';
import { Column } from 'react-table';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { PgpKeyInfoManageForm } from './PgpKeyInfoManageForm';

export const usePgpKeyInfoColumns = (): Array<Column<Required<PgpKeyInfo>>> => {
  const { t } = useTranslation(I18nNamespace.PGP_KEY_INFO_LIST);

  return useMemo(() => {
    const statuses: Status<PgpKeyInfoStatusEnum>[] = [
      {
        color: 'info',
        value: PgpKeyInfoStatusEnum.PENDING,
      },
      {
        color: 'success',
        value: PgpKeyInfoStatusEnum.APPROVED,
      },
      {
        color: 'error',
        value: PgpKeyInfoStatusEnum.REJECTED,
      },
      {
        value: PgpKeyInfoStatusEnum.DELETED,
      },
      {
        value: PgpKeyInfoStatusEnum.KEY_REVOKED,
      },
      {
        value: PgpKeyInfoStatusEnum.APPROVAL_REVOKED,
      },
    ];

    return [
      {
        id: 'fingerprint',
        Header: t<string>('columns.fingerprint'),
        accessor: 'fingerprint',
      },
      {
        id: 'keyUserId',
        Header: t<string>('columns.keyUserId'),
        accessor: 'keyUserId',
      },
      {
        id: 'keyEmail',
        Header: t<string>('columns.keyEmail'),
        accessor: 'keyEmail',
      },
      {
        id: 'status',
        Header: t<string>('columns.status'),
        accessor: 'status',
        Cell: (row) =>
          ColoredStatus<PgpKeyInfoStatusEnum>({
            value: row.value,
            statuses,
          }),
      },
    ];
  }, [t]);
};

export function usePgpKeyInfoManageForm() {
  const newPgpKeyInfo: Partial<PgpKeyInfo> = { fingerprint: undefined };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<PgpKeyInfo>>(newPgpKeyInfo);
  const pgpKeyInfoManageForm = data && (
    <PgpKeyInfoManageForm pgpKeyInfo={data} onClose={closeFormDialog} />
  );
  return {
    pgpKeyInfoManageForm,
    ...rest,
  };
}
