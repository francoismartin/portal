import React, { ReactElement, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  CheckboxField,
  FormDialog,
  LabelledField,
  requestAction,
  required,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { UPDATE_USER_PROFILE } from '../actions/actionTypes';
import { FormProvider } from 'react-hook-form';
import Alert from '@mui/material/Alert';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../i18n';
import { UserProfile } from '../api/generated';
import { styled } from '@mui/material/styles';

type FormData = {
  username: string;
  affiliationConsent: boolean;
};

const StyledCheckboxField = styled(CheckboxField)(({ theme }) => ({
  marginTop: theme.spacing(1),
}));

export const dialogTitle = 'userProfileDialog.title';
export const usernameLabel = 'userProfileDialog.usernameLabel';
export const affiliationConsentLabel =
  'userProfileDialog.affiliationConsentLabel';

export const UserProfileDialog = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);
  const dispatch = useDispatch();
  const userInfo = useSelector((state) => state.auth.user);
  const form = useEnhancedForm<FormData>();
  const isSubmitting = useSelector((state) => state.auth.isSubmitting);
  const isExistingUsername = useSelector(
    (state) => state.auth.isExistingUsername
  );
  const invalidMsg = useSelector((state) => state.auth.invalidMsg);
  const [invalidUsername, setInvalidUsername] = useState<string | undefined>();

  const usernameMissing = useMemo(
    () => !userInfo?.profile?.localUsername,
    [userInfo]
  );
  const affiliationConsentMissing = useMemo(
    () =>
      !!userInfo?.profile?.affiliationConsentRequired &&
      !userInfo?.profile?.affiliationConsent,
    [userInfo]
  );

  const usernameField = 'username';
  const affiliationConsentField = 'affiliationConsent';

  const watchUsername = form.watch(usernameField);

  const submit = ({ username, affiliationConsent }: FormData) => {
    setInvalidUsername(undefined);
    if (username || affiliationConsent) {
      const profile: Partial<UserProfile> = {};
      if (usernameMissing) {
        profile.localUsername = username;
      }
      if (affiliationConsentMissing) {
        profile.affiliationConsent = affiliationConsent;
      }
      dispatch(
        requestAction(UPDATE_USER_PROFILE, {
          user: {
            profile: {
              localUsername: username,
              affiliationConsent,
            },
          },
          id: String(userInfo?.id),
        })
      );
    }
  };

  const fieldError = useMemo(() => {
    // `invalidUsername` NOT set (is reset in submit call)
    if (!invalidUsername && (isExistingUsername || !!invalidMsg)) {
      setInvalidUsername(watchUsername);
    }
    // Input has changed and does NOT equal `invalidUsername`
    if (invalidUsername && watchUsername !== invalidUsername) {
      return;
    }
    if (isExistingUsername) {
      return {
        type: 'validate',
        message: t('userProfileDialog.validationUsernameAlreadyExists'),
      };
    }
    if (invalidMsg) {
      return {
        type: 'validate',
        message: invalidMsg,
      };
    }
  }, [t, invalidMsg, isExistingUsername, watchUsername, invalidUsername]);

  return (
    <FormProvider {...form}>
      <FormDialog<FormData>
        title={t(dialogTitle)}
        open={usernameMissing || affiliationConsentMissing}
        onSubmit={submit}
        isSubmitting={isSubmitting}
      >
        {usernameMissing && (
          <>
            <Alert severity="warning" sx={{ marginBottom: 2 }}>
              {t('userProfileDialog.warning')}
            </Alert>
            <LabelledField
              type="text"
              name={usernameField}
              label={t(usernameLabel)}
              fieldError={fieldError}
              validations={[required]}
              autofocus
              fullWidth
            />
          </>
        )}
        {affiliationConsentMissing && (
          <StyledCheckboxField
            name={affiliationConsentField}
            label={t(affiliationConsentLabel)}
            required
          />
        )}
      </FormDialog>
    </FormProvider>
  );
};
