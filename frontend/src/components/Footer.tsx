import { Box, Link, Typography } from '@mui/material';
import React from 'react';
import Image from 'next/image';
import { Version } from './Version';

export const Footer = () => {
  return (
    <Box sx={{ marginTop: 'auto!important', marginBottom: 2 }}>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
          marginBottom: 2,
        }}
      >
        <Typography variant="body2" sx={{ marginRight: 1 }}>
          A project of
        </Typography>
        <Link
          href={'https://www.sib.swiss/'}
          target={'_blank'}
          rel={'noopener noreferrer'}
        >
          <Image src={'/sib_logo.png'} width={52} height={40} />
        </Link>
      </Box>
      <Version />
    </Box>
  );
};
