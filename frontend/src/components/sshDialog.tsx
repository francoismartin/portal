import React, { MouseEventHandler, ReactElement } from 'react';
import Button from '@mui/material/Button';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import IconButton from '@mui/material/IconButton';
import FileCopy from '@mui/icons-material/FileCopy';

type SSHDialogProps = {
  command: string;
  onClose: MouseEventHandler;
  open: boolean;
};

export function SSHDialog({
  command,
  onClose,
  open,
}: SSHDialogProps): ReactElement {
  return (
    <Dialog onClose={onClose} aria-labelledby="ssh-dialog" open={open}>
      <DialogTitle>Connect To Your Instance</DialogTitle>
      <DialogContent>
        <DialogContentText>
          To connect, execute the following command in your terminal.
        </DialogContentText>
        <TextField
          InputProps={{ readOnly: true }}
          defaultValue={command}
          margin="normal"
          id="command"
          label="Command"
        />
        <IconButton
          aria-label="Copy"
          onClick={() => navigator.clipboard.writeText(command)}
          size="large"
        >
          <FileCopy fontSize="large" />
        </IconButton>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
}
