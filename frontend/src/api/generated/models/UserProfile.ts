/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface UserProfile
 */
export interface UserProfile {
    /**
     * 
     * @type {string}
     * @memberof UserProfile
     */
    readonly affiliation?: string;
    /**
     * 
     * @type {string}
     * @memberof UserProfile
     */
    readonly affiliationId?: string;
    /**
     * 
     * @type {number}
     * @memberof UserProfile
     */
    uid?: number;
    /**
     * 
     * @type {number}
     * @memberof UserProfile
     */
    gid?: number;
    /**
     * 
     * @type {boolean}
     * @memberof UserProfile
     */
    readonly affiliationConsentRequired?: boolean;
    /**
     * 
     * @type {string}
     * @memberof UserProfile
     */
    localUsername: string;
    /**
     * 
     * @type {string}
     * @memberof UserProfile
     */
    readonly displayName?: string;
    /**
     * 
     * @type {string}
     * @memberof UserProfile
     */
    readonly displayId?: string;
    /**
     * 
     * @type {string}
     * @memberof UserProfile
     */
    readonly namespace?: string;
    /**
     * 
     * @type {string}
     * @memberof UserProfile
     */
    readonly displayLocalUsername?: string;
    /**
     * 
     * @type {boolean}
     * @memberof UserProfile
     */
    affiliationConsent?: boolean;
}

export function UserProfileFromJSON(json: any): UserProfile {
    return UserProfileFromJSONTyped(json, false);
}

export function UserProfileFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserProfile {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'affiliation': !exists(json, 'affiliation') ? undefined : json['affiliation'],
        'affiliationId': !exists(json, 'affiliation_id') ? undefined : json['affiliation_id'],
        'uid': !exists(json, 'uid') ? undefined : json['uid'],
        'gid': !exists(json, 'gid') ? undefined : json['gid'],
        'affiliationConsentRequired': !exists(json, 'affiliation_consent_required') ? undefined : json['affiliation_consent_required'],
        'localUsername': json['local_username'],
        'displayName': !exists(json, 'display_name') ? undefined : json['display_name'],
        'displayId': !exists(json, 'display_id') ? undefined : json['display_id'],
        'namespace': !exists(json, 'namespace') ? undefined : json['namespace'],
        'displayLocalUsername': !exists(json, 'display_local_username') ? undefined : json['display_local_username'],
        'affiliationConsent': !exists(json, 'affiliation_consent') ? undefined : json['affiliation_consent'],
    };
}

export function UserProfileToJSON(value?: UserProfile | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'uid': value.uid,
        'gid': value.gid,
        'local_username': value.localUsername,
        'affiliation_consent': value.affiliationConsent,
    };
}

