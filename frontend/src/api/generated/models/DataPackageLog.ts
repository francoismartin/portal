/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface DataPackageLog
 */
export interface DataPackageLog {
    /**
     * 
     * @type {string}
     * @memberof DataPackageLog
     */
    node: string;
    /**
     * 
     * @type {string}
     * @memberof DataPackageLog
     */
    status: DataPackageLogStatusEnum;
    /**
     * 
     * @type {string}
     * @memberof DataPackageLog
     */
    fileName: string;
    /**
     * File size in bytes (min. 1b)
     * @type {number}
     * @memberof DataPackageLog
     */
    fileSize?: number;
    /**
     * 
     * @type {string}
     * @memberof DataPackageLog
     */
    metadata: string;
}

/**
* @export
* @enum {string}
*/
export enum DataPackageLogStatusEnum {
    ENTER = 'ENTER',
    PROCESSED = 'PROCESSED',
    ERROR = 'ERROR'
}

export function DataPackageLogFromJSON(json: any): DataPackageLog {
    return DataPackageLogFromJSONTyped(json, false);
}

export function DataPackageLogFromJSONTyped(json: any, ignoreDiscriminator: boolean): DataPackageLog {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'node': json['node'],
        'status': json['status'],
        'fileName': json['file_name'],
        'fileSize': !exists(json, 'file_size') ? undefined : json['file_size'],
        'metadata': json['metadata'],
    };
}

export function DataPackageLogToJSON(value?: DataPackageLog | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'node': value.node,
        'status': value.status,
        'file_name': value.fileName,
        'file_size': value.fileSize,
        'metadata': value.metadata,
    };
}

