/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface Node
 */
export interface Node {
    /**
     * 
     * @type {number}
     * @memberof Node
     */
    readonly id?: number;
    /**
     * 
     * @type {string}
     * @memberof Node
     */
    code: string;
    /**
     * 
     * @type {string}
     * @memberof Node
     */
    name: string;
    /**
     * 
     * @type {string}
     * @memberof Node
     */
    nodeStatus: NodeNodeStatusEnum;
    /**
     * 
     * @type {string}
     * @memberof Node
     */
    ticketingSystemEmail: string;
}

/**
* @export
* @enum {string}
*/
export enum NodeNodeStatusEnum {
    ON = 'ON',
    OFF = 'OFF'
}

export function NodeFromJSON(json: any): Node {
    return NodeFromJSONTyped(json, false);
}

export function NodeFromJSONTyped(json: any, ignoreDiscriminator: boolean): Node {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'id': !exists(json, 'id') ? undefined : json['id'],
        'code': json['code'],
        'name': json['name'],
        'nodeStatus': json['node_status'],
        'ticketingSystemEmail': json['ticketing_system_email'],
    };
}

export function NodeToJSON(value?: Node | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'code': value.code,
        'name': value.name,
        'node_status': value.nodeStatus,
        'ticketing_system_email': value.ticketingSystemEmail,
    };
}

