import { initialProjectState, projectReducer } from './project';
import {
  makeMockStore,
  MockServer,
  requestAction,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { LOAD_PROJECTS } from '../actions/actionTypes';
import { backend } from '../api/api';
import { ProjectFromJSON } from '../api/generated';
import { rest } from 'msw';
import listProjectsResponse from '../../test-data/listProjectsResponse.json';
import { rootSaga } from '../actions/sagas';

describe('projectReducer', () => {
  const listItem = {
    ipAddressInRange: true,
    ...listProjectsResponse[0],
  };

  let store;
  const server: MockServer = setupMockApi();

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  beforeEach(() => {
    store = makeMockStore(
      { project: initialProjectState },
      { project: projectReducer },
      rootSaga
    );
  });

  it('should add projects to `itemList` when `LOAD_PROJECTS` is dispatched', async () => {
    // given
    server.use(
      rest.get(`${backend}projects/`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listProjectsResponse))
      )
    );

    // when
    store.dispatch(requestAction(LOAD_PROJECTS));
    await store.waitFor(LOAD_PROJECTS.success);

    // then
    const projectState = store.getState().project;
    const projectListItems = projectState.itemList;

    expect(projectListItems).toHaveLength(2);

    expect(projectListItems[0]).toMatchObject(ProjectFromJSON(listItem));
  });
});
