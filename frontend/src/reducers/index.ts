import { combineReducers } from 'redux';
import { auth } from './auth';
import { projectReducer } from './project';
import {
  Approval,
  Contact,
  DataProvider,
  Feed,
  Flag,
  Message,
  Node,
  Permission,
  QuickAccessTile,
  User,
} from '../api/generated';
import {
  ADD_CONTACT,
  ADD_DATA_PROVIDER,
  ADD_FLAG,
  ADD_LOCAL_USER,
  ADD_NODE,
  DELETE_MESSAGE,
  LOAD_DATA_PROVIDERS,
  LOAD_FEED,
  LOAD_FLAGS,
  LOAD_MESSAGES,
  LOAD_NODES,
  LOAD_PERMISSIONS,
  LOAD_QUICK_ACCESSES,
  LOAD_USERS,
  UPDATE_APPROVAL,
  UPDATE_DATA_PROVIDER,
  UPDATE_FLAG,
  UPDATE_MESSAGE,
  UPDATE_NODE,
  UPDATE_USER,
} from '../actions/actionTypes';
import { reducer, toast } from '@biomedit/next-widgets';
import { IdRequired } from '../global';
import { dataTransfers } from './dataTransfer';
import { groupReducer } from './group';
import { pgpKeyInfo } from './pgpKeyInfo';

export const reducers = combineReducers({
  auth,
  toast,
  project: projectReducer,
  dataTransfers,
  groups: groupReducer,
  pgpKeyInfo: pgpKeyInfo,
  contact: reducer<IdRequired<Contact>>({ add: ADD_CONTACT }),
  users: reducer<IdRequired<User>>({
    load: LOAD_USERS,
    update: UPDATE_USER,
    add: ADD_LOCAL_USER,
  }),
  nodes: reducer<IdRequired<Node>>({
    load: LOAD_NODES,
    update: UPDATE_NODE,
    add: ADD_NODE,
  }),
  messages: reducer<IdRequired<Message>>({
    load: LOAD_MESSAGES,
    del: DELETE_MESSAGE,
    update: UPDATE_MESSAGE,
  }),
  dataProvider: reducer<IdRequired<DataProvider>>({
    load: LOAD_DATA_PROVIDERS,
    update: UPDATE_DATA_PROVIDER,
    add: ADD_DATA_PROVIDER,
  }),
  feed: reducer<IdRequired<Feed>>({ load: LOAD_FEED }),
  flags: reducer<IdRequired<Flag>>({
    load: LOAD_FLAGS,
    update: UPDATE_FLAG,
    add: ADD_FLAG,
  }),
  permissions: reducer<IdRequired<Permission>>({
    load: LOAD_PERMISSIONS,
  }),
  quickAccesses: reducer<IdRequired<QuickAccessTile>>({
    load: LOAD_QUICK_ACCESSES,
  }),
  approval: reducer<IdRequired<Approval>>({
    update: UPDATE_APPROVAL,
  }),
});
