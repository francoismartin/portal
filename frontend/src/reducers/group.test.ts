import {
  initialGroupState,
  groupReducer,
  withUpdatedPermissionsObject,
} from './group';
import {
  makeMockStore,
  MockServer,
  requestAction,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import {
  DELETE_GROUP,
  LOAD_GROUPS,
  UPDATE_GROUP,
} from '../actions/actionTypes';
import { backend } from '../api/api';
import { GroupFromJSON } from '../api/generated';
import { rest } from 'msw';
import listGroupsResponse from '../../test-data/listGroupsResponse.json';
import { rootSaga } from '../actions/sagas';

describe('projectReducer', () => {
  const listItem = listGroupsResponse[0];

  let store;
  const server: MockServer = setupMockApi();

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  beforeEach(() => {
    store = makeMockStore(
      { group: initialGroupState },
      { group: groupReducer },
      rootSaga
    );
  });

  it('should add groups to `itemList` when `LOAD_GROUPS` is dispatched', async () => {
    // given
    server.use(
      rest.get(`${backend}identity/group`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listGroupsResponse))
      )
    );

    // when
    store.dispatch(requestAction(LOAD_GROUPS));
    await store.waitFor(LOAD_GROUPS.success);

    // then
    const { itemList } = store.getState().group;

    expect(itemList).toHaveLength(2);

    expect(itemList[0]).toMatchObject(GroupFromJSON(listItem));
  });

  it('should set the store to initial state when `LOAD_GROUPS` returns nothing', async () => {
    // given
    server.use(
      rest.get(`${backend}identity/group`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, []))
      )
    );

    // when
    store.dispatch(requestAction(LOAD_GROUPS));
    await store.waitFor(LOAD_GROUPS.success);

    // then
    const groupState = store.getState().group;
    expect(groupState).toMatchObject(initialGroupState);
  });

  it('should update group in `itemList` when `UPDATE_GROUP` is dispatched', async () => {
    // given
    const updatedItemId = '1';
    const updatedItemName = 'New name';
    const updatedItem = { ...listItem, name: updatedItemName };

    server.use(
      rest.put(`${backend}identity/group/${updatedItemId}`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, updatedItem))
      ),
      rest.get(`${backend}identity/group`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listGroupsResponse))
      )
    );

    store.dispatch(requestAction(LOAD_GROUPS));
    await store.waitFor(LOAD_GROUPS.success);

    // when
    store.dispatch(
      requestAction(UPDATE_GROUP, { id: updatedItemId, group: updatedItem })
    );
    await store.waitFor(UPDATE_GROUP.success);

    // then
    const { itemList } = store.getState().group;
    expect(itemList[0].name).toMatch(updatedItem.name);
  });

  it('should remove group from `itemList` when `DELETE_GROUP` is dispatched', async () => {
    // given
    const deleteItemId = '2';

    server.use(
      rest.delete(`${backend}identity/group/${deleteItemId}`, (req, res, ctx) =>
        res(resJson(ctx))
      ),
      rest.get(`${backend}identity/group`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listGroupsResponse))
      )
    );

    store.dispatch(requestAction(LOAD_GROUPS));
    await store.waitFor(LOAD_GROUPS.success);

    // when
    store.dispatch(requestAction(DELETE_GROUP, { id: deleteItemId }));
    await store.waitFor(DELETE_GROUP.success);

    // then
    const { itemList } = store.getState().group;
    expect(itemList).toHaveLength(1);
  });

  it('should update groups permissions when `DELETE_GROUP` is dispatched', async () => {
    // given
    const deleteItemId = '2';

    server.use(
      rest.delete(`${backend}identity/group/${deleteItemId}`, (req, res, ctx) =>
        res(resJson(ctx))
      ),
      rest.get(`${backend}identity/group`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, listGroupsResponse))
      )
    );

    store.dispatch(requestAction(LOAD_GROUPS));
    await store.waitFor(LOAD_GROUPS.success);

    // when
    store.dispatch(requestAction(DELETE_GROUP, { id: deleteItemId }));
    await store.waitFor(DELETE_GROUP.success);

    // then
    const { itemList } = store.getState().group;
    expect(itemList[0]).toMatchObject(
      withUpdatedPermissionsObject(deleteItemId)(GroupFromJSON(listItem))
    );
  });
});

describe('withUpdatedPermissionsObject', () => {
  it.each`
    deletedItemId | expected
    ${1}          | ${[{ permission: 10, objects: [2] }, { permission: 12, objects: [2] }]}
    ${2}          | ${[{ permission: 10, objects: [1] }]}
    ${3}          | ${[{ permission: 10, objects: [1, 2] }, { permission: 12, objects: [2] }]}
  `(
    'should correctly update the permissions object ' +
      'when the deleted item has id = $deletedItemId',
    async ({ deletedItemId, expected }) => {
      const { permissionsObject } = withUpdatedPermissionsObject(deletedItemId)(
        GroupFromJSON(listGroupsResponse[0])
      );
      expect(permissionsObject).toMatchObject(expected);
    }
  );
});
