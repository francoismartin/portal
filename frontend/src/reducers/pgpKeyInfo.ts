import {
  ADD_PGP_KEY_INFO,
  CLEAR_PGP_KEY_INFOS,
  ClearPgpKeyInfos,
  LOAD_PGP_KEY_INFOS,
  RETIRE_PGP_KEY_INFO,
  UPDATE_PGP_KEY_INFO,
} from '../actions/actionTypes';
import produce from 'immer';
import {
  ApiAction,
  BaseReducerState,
  initialBaseReducerState,
} from '@biomedit/next-widgets';
import { PgpKeyInfo } from '../api/generated';
import { IdRequired } from '../global';
import {
  onAddSuccess,
  onClear,
  onFetchFailure,
  onFetchRequest,
  onLoadSuccess,
  onSubmitFailure,
  onSubmitRequest,
  onUpdateSuccess,
} from './utils';

export type PgpKeyInfoState = BaseReducerState<IdRequired<PgpKeyInfo>>;

export function pgpKeyInfo(
  state: PgpKeyInfoState = initialBaseReducerState<IdRequired<PgpKeyInfo>>(),
  action:
    | ApiAction<typeof LOAD_PGP_KEY_INFOS>
    | ApiAction<typeof UPDATE_PGP_KEY_INFO>
    | ApiAction<typeof ADD_PGP_KEY_INFO>
    | ApiAction<typeof RETIRE_PGP_KEY_INFO>
    | ClearPgpKeyInfos
): PgpKeyInfoState {
  return produce(state, (draft: PgpKeyInfoState) => {
    switch (action.type) {
      case LOAD_PGP_KEY_INFOS.request:
        onFetchRequest(draft);
        break;
      case LOAD_PGP_KEY_INFOS.success:
        onLoadSuccess(draft, action.response);
        break;
      case LOAD_PGP_KEY_INFOS.failure:
        onFetchFailure(draft);
        break;
      case UPDATE_PGP_KEY_INFO.request:
      case ADD_PGP_KEY_INFO.request:
      case RETIRE_PGP_KEY_INFO.request:
        onSubmitRequest(draft);
        break;
      case ADD_PGP_KEY_INFO.success:
        onAddSuccess(draft, action.response);
        break;
      case UPDATE_PGP_KEY_INFO.success:
      case RETIRE_PGP_KEY_INFO.success:
        onUpdateSuccess(draft, action.response);
        break;
      case UPDATE_PGP_KEY_INFO.failure:
      case ADD_PGP_KEY_INFO.failure:
      case RETIRE_PGP_KEY_INFO.failure:
        onSubmitFailure(draft);
        break;
      case CLEAR_PGP_KEY_INFOS:
        onClear(draft);
    }
  });
}
