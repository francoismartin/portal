import {
  BaseReducerState,
  IdType,
  initialBaseReducerState,
} from '@biomedit/next-widgets';

export const noUpdatedItemErrorMessage =
  'Response does NOT include updated item!';
export const noIdErrorMessage = 'Response does NOT include an id!';
export const noIdRequestedErrorMessage = 'Request did NOT include an id!';

export function onFetchRequest<T extends IdType>(
  draft: BaseReducerState<T>
): void {
  draft.isFetching = true;
}

export function onLoadSuccess<T extends IdType>(
  draft: BaseReducerState<T>,
  response?: T[],
  postProcessingFunction?: (item: T) => T
): void {
  draft.isFetching = false;
  if (response) {
    draft.itemList = response as T[];
    if (postProcessingFunction) {
      draft.itemList = draft.itemList.map(postProcessingFunction);
    }
  } else {
    draft.itemList = initialBaseReducerState<T>().itemList;
  }
}

export function onFetchFailure<T extends IdType>(draft: BaseReducerState<T>) {
  draft.isFetching = false;
}

export function onSubmitRequest<T extends IdType>(
  draft: BaseReducerState<T>
): void {
  draft.isSubmitting = true;
}

export function onSubmitFailure<T extends IdType>(
  draft: BaseReducerState<T>
): void {
  draft.isSubmitting = false;
}

export function onAddSuccess<T extends IdType>(
  draft: BaseReducerState<T>,
  response?: T,
  postProcessingFunction?: (item: T) => T
): void {
  draft.isSubmitting = false;
  if (response) {
    draft.itemList.push(response);
    if (postProcessingFunction) {
      draft.itemList = draft.itemList.map(postProcessingFunction);
    }
  }
}

export function onUpdateSuccess<T extends IdType>(
  draft: BaseReducerState<T>,
  response?: T,
  postProcessingFunction?: (item: T) => T
): void {
  draft.isSubmitting = false;
  if (response) {
    const updatedItemId = response?.['id'];
    if (updatedItemId !== undefined) {
      const updatedItemIndex = draft.itemList.findIndex(
        (item) => String(item['id']) === String(updatedItemId)
      );
      draft.itemList[updatedItemIndex] = response as T;
      if (postProcessingFunction) {
        draft.itemList = draft.itemList.map(postProcessingFunction);
      }
    } else {
      console.error(noIdErrorMessage);
    }
  } else {
    console.error(noUpdatedItemErrorMessage);
  }
}

export function onDeleteSuccess<T extends IdType>(
  draft: BaseReducerState<T>,
  deletedItemId?: string,
  postProcessingFunction?: (item: T) => T
) {
  draft.isSubmitting = false;
  if (deletedItemId) {
    draft.itemList = draft.itemList.filter(
      (item) => String(item['id']) !== String(deletedItemId)
    );
    if (postProcessingFunction) {
      draft.itemList = draft.itemList.map((item) =>
        postProcessingFunction(item)
      );
    }
  } else {
    console.error(noIdRequestedErrorMessage);
  }
}

export function onClear<T extends IdType>(draft: BaseReducerState<T>) {
  draft.itemList = initialBaseReducerState<T>().itemList;
}
