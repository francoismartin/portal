import { ProjectUsers, User, Userinfo } from './api/generated';

type UserProfileType = User | Userinfo | null;
type UserType = UserProfileType | ProjectUsers;

export function getPageTitle(name: string) {
  return `Portal / ${name}`;
}

export function getEduidUsername(user: UserType): string | undefined {
  return user?.username;
}

export function getLocalUsername(user: UserProfileType): string | undefined {
  return user?.profile?.localUsername ?? undefined;
}

export function getDisplayName(user: UserProfileType): string | undefined {
  return user?.profile?.displayName;
}

export const getFullName = (user: UserType) => {
  const firstName = user?.firstName;
  const lastName = user?.lastName;
  return (
    (firstName || lastName) &&
    (firstName ? firstName + ' ' : '') + (lastName ? lastName : '')
  );
};

export function getAffiliation(user: UserProfileType): string | undefined {
  return (user?.profile?.affiliation || '-').replace(',', ', ');
}
