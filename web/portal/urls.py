"""portal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path
from rest_framework.schemas import get_schema_view

# pylint: disable=ungrouped-imports
from django.conf import settings
from . import oidc

admin.site.site_header = "BioMedIT Portal Administration"
admin.site.site_title = "BioMedIT Portal"

urlpatterns = [
    path("backend/admin/", admin.site.urls),
    path("backend/accounts/", include("django.contrib.auth.urls")),
    re_path(
        r"^auth/local/", include("rest_framework.urls", namespace="rest_framework")
    ),
    path("auth/oidc/authenticate/", oidc.authenticate, name="oidc-authenticate"),
    path("auth/oidc/callback/", oidc.callback, name="oidc-callback"),
    path("auth/oidc/logout/", oidc.logout, name="oidc-logout"),
    re_path(r"^backend/", include("projects.urls")),
    re_path(r"^backend/identity/", include("identities.urls")),
    re_path(
        r"^backend/schema/$",
        get_schema_view(title="Portal API", version="1.0.0"),
        name="schema",
    ),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
