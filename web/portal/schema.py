from rest_framework.schemas.openapi import SchemaGenerator


class CustomSchemaGenerator(SchemaGenerator):
    def __init__(
        self,
        title=None,
        url=None,
        description=None,
        patterns=None,
        urlconf=None,
        version=None,
    ):
        super().__init__(
            title=title,
            url=url,
            description=description,
            patterns=patterns,
            urlconf=urlconf,
            version="1.0.0" if version is None else version,
        )
