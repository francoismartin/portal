from django.db import transaction
from django_drf_utils.serializers.utils import (
    get_request_username,
    DetailedValidationError,
)
from rest_framework import serializers
from rest_framework.exceptions import MethodNotAllowed

from .user import UserShortSerializer
from ..models.node import Node


class NodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Node
        read_only_fields = ("id",)
        fields = read_only_fields + (
            "code",
            "name",
            "node_status",
            "ticketing_system_email",
        )

    @transaction.atomic
    def update(self, instance, validated_data):
        user = get_request_username(self)
        # Only staff is allowed to edit 'code'
        if (
            not user.is_staff
            and validated_data.get("code", instance.code) != instance.code
        ):
            raise DetailedValidationError(
                "Only staff are allowed to change node code", field="code"
            )
        return super().update(instance, validated_data)


class NodeRoleSerializer(serializers.Serializer):
    admin = UserShortSerializer(read_only=True, many=True)
    manager = UserShortSerializer(read_only=True, many=True)
    viewer = UserShortSerializer(read_only=True, many=True)

    def create(self, validated_data):
        raise MethodNotAllowed(method="POST")

    def update(self, instance, validated_data):
        raise MethodNotAllowed(method="PUT")
