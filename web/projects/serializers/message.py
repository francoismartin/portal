from rest_framework import serializers

from ..models.message import Message


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        read_only_fields = ("id", "title", "body", "created")
        fields = read_only_fields + ("status",)
