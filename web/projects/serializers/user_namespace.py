from rest_framework import serializers

from ..models.user import UserNamespace


class UserNamespaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserNamespace
        fields = ("id", "name")
