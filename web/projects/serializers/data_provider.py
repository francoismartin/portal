from django.db import transaction
from rest_framework import serializers
from rest_framework.exceptions import MethodNotAllowed

from .user import UserShortSerializer
from ..models.data_provider import DataProvider
from ..models.node import Node


class DataProviderShortSerializer(serializers.ModelSerializer):
    node = serializers.SlugRelatedField(
        slug_field="code",
        queryset=Node.objects.all(),
    )

    class Meta:
        model = DataProvider
        read_only_fields = ("id",)
        fields = read_only_fields + (
            "code",
            "name",
            "enabled",
            "node",
        )


class DataProviderSerializer(DataProviderShortSerializer):
    coordinators = UserShortSerializer(
        many=True,
        read_only=True,
        help_text="Users who are allowed to change data transfer approval status.",
    )

    class Meta(DataProviderShortSerializer.Meta):
        coordinators = ("coordinators",)
        read_only_fields = (
            DataProviderShortSerializer.Meta.read_only_fields + coordinators
        )
        fields = DataProviderShortSerializer.Meta.fields + coordinators

    @transaction.atomic
    def create(self, validated_data):
        return super().create(validated_data)


class DataProviderRoleSerializer(serializers.Serializer):
    data_engineer = UserShortSerializer(read_only=True, many=True)
    coordinator = UserShortSerializer(read_only=True, many=True)
    manager = UserShortSerializer(read_only=True, many=True)
    security_officer = UserShortSerializer(read_only=True, many=True)
    technical_admin = UserShortSerializer(read_only=True, many=True)
    viewer = UserShortSerializer(read_only=True, many=True)

    def create(self, validated_data):
        raise MethodNotAllowed(method="POST")

    def update(self, instance, validated_data):
        raise MethodNotAllowed(method="PUT")
