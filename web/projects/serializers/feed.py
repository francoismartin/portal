from rest_framework import serializers

from ..models.feed import Feed


class FeedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feed
        fields = ("created", "label", "title", "message")
