from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import validators
from django.db import models, transaction
from django.db.models import QuerySet
from django_drf_utils.serializers.fields import EnumChoiceField
from django_drf_utils.serializers.utils import (
    DetailedValidationError,
    get_request_username,
    update_related_fields,
)
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from ..models.node import Node
from ..models.project import (
    Project,
    ProjectIPAddresses,
    ProjectRole,
    ProjectUserRoleHistory,
    Resource,
    ProjectUserRole,
)
from ..permissions.node import has_node_admin_permission
from ..signals.project import (
    update_permissions_signal,
    get_project_change_roles,
    DictUserRole,
)

User = get_user_model()

ARCHIVED_READ_ONLY_MESSAGE = "Archived Projects are read-only."


def check_archived_read_only(instance: Project):
    if instance.archived:
        raise ValidationError(ARCHIVED_READ_ONLY_MESSAGE)


class ResourceSerializer(serializers.ModelSerializer):
    location = serializers.CharField(
        validators=[
            validators.URLValidator(schemes=["http", "https", "ftp", "ftps", "ssh"])
        ]
    )

    class Meta:
        model = Resource
        read_only_fields = ("id",)
        fields = read_only_fields + ("name", "location", "description", "contact")


class ProjectIPAddressesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectIPAddresses
        fields = ("ip_address", "mask")


class ProjectUserSerializer(serializers.ModelSerializer):
    """ProjectUser serializer"""

    # explicit `id` field to make `id` NOT `read_only` to allow passing in `id` when updating roles
    # see https://www.django-rest-framework.org/api-guide/serializers/#customizing-multiple-update
    id = serializers.IntegerField(required=False)
    roles = serializers.ListField(child=EnumChoiceField(ProjectRole), allow_empty=True)
    affiliation = serializers.CharField(source="profile.affiliation", read_only=True)
    affiliation_id = serializers.CharField(
        source="profile.affiliation_id", read_only=True
    )
    display_name = serializers.CharField(source="profile.display_name", read_only=True)
    uid = serializers.CharField(source="profile.uid", read_only=True)
    gid = serializers.CharField(source="profile.gid", read_only=True)
    local_username = serializers.CharField(
        source="profile.local_username", read_only=True
    )

    class Meta:
        model = User
        read_only_fields = (
            "username",
            "local_username",
            "email",
            "first_name",
            "last_name",
            "display_name",
            "uid",
            "gid",
            "affiliation",
            "affiliation_id",
        )
        fields = read_only_fields + (
            "id",
            "roles",
        )


class ProjectUserListSerializer(serializers.ListSerializer):
    """ProjectUserList serializer

    Groups / ungroups rows of ProjectUserRole by user into / from  ProjectUser rows
    """

    @classmethod
    def role_by_name(cls, name) -> ProjectRole:
        try:
            return ProjectRole[name]
        except KeyError as e:
            raise DetailedValidationError(f"Invalid permission type: {e}") from e

    def to_representation(self, data):
        iterable = data.all() if isinstance(data, models.Manager) else data
        roles_by_user: dict[User, list[ProjectRole]] = {}
        user_role: ProjectUserRole
        for user_role in iterable.prefetch_related("user__profile").all():
            roles_by_user.setdefault(user_role.user, []).append(
                ProjectRole(user_role.role).name
            )
        # Extend User class with the roles field
        for user, roles in roles_by_user.items():
            user.roles = roles
        return [ProjectUserSerializer(user).data for user in roles_by_user]

    def to_internal_value(self, data):
        try:
            user_roles: list[tuple[int, list[ProjectRole]]] = [
                (int(project_user["id"]), project_user.get("roles", ()))
                for project_user in data
            ]
        except ValueError as e:
            raise DetailedValidationError("Invalid user field") from e
        except KeyError as e:
            raise DetailedValidationError("Missing user field") from e
        users: dict[int, User] = {
            u.id: u
            for u in User.objects.filter(pk__in=(user for user, _ in user_roles))
        }
        try:
            return [
                dict(
                    user=users[user],
                    role=self.role_by_name(role),
                )
                for user, roles in user_roles
                for role in roles
            ]
        except KeyError as e:
            raise DetailedValidationError(f"User does not exist: {e}") from e

    def save(self, **kwargs):
        raise NotImplementedError(
            "Save currently not supported (suppresses .save from ListSerializer"
        )


class ProjectRestrictedSerializer(serializers.ModelSerializer):
    """Basic/default project serializer"""

    users = ProjectUserListSerializer(child=ProjectUserSerializer())
    ip_address_ranges = ProjectIPAddressesSerializer(
        many=True,
        read_only=True,
    )
    resources = ResourceSerializer(
        many=True,
        read_only=True,
    )
    permissions = serializers.SerializerMethodField()
    destination = serializers.SlugRelatedField(
        queryset=Node.objects.all(),
        read_only=False,
        slug_field="code",
        required=False,
    )
    data_transfer_count = serializers.IntegerField(
        source="data_transfers.count", read_only=True
    )

    class Meta:
        model = Project
        read_only_fields = (
            "id",
            "created",
            "gid",
            "name",
            "code",
            "destination",
            "permissions",
            "data_transfer_count",
            "archived",
        )
        fields = read_only_fields + (
            "ip_address_ranges",
            "resources",
            "users",
        )

    @staticmethod
    def prefetch(queryset: QuerySet):
        return queryset.select_related("destination").prefetch_related(
            "ip_address_ranges",
            "resources",
        )

    def get_permissions(self, obj):
        requestor = get_request_username(self)
        staff = requestor.is_staff
        node_admin = has_node_admin_permission(requestor, obj.destination)
        return {
            "edit": {
                "name": not obj.archived and (staff or node_admin),
                "code": not obj.archived and (staff or node_admin),
                "destination": not obj.archived and staff,
                "users": []
                if obj.archived
                else [p.name for p in get_project_change_roles(requestor, obj)],
                "ip_address_ranges": not obj.archived and (staff or node_admin),
                "resources": not obj.archived and (staff or node_admin),
            },
            "archive": staff or node_admin,
        }

    @transaction.atomic
    def create(self, validated_data):
        """
        :raises: ServiceUnavailable
        """
        users: list[DictUserRole] = validated_data.pop("users", [])
        project: Project = super().create(validated_data)
        update_permissions_signal.send(
            sender=project, user=get_request_username(self), user_roles=users
        )
        return project

    @transaction.atomic
    def update(self, instance, validated_data):
        """
        :raises: ServiceUnavailable, ValidationError
        """
        check_archived_read_only(instance)
        update_permissions_signal.send(
            sender=instance,
            user=get_request_username(self),
            user_roles=validated_data.pop("users", []),
        )
        return super().update(instance, validated_data)

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if "users" in attrs:
            inactive = [
                user_role["user"].username
                for user_role in attrs["users"]
                if not user_role["user"].is_active
            ]
            if inactive:
                _users = ",".join(f'"{u}"' for u in inactive)
                msg = f"Cannot add inactive users {_users} to project"
                raise DetailedValidationError(msg, field="users")
        return attrs


class ProjectSerializer(ProjectRestrictedSerializer):
    """Extension of restricted project serializer for staff (PA - Portal Admin)"""

    ip_address_ranges = ProjectIPAddressesSerializer(many=True, required=False)
    resources = ResourceSerializer(many=True, required=False)
    # pylint: disable=no-member
    gid = serializers.IntegerField(
        validators=(
            validators.MinValueValidator(settings.CONFIG.project.gid.min),
            validators.MaxValueValidator(settings.CONFIG.project.gid.max),
        ),
        read_only=True,
    )

    class Meta:
        model = Project
        read_only_fields = (
            "id",
            "created",
            "gid",
            "permissions",
            "data_transfer_count",
            "archived",
        )
        fields = read_only_fields + (
            "name",
            "code",
            "destination",
            "ip_address_ranges",
            "resources",
            "users",
        )

    @transaction.atomic
    def create(self, validated_data):
        gid_max = Project.objects.aggregate(models.Max("gid"))["gid__max"]
        # pylint: disable=no-member
        validated_data["gid"] = (
            gid_max + 1 if gid_max else settings.CONFIG.project.gid.min
        )

        ip_address_ranges = validated_data.pop("ip_address_ranges", [])
        resources = validated_data.pop("resources", [])

        project = super().create(validated_data)

        update_related_fields(project, ProjectIPAddressesSerializer, ip_address_ranges)
        update_related_fields(project, ResourceSerializer, resources)
        return project

    @transaction.atomic
    def update(self, instance, validated_data):
        """
        :raises: ValidationError
        """
        check_archived_read_only(instance)
        ip_address_ranges = validated_data.pop("ip_address_ranges", [])
        resources = validated_data.pop("resources", [])
        update_related_fields(instance, ProjectIPAddressesSerializer, ip_address_ranges)
        update_related_fields(instance, ResourceSerializer, resources)
        return super().update(instance, validated_data)

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if attrs["name"] == attrs["code"]:
            raise DetailedValidationError(
                "Project code must not be the same as project name!", field="code"
            )
        return attrs


class ProjectUserRoleHistorySerializer(serializers.ModelSerializer):
    timestamp = serializers.DateTimeField(source="created")
    changed_by = serializers.CharField(source="display_changed_by")
    user = serializers.CharField(source="display_user")
    project = serializers.CharField(source="project_str")
    permission = serializers.CharField(source="display_permission")

    class Meta:
        model = ProjectUserRoleHistory
        fields = ("timestamp", "changed_by", "user", "project", "permission", "enabled")


class ProjectInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ("name", "code")
        read_only_fields = ("name", "code")
