from rest_framework.parsers import JSONParser

from . import SCIM_MEDIA_TYPE


# adds support for SCIM API's content-type and accept headers
class ScimParser(JSONParser):
    media_type = SCIM_MEDIA_TYPE
