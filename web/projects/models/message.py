from django.contrib.auth import get_user_model
from django.db import models
from django_drf_utils.models import CreatedMixin
from simple_history.models import HistoricalRecords

User = get_user_model()


class Message(CreatedMixin):
    class MessageStatus(models.TextChoices):
        READ = "R"
        UNREAD = "U"

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="messages",
    )
    title = models.CharField(
        max_length=512,
        help_text="Message title",
    )
    body = models.TextField()
    status = models.CharField(
        max_length=1,
        choices=MessageStatus.choices,
        default=MessageStatus.UNREAD,
    )
    history = HistoricalRecords()
