from django.core import validators
from django.db import models
from django.forms.fields import URLField
from django_drf_utils.models import CreatedMixin

from .flag import Flag

QUICK_ACCESS_TILE_URL_SCHEMES = ["http", "https"]


class QuickAccessTileUrlFormField(URLField):
    default_validators = [
        validators.URLValidator(schemes=QUICK_ACCESS_TILE_URL_SCHEMES)
    ]


class QuickAccessTileUrlModelField(models.URLField):
    default_validators = [
        validators.URLValidator(schemes=QUICK_ACCESS_TILE_URL_SCHEMES)
    ]

    def formfield(self, **kwargs):
        return super().formfield(form_class=QuickAccessTileUrlFormField, **kwargs)


class QuickAccessTile(CreatedMixin):
    title = models.CharField(max_length=128)
    description = models.TextField(blank=True)
    url = QuickAccessTileUrlModelField()
    image = models.ImageField(upload_to="tiles")
    order = models.SmallIntegerField(
        blank=True,
        null=True,
        help_text="The order of this quick access tile (for sorting).",
    )
    flag = models.ForeignKey(Flag, on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self) -> str:
        return f"{self.title}: {self.url}"
