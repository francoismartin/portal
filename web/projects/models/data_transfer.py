from django.contrib.auth import get_user_model
from django.core import validators
from django.db import models
from django_drf_utils.models import EnumField
from simple_history.models import HistoricalRecords

from .data_provider import DataProvider
from .node import Node
from .project import Project

User = get_user_model()


class DataTransfer(models.Model):
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name="data_transfers"
    )
    max_packages = models.IntegerField(
        validators=(validators.MinValueValidator(-1),), default=1
    )

    INITIAL = "INITIAL"
    AUTHORIZED = "AUTHORIZED"
    UNAUTHORIZED = "UNAUTHORIZED"
    EXPIRED = "EXPIRED"
    LABELS = (
        (INITIAL, "Initial"),
        (AUTHORIZED, "Authorized"),
        (UNAUTHORIZED, "Unauthorized"),
        (EXPIRED, "Expired"),
    )

    status = EnumField(choices=LABELS, default=INITIAL)
    data_provider = models.ForeignKey(
        DataProvider, on_delete=models.CASCADE, related_name="data_transfers"
    )
    requestor = models.ForeignKey(User, on_delete=models.CASCADE)
    requestor_pgp_key_fp = models.CharField(
        max_length=40,
        blank=True,
        help_text="Requestor PGP Key Fingerprint",
        validators=(
            validators.RegexValidator(
                regex=r"^[0-9A-F]+$", message='Allowed characters: "A-F", "0-9"'
            ),
        ),
    )
    legal_basis = models.CharField(
        max_length=128,
        blank=True,
        help_text="A reference to the legal basis document for the data transfer",
    )
    PRODUCTION = "PRODUCTION"
    TEST = "TEST"
    PURPOSE_LABELS = (
        (PRODUCTION, "Production"),
        (TEST, "Test"),
    )
    purpose = EnumField(PURPOSE_LABELS)
    history = HistoricalRecords()
    creation_date = models.DateTimeField(auto_now_add=True, null=True)
    change_date = models.DateTimeField(auto_now=True, null=True)

    @property
    def requestor_name(self):
        return self.requestor.profile.display_name

    @property
    def max_packages_repr(self):
        return "One time" if self.max_packages == 1 else "Recurring"

    @property
    def approvals(self):
        return tuple(
            list(self.node_approvals.all())
            + list(self.data_provider_approvals.all())
            + list(self.group_approvals.all())
        )

    def __str__(self) -> str:
        return f"DTR {self.id} ({self.purpose}, {self.status})"


class DataPackage(models.Model):
    metadata_hash = models.CharField(max_length=256, unique=True)
    data_transfer = models.ForeignKey(
        DataTransfer, on_delete=models.CASCADE, related_name="packages"
    )
    file_name = models.TextField()
    file_size = models.PositiveBigIntegerField(
        blank=True, null=True, verbose_name="Package Size"
    )
    metadata = models.TextField()
    PRODUCTION = "PRODUCTION"
    TEST = "TEST"
    PURPOSE_LABELS = (
        (PRODUCTION, "Production"),
        (TEST, "Test"),
    )
    purpose = EnumField(PURPOSE_LABELS)
    history = HistoricalRecords()


class DataPackageTrace(models.Model):
    """Trace of a data packages (timestamps of successful processings by lz's of that package)"""

    data_package = models.ForeignKey(
        DataPackage, on_delete=models.CASCADE, related_name="trace"
    )
    node = models.ForeignKey(Node, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    ENTER = "ENTER"
    PROCESSED = "PROCESSED"
    ERROR = "ERROR"
    STATUS_LABELS = ((ENTER, "Enter"), (PROCESSED, "Processed"), (ERROR, "Error"))
    status = EnumField(STATUS_LABELS)
