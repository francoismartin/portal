from django.db import models


# using attribute `last_run_at` in `PeriodicTask` from `django_celery_beat` is not reliable, see:
# https://dev.to/vergeev/django-celery-beat-how-to-get-the-last-time-a-periodictask-was-run-39k9
class PeriodicTaskRun(models.Model):
    task = models.CharField(max_length=512, verbose_name="Task Name")
    created_at = models.DateTimeField()

    class Meta:
        get_latest_by = "created_at"

    def __str__(self) -> str:
        return f"{self.task} ({self.created_at})"
