from typing import Iterator

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import models
from django.db.models import QuerySet
from django.dispatch import receiver
from django.utils.functional import cached_property
from django_drf_utils.models import CodeField, NameField
from guardian.shortcuts import (
    assign_perm,
    get_objects_for_user,
    get_users_with_perms,
    get_group_perms,
)

from identities.models import GroupProfile
from .const import (
    APP_PERM_CHANGE_GROUP,
    APP_PERM_DATA_PROVIDER_ADMIN,
    APP_PERM_DATA_PROVIDER_APPROVAL_STATUS_CHANGE,
    APP_PERM_DATA_PROVIDER_VIEWER,
    APP_PERM_VIEW_GROUP,
    PERM_DATA_PROVIDER_ADMIN,
    PERM_DATA_PROVIDER_VIEWER,
    PERM_DATA_PROVIDER_APPROVAL_STATUS,
)
from .node import Node

User = get_user_model()


class DataProvider(models.Model):
    code = CodeField()
    name = NameField()
    node = models.ForeignKey(Node, on_delete=models.SET_NULL, null=True, blank=False)
    enabled = models.BooleanField(default=True)

    class Meta:
        permissions = [
            (
                PERM_DATA_PROVIDER_ADMIN,
                "Can edit a data provider (except for Code and Node)",
            ),
        ]

    def __str__(self) -> str:
        return f"{self.name} ({self.code})"

    @cached_property
    def coordinators(self) -> Iterator[User]:
        return get_data_provider_coordinators(self)


# DP Viewer


def get_data_provider_viewer_data_providers(
    user: User,
) -> QuerySet[DataProvider]:
    return get_objects_for_user(user, APP_PERM_DATA_PROVIDER_VIEWER)


def get_data_provider_viewers(data_provider: DataProvider) -> QuerySet[User]:
    """Returns all DP viewers registered in this DP.

    Note that all roles with view permission, including DP coordinators, will be
    recognised as DP viewers as well.
    """
    return get_users_with_perms(
        data_provider,
        only_with_perms_in=[
            PERM_DATA_PROVIDER_VIEWER,
        ],
    )


def has_data_provider_viewer_data_providers(user: User) -> bool:
    return get_data_provider_viewer_data_providers(user).exists()


def has_data_provider_viewer_permission(
    user: User, data_provider: DataProvider | None = None
) -> bool:
    return user.has_perm(APP_PERM_DATA_PROVIDER_VIEWER, data_provider)


# DP Coordinator


def has_data_provider_coordinator_permission(
    user: User, data_provider: DataProvider | None = None
) -> bool:
    """Whether given user has DP Coordinator permission on given Data Provider.

    Being added to a group means that the user inherits the permissions of this group.
    Note that this method returns `True` for superusers.
    """
    return user.has_perm(
        APP_PERM_DATA_PROVIDER_APPROVAL_STATUS_CHANGE
    ) and has_data_provider_viewer_permission(user, data_provider)


def get_data_provider_coordinators(data_provider: DataProvider) -> Iterator[User]:
    """Return list of DP coordinators.

    A DP coordinator has been effectively assigned to the
    DP coordinators group (no shortcut for superusers).
    """

    def has_data_provider_approval_permission(user):
        # We can NOT use helper methods like `user.get_group_permissions()` or
        # `user.get_user_permissions()` as they return everything in case of superuser.
        for group in user.groups.all():
            # Find the group associated to given DP
            perms = get_group_perms(group, data_provider)
            if (
                perms
                # Given user has permission on THAT group
                and group.permissions.filter(
                    codename=PERM_DATA_PROVIDER_APPROVAL_STATUS
                ).exists()
            ):
                return True
        return False

    yield from (
        user
        for user in get_data_provider_viewers(data_provider)
        if has_data_provider_approval_permission(user)
    )


# DP entry administration rights (NOT to be confused with 'DP Technical Admin')


def get_data_provider_admin_data_providers(
    user: User,
) -> QuerySet[DataProvider]:
    return get_objects_for_user(user, APP_PERM_DATA_PROVIDER_ADMIN)


def has_data_provider_admin_data_providers(user: User) -> bool:
    return get_data_provider_admin_data_providers(user).exists()


def has_data_provider_admin_permission(
    user: User, data_provider: DataProvider | None = None
) -> bool:
    return user.has_perm(APP_PERM_DATA_PROVIDER_ADMIN, data_provider)


def has_any_data_provider_permissions(user: User) -> bool:
    return get_data_provider_permission_data_providers(user).exists()


def get_data_provider_permission_data_providers(
    user: User,
) -> QuerySet[DataProvider]:
    """Returns all the data providers associated with given user either by a
    `admin_dataprovider` (DP Manager) or a `view_dataprovider`
    (DP Viewer, DP Technical Admin, DP Coordinator) permission.
    """
    return get_data_provider_admin_data_providers(
        user
    ) | get_data_provider_viewer_data_providers(user)


def assign_dp_manager(
    dp_manager: Group,
    dp: DataProvider,
    *groups: Group,
):
    for group in groups:
        assign_perm(APP_PERM_VIEW_GROUP, dp_manager, group)
        assign_perm(APP_PERM_CHANGE_GROUP, dp_manager, group)
    # DP managers are only allowed to edit their own DP entry
    assign_perm(APP_PERM_DATA_PROVIDER_ADMIN, dp_manager, dp)


def get_data_provider_managers(dp: DataProvider) -> QuerySet[User]:
    return get_users_with_perms(
        dp,
        only_with_perms_in=[
            PERM_DATA_PROVIDER_ADMIN,
        ],
    )


def assign_dp_viewer(dp_viewer: Group, dp: DataProvider):
    assign_perm(APP_PERM_DATA_PROVIDER_VIEWER, dp_viewer, dp)


def assign_dp_technical_admin(dp_technical_admin: Group, dp: DataProvider):
    """DP Technical Admins might get more permissions in the future."""
    assign_dp_viewer(dp_technical_admin, dp)


def assign_dp_coordinator(dp_coordinator: Group, dp: DataProvider):
    """DP Coordinators might get more permissions in the future."""
    assign_dp_viewer(dp_coordinator, dp)
    assign_perm(APP_PERM_DATA_PROVIDER_APPROVAL_STATUS_CHANGE, dp_coordinator)


# pylint: disable=unused-argument
@receiver(models.signals.post_save, sender=DataProvider)
def create_dp_groups(sender, instance, created, *args, **kwargs):
    if created:
        dp_ta = Group.objects.create(
            name=f"{instance.code} DP Technical Admin",
        )
        GroupProfile.objects.create(
            group=dp_ta,
            description="Responsible for all technical tasks required for the setup and "
            "maintenance of a secure connection from the DP organization to the BioMedIT Node.",
        )
        assign_dp_technical_admin(dp_ta, instance)

        dp_viewer = Group.objects.create(name=f"{instance.code} DP Viewer")
        GroupProfile.objects.create(
            group=dp_viewer,
            description="A DP Viewer can monitor the status of data transfers from their DP institution.",
        )
        assign_dp_viewer(dp_viewer, instance)

        dp_coordinator = Group.objects.create(
            name=f"{instance.code} DP Coordinator",
        )
        GroupProfile.objects.create(
            group=dp_coordinator,
            description="Responsible for internally coordinating the DP's readiness to send data, "
            "ensuring the legal compliance, i.e., DTUA, DTPA, etc. and all technical measures "
            "required from the DP side are in place for projects in which Data Provider "
            "institution participates.",
        )
        assign_dp_coordinator(dp_coordinator, instance)

        dp_de = Group.objects.create(
            name=f"{instance.code} DP Data Engineer",
        )
        GroupProfile.objects.create(
            group=dp_de,
            description="Responsible to prepare, encrypt and sign the data packages, "
            "and transfer them from the DP organization to the assigned BioMedIT Node.",
        )
        assign_dp_viewer(dp_de, instance)

        dp_so = Group.objects.create(
            name=f"{instance.code} DP Security Officer",
        )
        GroupProfile.objects.create(
            group=dp_so,
            description="The designated point of contact from the DP organization to receive incident and security "
            "notifications from BioMedIT such as (but not limited to) scheduled and non-scheduled downtimes "
            "in response to security incidents, emergency changes and systems upgrades.",
        )
        assign_dp_viewer(dp_so, instance)

        dp_manager = Group.objects.create(
            name=f"{instance.code} DP Manager",
        )
        GroupProfile.objects.create(
            group=dp_manager,
            description="Represents the Data Provider institution within the scope of BioMedIT. "
            "Responsible for the institution's internal processes related to BioMedIT, "
            "delegates responsibilities to other users in their organization by assigning/removing DP roles.",
        )
        assign_dp_manager(
            dp_manager, instance, dp_ta, dp_viewer, dp_coordinator, dp_de, dp_so
        )
