from django.contrib.auth import get_user_model
from django.core import validators
from django.db import models
from django.dispatch import receiver
from django.utils.functional import cached_property
from simple_history.models import HistoricalRecords

User = get_user_model()


class UserNamespace(models.Model):
    name = models.CharField(
        max_length=4,
        unique=True,
        validators=(
            validators.MinLengthValidator(2),
            validators.RegexValidator(
                regex=r"^[a-z]{2,4}$",
                message='Enter a valid value. Allowed characters: "a-z"',
            ),
        ),
    )

    def __str__(self):
        return f"Namespace ({self.name})"


PROFILE_LOCAL_USERNAME_REGEX = r"^[a-z][a-z0-9_]*$"
PROFILE_LOCAL_USERNAME_REGEX_MESSAGE = (
    "Enter a valid value. "
    'Allowed characters: "a-z", "0-9", "_". '
    "Must start with a lowercase letter."
)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    affiliation = models.CharField(max_length=512, blank=True)
    affiliation_id = models.CharField(max_length=512, blank=True)
    local_username = models.CharField(
        blank=True,
        null=True,
        max_length=64,
        validators=(
            validators.RegexValidator(
                regex=PROFILE_LOCAL_USERNAME_REGEX,
                message=PROFILE_LOCAL_USERNAME_REGEX_MESSAGE,
            ),
        ),
    )
    namespace = models.ForeignKey(
        UserNamespace, on_delete=models.SET_NULL, blank=True, null=True
    )
    uid = models.PositiveIntegerField(blank=True, null=True, unique=True)
    gid = models.PositiveIntegerField(blank=True, null=True, unique=True)
    affiliation_consent = models.BooleanField(default=False)

    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["namespace", "local_username"], name="unique username"
            )
        ]

    def __str__(self):
        return f"Profile ({self.user.username})"

    @cached_property
    def display_id(self):
        return (
            f"ID: {self.user.username.split('@')[0]}"
            if "@" in self.user.username
            else None
        )

    @cached_property
    def display_name(self):
        if self.user.first_name or self.user.last_name:
            name = " ".join(filter(bool, (self.user.first_name, self.user.last_name)))
        else:
            name = self.local_username or self.user.username
        display_id = self.display_id and f"({self.display_id})"
        display_email = self.user.email and f"({self.user.email})"
        return " ".join(filter(bool, (f"{name}", display_id, display_email)))

    @cached_property
    def display_local_username(self):
        return "_".join(
            filter(None, (self.namespace and self.namespace.name, self.local_username))
        )


@receiver(models.signals.post_save, sender=User)
def create_or_update_user_profile(
    sender, instance, created, **kwargs
):  # pylint: disable=unused-argument
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()
