import logging

from django.contrib.auth import get_user_model
from rest_framework.permissions import (
    BasePermission,
    DjangoModelPermissions,
    IsAuthenticated,
    SAFE_METHODS,
)

from identities.permissions import has_change_user_permission

logger = logging.getLogger(__name__)

User = get_user_model()


class UserUniquePermission(BasePermission):
    methods = ("POST",)

    def has_permission(self, request, view):
        return (
            request.user
            and request.user.is_authenticated
            and request.method in self.methods
            and request.user.is_staff
        )


class UserModelPermission(DjangoModelPermissions):
    """Applies same permissions to user object than to user model (`has_object_permission`
    returning `True` by default in `super()`).

    And ensures that caller is applying PATCH updates ONLY.
    """

    def has_permission(self, request, view):
        return (
            super().has_permission(request, view)
            and request.method == UserDataPermission.update_user_method
        )

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)


class UserDataPermission(IsAuthenticated):
    update_user_method = "PATCH"
    create_local_user_method = "POST"

    def has_permission(self, request, view):
        if super().has_permission(request, view):
            if request.method == self.create_local_user_method:
                return request.user.is_staff
            return True
        return False

    # pylint: disable=too-many-return-statements
    def has_object_permission(self, request, view, obj):  # NOT called on POST!
        if request.method in SAFE_METHODS:
            # non-modifying operation
            return True

        # modifying operation
        requestor = request.user
        request_profile = request.data.get("profile", {})

        if request.method == self.update_user_method:
            username_field = "local_username"
            username_allowed = self.check_profile_field(
                request_profile, username_field, obj, requestor
            )

            consent_field = "affiliation_consent"
            consent_allowed = self.check_profile_field(
                request_profile, consent_field, obj, requestor
            )

            # check if either username or consent are defined in request
            if any(k in request_profile for k in (username_field, consent_field)):
                return username_allowed and consent_allowed

            # attempt to modify user
            if not requestor.is_staff:
                if not has_change_user_permission(requestor):
                    logger.error(
                        "Non-authorized user (ID: %s) attempted to modify user (ID: %s) "
                        "with data: %s",
                        requestor.id,
                        obj.id,
                        request.data,
                    )
                return False

            return True

        # not allowed modifying method
        logger.error(
            "User (ID: %s) attempted to modify user (ID: %s) "
            "using not allowed method %s with data: %s",
            requestor.id,
            obj.id,
            request.method,
            request.data,
        )
        return False

    @staticmethod
    def check_profile_field(
        request_profile, field_name: str, obj, requestor: User
    ) -> bool:
        """
        Checks if requestor has permission to modify field_name and the value of the field has not
        been set before.
        :return: True if requestor has permission to modify field_name in request_profile and the
                 value of the field has not been set before or if field_name is not defined
                 in the request.
                 False if requestor does not have permission to modify field_name in request_profile
                 or the value of the field has been set before.
        """
        if field_name in request_profile:
            # attempt to set field
            new_field_value = request_profile[field_name]

            # Only requestor can change their own field
            if requestor.id != obj.id:
                logger.error(
                    "User (ID: %s) attempted to set %s (%s) for user (ID: %s)",
                    requestor.id,
                    field_name,
                    new_field_value,
                    obj.id,
                )
                return False

            # field cannot be changed after it's set the first time
            if getattr(requestor.profile, field_name):
                logger.error(
                    "User (ID: %s) attempted to change their %s to %s after it was initially set",
                    requestor.id,
                    field_name,
                    new_field_value,
                )
                return False

        return True
