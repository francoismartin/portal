from rest_framework.permissions import BasePermission, SAFE_METHODS

from . import SAFE_AND_UPDATE_METHODS
from ..models.node import (
    has_node_admin_nodes,
    has_node_admin_permission,
    has_node_viewer_nodes,
    has_node_viewer_permission,
)


class IsNodeAdmin(BasePermission):
    """Node Admin can modify a node except for Node ID."""

    def has_permission(self, request, view):
        user = request.user
        return (
            user
            and user.is_authenticated
            and request.method in SAFE_AND_UPDATE_METHODS
            and has_node_admin_nodes(user)
        )

    def has_object_permission(self, request, view, obj):
        return has_node_admin_permission(request.user, obj)


class IsNodeViewer(BasePermission):
    """Node Viewer can view a node including associated users and their roles."""

    def has_permission(self, request, view):
        user = request.user
        return (
            user
            and user.is_authenticated
            and request.method in SAFE_METHODS
            and has_node_viewer_nodes(user)
        )

    def has_object_permission(self, request, view, obj):
        return has_node_viewer_permission(request.user, obj)
