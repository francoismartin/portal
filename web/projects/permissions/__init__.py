from django.contrib.auth import get_user_model
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.permissions import IsAuthenticated, BasePermission, SAFE_METHODS

User = get_user_model()

UPDATE_METHODS = ("PUT", "PATCH")
SAFE_AND_UPDATE_METHODS = SAFE_METHODS + UPDATE_METHODS


class IsStaff(IsAuthenticated):
    """Staff (or PA - Portal Admin) can read/write everything."""

    def has_permission(self, request, view):
        return super().has_permission(request, view) and request.user.is_staff

    def has_object_permission(self, request, view, _):
        """Can access and perform any action on any object"""
        return self.has_permission(request, view)


class ReadOnly(IsAuthenticated):
    """Authorized users with read-only access."""

    def has_permission(self, request, view):
        return super().has_permission(request, view) and request.method in SAFE_METHODS

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)


class IsStaffOrReadOnly(BasePermission):
    """Authenticated users can view, staff can edit."""

    def has_permission(self, request, view):
        return (
            request.user
            and request.user.is_authenticated
            and (request.method in SAFE_METHODS or request.user.is_staff)
        )


class IsAuthenticatedAndUniqueCheck(BasePermission):
    """Allows authenticated users to perform a unique check"""

    def has_permission(self, request, view):
        return (
            request.user and request.user.is_authenticated and request.method == "POST"
        )


class ObjectPermission(IsAuthenticated):
    """Check if user has correct object-level permission for the action

    This class is roughly based on rest_framework.permissions.DjangoObjectPermissions
    """

    perms_map = {
        "GET": ["{app_label}.view_{model_name}"],
        "OPTIONS": [],
        "HEAD": ["{app_label}.view_{model_name}"],
        "PUT": ["{app_label}.change_{model_name}"],
        "PATCH": ["{app_label}.change_{model_name}"],
        "DELETE": ["{app_label}.delete_{model_name}"],
    }

    def get_required_object_permissions(self, method, model_cls):
        kwargs = {
            "app_label": model_cls._meta.app_label,
            "model_name": model_cls._meta.model_name,
        }

        if method not in self.perms_map:
            raise MethodNotAllowed(method)

        return tuple(perm.format(**kwargs) for perm in self.perms_map[method])

    def has_permission(self, request, view):
        if not super().has_permission(request, view):
            return False
        if request.method not in self.perms_map:
            raise MethodNotAllowed(request.method)
        return True

    def has_object_permission(self, request, view, obj):
        perms = self.get_required_object_permissions(request.method, obj.__class__)
        return request.user.has_perms(perms, obj)


class IsOwner(IsAuthenticated):
    """Only the owner of an object can access it.

    Owner is the user assigned to the `user` field of the given object.
    """

    def has_object_permission(self, request, view, obj):
        return obj.user == request.user
