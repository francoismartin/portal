from django.conf import settings
from rest_framework.permissions import IsAuthenticated


# pylint: disable=no-member
class IsSwitchNotificationUser(IsAuthenticated):
    """Only the switch notification API can access."""

    def has_permission(self, request, view):
        return (
            # if no switch config is defined, disable access entirely
            bool(settings.CONFIG.switch)
            and super().has_permission(request, view)
            and (request.method == "PUT")
            and request.user.username == settings.CONFIG.switch.auth_username
        )
