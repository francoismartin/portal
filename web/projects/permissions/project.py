from django.core.exceptions import ObjectDoesNotExist
from rest_framework.permissions import IsAuthenticated, SAFE_METHODS

from ..models.node import Node, has_node_admin_permission, has_node_admin_nodes
from ..models.project import ProjectRole, is_manager


class ProjectPermission(IsAuthenticated):
    modify_methods = ("PUT",)
    create_modify_methods = ("POST",) + modify_methods

    def has_permission(self, request, view):
        return super().has_permission(request, view) and self.project_has_permission(
            request
        )

    def has_object_permission(self, request, view, obj):
        return (
            request.method in SAFE_METHODS
            or request.user.is_staff
            or (
                request.method in self.modify_methods
                and obj.users.filter(
                    user__id=request.user.id,
                    role__in=(
                        ProjectRole.PM.value,
                        ProjectRole.PL.value,
                    ),
                ).exists()
            )
            or (
                request.method in self.create_modify_methods
                and has_node_admin_permission(request.user, obj.destination)
                # node admin cannot change node of a project
                and request.data["destination"] == obj.destination.code
            )
        )

    def project_has_permission(self, request):
        try:
            node = Node.objects.get(code=request.data["destination"])
        except (ObjectDoesNotExist, KeyError):
            node = None
        return (
            request.method in SAFE_METHODS
            or request.user.is_staff
            or (is_manager(request.user) and request.method in self.modify_methods)
            or (
                has_node_admin_permission(request.user, node)
                and request.method in self.create_modify_methods
            )
        )


class ProjectUniquePermission(ProjectPermission):
    modify_methods = ("POST",)

    def has_permission(self, request, view):
        return super().has_permission(request, view) or (
            request.method in self.modify_methods and has_node_admin_nodes(request.user)
        )


class IsNodeAdmin(IsAuthenticated):
    """Allows Node Admins to update projects of the nodes they are administrating."""

    UPDATE_METHODS = ("PUT",)

    def has_permission(self, request, view):
        user = request.user
        return (
            super().has_permission(request, view)
            and has_node_admin_nodes(user)
            and request.method in self.UPDATE_METHODS
        )

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view) and has_node_admin_permission(
            request.user, obj.destination
        )
