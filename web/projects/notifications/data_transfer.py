from django.conf import settings

from ..models.data_transfer import DataTransfer
from ..models.project import ProjectRole


def create_data_transfer_creation_notification(dtr: DataTransfer) -> tuple[str, str]:
    subject = create_subject(dtr)
    is_production_dtr = dtr.purpose == DataTransfer.PRODUCTION
    legal_basis = (
        f"- There is an existing legal basis for the Data Transfer ('{dtr.legal_basis}')"
        if is_production_dtr
        else ""
    )
    node_names = ", ".join(approval.node.name for approval in dtr.node_approvals.all())
    # pylint: disable=no-member
    body = f"""Dear {node_names}, {dtr.data_provider.name},

We have received the following Data Transfer Request (DTR-{dtr.id}) from the {dtr.project.name} Project: 

{project_metadata(dtr)}

Declarations: 

  - I agreed with the data provider which data should be sent
  - I have a valid public key signed by the DCC and uploaded to the keyserver
  {legal_basis}

Please approve or reject this request using the Data Transfer Request Approval form.

If you have any questions or need support, please contact {settings.CONFIG.notification.ticket_mail}

Kind Regards,

BioMedIT Team

"""
    return subject, body


SEP = ", "


def dp_coordinators(dtr):
    return SEP.join(
        f"{coordinator.first_name} {coordinator.last_name} ({coordinator.email})"
        for coordinator in dtr.data_provider.coordinators
    )


def project_leads(dtr: DataTransfer) -> str:
    return SEP.join(
        f"{user.first_name} {user.last_name}"
        for user in dtr.project.users_by_role(ProjectRole.PL)
    )


def create_subject(dtr: DataTransfer) -> str:
    return (
        f"[DTR-{dtr.id}] - Data Transfer Request from "
        f"'{dtr.data_provider.name}' to '{dtr.project.name}' project"
    )


def project_metadata(dtr: DataTransfer):
    return f"""  - Project Name: {dtr.project.name}
  - Project Lead: {project_leads(dtr)}
  - Data Provider: {dtr.data_provider.name}, {dp_coordinators(dtr)}
  - Data Manager: {dtr.requestor.first_name} {dtr.requestor.last_name} ({dtr.requestor.email})
  - Frequency of transfer: {dtr.max_packages_repr}
  - Full fingerprint of the key:  {dtr.requestor_pgp_key_fp}
  - Data to be transferred is real patient data: {'Yes' if dtr.purpose == DataTransfer.PRODUCTION else 'No'}
"""
