from itertools import groupby

from ..models.project import Project, ProjectRole, ProjectUserRole


def project_archive_users_notification(project_name: str) -> str:
    return f"""Dear user,

The Project Leader has confirmed the archival of the project: "{project_name}"

The archival of this project results in the following on the Portal:"
- The project workspace hosted by the Node associated with the project becomes inaccessible via remote desktop or SSH.
    - If you are part of multiple projects, access to your other projects is still possible.
- The status of all existing Data Transfers of the project were set to "EXPIRED" and no new Data Transfers can be created.
- The project will still be visible in the Portal, but in a read-only mode.
    - The project and its Data Transfers can still be deleted, but not edited in any way.
    - Archived projects can be found by changing the filter on the projects page from "All Projects" to "Archived Projects".

The Project Leader may choose to allow continued access to other services like GitLab, Confluence and/or other external resources of the project to some or all project members.

Should you have any questions about the archival of the project, please reply to this email."""


def project_archive_node_notification(project: Project) -> str:
    body = [
        f"Project Name: {project.name}",
        f"Project Code: {project.code}",
        "",
        "Roles and Users:",
    ]

    for role, purs in groupby(
        ProjectUserRole.objects.filter(project__pk=project.pk).order_by("-role"),
        lambda pur: pur.role,
    ):
        body += (
            f"{ProjectRole(role).name}: {', '.join([p.user.profile.display_name for p in purs])}",
        )
    return "\n".join(body)
