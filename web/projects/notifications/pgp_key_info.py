from typing import Tuple

from django.contrib.auth import get_user_model

from ..models.pgp import PgpKeyInfo

User = get_user_model()


def create_pgp_key_info_new_approval_request_notification(
    pgp_key_info: PgpKeyInfo,
) -> Tuple[str, str]:
    user = pgp_key_info.user
    subject = f"PGP key approval request from {user.profile.display_name}"
    body = f"""A new approval request has just been submitted by {user.profile.display_name}.

Fingerprint:    {pgp_key_info.fingerprint}
User ID:        {pgp_key_info.key_user_id}
Email:          {pgp_key_info.key_email}
    """
    return subject, body


def create_pgp_key_info_approved_or_rejected_notification(
    pgp_key_info: PgpKeyInfo,
) -> Tuple[str, str]:
    if not pgp_key_info.status in [
        PgpKeyInfo.Status.APPROVED,
        PgpKeyInfo.Status.REJECTED,
    ]:
        raise ValueError("pgp_key_info status has to be APPROVED or REJECTED")

    outcome = (
        "approved" if pgp_key_info.status == PgpKeyInfo.Status.APPROVED else "rejected"
    )
    subject = f"Your PGP key approval request was {outcome}"
    body = f"""Your PGP key has been {outcome}.

Fingerprint:    {pgp_key_info.fingerprint}
User ID:        {pgp_key_info.key_user_id}
Email:          {pgp_key_info.key_email}
    """
    return subject, body
