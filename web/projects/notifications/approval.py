from itertools import chain

from django.conf import settings
from django.contrib.auth import get_user_model

from .data_transfer import create_subject, project_metadata
from ..models.approval import (
    NodeApproval,
    DataProviderApproval,
    BaseApproval,
    GroupApproval,
)
from ..models.data_transfer import DataTransfer
from ..models.project import ProjectRole

User = get_user_model()


def create_reject_notification(approval: BaseApproval, user: User) -> tuple[str, str]:
    dtr = approval.data_transfer
    subject = create_subject(dtr)
    body = f"""Dear all,

Please be informed that the DTR {dtr.id} has been rejected by {user}. Reason: {approval.rejection_reason}

Project:

{project_metadata(dtr)}"""
    return subject, body


def create_approve_notification(approval: BaseApproval, user: User) -> tuple[str, str]:
    dtr = approval.data_transfer
    subject = create_subject(dtr)
    institution = "DCC"
    if isinstance(approval, NodeApproval):
        institution = approval.node
    elif isinstance(approval, DataProviderApproval):
        institution = approval.data_provider
    project_space_ready = (
        "  - A project space in the BioMedIT environment has been setup: Yes"
        if (
            isinstance(approval, NodeApproval)
            and approval.type == NodeApproval.Type.HOSTING
        )
        else ""
    )
    legal_basis = "Yes" if dtr.purpose == DataTransfer.PRODUCTION else "No"
    technical_measures = (
        "  - All technical measures are in place to receive data: Yes"
        if not isinstance(approval, GroupApproval)
        else ""
    )
    body = f"""Dear all,
    
On behalf of '{institution}', I, {user}, hereby confirm:

  - There is a legal basis for the Data Transfer: {legal_basis}
{technical_measures}
{project_space_ready}

Project:

{project_metadata(dtr)}"""
    return subject, body


def create_final_approval_notification(
    dtr: DataTransfer,
) -> tuple[str, str]:
    data_managers = ", ".join(
        data_manager.profile.display_name
        for data_manager in dtr.project.users_by_role(ProjectRole.DM)
    )
    subject = (
        f"[DTR-{dtr.id}] - Approval of Data Transfer Request "
        f"from {dtr.data_provider.name} "
        f"to {dtr.project.name} Project"
    )
    approvals = "\n".join(
        f"- {insitution} by {user.profile.display_name} at {ts:%Y-%m-%d %H:%M:%S}"
        for (insitution, user, ts) in chain(
            (
                (
                    a.node.name,
                    a.history.latest().history_user,
                    a.change_date,
                )
                for a in NodeApproval.objects.filter(data_transfer=dtr)
            ),
            (
                (
                    a.data_provider.name,
                    a.history.latest().history_user,
                    a.change_date,
                )
                for a in DataProviderApproval.objects.filter(data_transfer=dtr)
            ),
        )
    )
    # pylint: disable=no-member
    body = f"""Dear All,

Clearance has been granted to transfer data in accordance with DTR {dtr.id} for project {dtr.project.name} ({dtr.project.code}).

DP Data Engineer from {dtr.data_provider.name} can now encrypt, sign and transfer the data package using sett
in accordance with the agreed procedures to the BioMedIT Node at {dtr.data_provider.node.name}.
You can find instructions on the use of the sett tool at https://sett.readthedocs.io.
 
Please inform Project's Data Manager {data_managers} once data package transfer is done as specified above;
so that data package's reception, integrity, and successful decryption in the Project Work Space could be confirmed.
Please feel free to contact {settings.CONFIG.notification.ticket_mail} in case you have further questions.
 
Thanks and Kind Regards,

BioMedIT Team


Approval log:

{approvals}
"""

    return subject, body
