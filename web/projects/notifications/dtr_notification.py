import itertools
from abc import ABC, abstractmethod
from urllib.parse import urljoin

from django.conf import settings
from django.contrib.auth import get_user_model
from django_drf_utils.email import sendmail

from ..models.approval import BaseApproval
from ..models.data_transfer import DataTransfer
from ..models.message import Message
from ..models.node import get_node_admins
from ..notifications.approval import (
    create_approve_notification,
    create_final_approval_notification,
    create_reject_notification,
)
from ..notifications.data_transfer import create_data_transfer_creation_notification

User = get_user_model()


class DtrNotification(ABC):
    def __init__(self, dtr: DataTransfer, base_url: str) -> None:
        self.dtr = dtr
        self.base_url = base_url
        self.dtr_path = f"data-transfers/{dtr.id}"
        self.nodes = tuple(approval.node for approval in dtr.node_approvals.all())
        node_admins = itertools.chain.from_iterable(
            get_node_admins(node) for node in self.nodes
        )
        coordinators = dtr.data_provider.coordinators
        self.recipients = tuple(
            itertools.chain(node_admins, coordinators, (dtr.requestor,))
        )

    @abstractmethod
    def create_notification(self) -> tuple[str, str]:
        """Generate subject and body for the notification"""

    def send_messages(self) -> None:
        subject, body = self.create_notification()
        Message.objects.bulk_create(
            Message(
                user=recipient,
                title=subject,
                # carriage return `\r` is necessary for the frontend to correctly
                # render new lines
                body=body.replace("\n", "\r\n")
                + f"\r\n[Go to the DTR]({self.dtr_path})",
            )
            for recipient in self.recipients
        )

    def email_recipients(self) -> tuple[str, ...]:
        """Generate list of dtr notification email recipients"""

        return tuple({recipient.email for recipient in self.recipients})

    def sendmail(self) -> None:
        subject, body = self.create_notification()
        sendmail(
            subject=subject,
            body=body + f"({urljoin(self.base_url, self.dtr_path)})",
            recipients=self.email_recipients(),
            # pylint: disable=no-member
            email_cfg=settings.CONFIG.email,
            reply_to=(settings.CONFIG.notification.ticket_mail,),
        )


class DtrCreatedNotification(DtrNotification):
    def create_notification(self) -> tuple[str, str]:
        return create_data_transfer_creation_notification(self.dtr)

    def email_recipients(self) -> tuple[str, ...]:
        return (
            super().email_recipients()
            + tuple(node.ticketing_system_email for node in self.nodes)
            + (settings.CONFIG.notification.ticket_mail,)  # pylint: disable=no-member
        )


class DtrApprovalNotification(DtrNotification):
    def __init__(
        self,
        base_url: str,
        approval: BaseApproval,
        user: User,
    ) -> None:
        super().__init__(approval.data_transfer, base_url)
        self.approval = approval
        self.user = user


class DtrApprovedNotification(DtrApprovalNotification):
    def create_notification(self) -> tuple[str, str]:
        return create_approve_notification(self.approval, self.user)


class DtrRejectedNotification(DtrApprovalNotification):
    def create_notification(self) -> tuple[str, str]:
        return create_reject_notification(self.approval, self.user)

    def email_recipients(self) -> tuple[str, ...]:
        return (
            super().email_recipients()
            + tuple(node.ticketing_system_email for node in self.nodes)
            + (settings.CONFIG.notification.ticket_mail,)  # pylint: disable=no-member
        )


class DtrAuthorizedNotification(DtrNotification):
    def create_notification(self) -> tuple[str, str]:
        return create_final_approval_notification(self.dtr)

    def email_recipients(self) -> tuple[str, ...]:
        return (
            super().email_recipients()
            + tuple(node.ticketing_system_email for node in self.nodes)
            + (settings.CONFIG.notification.ticket_mail,)  # pylint: disable=no-member
        )
