from dataclasses import dataclass

from django.db.models.functions import Lower
from rest_framework.filters import BaseFilterBackend, OrderingFilter


@dataclass
class QueryParam:
    name: str
    lookup: str
    description: str
    param_type: str


class BaseLookupFilter(BaseFilterBackend):
    """
    Filters entity using just lookups.
    Inheriting class should initialize `query_params`.
    """

    query_params: list[QueryParam]

    def _get_lookups(self, request):
        return {
            param.lookup: value
            for param in self.query_params
            if (value := request.query_params.get(param.name))
        }

    def filter_queryset(self, request, queryset, view):
        if lookups := self._get_lookups(request):
            return queryset.filter(**lookups).distinct()
        return queryset.all()

    def get_schema_operation_parameters(self, view):
        return [
            {
                "name": param.name,
                "required": False,
                "in": "query",
                "description": param.description,
                "schema": {"type": param.param_type},
            }
            for param in self.query_params
        ]


class CaseInsensitiveOrderingFilter(OrderingFilter):
    def filter_queryset(self, request, queryset, view):
        ordering = self.get_ordering(request, queryset, view)
        if ordering:
            return queryset.order_by(
                *[
                    Lower(f[1:]).desc() if f.startswith("-") else Lower(f)
                    for f in ordering
                ]
            )
        return queryset
