from .common import BaseLookupFilter, QueryParam


class PgpKeyInfoLookupFilter(BaseLookupFilter):
    """Filters users by `first_name`, `last_name` and `profile.affiliation`"""

    query_params = [
        QueryParam(
            "fingerprint",
            "fingerprint__iexact",
            "Includes only info about PGP keys with specified fingerprint",
            "string",
        ),
        QueryParam(
            "key_email",
            "key_email__iexact",
            "Includes only info about PGP keys verified with specified email",
            "string",
        ),
        QueryParam(
            "key_user_id",
            "key_user_id__iexact",
            "Includes only info about PGP keys with User ID",
            "string",
        ),
        QueryParam(
            "status",
            "status__iexact",
            "Includes only info about PGP keys in specified status",
            "string",
        ),
        QueryParam(
            "username",
            "user__username__iexact",
            "Includes only info about PGP keys associated to user with specified username",
            "string",
        ),
    ]
