from rest_framework.filters import BaseFilterBackend

from .common import BaseLookupFilter, QueryParam
from ..models.project import ProjectRole, ProjectUserRole


class UserActiveFilter(BaseFilterBackend):
    """Filter projects depending on the user being active (`is_active` is `True`)."""

    param = "include_inactive"

    def filter_queryset(self, request, queryset, view):
        if request.method != "GET" or request.query_params.get(self.param):
            # pylint: disable=no-member
            return queryset.all()
        # exclude inactive users by default
        return queryset.exclude(is_active=False)

    def get_schema_operation_parameters(self, view):
        return (
            {
                "name": self.param,
                "required": False,
                "in": "query",
                "description": "Includes users which are not active (`is_active` is `False`)",
                "schema": {"type": "boolean"},
            },
        )


class UserProjectRoleFilter(BaseFilterBackend):
    """
    Return users which have the specified role in the specified project.
    If no role is specified, all users with any role in the specified project are returned.
    """

    projectIdParam = "project_id"
    roleParam = "role"

    def filter_queryset(self, request, queryset, view):
        project_id: str | None = request.query_params.get(self.projectIdParam)
        role: str | None = request.query_params.get(self.roleParam)
        kwargs = {}
        if role:
            kwargs["role"] = ProjectRole[role].value
        if project_id:
            kwargs["project__id"] = project_id
        if kwargs:
            return queryset.filter(
                id__in=(pur.user.id for pur in ProjectUserRole.objects.filter(**kwargs))
            )
        return queryset.all()

    def get_schema_operation_parameters(self, view):
        return (
            {
                "name": self.projectIdParam,
                "required": False,
                "in": "query",
                "description": "Only includes users which have at least one role in this project.",
                "schema": {"type": "integer"},
            },
            {
                "name": self.roleParam,
                "required": False,
                "in": "query",
                "description": "Only includes users which have at least this role in any project.",
                "schema": {"type": "string", "enum": [p.name for p in ProjectRole]},
            },
        )


class UserLookupFilter(BaseLookupFilter):
    """Filters users by `first_name`, `last_name` and `profile.affiliation`"""

    query_params = [
        QueryParam(
            "first_name",
            "first_name__iexact",
            "Includes only users with specified first name",
            "string",
        ),
        QueryParam(
            "last_name",
            "last_name__iexact",
            "Includes only users with specified last name",
            "string",
        ),
        QueryParam(
            "affiliation",
            "profile__affiliation__icontains",
            "Includes only users for whom at least one of the linked affiliations matches (or contains) the provided string",
            "string",
        ),
    ]
