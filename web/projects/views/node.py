from django_drf_utils.views.utils import unique_check
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ..filters.node import NodePermissionsFilter
from ..models.node import Node, get_node_admins, get_node_viewers, get_node_managers
from ..permissions import (
    IsAuthenticatedAndUniqueCheck,
    IsStaff,
    ObjectPermission,
    ReadOnly,
)
from ..permissions.node import IsNodeAdmin, IsNodeViewer
from ..serializers.node import NodeSerializer, NodeRoleSerializer


@unique_check((IsAuthenticatedAndUniqueCheck,))
class NodeViewSet(ModelViewSet):  # pylint: disable=too-many-ancestors
    """Authenticated users can list, node admins can update
    their nodes and staff can create and update any node."""

    filter_backends = (NodePermissionsFilter,)
    search_fields = ("username",)

    serializer_class = NodeSerializer
    # pylint: disable=unsupported-binary-operation
    permission_classes = (IsStaff | IsNodeAdmin | ObjectPermission | ReadOnly,)
    queryset = Node.objects.all()

    @action(
        detail=True,
        # pylint: disable=unsupported-binary-operation
        permission_classes=(IsStaff | IsNodeAdmin | IsNodeViewer,),
        serializer_class=NodeRoleSerializer,
    )
    def roles(self, request, pk=None):  # pylint: disable=unused-argument
        node = self.get_object()
        node_roles = NodeRoleSerializer(
            {
                "admin": get_node_admins(node),
                "manager": get_node_managers(node),
                "viewer": get_node_viewers(node),
            },
        )
        return Response(node_roles.data)
