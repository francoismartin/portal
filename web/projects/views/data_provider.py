from django_drf_utils.views.utils import unique_check
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ..filters.data_provider import DataProviderPermissionsFilter
from ..models.data_provider import (
    DataProvider,
    get_data_provider_coordinators,
    get_data_provider_managers,
    get_data_provider_viewers,
    has_any_data_provider_permissions,
)
from ..permissions import (
    IsAuthenticatedAndUniqueCheck,
    IsStaff,
    ObjectPermission,
    ReadOnly,
)
from ..permissions.data_provider import CanViewDataProviderRoles, IsDataProviderAdmin
from ..serializers.data_provider import (
    DataProviderSerializer,
    DataProviderRoleSerializer,
)


@unique_check((IsAuthenticatedAndUniqueCheck,))
class DataProviderViewSet(ModelViewSet):  # pylint: disable=too-many-ancestors
    """Authenticated users can list, data provider managers can update their data
    providers and (portal) staff can create and update any data provider."""

    filter_backends = (DataProviderPermissionsFilter,)
    search_fields = ("username",)

    serializer_class = DataProviderSerializer
    # pylint: disable=unsupported-binary-operation
    permission_classes = (IsStaff | IsDataProviderAdmin | ObjectPermission | ReadOnly,)

    def get_queryset(self):
        queryset = DataProvider.objects.prefetch_related("node")
        if self.request and (
            self.request.user.is_staff
            or has_any_data_provider_permissions(self.request.user)
        ):
            return queryset.all()
        return queryset.filter(enabled=True)

    @action(
        detail=True,
        # pylint: disable=unsupported-binary-operation
        permission_classes=(IsStaff | CanViewDataProviderRoles,),
        serializer_class=DataProviderRoleSerializer,
        queryset=(),
    )
    def roles(self, request, pk=None):  # pylint: disable=unused-argument
        dp = self.get_object()
        dp_roles = DataProviderRoleSerializer(
            {
                "data_engineer": [],
                "coordinator": get_data_provider_coordinators(dp),
                "manager": get_data_provider_managers(dp),
                "security_officer": [],
                "technical_admin": [],
                "viewer": get_data_provider_viewers(dp),
            },
        )
        return Response(dp_roles.data)
