from django.db.models import Q
from rest_framework import viewsets

from ..models.data_provider import (
    get_data_provider_permission_data_providers,
)
from ..models.data_transfer import DataTransfer
from ..models.node import get_node_permission_nodes
from ..permissions.data_transfer import DataTransferPermission
from ..serializers import DataTransferSerializer


# pylint: disable=too-many-ancestors
class DataTransferViewSet(viewsets.ModelViewSet):
    permission_classes = (DataTransferPermission,)
    serializer_class = DataTransferSerializer
    lookup_field = "id"

    def get_queryset(self):
        requestor = self.request.user
        user_nodes = get_node_permission_nodes(requestor)
        data_providers = get_data_provider_permission_data_providers(requestor)
        if requestor.is_staff:
            query = DataTransfer.objects
        else:
            query = DataTransfer.objects.filter(
                Q(project__users__user=requestor)
                | Q(data_provider__in=data_providers)
                | (
                    Q(project__destination__in=user_nodes)
                    | Q(data_provider__node__in=user_nodes)
                )
            ).distinct()
        return self.serializer_class.prefetch(query)
