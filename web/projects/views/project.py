import logging
from collections import defaultdict
from datetime import datetime

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django_drf_utils.email import sendmail
from django_drf_utils.views.utils import DetailedResponse, unique_check
from rest_framework import mixins, permissions, status, response, viewsets, serializers
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter
from rest_framework.schemas.openapi import AutoSchema
from rest_framework.permissions import SAFE_METHODS

from ..filters.common import CaseInsensitiveOrderingFilter
from ..filters.project import ProjectsPermissionFilter, ProjectsLookupFilter
from ..models.data_transfer import DataTransfer
from ..models.node import (
    Node,
    get_node_permission_nodes,
    has_any_node_permissions,
    has_node_admin_permission,
)
from ..models.project import Project, ProjectRole, ProjectUserRoleHistory
from ..notifications.project_archival import (
    project_archive_users_notification,
    project_archive_node_notification,
)
from ..permissions import IsStaff
from ..permissions.audit import AuditPermission
from ..permissions.project import (
    ProjectPermission,
    ProjectUniquePermission,
    IsNodeAdmin,
)
from ..serializers import (
    ProjectInfoSerializer,
    ProjectRestrictedSerializer,
    ProjectSerializer,
    ProjectUserRoleHistorySerializer,
    UserSerializer,
    UserShortSerializer,
)
from ..signals.project import update_permissions_signal

User = get_user_model()
# pylint: disable=unsupported-binary-operation
archive_permission = [IsStaff | IsNodeAdmin]

logger = logging.getLogger(__name__)


class ProjectSchema(AutoSchema):
    def map_field(self, field):
        mapped_field = super().map_field(field)
        if isinstance(field, serializers.SerializerMethodField):
            field_name_to_component = {
                "transfer_path": {
                    "readOnly": True,
                    "type": "array",
                    "items": {"type": "string"},
                },
                "can_approve": {
                    "readOnly": True,
                    "type": "boolean",
                },
                "permissions": {
                    "readOnly": True,
                    "type": "object",
                    "properties": {
                        "edit": {
                            "type": "object",
                            "readOnly": True,
                            "properties": {
                                "name": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "code": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "destination": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "users": {
                                    "type": "array",
                                    "readOnly": True,
                                    "items": {"enum": [p.name for p in ProjectRole]},
                                },
                                "ip_address_ranges": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "resources": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                            },
                        },
                        "archive": {
                            "type": "boolean",
                            "readOnly": True,
                        },
                    },
                },
            }
            return field_name_to_component.get(field.field_name, mapped_field)
        return mapped_field


@unique_check((ProjectUniquePermission,))
class ProjectViewSet(viewsets.ModelViewSet):  # pylint: disable=too-many-ancestors
    """Show, add, and modify projects."""

    permission_classes = (ProjectPermission,)
    filter_backends = (
        SearchFilter,
        CaseInsensitiveOrderingFilter,
        ProjectsPermissionFilter,
        ProjectsLookupFilter,
    )
    search_fields = ("code", "name")
    ordering_fields = ("code", "name")
    schema = ProjectSchema(component_name="Project", operation_id_base="Project")

    def has_node_admin_permission(self):
        if self.request.method in SAFE_METHODS:
            return has_node_admin_permission(user=self.request.user)
        try:
            return has_node_admin_permission(
                user=self.request.user,
                node=Node.objects.get(code=self.request.data["destination"]),
            )
        except (ObjectDoesNotExist, KeyError) as exc:
            logger.warning(str(exc))
            return False

    def get_serializer_class(self):
        is_generate_schema = not self.request and settings.DEBUG
        if is_generate_schema or (
            self.request
            and (self.request.user.is_staff or self.has_node_admin_permission())
        ):
            return ProjectSerializer
        return ProjectRestrictedSerializer

    def perform_destroy(self, instance):
        # All the permissions must be removed on project deletion
        update_permissions_signal.send(
            sender=instance, user=self.request.user, user_roles=[]
        )
        super().perform_destroy(instance)

    def get_queryset(self):
        if self.request and self.request.user.is_staff:
            query = Project.objects
        else:
            users_projects = Q(users__user__username=self.request.user.username)
            if self.request and has_any_node_permissions(self.request.user):
                # User can see all projects where
                # a) she is a member of
                # b) the destination points to a node where she has any permission
                query = Project.objects.filter(
                    users_projects
                    | Q(
                        destination__in=get_node_permission_nodes(
                            self.request.user
                        ).all()
                    )
                ).distinct()
            else:
                query = Project.objects.filter(users_projects).distinct()
        return self.get_serializer_class().prefetch(query)

    @action(detail=True, permission_classes=[AuditPermission])
    def users(self, request, pk=None):  # pylint: disable=unused-argument
        """List users of a given project by permission.

        GET params:
        - role: one of projects.models.ProjectRole
                (default: USER)
        - disabled: if true show all users who are not assigned to this project
                (default: false)
        - unassigned: if true same behaviour as disabled param: show all users who are not assigned to this project
                (default: false)
        - short: show shortend version of User records
                (default: true)
        """
        project = self.get_object()
        request_role = request.query_params.get("role", ProjectRole.USER.name)
        request_role_id = 0
        for role in ProjectRole:
            if request_role.upper() == role.name:
                request_role_id = role.value
                break
        else:
            return DetailedResponse(
                "Provided role does not match any of the predefined roles.",
                status_code=status.HTTP_400_BAD_REQUEST,
            )

        unassigned_users = (
            request.query_params.get("disabled", "false").lower() == "true"
            or request.query_params.get("unassigned", "false").lower() == "true"
        )
        short = request.query_params.get("short", "true").lower() == "true"

        if request_role_id:
            users = {x.user for x in project.users.filter(role=request_role_id)}
        else:
            users = {x.user for x in project.users.all()}

        user_roles = defaultdict(list)
        for user in project.users.all():
            # go through all user_roles combinations of this project
            # create a dict like { "username": [role1, role2]}
            for role in ProjectRole:
                if role.value == user.role:
                    user_roles[user.user.username].append(role.name)

        if unassigned_users:
            users = set(User.objects.all()) - users

        serializer = UserShortSerializer if short else UserSerializer
        serialized = serializer(users, many=True, context={"user_roles": user_roles})
        return response.Response(serialized.data)

    @action(detail=True, permission_classes=archive_permission, methods=["put"])
    def archive(self, request, pk=None):  # pylint: disable=unused-argument
        project = self.get_object()
        project.archived = True
        project.save()

        for dtr in project.data_transfers.all():
            dtr.status = DataTransfer.EXPIRED
            dtr.save()

        subject = f'Project "{project.name}" was archived'

        # notify users
        sendmail(
            subject=subject,
            body=project_archive_users_notification(project.name),
            recipients=tuple({pur.user.email for pur in project.users.all()}),
            # pylint: disable=no-member
            email_cfg=settings.CONFIG.email,
            reply_to=(settings.CONFIG.notification.ticket_mail,),
        )

        # notify node
        # pylint: disable=no-member
        sendmail(
            subject=subject,
            body=project_archive_node_notification(project),
            recipients=(project.destination.ticketing_system_email,),
            email_cfg=settings.CONFIG.email,
        )

        return response.Response(
            ProjectSerializer(project, context={"request": request}).data
        )

    # pylint: disable=unsupported-binary-operation
    @action(detail=True, permission_classes=archive_permission, methods=["put"])
    def unarchive(self, request, pk=None):  # pylint: disable=unused-argument
        project = self.get_object()
        project.archived = False
        project.save()
        return response.Response(
            ProjectSerializer(project, context={"request": request}).data
        )


class ProjectInfoViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = ProjectInfoSerializer
    permission_classes = (permissions.AllowAny,)
    queryset = Project.objects.all()
    schema = AutoSchema(operation_id_base="ProjectsInfo")


# pylint: disable=too-many-ancestors
class ProjectUserRoleHistoryViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (AuditPermission,)
    serializer_class = ProjectUserRoleHistorySerializer

    def get_queryset(self):
        if not self.request:
            return ()
        if self.request.user.is_staff:
            return ProjectUserRoleHistory.objects.all()
        return ProjectUserRoleHistory.objects.filter(
            project__in=Project.objects.filter(
                destination__in=get_node_permission_nodes(self.request.user)
            )
        )

    def filter_queryset(self, queryset):
        filters = {}
        param_from = self.request.query_params.get("from", None)
        if param_from is not None:
            filters["created__gte"] = datetime.fromisoformat(param_from)
        param_to = self.request.query_params.get("to", None)
        if param_to is not None:
            filters["created__lte"] = datetime.fromisoformat(param_to)
        return queryset.filter(**filters)
