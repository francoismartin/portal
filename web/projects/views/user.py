from django.contrib.auth import get_user_model
from django_drf_utils.views.utils import unique_check
from rest_framework import mixins, permissions, viewsets, generics, serializers
from rest_framework.filters import SearchFilter
from rest_framework.schemas.openapi import AutoSchema

from identities.permissions import has_group_manager_groups, has_change_user_permission

from ..filters.user import UserActiveFilter, UserProjectRoleFilter, UserLookupFilter
from ..models.user import UserNamespace
from ..permissions.node import has_node_admin_nodes
from ..permissions.project import is_manager
from ..permissions.user import (
    UserDataPermission,
    UserModelPermission,
    UserUniquePermission,
)
from ..serializers import (
    UserNamespaceSerializer,
    UserSerializer,
    UserinfoSerializer,
)

User = get_user_model()


class UserinfoSchema(AutoSchema):
    def map_field(self, field):
        mapped_field = super().map_field(field)
        if isinstance(field, serializers.SerializerMethodField):
            field_name_to_component = {
                "permissions": {
                    "type": "object",
                    "readOnly": True,
                    "properties": {
                        "manager": {"readOnly": True, "type": "boolean"},
                        "staff": {"readOnly": True, "type": "boolean"},
                        "data_manager": {"readOnly": True, "type": "boolean"},
                        "project_leader": {"readOnly": True, "type": "boolean"},
                        "data_provider_admin": {"readOnly": True, "type": "boolean"},
                        "data_provider_viewer": {"readOnly": True, "type": "boolean"},
                        "node_admin": {"readOnly": True, "type": "boolean"},
                        "node_viewer": {"readOnly": True, "type": "boolean"},
                        "group_manager": {"readOnly": True, "type": "boolean"},
                        "has_projects": {"readOnly": True, "type": "boolean"},
                    },
                },
                "manages": {
                    "readOnly": True,
                    "type": "object",
                    "properties": {
                        "data_provider_admin": {
                            "readOnly": True,
                            "type": "array",
                            "items": {"$ref": "#/components/schemas/DataProvider"},
                        },
                        "node_admin": {
                            "readOnly": True,
                            "type": "array",
                            "items": {"$ref": "#/components/schemas/Node"},
                        },
                    },
                },
                "affiliation_consent_required": {
                    "type": "boolean",
                    "readOnly": True,
                },
            }
            return field_name_to_component.get(field.field_name, mapped_field)
        return mapped_field


# pylint: disable=too-many-ancestors
@unique_check((UserUniquePermission,))
class UserViewSet(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    """Show details of registered users."""

    serializer_class = UserSerializer
    # pylint: disable=unsupported-binary-operation
    permission_classes = (UserDataPermission | UserModelPermission,)
    filter_backends = (
        SearchFilter,
        UserActiveFilter,
        UserProjectRoleFilter,
        UserLookupFilter,
    )
    search_fields = ("username", "email")
    schema = UserinfoSchema()

    def get_queryset(self):
        user = self.request.user
        if (
            user.is_staff
            or is_manager(user)
            or has_group_manager_groups(user)
            or has_change_user_permission(user)
            or has_node_admin_nodes(user)
        ):
            return User.objects.all()
        # Restrict unnecessary access to other people data
        return User.objects.filter(username=user.username)

    def create(self, request, *args, **kwargs):
        # False positive, exists on mixins.CreateModelMixin
        # pylint: disable=no-member
        response = super().create(request, *args, **kwargs)
        created_user = User.objects.get(id=response.data["id"])
        created_user.set_unusable_password()
        created_user.save()
        return response


class UserinfoView(generics.RetrieveAPIView):
    serializer_class = UserinfoSerializer
    permission_classes = (permissions.IsAuthenticated,)
    schema = UserinfoSchema()

    def get_queryset(self):
        return User.objects.filter(username=self.request.user.username)

    def get_object(self):
        return self.get_queryset()[0]


class UserNamespaceViewSet(viewsets.ReadOnlyModelViewSet):
    """View available namespaces."""

    queryset = UserNamespace.objects.all()
    serializer_class = UserNamespaceSerializer
    permission_classes = (permissions.IsAuthenticated,)
