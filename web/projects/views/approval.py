from django.apps import apps
from django.contrib.auth import get_user_model
from django_drf_utils.views.utils import DetailedResponse
from rest_framework import mixins, viewsets, permissions, status, serializers
from rest_framework.response import Response
from rest_framework.schemas.openapi import AutoSchema

from ..apps import APP_NAME
from ..serializers import ApprovalSerializer, DataTransferSerializer
from ..serializers.approval import INSTITUTIONS

INSTITUTION_URL_PARAM = "institution"

User = get_user_model()


def get_approval_model_by_name(name: str):
    """For given name returns associated approval table (removing any underscore in it first)."""
    return apps.get_model(
        app_label=APP_NAME, model_name=f"{name.replace('_', '')}approval"
    )


class ApprovalSchema(AutoSchema):
    def map_field(self, field):
        mapped_field = super().map_field(field)
        if isinstance(field, serializers.SerializerMethodField):
            field_name_to_component = {
                "can_approve": {
                    "type": "boolean",
                    "readOnly": True,
                },
                "transfer_path": {
                    "readOnly": True,
                    "type": "array",
                    "items": {"type": "string"},
                },
            }
            return field_name_to_component.get(field.field_name, mapped_field)
        return mapped_field

    def get_response_serializer(self, path, method):
        if method in ("PUT", "PATCH"):
            return DataTransferSerializer()
        return super().get_response_serializer(path, method)


class ApprovalViewSet(
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):  # pylint: disable=too-many-ancestors
    permission_classes = (permissions.IsAuthenticated,)
    lookup_field = "id"
    serializer_class = ApprovalSerializer
    schema = ApprovalSchema()

    def update(self, request, *args, **kwargs):
        try:
            data_transfer = self.get_object().data_transfer
            super().update(request, *args, **kwargs)
            data_transfer.refresh_from_db()
            serializer = DataTransferSerializer(
                data_transfer, context={"request": request}
            )
            return Response(serializer.data)
        except KeyError as e:
            return DetailedResponse(
                str(e),
                status_code=status.HTTP_400_BAD_REQUEST,
            )

    def get_queryset(self):
        model = get_approval_model_by_name(self.get_institution())
        approval_id = self.kwargs[self.lookup_field]
        return model.objects.filter(id=approval_id)

    def get_institution(self):
        """Lookups (and validates) the institution in the provided URL params"""
        institution = self.request.data.setdefault(INSTITUTION_URL_PARAM, None)
        if not institution:
            raise KeyError(f"{INSTITUTION_URL_PARAM} NOT present in request data")
        if institution not in INSTITUTIONS:
            raise KeyError(f"'{institution}' MUST be one of '{INSTITUTIONS}'")
        return institution
