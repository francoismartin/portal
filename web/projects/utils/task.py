import logging
from datetime import datetime

from ..models.task import PeriodicTaskRun

logger = logging.getLogger(__name__)


def get_last_run(self) -> datetime | None:
    """
    Returns the date and time of the last run of a scheduled task or `None`, if the task ran for
    the first time.
    :param self: parameter of the task method with `@shared_task(bind=True)`
    :return: `datetime` object or `None`
    """
    last_runs = PeriodicTaskRun.objects.filter(task=self.name)
    if last_runs.exists():
        last_run = last_runs.latest()
        logger.debug(
            "Scheduled task '%s' last run: %s", last_run.task, last_run.created_at
        )
        return last_run.created_at
    logger.debug("Scheduled task '%s' run the first time")
    return None
