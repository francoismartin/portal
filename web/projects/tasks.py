from datetime import datetime, timezone
import logging
import time

from celery import shared_task
from django.conf import settings
from django_drf_utils.email import sendmail


from .models.project import Project, ProjectRole, ProjectUserRole
from .models.pgp import PgpKeyInfo
from .models.task import PeriodicTaskRun
from .notifications.project_user_notification import review_user_roles_notification
from .utils.pgp import KeyDownloadError, KeyStatus, download_key_metadata


logger = logging.getLogger(__name__)


def review_user_roles_recipients(project_user_roles):
    return [
        role.user.email
        for role in project_user_roles.filter(
            role__in=[ProjectRole.PL.value, ProjectRole.PM.value]
        )
    ]


def send_review_user_roles_notification():
    for project in Project.objects.filter(archived=False):
        project_user_roles = ProjectUserRole.objects.filter(project=project)
        subject, body = review_user_roles_notification(
            project.name,
            {(role.user, ProjectRole(role.role)) for role in project_user_roles},
        )

        # pylint: disable=no-member
        sendmail(
            subject,
            body,
            review_user_roles_recipients(project_user_roles),
            email_cfg=settings.CONFIG.email,
        )


# Remember to migrate `PeriodicTask` and `PeriodicTaskRun` when you move / rename this method!
@shared_task(
    bind=True,
    # Retry policy. See https://docs.celeryproject.org/en/stable/userguide/tasks.html#retrying for details.
    acks_late=True,
    autoretry_for=(Exception,),
    max_retries=5,
    ignore_result=True,
)
def review_user_roles(self):
    now = datetime.now(timezone.utc)
    send_review_user_roles_notification()
    PeriodicTaskRun.objects.create(task=self.name, created_at=now)
    logger.info("Successfully sent notification emails for user roles review")


@shared_task(ignore_result=True)
def update_pgp_keys(delay: float = 0.25):
    """Update PGP key info from a keyserver.

    Update the PGP key status based on key information received from a
    keyserver, e.g. in the event of removing user ID from a key.

    :param delay: delay in seconds between subsequent queries to a keyserver.
        The current rate limits are available at
        https://keys.openpgp.org/about/api#rate-limiting
    """

    keys = PgpKeyInfo.objects.exclude(
        status__in=(PgpKeyInfo.Status.DELETED, PgpKeyInfo.Status.KEY_REVOKED)
    )
    errors = []
    deleted = []
    for key in keys:
        try:
            metadata = download_key_metadata(
                fingerprint=key.fingerprint,
                keyserver_url=settings.CONFIG.pgp.keyserver,  # pylint: disable=no-member
            )
            # TODO: handle `NONVERIFIED` and `REVOKED` keys differently once
            # `pgp.extract_public_key_metadata` implements the distinction
            if metadata.status != KeyStatus.VERIFIED:
                key.status = PgpKeyInfo.Status.DELETED
                deleted.append(key)
        except KeyDownloadError as e:
            errors.append(f"{key.fingerprint} {e}")
        time.sleep(delay)

    if deleted:
        updated_count = PgpKeyInfo.objects.bulk_update(
            deleted, ("status",), batch_size=50
        )
        logger.info(
            "Assigned %s status to %s PGP keys: %s",
            PgpKeyInfo.Status.DELETED.value,
            updated_count,
            ", ".join(key.fingerprint for key in deleted),
        )
    if errors:
        logger.error(
            "PGP key update failed with the following errors: %s", "; ".join(errors)
        )
    not_changed_count = len(keys) - len(deleted) - len(errors)
    if not_changed_count > 0:
        logger.debug("No changes detected in %s keys", not_changed_count)
