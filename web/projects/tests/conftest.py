import pytest

# pylint: disable=unused-import
from django_drf_utils.tests.utils import (
    no_requests,
    patch_request,
    patch_request_side_effect,
)
from portal.config import (
    Email as EmailConfig,
    Notification as NotificationConfig,
    Pgp as PgpConfig,
)

# pylint: enable=unused-import
from .factories import (
    DataPackageFactory,
    FeedFactory,
    GroupFactory,
    MessageFactory,
    PeriodicTaskRunFactory,
    ProjectFactory,
    UserFactory,
    USER_PASSWORD,
    DataTransferFactory,
    DataProviderFactory,
    ProjectUserRoleFactory,
    FlagFactory,
    QuickAccessTileFactory,
    NodeFactory,
    DataProviderApprovalFactory,
    NodeApprovalFactory,
    UserNamespaceFactory,
    ProfileFactory,
    PgpKeyInfoFactory,
    GroupApprovalFactory,
)

SWITCH_USERNAME = "switch@user.com"


@pytest.fixture
def feed_factory(db):  # pylint: disable=unused-argument
    return FeedFactory


@pytest.fixture
def project_factory(db):  # pylint: disable=unused-argument
    return ProjectFactory


@pytest.fixture
def user_factory(db):  # pylint: disable=unused-argument
    return UserFactory


@pytest.fixture
def profile_factory(db):  # pylint: disable=unused-argument
    return ProfileFactory


@pytest.fixture
def group_factory(db):  # pylint: disable=unused-argument
    return GroupFactory


@pytest.fixture
def data_transfer_factory(db):  # pylint: disable=unused-argument
    return DataTransferFactory


@pytest.fixture
def data_provider_factory(db):  # pylint: disable=unused-argument
    return DataProviderFactory


@pytest.fixture
def node_factory(db):  # pylint: disable=unused-argument
    return NodeFactory


@pytest.fixture
def project_user_role_factory(db):  # pylint: disable=unused-argument
    return ProjectUserRoleFactory


@pytest.fixture
def switch_user_auth(db, client):  # pylint: disable=unused-argument
    """Return authenticated switch user"""
    user = UserFactory(basic=True, username=SWITCH_USERNAME)
    client.login(username=SWITCH_USERNAME, password=USER_PASSWORD)
    yield user
    client.logout()


@pytest.fixture
def basic_user_auth(db, client):  # pylint: disable=unused-argument
    """Return authenticated basic user"""
    user = UserFactory(basic=True)
    client.login(username=user.username, password=USER_PASSWORD)
    yield user
    client.logout()


@pytest.fixture
def staff_user_auth(db, client):  # pylint: disable=unused-argument
    """Return authenticated staff user"""
    user = UserFactory(staff=True)
    client.login(username=user.username, password=USER_PASSWORD)
    yield user
    client.logout()


@pytest.fixture
def flag_factory(db):  # pylint: disable=unused-argument
    return FlagFactory


@pytest.fixture
def quick_access_tile_factory(db):  # pylint: disable=unused-argument
    return QuickAccessTileFactory


@pytest.fixture
def message_factory(db):  # pylint: disable=unused-argument
    return MessageFactory


@pytest.fixture
def node_approval_factory(db):  # pylint: disable=unused-argument
    return NodeApprovalFactory


@pytest.fixture
def data_provider_approval_factory(db):  # pylint: disable=unused-argument
    return DataProviderApprovalFactory


@pytest.fixture
def group_approval_factory(db):  # pylint: disable=unused-argument
    return GroupApprovalFactory


@pytest.fixture
def periodic_task_run_factory(db):  # pylint: disable=unused-argument
    return PeriodicTaskRunFactory


@pytest.fixture
def user_namespace_factory(db):  # pylint: disable=unused-argument
    return UserNamespaceFactory


@pytest.fixture
def pgp_key_info_factory(db):  # pylint: disable=unused-argument
    return PgpKeyInfoFactory


@pytest.fixture
def data_package_factory(db):  # pylint: disable=unused-argument
    return DataPackageFactory


@pytest.fixture
def apply_notification_settings(settings):
    settings.CONFIG.email = EmailConfig(  # nosec
        host="localhost",
        port=25,
        use_tls=False,
        user="",
        password="",
        from_address="from@example.org",
        subject_prefix="",
    )
    settings.CONFIG.notification = NotificationConfig(
        ticket_mail="to@example.org",
        contact_form_recipient="contact_form@example.org",
    )
    return settings
