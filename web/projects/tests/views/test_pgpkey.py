import itertools
from unittest import mock
from unittest.mock import Mock

import pytest
import factory
from django.test import Client
from django.urls import reverse
from rest_framework import status
from identities.models import User
from projects.apps import APP_NAME
from projects.models.pgp import PgpKeyInfo
from projects.serializers.pgp import PgpKeyInfoSerializer
from projects.utils.pgp import (
    KeyAlgorithm,
    KeyDownloadError,
    KeyMetadata,
    KeyStatus,
)

from .. import APPLICATION_JSON
from ..factories import (
    USER_PASSWORD,
    PgpKeyInfoFactory,
    UserFactory,
)

CN_FINGERPRINT = "CB5A3A5C2C4419BB09EFF879D6D229C51AB5D107"
CN_USER_ID = "Chuck Norris"
CN_EMAIL = "chuck.norris@roundhouse.gov"
SGT_HARTMANN_FINGERPRINT = "DA1A363BC5E11DA73806A8A4E3E4024A0E56221E"
SGT_HARTMANN_USER_ID = "Gunnery Sgt. Hartmann"
SGT_HARTMANN_EMAIL = "hartmann@bullshit.gov"
SGT_HARTMANN_KEY_METADATA = KeyMetadata(
    status=KeyStatus.VERIFIED,
    fingerprint=SGT_HARTMANN_FINGERPRINT,
    user_id=SGT_HARTMANN_USER_ID,
    email=SGT_HARTMANN_EMAIL,
    length=4096,
    algorithm=KeyAlgorithm.RSA,
)
TEST_FINGERPRINT_A = "A" * 40
TEST_FINGERPRINT_B = "B" * 40
TEST_FINGERPRINT_C = "C" * 40
UNKNOWN_KEY_FINGERPRINT_D = "D" * 40
UNKNOWN_KEY_FINGERPRINT_E = "E" * 40

# This test data simulates a scenario where Sgt. Hartmann pretends to be
# C.Norris (he uses his user ID and email). He also wants to get his key
# directly in APPROVED status, which is not allowed.
# The "EXPECTED_SGT_DATA" is the data returned by the backend, after
# it has downloaded the actual key of Sgt. Hartmann from the keyserver and
# added the new entry to the database (discarding the APPROVED key status and
# setting to PENDING by default).
POSTED_SGT_DATA = {
    "fingerprint": SGT_HARTMANN_FINGERPRINT,
    "status": PgpKeyInfo.Status.APPROVED.value,
    "key_user_id": CN_USER_ID,
    "key_email": CN_EMAIL,
}
EXPECTED_SGT_DATA = {
    "fingerprint": SGT_HARTMANN_FINGERPRINT,
    "status": PgpKeyInfo.Status.PENDING.value,
    "id": 1,
    "key_user_id": SGT_HARTMANN_USER_ID,
    "key_email": SGT_HARTMANN_EMAIL,
}


def endpoint_url(pk: int | None = None, action: str | None = None) -> str:
    if action is None:
        if pk is None:
            action = "list"
        else:
            action = "detail"
    return reverse(
        f"{APP_NAME}:pgpkey-{action}",
        kwargs=None if pk is None else {"pk": pk},
    )


def generate_pgp_user_id(user: User) -> str:
    return f"{user.first_name} {user.last_name}"


def test_list_unauthenticated_access(client: Client) -> None:
    """Verify that unauthenticated access to the endpoint is denied."""
    response = client.get(endpoint_url())
    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_detail_access_unauthenticated(
    client: Client, pgp_key_info_factory: PgpKeyInfoFactory
) -> None:
    """Verify that access to individual records is disabled for
    unauthenticated users.
    """
    # Create a database with a single record.
    pgp_key_info_factory()
    assert len(PgpKeyInfo.objects.all()) == 1

    # Check that record cannot be accessed.
    response = client.get(endpoint_url(pk=1))
    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_post_unauthenticated(client: Client) -> None:
    """Verify that unauthenticated users cannot POST to the endpoint."""
    response = client.post(path=endpoint_url(), data=POSTED_SGT_DATA)
    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_access_missing_object(
    client: Client, basic_user_auth: User, pgp_key_info_factory: PgpKeyInfoFactory
) -> None:
    """Verify that accessing a missing object returns a Not Found error."""
    pgp_key_info_factory.create(user=basic_user_auth)
    assert len(PgpKeyInfo.objects.all()) == 1
    response = client.get(endpoint_url(pk=2))
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.parametrize("user_type", ["basic_user_auth", "staff_user_auth"])
def test_list_basic_user(
    client: Client,
    pgp_key_info_factory: PgpKeyInfoFactory,
    user_type: str,
    request: pytest.FixtureRequest,
) -> None:
    """Verify that authenticated users can access the endpoint and list
    their own key(s).
    The test is run for both basic users and staff users (admins):
        * Basic users: can only list their own keys.
        * Staff users: can list all keys.
    """
    # Retrieve the active user from the fixture.
    active_user: User = request.getfixturevalue(user_type)

    # Add a number of keys to the database that do not belong to the
    # active user. The active user is allowed to send a GET request to
    # the endpoint. In addition:
    #  * Basic user: is not allowed to see keys that do not belong to them.
    #    Since at this point the active user has no keys in the database,
    #    the response must be empty.
    #  * Staff user: can see all keys.
    keys: list[PgpKeyInfo] = pgp_key_info_factory.create_batch(3)
    assert len(PgpKeyInfo.objects.all()) == len(keys)
    response = client.get(endpoint_url())
    assert response.status_code == status.HTTP_200_OK
    if active_user.is_staff:
        assert len(response.json()) == len(keys)
    else:
        assert response.json() == []

    # Add a key for the active user. At this point:
    # * Basic user: the active user should now be able to see his own key.
    # * Staff user: the active user should see all keys in the list.
    active_user_key: PgpKeyInfo = pgp_key_info_factory.create(
        user=active_user,
        key_user_id=generate_pgp_user_id(active_user),
        key_email=active_user.email,
    )
    active_user_key_serialized = PgpKeyInfoSerializer(active_user_key).data
    keys.append(active_user_key)
    response = client.get(endpoint_url())
    assert response.status_code == status.HTTP_200_OK
    if active_user.is_staff:
        assert len(response.json()) == len(keys)
        assert response.json() == [PgpKeyInfoSerializer(k).data for k in keys]
    else:
        assert response.json() == [active_user_key_serialized]

    # Detail access - GET requests to individual objects.
    # * Basic user: can only access their own key.
    # * Staff user: can access all keys.
    for key in keys:
        detail_url = endpoint_url(pk=key.pk)
        response = client.get(detail_url)
        if key == active_user_key:
            assert response.status_code == status.HTTP_200_OK
            assert response.json() == active_user_key_serialized
        else:
            if active_user.is_staff:
                assert response.status_code == status.HTTP_200_OK
                assert response.json() == PgpKeyInfoSerializer(key).data
            else:
                assert response.status_code == status.HTTP_404_NOT_FOUND

    # Non-existing keys should return a 404 error.
    detail_url = endpoint_url(pk=active_user_key.pk + 1)
    response = client.get(detail_url)
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.usefixtures("basic_user_auth")
def test_post_new_instance(client: Client, settings) -> None:
    """Verify that newly created PgpKeyInfo instances have their key status set
    to PENDING ("P").
    """
    # Create a mock object to avoid making a real call to the keyserver.
    mock_download_key_metadata = Mock(return_value=SGT_HARTMANN_KEY_METADATA)
    function_to_patch = "projects.serializers.pgp.download_key_metadata"

    # Post data to the endpoint, requesting the key "status" to be set to
    # APPROVED.
    with mock.patch(function_to_patch, mock_download_key_metadata):
        response = client.post(endpoint_url(), data=POSTED_SGT_DATA)

    # Check that the posted data was entered into the database with the
    # correct key status (PENDING), user ID and email.
    mock_download_key_metadata.assert_called_once_with(
        fingerprint=SGT_HARTMANN_FINGERPRINT,
        keyserver_url=settings.CONFIG.pgp.keyserver,
    )
    assert response.json() == PgpKeyInfoSerializer(EXPECTED_SGT_DATA).data
    assert PgpKeyInfoSerializer(PgpKeyInfo.objects.get(pk=1)).data == EXPECTED_SGT_DATA

    # Verify that posting a fingerprint for which a key in APPROVED or PENDING
    # status already exists returns an error.
    with mock.patch(function_to_patch, mock_download_key_metadata):
        response = client.post(endpoint_url(), data=POSTED_SGT_DATA)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    # Verify that posting a fingerprint that is not a valid fingerprint
    # raises an error.
    response = client.post(endpoint_url(), data={"fingerprint": "AAA"})
    assert "Invalid PGP fingerprint" in response.json()["fingerprint"][0]


@pytest.mark.usefixtures("basic_user_auth")
def test_post_fingerprint_of_key_missing_from_keyserver(
    patch_request, client: Client
) -> None:
    """Verify that posting a fingerprint that is absent from the keyserver
    raises a ValidationError.
    """
    mock_request: mock.Mock = patch_request(
        mock.Mock(ok=False, status_code=status.HTTP_404_NOT_FOUND)
    )
    response = client.post(endpoint_url(), data=POSTED_SGT_DATA)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert "key not found on keyserver" in response.json()[0]
    mock_request.assert_called_once()


@pytest.mark.parametrize(
    "key_status, should_pass",
    [
        (PgpKeyInfo.Status.PENDING, False),
        (PgpKeyInfo.Status.APPROVED, False),
        (PgpKeyInfo.Status.APPROVAL_REVOKED, True),
        (PgpKeyInfo.Status.REJECTED, True),
        (PgpKeyInfo.Status.KEY_REVOKED, True),
    ],
)
def test_post_duplicated_instance(
    client: Client,
    basic_user_auth: User,
    pgp_key_info_factory: PgpKeyInfoFactory,
    key_status: PgpKeyInfo.Status,
    should_pass: bool,
) -> None:
    """Verify that posting a new key when the user already has a key is:
    * Not allowed if the existing key is in PENDING or APPROVED status.
    * Allowed if key is in APPROVAL_REVOKED, KEY_REVOKED or REJECTED status.
    """

    # Populate the database with a key for the user.
    initial_fingerprint = "D" * 40
    updated_fingerprint = "E" * 40
    key_user_id = generate_pgp_user_id(basic_user_auth)
    key_email = basic_user_auth.email
    pgp_key_info_factory.create(
        user=basic_user_auth,
        fingerprint=initial_fingerprint,
        key_user_id=key_user_id,
        key_email=key_email,
        status=key_status.value,
    )
    key_metadata = KeyMetadata(
        status=KeyStatus.VERIFIED,
        fingerprint=updated_fingerprint,
        user_id=key_user_id,
        email=key_email,
        length=4096,
        algorithm=KeyAlgorithm.RSA,
    )

    # Attempt to add a new key for the user, with a different fingerprint.
    # This should fail if the original key is in PENDING or APPROVED status,
    # and succeed otherwise.
    function_to_patch = "projects.serializers.pgp.download_key_metadata"
    mock_download_key_metadata = mock.Mock(return_value=key_metadata)
    with mock.patch(function_to_patch, mock_download_key_metadata):
        response = client.post(
            endpoint_url(), data={"fingerprint": updated_fingerprint}
        )
        if should_pass:
            assert response.status_code == status.HTTP_201_CREATED
            assert len(PgpKeyInfo.objects.all()) == 2
        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST
            assert len(PgpKeyInfo.objects.all()) == 1


@pytest.mark.parametrize("user_type", ["basic_user_auth", "staff_user_auth"])
def test_update_instance(
    client: Client,
    pgp_key_info_factory: PgpKeyInfoFactory,
    user_type: str,
    request: pytest.FixtureRequest,
) -> None:
    """Verify that:
    * PUT requests are not allowed.
    * Existing PgpKeyInfo entries can only be modified by admin users.
    * Only the "status" field can be modified.
    """
    # Retrieve the active user from the fixture.
    active_user: User = request.getfixturevalue(user_type)

    # Create a new PgpKeyInfo instance for user.
    active_user_key: PgpKeyInfo = pgp_key_info_factory.create(
        user=active_user,
        fingerprint=CN_FINGERPRINT,
        key_user_id=generate_pgp_user_id(active_user),
        key_email=active_user.email,
    )

    # Attempt to modify the instance via PUT. This should not be allowed.
    detail_url = endpoint_url(pk=active_user_key.pk)
    updated_data = {
        "fingerprint": SGT_HARTMANN_FINGERPRINT,
        "status": PgpKeyInfo.Status.APPROVED.value,
        "key_user_id": "Sgt. Harmann",
        "key_email": SGT_HARTMANN_EMAIL,
    }
    response = client.put(detail_url, data=updated_data)
    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    # Attempt to modify the instance via PATCH. This should only be
    # allowed for admin users. Note: "download_key_metadata" must be patched
    # to avoid making a real call to the keyserver.
    mock_download_key_metadata = Mock(return_value=SGT_HARTMANN_KEY_METADATA)
    with mock.patch(
        "projects.serializers.pgp.download_key_metadata", mock_download_key_metadata
    ):
        response = client.patch(
            detail_url, data=updated_data, content_type=APPLICATION_JSON
        )

    if active_user.is_staff:
        assert response.status_code == status.HTTP_200_OK
        key_from_db: PgpKeyInfo = PgpKeyInfo.objects.get(id=active_user_key.pk)

        # Verify that the key status has been updated.
        assert key_from_db.status == PgpKeyInfo.Status.APPROVED
        # Verify that other fields have not been updated.
        assert key_from_db.fingerprint == CN_FINGERPRINT
        assert key_from_db.key_email == active_user.email
        assert key_from_db.key_user_id == active_user_key.key_user_id

    else:
        assert response.status_code == status.HTTP_403_FORBIDDEN
        assert active_user_key.status == PgpKeyInfo.Status.PENDING


@pytest.mark.usefixtures("staff_user_auth")
@mock.patch(
    "projects.serializers.pgp.download_key_metadata",
    mock.Mock(side_effect=KeyDownloadError(keyserver="hagrid.hogwarts.org")),
)
def test_approve_revoked_key(
    client: Client, pgp_key_info_factory: PgpKeyInfoFactory
) -> None:
    """If a key has been deleted/revoked from the keyserver, the approval of
    the key should fail. This is only tested for staff users since normal users
    are not allowed to approve keys anyway.
    """
    key: PgpKeyInfo = pgp_key_info_factory()
    response = client.patch(
        path=endpoint_url(pk=key.pk),
        data={"status": PgpKeyInfo.Status.APPROVED.value},
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert key.status == PgpKeyInfo.Status.PENDING


@pytest.mark.usefixtures("staff_user_auth")
def test_update_instance_bad_request(
    client: Client, pgp_key_info_factory: PgpKeyInfoFactory
) -> None:
    """Verify that a PATCH request fails if "status" is missing in the data
    associated to the request.
    """
    # Add an entry to the database.
    key: PgpKeyInfo = pgp_key_info_factory()

    # Attempt to update it, without passing any "status" value.
    detail_url = endpoint_url(pk=key.pk)
    response = client.patch(
        detail_url,
        data={"fingerprint": CN_FINGERPRINT},
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.usefixtures("staff_user_auth")
@mock.patch(
    "projects.serializers.pgp.download_key_metadata",
    mock.Mock(return_value=KeyMetadata(status=KeyStatus.VERIFIED)),
)
@pytest.mark.parametrize(
    "initial_status, updated_status",
    list(itertools.product(list(PgpKeyInfo.Status), repeat=2)),
)
def test_key_status_transitions(
    client: Client,
    pgp_key_info_factory: PgpKeyInfoFactory,
    initial_status: PgpKeyInfo.Status,
    updated_status: PgpKeyInfo.Status,
) -> None:
    """Verify that only authorized "status" transitions can be carried-out.
    Note: this test is carried-out as admin/staff user, as only admins are
    allowed to update key values.
    """
    allowed_transitions = [
        (PgpKeyInfo.Status.PENDING, PgpKeyInfo.Status.APPROVED),
        (PgpKeyInfo.Status.PENDING, PgpKeyInfo.Status.REJECTED),
        (PgpKeyInfo.Status.PENDING, PgpKeyInfo.Status.DELETED),
        (PgpKeyInfo.Status.PENDING, PgpKeyInfo.Status.KEY_REVOKED),
        (PgpKeyInfo.Status.APPROVED, PgpKeyInfo.Status.APPROVAL_REVOKED),
        (PgpKeyInfo.Status.APPROVED, PgpKeyInfo.Status.DELETED),
        (PgpKeyInfo.Status.APPROVED, PgpKeyInfo.Status.KEY_REVOKED),
        (PgpKeyInfo.Status.APPROVAL_REVOKED, PgpKeyInfo.Status.KEY_REVOKED),
        (PgpKeyInfo.Status.DELETED, PgpKeyInfo.Status.KEY_REVOKED),
    ]

    # Add a PGP key to the database with the specific initial status.
    key: PgpKeyInfo = pgp_key_info_factory(status=initial_status)

    # Try to update the PGP key's "status" value with a PATCH request.
    response = client.patch(
        endpoint_url(pk=key.pk),
        data={"status": updated_status},
        content_type=APPLICATION_JSON,
    )

    # Verify the requests succeed and fail as expected.
    key_from_db: PgpKeyInfo = PgpKeyInfo.objects.get(id=key.pk)
    if (initial_status.value, updated_status.value) in allowed_transitions:
        assert response.status_code == status.HTTP_200_OK
        assert key_from_db.status == updated_status.value
    else:
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert key_from_db.status == initial_status.value


@pytest.mark.parametrize("user_type", ["basic_user_auth", "staff_user_auth"])
def test_deletion(
    client: Client,
    pgp_key_info_factory: PgpKeyInfoFactory,
    user_type: str,
    request: pytest.FixtureRequest,
) -> None:
    """Verify that deleting instances is not allowed."""
    # Retrieve the active user from the fixture.
    active_user: User = request.getfixturevalue(user_type)

    # Create a new PgpKeyInfo instance for user.
    active_user_key: PgpKeyInfo = pgp_key_info_factory.create(user=active_user)

    # Attempt to delete the entry in the database via a DELETE request.
    detail_url = endpoint_url(pk=active_user_key.pk)
    response = client.delete(detail_url)
    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED


@pytest.mark.parametrize(
    "fingerprints, error_msg",
    [
        # Tests that should PASS:
        #  * Keys with valid fingerprints that are present in the database or
        #    not.
        ([CN_FINGERPRINT], None),
        ([CN_FINGERPRINT, SGT_HARTMANN_FINGERPRINT, CN_FINGERPRINT], None),
        ([TEST_FINGERPRINT_A, TEST_FINGERPRINT_B, TEST_FINGERPRINT_C], None),
        (
            [
                CN_FINGERPRINT,
                UNKNOWN_KEY_FINGERPRINT_D,
                SGT_HARTMANN_FINGERPRINT,
                UNKNOWN_KEY_FINGERPRINT_E,
            ],
            None,
        ),
        #
        # Tests that should FAIL:
        #  * One or more fingerprints are invalid.
        (["BAD", "FINGERPRINTS"], "Invalid PGP fingerprint"),
        ([CN_FINGERPRINT, "BAD", "FINGERPRINTS"], "Invalid PGP fingerprint"),
        # Nothing is passed in the POST request to the endpoint.
        (None, "This field is required"),
    ],
)
def test_pgpkeystatus(
    client: Client,
    pgp_key_info_factory: PgpKeyInfoFactory,
    fingerprints: str,
    error_msg: str | None,
) -> None:
    """Verify that POSTing one or more fingerprints returns the status of the
    respective PGP keys. If a bad/missing fingerprint is POSTed, and error
    should be returned.
    """
    # Create a database with a few PGP key info records.
    known_keys = {
        CN_FINGERPRINT: PgpKeyInfo.Status.APPROVED,
        TEST_FINGERPRINT_A: PgpKeyInfo.Status.PENDING,
        SGT_HARTMANN_FINGERPRINT: PgpKeyInfo.Status.REJECTED,
        TEST_FINGERPRINT_B: PgpKeyInfo.Status.APPROVAL_REVOKED,
        TEST_FINGERPRINT_C: PgpKeyInfo.Status.KEY_REVOKED,
    }
    all_keys = {
        **known_keys,
        UNKNOWN_KEY_FINGERPRINT_D: "UNKNOWN_KEY",
        UNKNOWN_KEY_FINGERPRINT_E: "UNKNOWN_KEY",
    }
    keys: list[PgpKeyInfo] = pgp_key_info_factory.create_batch(
        size=5,
        fingerprint=factory.Iterator(known_keys.keys()),
        status=factory.Iterator(known_keys.values()),
    )
    assert len(PgpKeyInfo.objects.all()) == len(keys)

    # Make a POST request that passes the fingerprint of the current test.
    endpoint = endpoint_url(action="status")
    post_data = {"fingerprints": fingerprints} if fingerprints else None
    response = client.post(endpoint, data=post_data, content_type=APPLICATION_JSON)

    # If all fingerprints in the input list are valid (they can be present in
    # the database or not), the request should succeed and return the expected
    # key_status for each key.
    if not error_msg:  # all(fingerprint in all_keys for fingerprint in fingerprints):
        assert response.status_code == status.HTTP_200_OK
        expected_response = [
            {"fingerprint": f, "status": all_keys[f]} for f in fingerprints
        ]
        assert response.json() == expected_response
    else:
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        # Check the error message is what we expect.
        response_msg = response.json()["fingerprints"]
        if error_msg == "Invalid PGP fingerprint":
            list(response_msg.values())[0][0].startswith(error_msg)
        elif error_msg == "This field is required":
            assert response_msg[0].startswith(error_msg)
        else:
            raise ValueError("Test case not implemented.")


@pytest.mark.parametrize(
    "current_status, expected_status",
    [
        (PgpKeyInfo.Status.PENDING, PgpKeyInfo.Status.REJECTED),
        (PgpKeyInfo.Status.APPROVED, PgpKeyInfo.Status.APPROVAL_REVOKED),
    ],
)
def test_retire_to_correct_status(
    current_status: PgpKeyInfo.Status,
    expected_status: PgpKeyInfo.Status,
    client: Client,
    basic_user_auth: User,
    pgp_key_info_factory: PgpKeyInfoFactory,
):
    pk = pgp_key_info_factory.create(user=basic_user_auth, status=current_status).pk
    response = client.post(endpoint_url(pk, "retire"))

    assert response.status_code == status.HTTP_200_OK
    assert PgpKeyInfo.objects.get(pk=pk).status == expected_status


@pytest.mark.parametrize(
    "requestor, expected_response_status, expected_pgp_key_status",
    [
        (0, status.HTTP_401_UNAUTHORIZED, PgpKeyInfo.Status.PENDING),
        (1, status.HTTP_200_OK, PgpKeyInfo.Status.REJECTED),
    ],
)
def test_retire_only_if_requestor_is_owner(
    client: Client,
    requestor: int,
    expected_response_status: status,
    expected_pgp_key_status: PgpKeyInfo.Status,
    pgp_key_info_factory: PgpKeyInfoFactory,
    user_factory: UserFactory,
):
    # The non-owner user has to be staff, otherwise the endpoint just returns
    # 404 and the code is not properly tested
    [_, owner] = users = [user_factory(staff=True), user_factory()]
    pk = pgp_key_info_factory.create(user=owner, status=PgpKeyInfo.Status.PENDING).pk
    client.login(username=users[requestor].username, password=USER_PASSWORD)
    response = client.post(endpoint_url(pk, "retire"))

    assert response.status_code == expected_response_status
    assert PgpKeyInfo.objects.get(pk=pk).status == expected_pgp_key_status
    client.logout()
