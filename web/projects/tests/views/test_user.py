from assertpy import assert_that
from django.contrib.auth import get_user_model
from django.core import mail
from django.test import TestCase
from django.urls import reverse
from guardian.shortcuts import assign_perm
from rest_framework import status

from identities.permissions import perm_change_user, has_change_user_permission
from projects.apps import APP_NAME
from projects.models.data_provider import has_any_data_provider_permissions
from projects.models.flag import Flag
from projects.models.node import has_node_admin_nodes
from projects.models.project import ProjectUserRole, ProjectRole
from .. import APPLICATION_JSON
from ..factories import (
    DataProviderFactory,
    FlagFactory,
    NodeFactory,
    ProjectFactory,
    ProjectUserRoleFactory,
    UserFactory,
    USER_PASSWORD,
    UserNamespaceFactory,
    make_data_provider_viewer,
    make_node_admin,
    make_project_user,
)

CLINERION_FLAG_NAME = "clinerion"
User = get_user_model()


class TestUserView(TestCase):
    def setUp(self):
        self.project1, self.project2 = ProjectFactory.create_batch(2)
        self.basic_user = make_project_user(self.project1, ProjectRole.USER)
        self.staff = UserFactory(staff=True)
        self.pm_user = make_project_user(self.project1, ProjectRole.PM)
        self.dm_user = make_project_user(self.project1, ProjectRole.DM)
        self.user_with_perm = UserFactory()
        assign_perm(perm_change_user, self.user_with_perm)
        self.flag = FlagFactory(code=CLINERION_FLAG_NAME)
        self.node = NodeFactory()
        self.node_admin = make_node_admin(node=self.node)

    def test_view_list(self):
        url = reverse(f"{APP_NAME}:user-list")
        # Basic user
        self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
        r = self.client.get(url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.json()), 1)
        self.assertEqual(r.json()[0]["username"], self.basic_user.username)
        self.client.logout()
        # Powerusers
        for username in (
            self.staff.username,
            self.user_with_perm.username,
            self.pm_user.username,
            self.node_admin.username,
        ):
            self.client.login(username=username, password=USER_PASSWORD)
            # All
            r = self.client.get(url)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(len(r.json()), User.objects.count())
            # Filter by username / email
            r = self.client.get(url, {"search": self.basic_user.username})
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(len(r.json()), 1)
            self.client.logout()

    def test_view_list_hide_not_active_users(self):
        url = reverse(f"{APP_NAME}:user-list")
        user_count = User.objects.count()
        self.client.login(username=self.staff.username, password=USER_PASSWORD)

        self.assertEqual(len(self.client.get(url).json()), user_count)

        # inactive users are hidden by default
        self.basic_user.is_active = False
        self.basic_user.save()
        self.assertEqual(len(self.client.get(url).json()), user_count - 1)

        # adding request parameter includes inactive users as well
        self.assertEqual(
            len(self.client.get(url, {"include_inactive": True}).json()), user_count
        )

        self.client.logout()

    def test_view_list_user_project_role_filter(self):
        url = reverse(f"{APP_NAME}:user-list")
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        ProjectUserRoleFactory(
            project=self.project1, user=self.staff, role=ProjectRole.USER.value
        )
        ProjectUserRoleFactory(
            project=self.project2, user=self.staff, role=ProjectRole.USER.value
        )
        ProjectFactory()

        user = ProjectRole.USER.name
        dm = ProjectRole.DM.name
        pl = ProjectRole.PL.name

        for (parameters, expected_user_ids) in (
            ({"project_id": 1}, [2, 3, 4, 5]),
            ({"project_id": 2}, [3]),
            ({"project_id": 3}, []),
            ({"role": user}, [2, 3]),
            ({"role": dm}, [5]),
            ({"role": pl}, []),
            ({"project_id": 1, "role": user}, [2, 3]),
            ({"project_id": 2, "role": user}, [3]),
            ({"project_id": 2, "role": dm}, []),
        ):
            response = self.client.get(url, parameters).json()
            assert_that(response).extracting("id").is_equal_to(expected_user_ids)

        self.client.logout()

    def test_update_as_user_with_perm(self):
        url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.basic_user.pk})

        self.client.login(username=self.user_with_perm.username, password=USER_PASSWORD)
        self.assertTrue(has_change_user_permission(self.user_with_perm))

        with self.subTest(
            "Changing 'local_username' and/or 'uid' possible if "
            "the user has the permission 'identities.change_user'"
        ):
            for expected_profile in (
                {"uid": 1000020},
                {"local_username": "new_user_local"},
                {"local_username": "cookie_monster", "uid": 1000050},
            ):
                change = {"profile": expected_profile}
                r = self.client.patch(
                    url,
                    change,
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(r.status_code, status.HTTP_200_OK)
                # Verify that user has been updated
                profile = User.objects.get(pk=self.basic_user.pk).profile
                assert_that(expected_profile).is_subset_of(profile.__dict__)

        with self.subTest(
            "Updating 'flags' possible if the user has the permission 'identities.change_user'"
        ):
            # Ensure flag `CLINERION_FLAG_NAME` does NOT have any user
            flag = Flag.objects.get(code=CLINERION_FLAG_NAME)
            assert_that(flag.users.all()).is_length(0)
            change = {"flags": [CLINERION_FLAG_NAME]}
            r = self.client.patch(
                url,
                change,
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            # Verify that user has been updated
            flag = Flag.objects.get(code=CLINERION_FLAG_NAME)
            users = flag.users.all()
            assert_that(users).is_length(1)
            assert_that(users[0]).is_equal_to(self.basic_user)

        with self.subTest(
            "Even with the permission 'identities.change_user' you can only "
            "update some specific user properties. Everything else will be ignored."
        ):
            for change in (
                # Non-updatable fields
                {"profile": {"affiliation_id": "21501@fhnw.ch"}},
                {"first_name": "Kirk"},
                # Non-existing fields will be ignored
                {"not_a_user_field": 12345},
            ):
                r = self.client.patch(
                    url,
                    change,
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(r.status_code, status.HTTP_200_OK)
                user = User.objects.get(pk=self.basic_user.pk)
                assert_that(user).is_equal_to(self.basic_user)

            new_local_username = "new_user_local"
            r = self.client.patch(
                url,
                {
                    "profile": {
                        "affiliation_id": "21501@fhnw.ch",
                        "local_username": new_local_username,
                    },
                },
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            profile = User.objects.get(pk=self.basic_user.pk).profile
            assert_that(profile).has_local_username(
                new_local_username
            ).has_affiliation_id(self.basic_user.profile.affiliation_id)

        with self.subTest(
            "Even with the permission 'identities.change_user' you can only "
            "use method PATCH"
        ):
            r = self.client.put(
                url,
                {"profile": {"local_username": "new_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)

        self.client.logout()

    def test_update_as_user(self):
        # pylint: disable=too-many-statements
        default_namespace = "ch"
        user = User.objects.get(username=self.pm_user.username)
        user.profile.local_username = "user_local"
        user.profile.namespace = UserNamespaceFactory(name=default_namespace)
        user.profile.save()
        self.basic_user.profile.affiliation_consent = True
        self.basic_user.profile.save()
        url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.basic_user.pk})
        dm_url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.dm_user.pk})
        na_url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.node_admin.pk})

        self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
        with self.subTest("local_username is missing"):
            for value in ("", None):
                r = self.client.patch(
                    url,
                    {"profile": {"local_username": value}},
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)
            r = self.client.patch(url, {"profile": {}}, content_type=APPLICATION_JSON)
            self.assert_no_permission(r)

        with self.subTest(
            "should not be allowed to use modifying methods other than PATCH"
        ):
            r = self.client.put(
                url,
                {"profile": {"local_username": "my_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest("`local_username` has a max length"):
            too_long_local_username = "a" * 100
            r = self.client.patch(
                url,
                {"profile": {"local_username": too_long_local_username}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_422_UNPROCESSABLE_ENTITY)

        with self.subTest("`local_username` has a min length"):
            r = self.client.patch(
                url,
                {"profile": {"local_username": "short"}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_422_UNPROCESSABLE_ENTITY)
            response = r.json()
            self.assertEqual(
                response["detail"],
                "Ensure this value has at least 6 characters (it has 5).",
            )

        with self.subTest("local_username and namespace must be unique together"):
            r = self.client.patch(
                url,
                {"profile": {"local_username": user.profile.local_username}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_409_CONFLICT)
            response = r.json()
            self.assertEqual(
                response["detail"], "A user with the specified username already exists"
            )
            r = self.client.patch(
                url,
                {
                    "first_name": "Chuck",
                    "profile": {"local_username": "my_user_local"},
                },
                content_type=APPLICATION_JSON,
            )
            response = r.json()
            self.assertEqual(response["profile"]["namespace"], default_namespace)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(response["profile"]["local_username"], "my_user_local")
            self.assertEqual(response["first_name"], self.basic_user.first_name)

        with self.subTest("Changing local_username is forbidden"):
            self.basic_user.profile.local_username = "user_local"
            self.basic_user.profile.save()
            r = self.client.patch(
                url,
                {"profile": {"local_username": "new_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest("Revoking affiliation_consent is forbidden"):
            r = self.client.patch(
                url,
                {"profile": {"affiliation_consent": False}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest(
            "Changing username and revoking affiliation_consent is forbidden"
        ):
            r = self.client.patch(
                url,
                {
                    "profile": {
                        "affiliation_consent": False,
                        "local_username": "new_user_local",
                    }
                },
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest("Ignore explicit namespace when setting local_username"):
            self.client.login(username=self.dm_user.username, password=USER_PASSWORD)
            r = self.client.patch(
                dm_url,
                {
                    "profile": {
                        "local_username": "dm_username",
                        "namespace": "foo",
                    }
                },
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(r.json()["profile"]["namespace"], default_namespace)
            self.assertEqual(r.json()["profile"]["local_username"], "dm_username")
            self.assertEqual(r.json()["profile"]["affiliation_consent"], False)

        with self.subTest("Cannot change username when giving affiliation consent"):
            r = self.client.patch(
                dm_url,
                {
                    "profile": {
                        "local_username": "local_us",
                        "affiliation_consent": True,
                        "namespace": "foo",
                    }
                },
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest(
            "Can give affiliation consent independently of username already being set"
        ):
            r = self.client.patch(
                dm_url,
                {
                    "profile": {
                        "affiliation_consent": True,
                    }
                },
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(r.json()["profile"]["namespace"], default_namespace)
            self.assertEqual(r.json()["profile"]["local_username"], "dm_username")
            self.assertEqual(r.json()["profile"]["affiliation_consent"], True)

        with self.subTest(
            "Can set both local_username and affiliation_consent at once"
        ):
            self.client.login(username=self.node_admin.username, password=USER_PASSWORD)
            r = self.client.patch(
                na_url,
                {
                    "profile": {
                        "local_username": "na_username",
                        "namespace": "foo",
                        "affiliation_consent": True,
                    }
                },
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(r.json()["profile"]["namespace"], default_namespace)
            self.assertEqual(r.json()["profile"]["local_username"], "na_username")
            self.assertEqual(r.json()["profile"]["affiliation_consent"], True)

        self.client.logout()
        # pylint: enable=too-many-statements

    def test_update_as_staff(self):
        url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.user_with_perm.pk})
        self.client.login(username=self.staff.username, password=USER_PASSWORD)

        with self.subTest("should not be allowed to set username of someone else"):
            r = self.client.patch(
                url,
                {"profile": {"local_username": "my_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest(
            "should not be allowed to give affiliation consent for someone else"
        ):
            r = self.client.patch(
                url,
                {"profile": {"affiliation_consent": True}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest(
            "should remove objects associated to the user when setting `is_active` to `False`"
        ):
            p3_node = NodeFactory()
            active_user = make_node_admin(node=p3_node)

            url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": active_user.pk})

            make_node_admin(self.staff, p3_node)
            users = (active_user, self.staff)

            p3 = ProjectFactory(destination=p3_node)
            ProjectUserRoleFactory(
                project=p3,
                user=active_user,
                role=ProjectRole.USER.value,
            )
            make_project_user(p3, ProjectRole.DM, active_user)
            p3_staff_pl = ProjectUserRoleFactory(
                project=p3,
                user=self.staff,
                role=ProjectRole.PL.value,
            )

            p4 = ProjectFactory(destination=NodeFactory())
            p4_staff_pl = ProjectUserRoleFactory(
                project=p4,
                user=self.staff,
                role=ProjectRole.PL.value,
            )

            p5 = ProjectFactory(destination=NodeFactory())
            ProjectUserRoleFactory(
                project=p5,
                user=active_user,
                role=ProjectRole.USER.value,
            )

            # project without ProjectUserRoles to check for raised exceptions
            ProjectFactory(destination=NodeFactory())

            data_provider = DataProviderFactory(node=p3_node)
            for u in users:
                make_data_provider_viewer(u, data_provider)
            data_provider.save()

            flag = FlagFactory()
            flag.users.set(users)
            flag.save()

            self.assertTrue(has_node_admin_nodes(self.staff))
            self.assertTrue(has_node_admin_nodes(active_user))

            r = self.client.patch(
                url,
                {"is_active": False},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)

            email = mail.outbox

            assert_that(email).is_length(3).extracting("subject").is_equal_to(
                [
                    f"Permissions changed in '{p3.name}'",
                    f"User(s) added to/removed from project '{p3.name}'",
                    f"User(s) added to/removed from project '{p5.name}'",
                ]
            )

            self.assertEqual(
                ProjectUserRole.objects.filter(project=p3).get(), p3_staff_pl
            )
            self.assertEqual(
                ProjectUserRole.objects.filter(project=p4).get(), p4_staff_pl
            )
            self.assertEqual(ProjectUserRole.objects.filter(project=p5).count(), 0)

            self.assertTrue(has_node_admin_nodes(self.staff))
            self.assertFalse(has_node_admin_nodes(active_user))
            self.assertFalse(has_any_data_provider_permissions(active_user))
            self.assertEqual(flag.users.get(), self.staff)

        with self.subTest("should be able to change `is_active` to `True` again"):
            r = self.client.patch(
                url,
                {"is_active": True},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)

        self.client.logout()

    def test_create_local_user(self):
        url = reverse(f"{APP_NAME}:user-list")
        data = {
            "first_name": "Chuck",
            "last_name": "Norris",
            "email": "chuck@norris.gov",
            "profile": {"local_username": "chuck_norris"},
            "flags": [],
        }

        with self.subTest("Creating local user as non-staff user is forbidden"):
            self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
            r = self.client.post(url, data, content_type=APPLICATION_JSON)
            self.assertEqual(
                r.status_code,
                status.HTTP_403_FORBIDDEN,
            )
            self.client.logout()

        with self.subTest(
            "Creating local user as user with 'identities.change_user' is forbidden"
        ):
            self.client.login(
                username=self.user_with_perm.username,
                password=USER_PASSWORD,
            )
            r = self.client.post(url, data, content_type=APPLICATION_JSON)
            self.assertEqual(
                r.status_code,
                status.HTTP_403_FORBIDDEN,
            )
            self.client.logout()

        with self.subTest("Create local user as staff"):
            self.client.login(username=self.staff.username, password=USER_PASSWORD)
            r = self.client.post(url, data, content_type=APPLICATION_JSON)
            response = r.json()
            self.assertEqual(r.status_code, status.HTTP_201_CREATED)
            self.assertEqual(response["first_name"], data["first_name"])
            self.assertEqual(response["last_name"], data["last_name"])
            self.assertEqual(response["email"], data["email"])
            self.assertEqual(response["username"], data["email"])
            self.assertIsNone(response["profile"]["local_username"])
            created_local_user = User.objects.get(id=response["id"])
            # make sure user cannot be used to log in
            self.assertFalse(created_local_user.has_usable_password())
            self.client.logout()

        with self.subTest("Cannot create user with non-unique email address"):
            self.client.login(username=self.staff.username, password=USER_PASSWORD)
            r = self.client.post(url, data, content_type=APPLICATION_JSON)
            self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(
                r.json(), {"email": ["user with this email address already exists."]}
            )
            self.client.logout()

    def assert_no_permission(self, r):
        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)
        self.assertDictEqual(
            r.json(),
            {"detail": "You do not have permission to perform this action."},
        )
