import pytest
from assertpy import assert_that
from django.urls import reverse
from django.utils import timezone
from rest_framework import status

from projects.apps import APP_NAME
from projects.serializers.feed import FeedSerializer


@pytest.mark.usefixtures("basic_user_auth")
def test_feed(client, feed_factory):
    one_hour_delta = timezone.timedelta(hours=1)
    one_day_delta = timezone.timedelta(days=1)
    now = timezone.now()

    unbound = feed_factory()
    from_yesterday_until_tomorrow = feed_factory(
        from_date=now - one_day_delta, until_date=now + one_day_delta
    )
    from_yesterday_until_one_hour_later = feed_factory(
        from_date=now - one_day_delta, until_date=now + one_hour_delta
    )
    from_yesterday_until_unbound = feed_factory(from_date=now - one_day_delta)
    from_yesterday_until_one_hour_ago = feed_factory(
        from_date=now - one_hour_delta, until_date=now - one_hour_delta
    )
    from_unbound_until_one_hour_ago = feed_factory(until_date=now - one_hour_delta)
    from_unbound_until_one_hour_later = feed_factory(until_date=now + one_hour_delta)
    from_tomorrow = feed_factory(from_date=now + one_day_delta)
    r = client.get(reverse(f"{APP_NAME}:feed-list"))
    assert r.status_code == status.HTTP_200_OK
    assert_that(r.json()).contains(
        *(
            FeedSerializer(f).data
            for f in (
                unbound,
                from_yesterday_until_tomorrow,
                from_yesterday_until_one_hour_later,
                from_yesterday_until_unbound,
                from_unbound_until_one_hour_later,
            )
        )
    ).does_not_contain(
        *(
            FeedSerializer(f).data
            for f in (
                from_yesterday_until_one_hour_ago,
                from_unbound_until_one_hour_ago,
                from_tomorrow,
            )
        )
    )
