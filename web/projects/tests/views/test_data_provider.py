from assertpy import assert_that
from django.contrib.auth.models import Group
from django.urls import reverse
from guardian.shortcuts import get_perms
from rest_framework import status

from projects.apps import APP_NAME
from projects.models.const import (
    APP_PERM_CHANGE_GROUP,
    APP_PERM_DATA_PROVIDER_ADMIN,
    APP_PERM_DATA_PROVIDER_VIEWER,
    APP_PERM_VIEW_GROUP,
)
from projects.models.data_provider import (
    DataProvider,
    assign_dp_manager,
    get_data_provider_coordinators,
)
from projects.serializers.user import UserShortSerializer

from .test_data_transfer import TransferViewTestBase, assert_access_forbidden
from .. import APPLICATION_JSON
from ..factories import (
    DataProviderFactory,
    USER_PASSWORD,
    make_data_provider_coordinator,
    make_data_provider_viewer,
    make_node_admin,
    make_node_viewer,
)


class TestDataProviderView(TransferViewTestBase):
    url = reverse(f"{APP_NAME}:dataprovider-list")

    def setUp(self):
        super().setUp()
        self.data_provider_2 = DataProviderFactory(node=self.dest_node)

    def test_view_list(self):
        # Unauthenticated cannot see data providers
        assert_access_forbidden(self, self.url)
        make_data_provider_coordinator(self.staff, self.data_provider_2)

        # Staff and authenticated users can see data providers
        for username in (self.staff.username, self.basic_user.username):
            self.client.login(username=username, password=USER_PASSWORD)
            r = self.client.get(self.url)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            data_provider_list = r.json()
            data_provider_list_expected = [
                {
                    "id": dp.id,
                    "name": dp.name,
                    "code": dp.code,
                    "enabled": dp.enabled,
                    "node": (dp.node.code if dp.node is not None else None),
                    "coordinators": UserShortSerializer(
                        get_data_provider_coordinators(dp),
                        many=True,
                    ).data,
                }
                for dp in DataProvider.objects.all()
            ]
            assert_that(data_provider_list).contains_only(*data_provider_list_expected)
            self.client.logout()

        self.data_provider.enabled = False
        self.data_provider.save()
        self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        assert_that(r.json()).contains_only(
            *(
                dp
                for dp in data_provider_list_expected
                if dp["code"] != self.data_provider.code
            )
        )
        self.client.logout()

    def test_dp_create(self):
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        url = reverse(f"{APP_NAME}:dataprovider-list")
        dp_code = "chuck"
        data = {"name": "Chuck", "code": dp_code, "node": self.dest_node.code}
        self.assertEqual(
            self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
            status.HTTP_201_CREATED,
        )
        dp = DataProvider.objects.get(code=dp_code)
        dp_ta = Group.objects.get(name=f"{dp_code} DP Technical Admin")
        dp_viewer = Group.objects.get(name=f"{dp_code} DP Viewer")
        dp_manager = Group.objects.get(name=f"{dp_code} DP Manager")
        dp_coordinator = Group.objects.get(name=f"{dp_code} DP Coordinator")
        # DP Viewer, Technical Admin, Coordinator
        for group in (dp_ta, dp_viewer, dp_coordinator):
            assert_that(APP_PERM_DATA_PROVIDER_VIEWER).ends_with(
                get_perms(group, dp)[0]
            )
        # DP Manager
        for group in (dp_ta, dp_viewer, dp_coordinator):
            assert_that(APP_PERM_CHANGE_GROUP).ends_with(
                sorted(get_perms(dp_manager, group))[0]
            )
            assert_that(APP_PERM_VIEW_GROUP).ends_with(
                sorted(get_perms(dp_manager, group))[1]
            )
        assert_that(APP_PERM_DATA_PROVIDER_ADMIN).ends_with(
            get_perms(dp_manager, dp)[0]
        )
        self.client.logout()


def test_data_provider_get_roles_unprivileged(
    client, data_provider_factory, user_factory
):
    url = reverse(
        f"{APP_NAME}:dataprovider-roles", kwargs={"pk": data_provider_factory().id}
    )
    client.login(username=user_factory().username, password=USER_PASSWORD)
    assert client.get(url).status_code == status.HTTP_403_FORBIDDEN
    client.logout()


def test_data_provider_get_roles_privileged(
    client, data_provider_factory, user_factory, group_factory
):
    dp = data_provider_factory()
    url = reverse(f"{APP_NAME}:dataprovider-roles", kwargs={"pk": dp.id})
    staff = user_factory(staff=True)
    na = make_node_admin()
    nv = make_node_viewer()
    dpv = make_data_provider_viewer(data_provider=dp)
    dpc = make_data_provider_coordinator(data_provider=dp)
    dpm = user_factory()
    dpm_group = group_factory()
    dpm_group.user_set.add(dpm)
    assign_dp_manager(dpm_group, dp, group_factory())
    for u in (staff, na, nv, dpv, dpc, dpm):
        client.login(username=u.username, password=USER_PASSWORD)
        r = client.get(url)
        assert r.status_code == status.HTTP_200_OK
        assert list(r.json().keys()) == [
            "data_engineer",
            "coordinator",
            "manager",
            "security_officer",
            "technical_admin",
            "viewer",
        ]
        assert r.json()["data_engineer"] == []
        assert r.json()["coordinator"][0]["username"] == dpc.username
        assert r.json()["manager"][0]["username"] == dpm.username
        assert r.json()["security_officer"] == []
        assert r.json()["technical_admin"] == []
        assert r.json()["viewer"][0]["username"] == dpv.username
        client.logout()
