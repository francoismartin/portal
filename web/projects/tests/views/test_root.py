import pytest

from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME

URL = reverse(f"{APP_NAME}:api-root")


def test_view_is_forbidden_to_unauthenticated(client):
    response = client.get(URL)
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.usefixtures("basic_user_auth")
def test_view_url_exists_at_desired_location(client):
    response = client.get(URL)
    assert response.status_code == status.HTTP_200_OK
