from smtplib import SMTPException
from unittest.mock import patch

from assertpy import assert_that
from django.core import mail
from rest_framework import status

from projects.models.project import ProjectRole

from .test_project import get_project_detail_url, ProjectViewTestMixin
from .. import APPLICATION_JSON
from ..factories import (
    ProjectUserRoleFactory,
    UserFactory,
    USER_PASSWORD,
    make_project_user,
)

EXCEPTION_STACK_TRACE = "Chuck Norris roundhouse kicked the server again."


class TestProjectNotification(ProjectViewTestMixin):
    def setUp(self):
        super().setUp()
        self.new_user_initial = UserFactory()

    def test_add_user_notification(self):
        project = self.project1
        url = get_project_detail_url(project)
        self.client.login(
            username=self.permission_manager.username,
            password=USER_PASSWORD,
        )
        for usr in [self.new_user, self.new_user_initial]:
            assert usr not in {
                p.user for p in project.users.all()
            }, f"'{usr}' already assigned in project '{project}.'"
            expected_roles = (ProjectRole.USER,)
            # PM adds adds permission USER for new_user
            response = self.client.put(
                url,
                self.users_payload(expected_roles, usr),
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assert_user_has_roles(usr, expected_roles)
            emails = {(m.subject, tuple(m.to)) for m in mail.outbox}
            assert emails == {
                # email to PL
                (
                    f"Permissions changed in '{self.project1.name}'",
                    (self.pl_user.email,),
                ),
                # email to Node
                (
                    f"User(s) added to/removed from project '{self.project1.name}'",
                    (self.destination1.ticketing_system_email,),
                ),
            }
        self.client.logout()

    def test_update_permission_add_and_delete(self):
        make_project_user(self.project1, ProjectRole.DM, self.new_user)
        self.client.login(
            username=self.permission_manager.username,
            password=USER_PASSWORD,
        )
        expected_roles = (ProjectRole.USER,)
        self.put_expected_roles_to_new_user(expected_roles)
        assert len(mail.outbox) == 1, "Sent one email"
        email = mail.outbox[0]
        assert email.subject == f"Permissions changed in '{self.project1.name}'"
        assert email.to == [self.pl_user.email]
        assert_that(email.body).contains(
            f"Project name: {self.project1.name}",
            f"Changed by: {self.permission_manager.profile.display_name}",
        )
        assert_that(email.body).contains(
            "Added permissions:\n"
            f"User: {self.new_user.profile.display_name} "
            f"({self.new_user.profile.affiliation})\t"
            "Permission: User"
            "\n\n\n"
            "Deleted permissions:\n"
            f"User: {self.new_user.profile.display_name} "
            f"({self.new_user.profile.affiliation})\t"
            "Permission: Data Manager"
        )
        user_without_affiliation = UserFactory(profile__affiliation="")
        self.put_expected_roles_to_new_user((ProjectRole.DM,), user_without_affiliation)
        assert_that(mail.outbox[1].body).contains(
            "Added permissions:\n"
            f"User: {user_without_affiliation.profile.display_name} (no affiliation)\t"
            "Permission: Data Manager"
        )
        self.client.logout()

    def test_update_permission_add(self):
        with self.pm_add_permission_to_user():
            email = mail.outbox[0]
            assert_that(email.body).contains(
                "Added permissions",
                self.new_user.profile.display_name,
                "Permission: User",
            ).does_not_contain("Deleted permissions", "Permission: Data Manager")

    def test_update_permission_delete(self):
        for role in (ProjectRole.USER, ProjectRole.DM):
            ProjectUserRoleFactory(
                project=self.project1, user=self.new_user, role=role.value
            )
        self.client.login(
            username=self.permission_manager.username,
            password=USER_PASSWORD,
        )
        expected_roles = (ProjectRole.USER,)
        self.put_expected_roles_to_new_user(expected_roles)
        email = mail.outbox[0]
        assert_that(email.body).contains(
            "Deleted permissions",
            self.new_user.profile.display_name,
            "Permission: Data Manager",
        ).does_not_contain("Added permissions", "Permission: User")
        self.client.logout()

    @patch(
        "django.core.mail.EmailMessage.send",
        side_effect=SMTPException(EXCEPTION_STACK_TRACE),
    )
    def test_update_permission_smtp_error(self, _):
        project = self.project1
        url = get_project_detail_url(project)
        self.client.login(
            username=self.permission_manager.username,
            password=USER_PASSWORD,
        )
        expected_roles = (ProjectRole.USER,)
        # sending email will result in SMTPException
        with self.assertLogs("django_drf_utils.email", level="ERROR") as cm:
            response = self.client.put(
                url,
                self.users_payload(expected_roles),
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(response.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
            assert_that(cm.output[0]).contains(
                "ERROR",
                "Failed to send email notification",
                f"Exception: {EXCEPTION_STACK_TRACE}",
            )
            self.client.logout()

    @patch("projects.views.project.update_permissions_signal.send")
    def test_delete(self, mock):
        url = get_project_detail_url(self.project1)
        # Only staff can delete projects
        self.assertEqual(self.client.delete(url).status_code, status.HTTP_403_FORBIDDEN)
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        self.assertEqual(
            self.client.delete(url).status_code, status.HTTP_204_NO_CONTENT
        )
        self.client.logout()
        # Verify signal was triggered
        mock.assert_called_once()
