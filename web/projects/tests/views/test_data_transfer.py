from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME
from projects.models.data_transfer import DataTransfer
from projects.models.project import Project, ProjectRole
from projects.serializers.data_transfer import (
    CREATE_MESSAGE_ARCHIVED_PROJECT,
    UPDATE_MESSAGE_ARCHIVED_PROJECT,
)
from .. import APPLICATION_JSON
from ..factories import (
    DataPackageFactory,
    DataProviderFactory,
    DataTransferFactory,
    NodeFactory,
    PgpKeyInfoFactory,
    ProjectFactory,
    ProjectUserRoleFactory,
    USER_PASSWORD,
    UserFactory,
    make_data_provider_users,
    make_node_admin,
    make_node_viewer,
    make_project_user,
)
from ...models.pgp import PgpKeyInfo


class TransferViewTestBase(TestCase):
    def setUp(self):
        self.dest_node = NodeFactory()
        self.project = ProjectFactory(destination=self.dest_node)
        (
            self.basic_user,
            self.project_leader,
            self.data_manager,
            self.node_viewer,
        ) = UserFactory.create_batch(4)
        self.staff = UserFactory(staff=True)
        for u, r in (
            (self.basic_user, ProjectRole.USER),
            (self.project_leader, ProjectRole.PL),
            (self.data_manager, ProjectRole.DM),
            (self.staff, ProjectRole.DM),
        ):
            ProjectUserRoleFactory(project=self.project, user=u, role=r.value)
        make_node_viewer(self.node_viewer, self.dest_node)
        self.data_provider = DataProviderFactory(node=self.dest_node)
        self.data_manager_pgp_key_fp = "A0B1C2D3E4F5A6B7C8D9E0F1A2B3C4D5E6F7A8B9"
        self.purpose = DataTransfer.PRODUCTION
        self.transfer = self.create_transfer(1, self.purpose)
        self.node = NodeFactory()

    def create_transfer(self, id_, purpose, project: Project | None = None):
        return DataTransferFactory(
            id=id_,
            project=project or self.project,
            data_provider=self.data_provider,
            requestor=self.data_manager,
            requestor_pgp_key_fp=self.data_manager_pgp_key_fp,
            status=DataTransfer.AUTHORIZED,
            purpose=purpose,
        )


class TestDataTransferView(TransferViewTestBase):
    reverse_url = f"{APP_NAME}:datatransfer"
    url = reverse(reverse_url + "-list")

    def response_json(self, id_: int | None = None, project: Project | None = None):
        if not project:
            project = self.project
        transfer_path = [
            self.data_provider.name,
            self.data_provider.node.name,
            project.name,
        ]
        if self.data_provider.node != project.destination:
            transfer_path.insert(2, project.destination.name)
        return {
            "id": id_ or self.transfer.id,
            "project": project.id,
            "project_archived": False,
            "project_name": project.name,
            "max_packages": self.transfer.max_packages,
            "status": self.transfer.status,
            "data_provider": self.data_provider.code,
            "requestor": self.data_manager.id,
            "requestor_pgp_key_fp": self.data_manager_pgp_key_fp,
            "requestor_name": self.data_manager.profile.display_name,
            "requestor_first_name": self.data_manager.first_name,
            "requestor_last_name": self.data_manager.last_name,
            "requestor_email": self.data_manager.email,
            "requestor_display_id": None,
            "purpose": self.purpose,
            "packages": [],
            "node_approvals": [],
            "group_approvals": [],
            "data_provider_approvals": [],
            "legal_basis": self.transfer.legal_basis,
            "creation_date": self.transfer.creation_date.strftime(
                "%Y-%m-%dT%H:%M:%S.%fZ"
            ),
            "change_date": self.transfer.change_date.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            "transfer_path": transfer_path,
        }

    def test_view_list(self):
        (dp_viewer, dp_coordinator, dp_technical_admin) = make_data_provider_users(
            self.data_provider
        )

        response_json = self.response_json()
        for username, expected_json in (
            (self.staff.username, [response_json]),
            (self.data_manager.username, [response_json]),
            (self.project_leader.username, [response_json]),
            (self.node_viewer.username, [response_json]),
            (dp_viewer.username, [response_json]),
            (dp_technical_admin.username, [response_json]),
            (dp_coordinator.username, [response_json]),
        ):
            with self.subTest(username=username):
                self.client.login(username=username, password=USER_PASSWORD)
                r = self.client.get(self.url)
                self.assertEqual(r.status_code, status.HTTP_200_OK)
                self.assertEqual(r.json(), expected_json)
                self.client.logout()
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_list_with_node_admin(self):
        na_user_1, na_user_2 = UserFactory.create_batch(2)
        make_node_admin(na_user_1, self.dest_node)
        make_node_admin(na_user_2, self.node)
        expected_json = self.response_json()
        # 'self.node' NOT associated to DP yet
        for username, answer_json in (
            (na_user_1.username, [expected_json]),
            (na_user_2.username, []),
        ):
            with self.subTest(username=username):
                self.client.login(username=username, password=USER_PASSWORD)
                r = self.client.get(self.url)
                self.assertEqual(r.status_code, status.HTTP_200_OK)
                self.assertEqual(r.json(), answer_json)
                self.client.logout()
        # 'self.node' associated to DP NOW
        self.data_provider.node = self.node
        self.client.login(username=na_user_2.username, password=USER_PASSWORD)
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.json(), answer_json)
        self.client.logout()

    def test_view_list_with_node_viewer(self):
        nv_user = UserFactory()
        make_node_viewer(nv_user, self.node)
        project_2 = ProjectFactory(destination=self.node)
        make_project_user(project_2, ProjectRole.DM, self.data_manager)
        dtr_id_2 = 2
        self.create_transfer(dtr_id_2, self.purpose, project_2)
        self.client.login(username=nv_user.username, password=USER_PASSWORD)
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        # doesn't include transfer with id 1
        received_data = r.json()
        del received_data[0]["creation_date"]
        del received_data[0]["change_date"]
        expected_data = [self.response_json(dtr_id_2, project_2)]
        del expected_data[0]["creation_date"]
        del expected_data[0]["change_date"]

        self.assertEqual(received_data, expected_data)
        self.client.logout()

    def test_write_methods_not_allowed(self):
        # PUT, DELETE are not allowed
        self.assertEqual(
            self.client.post(self.url).status_code, status.HTTP_403_FORBIDDEN
        )
        assert_access_forbidden(
            self,
            self.url,
            (
                (self.basic_user.username, status.HTTP_403_FORBIDDEN),
                (self.staff.username, status.HTTP_405_METHOD_NOT_ALLOWED),
                (self.node_viewer.username, status.HTTP_403_FORBIDDEN),
                (self.data_manager.username, status.HTTP_403_FORBIDDEN),
            ),
            methods=("put", "delete"),
            test_unauthorized=False,
        )

    def check_approvals(self, purpose):
        (dp_viewer, dp_coordinator, _) = make_data_provider_users(self.data_provider)
        for username, can_node_approve, can_dp_approve, can_group_approve in (
            (self.staff.username, True, True, True),
            (dp_viewer.username, False, False, False),
            (dp_coordinator.username, False, True, False),
        ):
            self.client.login(username=username, password=USER_PASSWORD)
            r = self.client.get(self.url)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            data_transfers = r.json()
            self.assertEqual(len(data_transfers), 3)
            for approvals_type in (
                "node_approvals",
                "data_provider_approvals",
                "group_approvals",
            ):
                approvals = data_transfers[1][approvals_type]
                if purpose == DataTransfer.TEST and approvals_type == "group_approvals":
                    self.assertEqual(len(approvals), 0)
                else:
                    self.assertEqual(len(approvals), 1)
                    self.assertIn("created", approvals[0])
                    self.assertIn("change_date", approvals[0])
                    del approvals[0]["created"]
                    del approvals[0]["change_date"]
            self.assertEqual(
                data_transfers[1]["node_approvals"],
                [
                    {
                        "id": 1,
                        "node": {
                            "code": self.dest_node.code,
                            "id": self.dest_node.pk,
                            "name": self.dest_node.name,
                            "node_status": "ON",
                            "ticketing_system_email": self.dest_node.ticketing_system_email,
                        },
                        "status": "W",
                        "type": "H",
                        "can_approve": can_node_approve,
                        "rejection_reason": "",
                    }
                ],
            )
            self.assertEqual(
                data_transfers[1]["data_provider_approvals"],
                [
                    {
                        "id": 1,
                        "data_provider": {
                            "code": self.data_provider.code,
                            "enabled": self.data_provider.enabled,
                            "id": self.data_provider.pk,
                            "name": self.data_provider.name,
                            "node": self.data_provider.node.code,
                        },
                        "status": "W",
                        "can_approve": can_dp_approve,
                        "rejection_reason": "",
                    }
                ],
            )
            if purpose == DataTransfer.PRODUCTION:
                self.assertEqual(
                    data_transfers[1]["group_approvals"],
                    [
                        {
                            "id": 1,
                            "group": {
                                "name": "ELSI Help Desk",
                            },
                            "status": "W",
                            "can_approve": can_group_approve,
                            "rejection_reason": "",
                        }
                    ],
                )
            self.client.logout()

    def test_create_production(self):
        self._create(DataTransfer.PRODUCTION)

    def test_create_test(self):
        self._create(DataTransfer.TEST)

    def _create(self, purpose):
        PgpKeyInfoFactory(
            fingerprint=self.data_manager_pgp_key_fp,
            status=PgpKeyInfo.Status.APPROVED,
        )
        data = {
            "project": self.project.id,
            "data_provider": self.data_provider.code,
            "requestor_pgp_key_fp": self.data_manager_pgp_key_fp,
            "max_packages": -1,
            "purpose": purpose,
            "data_selection_confirmation": True,
            "legal_basis": "some",
        }
        check_method(
            self,
            "post",
            self.reverse_url,
            allowed=(
                self.staff.username,
                self.data_manager.username,
            ),
            forbidden=(
                self.basic_user.username,
                self.node_viewer.username,
            ),
            data=data,
            content_type=APPLICATION_JSON,
        )
        self.check_approvals(purpose)

    def test_create_without_legal_basis_or_confirmation(self):
        data = {
            "project": self.project.id,
            "data_provider": self.data_provider.code,
            "requestor_pgp_key_fp": self.data_manager_pgp_key_fp,
            "max_packages": -1,
        }
        PgpKeyInfoFactory(
            fingerprint=self.data_manager_pgp_key_fp, status=PgpKeyInfo.Status.APPROVED
        )
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        for purpose, confirmation, legal_basis, code in (
            (DataTransfer.PRODUCTION, True, "something", status.HTTP_201_CREATED),
            (DataTransfer.PRODUCTION, True, "", status.HTTP_400_BAD_REQUEST),
            (
                DataTransfer.PRODUCTION,
                False,
                "something",
                status.HTTP_400_BAD_REQUEST,
            ),
            (DataTransfer.TEST, True, "something", status.HTTP_201_CREATED),
            (DataTransfer.TEST, True, "", status.HTTP_201_CREATED),
            (DataTransfer.TEST, False, "", status.HTTP_400_BAD_REQUEST),
        ):
            self.assertEqual(
                self.client.post(
                    self.url,
                    data=data
                    | {
                        "purpose": purpose,
                        "legal_basis": legal_basis,
                        "data_selection_confirmation": confirmation,
                    },
                    content_type=APPLICATION_JSON,
                ).status_code,
                code,
            )
        self.assertEqual(
            self.client.post(
                self.url,
                # data_selection_confirmation field is missing
                data=data
                | {"purpose": DataTransfer.PRODUCTION, "legal_basis": "something"},
                content_type=APPLICATION_JSON,
            ).status_code,
            status.HTTP_400_BAD_REQUEST,
        )
        self.client.logout()

    def test_create_with_wrong_status(self):
        data = {
            "project": self.project.id,
            "data_provider": self.data_provider.code,
            "requestor": self.data_manager.id,
            "requestor_pgp_key_fp": self.data_manager_pgp_key_fp,
            "purpose": self.purpose,
            "max_packages": -1,
            "status": DataTransfer.AUTHORIZED,  # not allowed
            "data_selection_confirmation": True,
            "legal_basis": "some",
        }
        PgpKeyInfoFactory(
            fingerprint=self.data_manager_pgp_key_fp, status=PgpKeyInfo.Status.APPROVED
        )
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        self.assertEqual(
            self.client.post(
                self.url, data=data, content_type=APPLICATION_JSON
            ).status_code,
            status.HTTP_400_BAD_REQUEST,
        )
        self.client.logout()

    def test_create_requestor_not_dm(self):
        data = {
            "project": self.project.id,
            "data_provider": self.data_provider.code,
            "purpose": self.purpose,
            "requestor": UserFactory().id,
            "requestor_pgp_key_fp": self.data_manager_pgp_key_fp,
            "max_packages": -1,
            "status": DataTransfer.INITIAL,
            "data_selection_confirmation": True,
            "legal_basis": "some",
        }
        PgpKeyInfoFactory(
            fingerprint=self.data_manager_pgp_key_fp, status=PgpKeyInfo.Status.APPROVED
        )
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        self.assertContains(
            self.client.post(self.url, data=data, content_type=APPLICATION_JSON),
            "Requestor is not a data manager of the project",
            status_code=status.HTTP_400_BAD_REQUEST,
        )

        self.client.logout()

    def test_create_as_user_with_requestor(self):
        data = {
            "project": self.project.id,
            "data_provider": self.data_provider.code,
            "purpose": self.purpose,
            "requestor_pgp_key_fp": self.data_manager_pgp_key_fp,
            "max_packages": -1,
            "status": DataTransfer.INITIAL,
            "data_selection_confirmation": True,
            "legal_basis": "some",
        }
        PgpKeyInfoFactory(
            fingerprint=self.data_manager_pgp_key_fp, status=PgpKeyInfo.Status.APPROVED
        )
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        # allowed:
        response = self.client.post(self.url, data=data, content_type=APPLICATION_JSON)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # takes current user as not specified
        self.assertEqual(response.data["requestor"], self.staff.id)
        self.client.logout()

    def test_create_with_not_approved_key(self):
        data = {
            "project": self.project.id,
            "data_provider": self.data_provider.code,
            "requestor": self.data_manager.id,
            "requestor_pgp_key_fp": self.data_manager_pgp_key_fp,
            "max_packages": -1,
            "status": DataTransfer.INITIAL,
            "data_selection_confirmation": True,
            "legal_basis": "some",
        }
        PgpKeyInfoFactory(
            fingerprint=self.data_manager_pgp_key_fp, status=PgpKeyInfo.Status.PENDING
        )
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        self.assertEqual(
            self.client.post(
                self.url, data=data, content_type=APPLICATION_JSON
            ).status_code,
            status.HTTP_400_BAD_REQUEST,
        )
        self.client.logout()

    def test_create_for_archived_project(self):
        data = {
            "project": self.project.id,
            "data_provider": self.data_provider.code,
            "purpose": self.purpose,
            "requestor_pgp_key_fp": self.data_manager_pgp_key_fp,
            "max_packages": -1,
            "status": DataTransfer.INITIAL,
            "data_selection_confirmation": True,
            "legal_basis": "some",
        }
        PgpKeyInfoFactory(
            fingerprint=self.data_manager_pgp_key_fp, status=PgpKeyInfo.Status.APPROVED
        )
        self.project.archived = True
        self.project.save()
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        self.assertContains(
            self.client.post(self.url, data=data, content_type=APPLICATION_JSON),
            CREATE_MESSAGE_ARCHIVED_PROJECT,
            status_code=status.HTTP_400_BAD_REQUEST,
        )
        self.client.logout()

    def test_patch(self):
        data = {"status": DataTransfer.AUTHORIZED}
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        url = reverse(self.reverse_url + "-detail", kwargs={"id": self.transfer.id})
        self.assertEqual(
            self.client.patch(
                url, data={**data, "max_packages": -1}, content_type=APPLICATION_JSON
            ).status_code,
            status.HTTP_200_OK,
        )
        self.client.logout()
        check_method(
            self,
            "patch",
            self.reverse_url,
            allowed=(self.staff.username,),
            forbidden=(self.basic_user.username, self.node_viewer.username),
            data=data,
            content_type=APPLICATION_JSON,
            lookup_field="id",
            ids=(1,),
        )

    def test_update_after_pkg(self):
        DataPackageFactory(
            metadata_hash="hash",
            data_transfer=self.transfer,
            purpose=DataTransfer.PRODUCTION,
        )
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        url = reverse(self.reverse_url + "-detail", kwargs={"id": self.transfer.id})
        for data, expected_status_code in (
            ({"purpose": DataTransfer.TEST}, status.HTTP_400_BAD_REQUEST),
            ({"status": DataTransfer.UNAUTHORIZED}, status.HTTP_200_OK),
            ({"max_packages": 1}, status.HTTP_200_OK),
        ):
            self.assertEqual(
                self.client.patch(
                    url, data={**data}, content_type=APPLICATION_JSON
                ).status_code,
                expected_status_code,
            )
        self.client.logout()

    def test_update_for_archived_project(self):
        self.project.archived = True
        self.project.save()
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        url = reverse(self.reverse_url + "-detail", kwargs={"id": self.transfer.id})
        self.assertContains(
            self.client.patch(
                url,
                data={"status": DataTransfer.UNAUTHORIZED},
                content_type=APPLICATION_JSON,
            ),
            UPDATE_MESSAGE_ARCHIVED_PROJECT,
            status_code=status.HTTP_400_BAD_REQUEST,
        )
        self.client.logout()


def assert_access_forbidden(
    test_class,
    url,
    users=(),
    test_unauthorized=True,
    methods=("get",),
    return_code=status.HTTP_403_FORBIDDEN,
):
    for method in methods:
        with test_class.subTest(method=method):
            # Unauthenticated is not allowed
            if test_unauthorized:
                test_class.assertEqual(
                    getattr(test_class.client, method)(url).status_code, return_code
                )
            for username, user_return_code in users:
                with test_class.subTest(user=username):
                    test_class.client.login(username=username, password=USER_PASSWORD)
                    test_class.assertEqual(
                        getattr(test_class.client, method)(url).status_code,
                        user_return_code,
                    )
                    test_class.client.logout()


def check_method(
    test_class,
    method,
    url,
    allowed,
    forbidden,
    data=None,
    ids=(),
    test_unauthorized=True,
    lookup_field="pk",
    **kwargs,
):
    m = getattr(test_class.client, method)
    # Unauthenticated is not allowed
    id_kwargs = ({lookup_field: i} for i in ids)
    args = () if data is None else (data,)
    if not ids:
        id_kwargs = ({},)
    status_code = {
        "post": status.HTTP_201_CREATED,
        "delete": status.HTTP_204_NO_CONTENT,
        "patch": status.HTTP_200_OK,
    }[method]
    url_suffix = {"post": "-list", "delete": "-detail", "patch": "-detail"}[method]
    for i_kwargs in id_kwargs:
        url = reverse(url + url_suffix, kwargs=i_kwargs)
        if test_unauthorized:
            test_class.assertEqual(m(url).status_code, status.HTTP_403_FORBIDDEN)
        for username in forbidden:
            test_class.client.login(username=username, password=USER_PASSWORD)
            test_class.assertEqual(
                status.HTTP_403_FORBIDDEN,
                m(url, *args, **kwargs).status_code,
                f"Wrong HTTP status for username '{username}'",
            )
            test_class.client.logout()
        for username in allowed:
            test_class.client.login(username=username, password=USER_PASSWORD)
            r = m(url, *args, **kwargs)
            test_class.assertEqual(r.status_code, status_code)
            test_class.client.logout()
