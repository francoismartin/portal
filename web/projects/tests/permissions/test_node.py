from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory, TestCase

from projects.permissions import UPDATE_METHODS
from projects.permissions.node import IsNodeAdmin, IsNodeViewer
from ..factories import NodeFactory, UserFactory, make_node_admin, make_node_viewer


class TestIsNodeAdmin(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = UserFactory()
        self.staff = UserFactory(staff=True)
        self.node_1, self.node_2 = NodeFactory.create_batch(2)
        self.node_admin_1 = make_node_admin(node=self.node_1)
        self.node_admin_2 = make_node_admin(node=self.node_2)
        self.permission_obj = IsNodeAdmin()
        self.request = self.factory.request()
        self.request.method = "PUT"

    def test_anonymous(self):
        self.request.user = AnonymousUser()
        self.assertFalse(self.permission_obj.has_permission(self.request, None))

    def test_user(self):
        self.request.user = self.user
        self.assertFalse(self.permission_obj.has_permission(self.request, None))

    def test_node_admin_own_node(self):
        for user, node, method in (
            (self.node_admin_1, self.node_1, "PUT"),
            (self.node_admin_1, self.node_1, "PATCH"),
            (self.node_admin_2, self.node_2, "PUT"),
            (self.node_admin_2, self.node_2, "PATCH"),
        ):
            self.request.user = user
            self.request.method = method
            self.assertTrue(self.permission_obj.has_permission(self.request, None))
            self.assertTrue(
                self.permission_obj.has_object_permission(self.request, None, node)
            )

    def test_node_admin_other_node(self):
        for user, node in (
            (self.node_admin_1, self.node_2),
            (self.node_admin_2, self.node_1),
        ):
            self.request.user = user
            self.assertTrue(self.permission_obj.has_permission(self.request, None))
            self.assertFalse(
                self.permission_obj.has_object_permission(self.request, None, node)
            )

    def test_node_admin_forbidden_methods(self):
        for method in ("POST", "DELETE"):
            self.request.user = self.node_admin_1
            self.request.method = method
            self.assertFalse(self.permission_obj.has_permission(self.request, None))

    def test_staff(self):
        self.request.user = self.staff
        for method in UPDATE_METHODS:
            self.request.method = method
            self.assertFalse(self.permission_obj.has_permission(self.request, None))


def test_node_viewer(rf, node_factory):
    request = rf.get("/")
    perm = IsNodeViewer()
    node1, node2 = node_factory.create_batch(2)
    request.user = AnonymousUser()
    assert perm.has_permission(request, None) is False
    user = make_node_viewer(node=node1)
    request.user = user
    assert perm.has_permission(request, None) is True
    assert perm.has_object_permission(request, None, node1) is True
    assert perm.has_object_permission(request, None, node2) is False
