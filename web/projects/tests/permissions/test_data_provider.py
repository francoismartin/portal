import pytest
from assertpy import assert_that
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory, TestCase

from projects.permissions import UPDATE_METHODS
from projects.permissions.data_provider import (
    CanViewDataProviderRoles,
    IsDataProviderAdmin,
)
from ..factories import (
    UserFactory,
    DataProviderFactory,
    make_data_provider_admin,
    make_data_provider_coordinator,
    make_data_provider_viewer,
    make_node_admin,
    make_node_viewer,
)


class TestIsDataProviderAdmin(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user, self.admin1, self.admin2 = UserFactory.create_batch(3)
        self.staff = UserFactory(staff=True)
        self.dp1, self.dp2 = DataProviderFactory.create_batch(2)
        make_data_provider_admin(self.admin1, self.dp1)
        make_data_provider_admin(self.admin2, self.dp2)
        self.permission_obj = IsDataProviderAdmin()
        self.request = self.factory.request()
        self.request.method = "PUT"

    def test_no_permission(self):
        # Anonymous user and user with no data provider permissions
        for user in (AnonymousUser(), self.user):
            self.request.user = user
            assert_that(
                self.permission_obj.has_permission(self.request, None)
            ).is_false()

    def test_staff(self):
        self.request.user = self.staff
        for method in UPDATE_METHODS:
            self.request.method = method
            assert_that(
                self.permission_obj.has_permission(self.request, None)
            ).is_false()

    def test_dp_admin_own_dp(self):
        for method in ("PUT", "PATCH"):
            for user, dp in ((self.admin1, self.dp1), (self.admin2, self.dp2)):
                self.request.user = user
                self.request.method = method
                assert_that(
                    self.permission_obj.has_permission(self.request, None)
                ).is_true()
                assert_that(
                    self.permission_obj.has_object_permission(self.request, None, dp)
                ).is_true()

    def test_dp_admin_other_dp(self):
        for user, node in (
            (self.admin1, self.dp2),
            (self.admin2, self.dp1),
        ):
            self.request.user = user
            assert_that(
                self.permission_obj.has_permission(self.request, None)
            ).is_true()
            assert_that(
                self.permission_obj.has_object_permission(self.request, None, node)
            ).is_false()

    def test_dp_admin_forbidden_methods(self):
        self.request.user = self.admin1
        for method in ("HEAD", "GET", "POST", "DELETE"):
            self.request.method = method
            assert_that(
                self.permission_obj.has_permission(self.request, None)
            ).is_false()


@pytest.mark.parametrize(
    "user_maker,has_access",
    (
        (AnonymousUser, (False, False, False)),
        (UserFactory, (True, False, False)),
        (make_node_admin, (True, True, True)),
        (make_node_viewer, (True, True, True)),
    ),
)
def test_can_view_data_provider_roles(
    rf, data_provider_factory, user_maker, has_access
):
    perm = CanViewDataProviderRoles()
    dp1, dp2 = data_provider_factory.create_batch(2)
    request = rf.get("/")
    request.user = user_maker()
    assert perm.has_permission(request, None) is has_access[0]
    assert perm.has_object_permission(request, None, dp1) is has_access[1]
    assert perm.has_object_permission(request, None, dp2) is has_access[2]


@pytest.mark.parametrize(
    "user_maker",
    (
        make_data_provider_viewer,
        make_data_provider_admin,
        make_data_provider_coordinator,
    ),
)
def test_can_view_data_provider_roles_dp_users(rf, data_provider_factory, user_maker):
    perm = CanViewDataProviderRoles()
    dp1, dp2 = data_provider_factory.create_batch(2)
    request = rf.get("/")
    request.user = user_maker(data_provider=dp1)
    assert perm.has_permission(request, None) is True
    assert perm.has_object_permission(request, None, dp1) is True
    assert perm.has_object_permission(request, None, dp2) is False
