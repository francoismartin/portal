import logging
from unittest import mock

import pytest
from django.contrib.auth import get_user_model

from projects import tasks
from projects.tasks import (
    review_user_roles_recipients,
    send_review_user_roles_notification,
    update_pgp_keys,
)
from projects.models.pgp import PgpKeyInfo
from projects.models.project import ProjectRole, ProjectUserRole
from projects.utils.pgp import KeyDownloadError, KeyMetadata, KeyStatus

User = get_user_model()
PROJECT_NAME = "Doghouse"


def test_review_user_roles_recipients(
    project_factory, user_factory, project_user_role_factory
):
    project = project_factory(name=PROJECT_NAME)
    project_user_role_factory.create(
        project=project,
        user=user_factory.create(email="walter.white@doghouse.com"),
        role=ProjectRole.PL.value,
    )
    project_user_role_factory.create(
        project=project,
        user=user_factory.create(email="jesse.pinkman@doghouse.com"),
        role=ProjectRole.PM.value,
    )
    project_user_role_factory.create_batch(5, role=ProjectRole.USER.value)
    project_factory.create()

    assert set(
        review_user_roles_recipients(ProjectUserRole.objects.filter(project=project))
    ) == {
        "walter.white@doghouse.com",
        "jesse.pinkman@doghouse.com",
    }


def test_review_user_roles_notification(project_factory):
    with mock.patch.object(tasks, "sendmail") as mock_sendmail:
        send_review_user_roles_notification()
        mock_sendmail.assert_not_called()
        active_projects = project_factory.create_batch(3, archived=False)
        project_factory.create(archived=True)
        send_review_user_roles_notification()
        assert mock_sendmail.call_count == len(active_projects)


@pytest.mark.parametrize(
    "keyserver_result",
    (
        (KeyStatus.VERIFIED, KeyStatus.VERIFIED),
        (
            KeyStatus.VERIFIED,
            KeyDownloadError("https://keys.example.org", "Something went wrong"),
        ),
        (KeyDownloadError("https://keys.example.org", "Something went wrong"),),
        (KeyStatus.VERIFIED, KeyStatus.NONVERIFIED, KeyStatus.VERIFIED),
        (KeyStatus.NONVERIFIED, KeyStatus.VERIFIED),
        (KeyStatus.NONVERIFIED, KeyStatus.NONVERIFIED),
        (KeyStatus.REVOKED, KeyStatus.NONVERIFIED),
        (KeyStatus.REVOKED, KeyStatus.REVOKED),
    ),
)
def test_update_pgp_keys(keyserver_result, pgp_key_info_factory, caplog):
    keys = pgp_key_info_factory.create_batch(
        len(keyserver_result), status=PgpKeyInfo.Status.APPROVED
    )
    caplog.set_level(logging.DEBUG, logger="projects.tasks")
    with mock.patch(
        "projects.tasks.download_key_metadata",
        mock.Mock(
            side_effect=[
                KeyMetadata(status=s) if isinstance(s, KeyStatus) else s
                for s in keyserver_result
            ]
        ),
    ):
        update_pgp_keys(delay=0)

    # Check for the info log and verify that keys have been updated in the DB
    if set(keyserver_result) & {KeyStatus.NONVERIFIED, KeyStatus.REVOKED}:
        deleted = [
            k
            for k, r in zip(keys, keyserver_result)
            if isinstance(r, KeyStatus) and r != KeyStatus.VERIFIED
        ]
        assert caplog.records[0].levelname == "INFO"
        assert caplog.records[
            0
        ].message == f"Assigned DELETED status to {len(deleted)} PGP keys: " + ", ".join(
            k.fingerprint for k in deleted
        )
        # Verify that deleted keys have DELETED status in the DB
        for k in deleted:
            k.refresh_from_db()
            assert k.status == PgpKeyInfo.Status.DELETED
        # Verify that other keys have their original status in the DB
        for k in keys:
            if k not in deleted:
                k.refresh_from_db()
                assert k.status == PgpKeyInfo.Status.APPROVED

    # Check for the error log
    errors = [
        f"{k.fingerprint} {r}"
        for k, r in zip(keys, keyserver_result)
        if isinstance(r, KeyDownloadError)
    ]
    if errors:
        err_log = next(rec for rec in caplog.records if rec.levelname == "ERROR")
        assert err_log.message.endswith("; ".join(errors))

    # Check for the debug log
    verified_count = keyserver_result.count(KeyStatus.VERIFIED)
    if verified_count:
        assert caplog.records[-1].levelname == "DEBUG"
        assert (
            caplog.records[-1].message
            == f"No changes detected in {verified_count} keys"
        )
