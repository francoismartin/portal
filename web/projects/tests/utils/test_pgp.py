import contextlib
import re
from typing import Any
from unittest import mock

import gpg_lite as gpg
import pytest
import requests
from rest_framework import status
from projects.utils.pgp import (
    PGP_BEGIN_BLOCK,
    PGP_END_BLOCK,
    KeyDownloadError,
    KeyNotVerifiedError,
    download_ascii_armored_public_pgp_key,
    download_key_metadata,
)


KEYSERVER_URL = "hagrid.hogwarts.org"
CN_USER_ID = "CN Testkey 4096 (test key)"
CN_EMAIL = "chuck.norris@roundhouse.swiss"

CN_FINGERPRINT_1024 = "E24885A01C0B2105B7FAE870462AA4EFE51C9E39"
CN_PUB_KEY_NONVERIFIED_1024 = b"""-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: E248 85A0 1C0B 2105 B7FA E870 462A A4EF E51C 9E39

xo0EYk1mfwEEALAY34boAQH90K+yF8ewfk9rgMXR9tuix8p9icD6q0R6gMsgY27T
rAwMeq2zQVUWtzoU/fVKwZQ6h+D6vwKqbp0PKa8bzcDSY7urCMM8BLmUZmcg3D+x
jXbXXueQwzaE64P0iDWUX+feihr8RKbwm6MAS6hLdOtPw4TZKV7RFPrVABEBAAHO
jQRiTWZ/AQQAp9MS67VTtY1yh9aqebhAhPFBJkAmqefnkQN8L1JzhN0WX03iAjzF
cj3EVtRmK/MOY1OeTbQo1thNIlyyvjiolJ8tXIRq1R12lWcMm8/cuoqlkZGsnE4F
wt2SiaH6ZASEu2QbESqhQraw5cY+QceRxMl+jFODvAipgV2012aAT0cAEQEAAcK2
BBgBCgAgFiEE4kiFoBwLIQW3+uhwRiqk7+UcnjkFAmJNZn8CGwwACgkQRiqk7+Uc
njlf7AQAr8KyhHqLWRb04uPlTXu47U9y6hM5jE5SrODvXuqQoaTl3VYt2rs20ah5
vrTLPOFBSEl/Rg3ewXwHM0ERswOPNlKv1ztTrQncsPlFrn2M1jEw67oJeGz7mlX7
uWP60CL8T8HYm7yqBWvWR2lJIty8Y4A4DyaDmZFZuwlxUEC8Z18=
=ioK3
-----END PGP PUBLIC KEY BLOCK-----
"""
CN_PUB_KEY_VERIFIED_1024 = b"""-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: E248 85A0 1C0B 2105 B7FA E870 462A A4EF E51C 9E39
Comment: CN Testkey 1024 (test key) <chuck.norris@roundhouse.swiss>

mI0EYk1mfwEEALAY34boAQH90K+yF8ewfk9rgMXR9tuix8p9icD6q0R6gMsgY27T
rAwMeq2zQVUWtzoU/fVKwZQ6h+D6vwKqbp0PKa8bzcDSY7urCMM8BLmUZmcg3D+x
jXbXXueQwzaE64P0iDWUX+feihr8RKbwm6MAS6hLdOtPw4TZKV7RFPrVABEBAAG0
OkNOIFRlc3RrZXkgMTAyNCAodGVzdCBrZXkpIDxjaHVjay5ub3JyaXNAcm91bmRo
b3VzZS5zd2lzcz6IzQQTAQoAOBYhBOJIhaAcCyEFt/rocEYqpO/lHJ45BQJiTWZ/
AhsDBQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAAAoJEEYqpO/lHJ45As0D+NoEsr4t
eXpIKcUuFrUGsEnWQxw1dyN5lcQz3ZcHJFrZlgxUXY1Q1/Ipysn91XuNAA3NNv4A
xQ8aTeIimWdiZDFKOh9YDDVRcb93cTANqwIqYKeNMqWkVJhBCJr+ffyz5eaKGDrp
qnLH5QqhjUKJ2oeCngzm+jyKvmX6bs20MyS4jQRiTWZ/AQQAp9MS67VTtY1yh9aq
ebhAhPFBJkAmqefnkQN8L1JzhN0WX03iAjzFcj3EVtRmK/MOY1OeTbQo1thNIlyy
vjiolJ8tXIRq1R12lWcMm8/cuoqlkZGsnE4Fwt2SiaH6ZASEu2QbESqhQraw5cY+
QceRxMl+jFODvAipgV2012aAT0cAEQEAAYi2BBgBCgAgFiEE4kiFoBwLIQW3+uhw
Riqk7+UcnjkFAmJNZn8CGwwACgkQRiqk7+Ucnjlf7AQAr8KyhHqLWRb04uPlTXu4
7U9y6hM5jE5SrODvXuqQoaTl3VYt2rs20ah5vrTLPOFBSEl/Rg3ewXwHM0ERswOP
NlKv1ztTrQncsPlFrn2M1jEw67oJeGz7mlX7uWP60CL8T8HYm7yqBWvWR2lJIty8
Y4A4DyaDmZFZuwlxUEC8Z18=
=4Pkn
-----END PGP PUBLIC KEY BLOCK-----
"""

CN_FINGERPRINT_1024_DSA = "2A8DC71A041F55B171A3EA9E0514333665203F7C"
CN_PUB_KEY_VERIFIED_1024_DSA = b"""-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: 2A8D C71A 041F 55B1 71A3 EA9E 0514 3336 6520 3F7C
Comment: CN Testkey 1024 (test key) <chuck.norris@roundhouse.swiss>

mQGiBGJNhZkRBACQ0rrK7MuueFMm36TAMTVAiVh6Oj1oyyWaIJSQLsrCN1KIK4hQ
yQ/XWvm/CAxagCHEqWJm1p4CZbsuWHfJDDMCLimqyOhdJsoBms8k0vocKNmrtSzs
8Dn992WLm6BzfdFmz5winvfA5MmGbjrQzWvdkAXw/s40Se0qkeRnuJDABwCgvrCI
T9ytg0dmaQHC0QoR8X4PIVkD/iX4j/Wi0JZqkVXLudYZyPGQO2Hk0oRbGMZ03pZB
a9tzhVTAm+MmkG9KT/tASiz1ofUUS31YwhPOIwlpVOX1RYpYc1PwUK7i+8VlZNc1
68RyvcKWNLbPaRw3zLb6keLELOJY+CEZ0y6BrjR0VDr4Z3Cr68HGdEjQBQOGiFkw
FdLlA/9pE9okkZza9H2xR3Ug2ySm+fjnJdNp8JjVsN3ZsRDF45u0aHJLnt+UWebT
Cv7L8rXGTNUqXTcx4Mp4Kn0RBBaw95RBn+1u32soO4atlK56cppBETyot6RkKfSn
Iu1bTA5U7IOSQnNkNzw8VLXUFBlXoGICsc7AllGgS6zMGATOMLQ+Q04gVGVzdGtl
eSAxMDI0LURTQSAodGVzdCBrZXkpIDxjaHVjay5ub3JyaXNAcm91bmRob3VzZS5z
d2lzcz6IeAQTEQIAOBYhBCqNxxoEH1WxcaPqngUUMzZlID98BQJiTYWZAhsDBQsJ
CAcCBhUKCQgLAgQWAgMBAh4BAheAAAoJEAUUMzZlID98e7YAn2NOMjMz8milNMvi
hS5W3bBgK1GwAJ4lxHcNQL67r6XyBIxLl0Ft93yBFrkBDQRiTYWZEAQA99UwwVvO
OJEHPYa4lgwbEx7OzvruvCs6wMZg+/i46yQPkzJmx8GvMUjxdQ5oL94xnUA51/QG
4iEjQAzhrTu1Mdsi0w01tD9tZ1/6BUpGyKzAreZGdJJy93fF3hxbbqnQ2uYZHFq7
Abk/TYhKw9W8B4Tc2lcGXWMAOyVSvIUr3BMAAwYEAI0xkydNz2b33GYGM4vL5wws
0uJI+iAcHw4AU4wI/yCp06/yBY7TEbLIqU+GqL28XwX+BefX026yF0csjmhQXJAo
QFLXWCtpvn+5wBrt128oo/UqnWCLYhFkhqxOZ8TTeAdi1W5ninGsMohDxgtBW722
s/FKGskHcZ8vchYi4dOMiGAEGBECACAWIQQqjccaBB9VsXGj6p4FFDM2ZSA/fAUC
Yk2FmQIbDAAKCRAFFDM2ZSA/fKldAJ979nosVsCaV30MEOFMVZ6rtsWMxQCeKapg
G04upZUvhQCWqh+Ib33NF7I=
=ppU5
-----END PGP PUBLIC KEY BLOCK-----
"""

CN_FINGERPRINT_4096 = "B2E961753ECE0B345E718E74BA6F29C998DDD9BF"
CN_PUB_KEY_NONVERIFIED_4096 = b"""-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: B2E9 6175 3ECE 0B34 5E71 8E74 BA6F 29C9 98DD D9BF

xsFNBGJNZD4BEADx72ORnn64kWKbhbEh4V/6YxBykBo7neh0S1Hyy+QLJEv9Qqsg
Zhl0WZIDbA19FXrXFlGLICGYo1AH49PgiqPyzSoPXO1umQkvcwQ+mdnQ+3DVWK7d
HVtrbfYUN+NGIJc/1a5w85oKth0CKeULVtau38QlQ8N+sVG+Cj1TKfXFFk/bc5iC
7LfndPoq+lhlOBqlhUBo6dFjhqkYagt+vwYW9pDbrQk++LtX7gF0707pSQ2/gjb8
rQbRsvxEIIcebtKeN6sh3L2O8jGuw1iSWsffEimqKknVx+vgSdlFx1y4VZ8+pqK9
1PDrFT0d2P4HD2oxaNYMKyLUf0UkUBEtiB9O9M+UtSoEHUsImIEPBK06a+WSIRCi
8DyH10oq8gArnFlsetmCVu9oGIW8wOoIgvT9+o+NdYQOwCmp/fuGSLx/r1TqtVM6
i8B39gqcgo/lZGpfDcY0+bm06bYf67mNHF6krpzYSS/o7irUuGsFjm2Rpvmn3qkq
9TspN5zTk1xA0lYAEYDQVpwGeOm0bC7jI24BVjRUE2OIjP/VEQOqzQxUOXn3ArM8
XA/nfLObQeWKaZmcGMQV7kY+Jco2PGb19tlcjeTlt04+mD2FLTTSoEMwN5aJ06OB
Yg88BkvcTTV87+HYn7yuYRw02p8zy3JWHnsUvQ2HVQyy2R6YiBOE9+PONQARAQAB
zsFNBGJNZD4BEACgK4noXdCfihQ6uSseJPgqXnnZ1D37Ew0poSC+mfKmUZA9pEkB
wPBDwjJYsPK1hvw64Y6uY8TATzRXLd6CdHewFwgKoNLuNZFN3/eWf5XXj1maZk4m
kO+GiDDIcGOViutUJ11FK5AkKWZ1RKIRGyeblP9lV0TC/IWeZXj+Y9kcdy1O30gb
l3n2Q4ZTNc+shKUuqfT+Ls7RvGEmzzRZYMbuBPn3kBT957TZVj5qHctRRhfC+D48
ZgBdsdgoc8RuVw8eHxq9eyVil7qz0/RlLipDuJf1TUR+fLujhYKK7lZLlLHIDkXx
N4Nv9FHg/hMJNp6+ya9tw4hiW+J36dgM3sNzKI/oa5J8/nX/wDt8dbA/BiGWcEQ7
bVGuF0NQaP0UBxBAJ+Vfk7Otz3TQGLWYndV+ipgalcVY7k1T/SqwgYOZsXeyzQ9G
Oy9SJXkYrkCOQXv+XDaabcTHjwPcvpwyI6/32WBzc/lAXogHuHOX33mCHFZQ4wxH
PWZLCBwYRogWqyiCNi1u1uu5oSkkjgPUXL+IQaz+W2OTn6gnO9sm4WR+Tff9c/eN
t+E4mLOXCq3kRPrzdfbzkQFjaHDkFg+s7GKIm/NiqgtYjbKtJ7fnArfmpgTLyuqA
M9PKzsV8BndopvcaNFtGFRmxQ66/A34CL2IwZeobs6+y0D3bvu7/KpIYWwARAQAB
wsF2BBgBCgAgFiEEsulhdT7OCzRecY50um8pyZjd2b8FAmJNZD4CGwwACgkQum8p
yZjd2b+ZyxAAs1zbexIuU+LE7/jXTDgpk8V3NBpk7YpFAlAOIzabXj4oSAu8K8CI
1YWven+o8dVJ34m1eD6Ftrk+KKengIeI16o3/MrndNro8GVF2rnl+fSLlNyUUwEP
DdIuxwqsWmXYivW4WHO4kKKSf8oavhmDylLMK8tWZ6Cbh05n47LfD9IpZwCWgsb7
XCkzrdCYpHY6GZaFHbH2d31zDd5A7SwokK+j617lRDBpmZyLsyLqXHaKpXVBTT17
FuJO8rdWO8/2+62zqSxgcj568edCtAOVYl/sAo8Er9YME2BDFHaRdJEhu7OzU0kQ
wFCJLoi9SuSkl55kanHGxt9t6FElHQVKsRlcwGdoPKcrVVv8nVOkRNoHLu7KIUZF
rsqcX9Bx5FTQmd/cpI5F977wXGUJTdYxYZpA3iCpfQXmzv+OJjAhonxwxqNBheoK
XLmEJsJ40HXBo/KSJO6Ya1lalznJQnS7zNNxv7Ik0MiXKomSsFjqzDlP5cNI+qyM
e5fWSLm8lS7fzXth/7wOgP4jPJxOVaKOObvViKNYLj0TLSmqt1Sed76Mxsd9Jbv4
Aif+1X7Q0aWS6k3SfPCuUXMs0PqJMBlX5cOze8FhtzeT1o+oSTBydysBjp7nta/d
OFE37vekV3bXHh3uP/32VgOe0JdYue8HSPZYIPJZYwu1CzBSEQqJR5w=
=2CDr
-----END PGP PUBLIC KEY BLOCK-----
"""
CN_PUB_KEY_VERIFIED_4096 = b"""-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: B2E9 6175 3ECE 0B34 5E71 8E74 BA6F 29C9 98DD D9BF
Comment: CN Testkey 4096 (test key) <chuck.norris@roundhouse.swiss>

mQINBGJNZD4BEADx72ORnn64kWKbhbEh4V/6YxBykBo7neh0S1Hyy+QLJEv9Qqsg
Zhl0WZIDbA19FXrXFlGLICGYo1AH49PgiqPyzSoPXO1umQkvcwQ+mdnQ+3DVWK7d
HVtrbfYUN+NGIJc/1a5w85oKth0CKeULVtau38QlQ8N+sVG+Cj1TKfXFFk/bc5iC
7LfndPoq+lhlOBqlhUBo6dFjhqkYagt+vwYW9pDbrQk++LtX7gF0707pSQ2/gjb8
rQbRsvxEIIcebtKeN6sh3L2O8jGuw1iSWsffEimqKknVx+vgSdlFx1y4VZ8+pqK9
1PDrFT0d2P4HD2oxaNYMKyLUf0UkUBEtiB9O9M+UtSoEHUsImIEPBK06a+WSIRCi
8DyH10oq8gArnFlsetmCVu9oGIW8wOoIgvT9+o+NdYQOwCmp/fuGSLx/r1TqtVM6
i8B39gqcgo/lZGpfDcY0+bm06bYf67mNHF6krpzYSS/o7irUuGsFjm2Rpvmn3qkq
9TspN5zTk1xA0lYAEYDQVpwGeOm0bC7jI24BVjRUE2OIjP/VEQOqzQxUOXn3ArM8
XA/nfLObQeWKaZmcGMQV7kY+Jco2PGb19tlcjeTlt04+mD2FLTTSoEMwN5aJ06OB
Yg88BkvcTTV87+HYn7yuYRw02p8zy3JWHnsUvQ2HVQyy2R6YiBOE9+PONQARAQAB
tDpDTiBUZXN0a2V5IDQwOTYgKHRlc3Qga2V5KSA8Y2h1Y2subm9ycmlzQHJvdW5k
aG91c2Uuc3dpc3M+iQJOBBMBCgA4FiEEsulhdT7OCzRecY50um8pyZjd2b8FAmJN
ZD4CGwMFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AACgkQum8pyZjd2b+Vmw//dFBS
+th2ZCtWrDCjodXFoLyb4gG5mR6NGPo+esp17Obzs5POPrtLRLi41tNPBtNy//jF
Dwg9vFrwUzoC8zniV/dPQhxNjShB4Tjlg8kNssftXbbc/tgWLPedH1OluwuLtuEI
CTbPpVL1vAA/WytEhNw3CCC73Y1RpFNx7wi8wlZZUgjtgsDLQlzP5ogwuNdLlvhX
0QRi6hAkdPRScqi0n+pDZXamNwyeYEt2lRXYce153jqZJh3P8CYRVFmfZeWruSX5
yyZSabWy7aIFZ48RimOyTyHfmeOQM93GCDQa9g8sLtfSSXtKrZbCRpmd1s848TjI
1PFHgahy3Xoa97KhXG8EtmqzASO3fpwkcfM6nUelMtoIoG4IdbCZytFXPXvAwPMy
QY0dY0/uTrdrFWkXanQkwdqy++iOIGilI0MDi80UzqmXbY3a93oOJl4mqUlUMVwA
EpW0PXVo8QnDc0t5XLUZJq9UA0Nk2qAKm3OUTSqtuq6zdm7l5K1lbMFEBoBa6+4Z
EccK9Liz5lFtDmxqoi/rVGO6WEWvZ/TD3qbfCUWEIqsm0vKxDLaUrDvjQjcZoyHO
HGBTXhwmRceMcOM7OsqttSYDqAFRDqrsnfROJBqizUltER16qcWmSCJzJKRTxbQ3
Iql6s/Gchn2m0wkUxG/DN51UGBAb965Alaa+BsG5Ag0EYk1kPgEQAKAriehd0J+K
FDq5Kx4k+CpeednUPfsTDSmhIL6Z8qZRkD2kSQHA8EPCMliw8rWG/Drhjq5jxMBP
NFct3oJ0d7AXCAqg0u41kU3f95Z/ldePWZpmTiaQ74aIMMhwY5WK61QnXUUrkCQp
ZnVEohEbJ5uU/2VXRML8hZ5leP5j2Rx3LU7fSBuXefZDhlM1z6yEpS6p9P4uztG8
YSbPNFlgxu4E+feQFP3ntNlWPmody1FGF8L4PjxmAF2x2ChzxG5XDx4fGr17JWKX
urPT9GUuKkO4l/VNRH58u6OFgoruVkuUscgORfE3g2/0UeD+Ewk2nr7Jr23DiGJb
4nfp2Azew3Moj+hrknz+df/AO3x1sD8GIZZwRDttUa4XQ1Bo/RQHEEAn5V+Ts63P
dNAYtZid1X6KmBqVxVjuTVP9KrCBg5mxd7LND0Y7L1IleRiuQI5Be/5cNpptxMeP
A9y+nDIjr/fZYHNz+UBeiAe4c5ffeYIcVlDjDEc9ZksIHBhGiBarKII2LW7W67mh
KSSOA9Rcv4hBrP5bY5OfqCc72ybhZH5N9/1z94234TiYs5cKreRE+vN19vORAWNo
cOQWD6zsYoib82KqC1iNsq0nt+cCt+amBMvK6oAz08rOxXwGd2im9xo0W0YVGbFD
rr8DfgIvYjBl6huzr7LQPdu+7v8qkhhbABEBAAGJAjYEGAEKACAWIQSy6WF1Ps4L
NF5xjnS6bynJmN3ZvwUCYk1kPgIbDAAKCRC6bynJmN3Zv5nLEACzXNt7Ei5T4sTv
+NdMOCmTxXc0GmTtikUCUA4jNptePihIC7wrwIjVha96f6jx1UnfibV4PoW2uT4o
p6eAh4jXqjf8yud02ujwZUXaueX59IuU3JRTAQ8N0i7HCqxaZdiK9bhYc7iQopJ/
yhq+GYPKUswry1ZnoJuHTmfjst8P0ilnAJaCxvtcKTOt0JikdjoZloUdsfZ3fXMN
3kDtLCiQr6PrXuVEMGmZnIuzIupcdoqldUFNPXsW4k7yt1Y7z/b7rbOpLGByPnrx
50K0A5ViX+wCjwSv1gwTYEMUdpF0kSG7s7NTSRDAUIkuiL1K5KSXnmRqccbG323o
USUdBUqxGVzAZ2g8pytVW/ydU6RE2gcu7sohRkWuypxf0HHkVNCZ39ykjkX3vvBc
ZQlN1jFhmkDeIKl9BebO/44mMCGifHDGo0GF6gpcuYQmwnjQdcGj8pIk7phrWVqX
OclCdLvM03G/siTQyJcqiZKwWOrMOU/lw0j6rIx7l9ZIubyVLt/Ne2H/vA6A/iM8
nE5Voo45u9WIo1guPRMtKaq3VJ53vozGx30lu/gCJ/7VftDRpZLqTdJ88K5RcyzQ
+okwGVflw7N7wWG3N5PWj6hJMHJ3KwGOnue1r904UTfu96RXdtceHe4//fZWA57Q
l1i57wdI9lgg8lljC7ULMFIRColHnA==
=SAz/
-----END PGP PUBLIC KEY BLOCK-----
"""
CN_PUB_KEY = CN_PUB_KEY_VERIFIED_4096
CN_FINGERPRINT = CN_FINGERPRINT_4096


@pytest.mark.parametrize(
    "status_ok, status_code, key_bloc_content, should_pass",
    [
        # Invalid key format.
        (True, status.HTTP_200_OK, re.sub(PGP_BEGIN_BLOCK, b"", CN_PUB_KEY), False),
        (True, status.HTTP_200_OK, re.sub(PGP_END_BLOCK, b"", CN_PUB_KEY), False),
        # Missing key scenarios.
        (True, status.HTTP_404_NOT_FOUND, CN_PUB_KEY, False),
        (False, status.HTTP_200_OK, CN_PUB_KEY, False),
        # Valid key scenario.
        (True, status.HTTP_200_OK, CN_PUB_KEY, True),
    ],
)
def test_download_ascii_armored_pgp_key(
    patch_request,
    status_ok: bool,
    status_code: int,
    key_bloc_content: str,
    should_pass: bool,
) -> None:
    """Verify that:
    * A bad key formats raise an error:
        -> Missing PGP key BEGIN block.
        -> Missing PGP key END block.
    * A Valid key returns the downloaded public key bloc.
    * A missing key raises an error.
    """
    mock_request: mock.Mock = patch_request(
        mock.Mock(ok=status_ok, status_code=status_code, content=key_bloc_content)
    )
    with contextlib.nullcontext() if should_pass else pytest.raises(KeyDownloadError):
        downloaded_key = download_ascii_armored_public_pgp_key(
            fingerprint=CN_FINGERPRINT, keyserver_url=KEYSERVER_URL
        )
        assert CN_PUB_KEY == downloaded_key
        assert mock_request.assert_called_once
        mock_call_args_value = mock_request.call_args.kwargs["url"]
        assert KEYSERVER_URL in mock_call_args_value
        assert CN_FINGERPRINT in mock_call_args_value


def test_download_ascii_armored_pgp_key_no_connection(patch_request) -> None:
    """Verify a non-existing keyserver raises an error."""

    mock_request: mock.Mock = patch_request(
        mock.Mock(ok=False, status_code=status.HTTP_503_SERVICE_UNAVAILABLE)
    )
    mock_request.side_effect = requests.exceptions.ConnectionError
    with pytest.raises(KeyDownloadError):
        download_ascii_armored_public_pgp_key(
            fingerprint=CN_FINGERPRINT, keyserver_url="https://fake.server.org"
        )


@pytest.mark.parametrize(
    "key_bloc_content, fingerprint, should_pass, error_on_missing_key, expected_exception",
    [
        # Non-verified or deleted key (i.e. missing user ID info).
        (
            CN_PUB_KEY_NONVERIFIED_1024,
            CN_FINGERPRINT_1024,
            False,
            True,
            KeyNotVerifiedError,
        ),
        (
            CN_PUB_KEY_NONVERIFIED_4096,
            CN_FINGERPRINT_4096,
            False,
            True,
            KeyNotVerifiedError,
        ),
        # Non-verified or deleted key (i.e. missing user ID info) with a
        # GPG store that does not raise an error when importing a key with
        # missing user ID.
        (
            CN_PUB_KEY_NONVERIFIED_1024,
            CN_FINGERPRINT_1024,
            False,
            False,
            KeyNotVerifiedError,
        ),
        (
            CN_PUB_KEY_NONVERIFIED_4096,
            CN_FINGERPRINT_4096,
            False,
            False,
            KeyNotVerifiedError,
        ),
        # Fingerprint of returned key does not match fingerprint from request
        (CN_PUB_KEY_VERIFIED_4096, CN_FINGERPRINT_1024, False, True, KeyDownloadError),
        # Bad key algorithm (DSA keys are not accepted).
        (
            CN_PUB_KEY_VERIFIED_1024_DSA,
            CN_FINGERPRINT_1024_DSA,
            False,
            True,
            KeyDownloadError,
        ),
        # Bad key length.
        (CN_PUB_KEY_VERIFIED_1024, CN_FINGERPRINT_1024, False, True, KeyDownloadError),
        # Valid key scenario.
        (CN_PUB_KEY_VERIFIED_4096, CN_FINGERPRINT_4096, True, True, KeyDownloadError),
    ],
)
def test_download_key_metadata(
    key_bloc_content: str,
    fingerprint: str,
    should_pass: bool,
    error_on_missing_key: bool,
    expected_exception: Exception,
) -> None:
    """Verify that:
    * A non-verified/deleted/revoked key (key missing the user ID information)
      raises an error.
    * A mismatch between the specified fingerprint and the fingerprint
      extracted from the key bloc raises an error.
    * Non RSA keys and keys with length < 4096 raise an error.
    * A Valid key bloc returns a metadata object with the correct user ID and
      email.
    """
    mock_download_pgp_key = mock.Mock(return_value=key_bloc_content)
    mock_gpg_store = MockGPGStore() if error_on_missing_key else MockGPGStoreWithNoKey()
    with mock.patch("gpg_lite.GPGStore", mock_gpg_store), mock.patch(
        "projects.utils.pgp.download_ascii_armored_public_pgp_key",
        mock_download_pgp_key,
    ):
        with contextlib.nullcontext() if should_pass else pytest.raises(
            expected_exception
        ):
            key_metadata = download_key_metadata(
                fingerprint=fingerprint,
                keyserver_url=KEYSERVER_URL,
            )
            assert key_metadata.user_id == CN_USER_ID
            assert key_metadata.email == CN_EMAIL


class MockGPGStore(mock.Mock):
    """A fake GPG store for testing purposes."""

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self.key_list = []

    def import_file(self, key_bloc: bytes):
        try:
            fpr, user_and_email = (
                x[len("Comment: ") :].strip()
                for x in key_bloc.decode().split("\n")
                if x.startswith("Comment: ")
            )
        except ValueError:
            raise gpg.GPGError("No user ID. Cannot import key.") from None

        fpr = fpr.replace(" ", "")
        email = re.search(r"<.*@.*>$", user_and_email).group(0).strip("<>")
        user_id = user_and_email[: -(len(email) + 2)].strip()
        key_length = 1024 if "1024" in user_id else 4096
        key_algorithm = 2 if fpr.startswith("2A8DC71A") else 1
        self.key_list.append(
            mock_key(
                fingerprint=fpr,
                full_name=user_id,
                email=email,
                key_length=key_length,
                pub_key_algorithm=key_algorithm,
            )
        )

    def list_pub_keys(self) -> list[gpg.Key]:
        return self.key_list


class MockGPGStoreWithNoKey(MockGPGStore):
    """Another fake GPG store for testing purposes. This one simply never
    contains any PGP key, no matter how many keys are imported.
    """

    def import_file(self, key_bloc: bytes):
        pass


def mock_key(
    fingerprint: str,
    full_name: str = "CN Testkey",
    email: str = "chuck.norris@roundhouse.swiss",
    key_length: int = 4096,
    pub_key_algorithm: int = 1,
) -> gpg.Key:
    """Generate a mock PGP key."""

    uid = gpg.Uid(full_name=full_name, email=email)
    self_signature = gpg.Signature(
        issuer_uid=uid,
        issuer_key_id=fingerprint[-16:],
        issuer_fingerprint=fingerprint,
        creation_date="1550241679",
        signature_class="13x",
        validity=gpg.model.SignatureValidity.good,
    )
    return gpg.Key(
        key_id=fingerprint[-16:],
        fingerprint=fingerprint,
        validity=gpg.Validity.ultimately_valid,
        key_length=key_length,
        pub_key_algorithm=pub_key_algorithm,
        creation_date="1550241679",
        uids=(uid,),
        owner_trust="u",
        key_type=gpg.KeyType.public,
        origin="http://hagrid.hogwarts.org:11371",
        signatures=(self_signature,),
        key_capabilities=frozenset(
            (
                gpg.KeyCapability.sign,
                gpg.KeyCapability.certify,
                gpg.KeyCapability.encrypt,
            )
        ),
    )
