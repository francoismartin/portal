from collections import namedtuple
from datetime import datetime, timezone

import factory
import pytest

from projects.utils.task import get_last_run


shared_task_mock = namedtuple("shared_task_mock", ("name",))


@pytest.mark.django_db
def test_get_last_run_no_runs():
    assert get_last_run(shared_task_mock("foo")) is None


def test_get_last_run_one_run_match(periodic_task_run_factory):
    now = datetime.now(timezone.utc)
    pt = periodic_task_run_factory(created_at=now)
    assert get_last_run(shared_task_mock(pt.task)) == now


def test_get_last_run_one_run_no_match(periodic_task_run_factory):
    periodic_task_run_factory(task="foo")
    assert get_last_run(shared_task_mock("bar")) is None


def test_get_last_run_multiple_runs(periodic_task_run_factory):
    dates = (
        datetime(2018, 12, 7, 3, 0, 0, 1, timezone.utc),
        datetime(2020, 12, 5, 3, 0, 0, 1, timezone.utc),
        datetime(2019, 12, 5, 3, 0, 0, 1, timezone.utc),
        datetime(2020, 12, 7, 3, 0, 0, 1, timezone.utc),
    )

    periodic_task_run_factory.create_batch(
        4,
        task=factory.Iterator(("foo", "foo", "foo", "bar")),
        created_at=factory.Iterator(dates),
    )

    assert get_last_run(shared_task_mock("foo")) == dates[1]
