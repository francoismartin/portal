from datetime import timezone
from typing import Tuple

import factory
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db.models import signals
from guardian.shortcuts import assign_perm

from identities.permissions import perm_group_manager
from projects.models.approval import DataProviderApproval, NodeApproval, GroupApproval
from projects.models.const import (
    APP_PERM_DATA_PROVIDER_ADMIN,
    APP_PERM_NODE_ADMIN,
    APP_PERM_NODE_VIEWER,
)
from projects.models.data_provider import (
    DataProvider,
    assign_dp_coordinator,
    assign_dp_viewer,
)
from projects.models.data_transfer import DataPackage, DataPackageTrace, DataTransfer
from projects.models.feed import Feed
from projects.models.flag import Flag
from projects.models.message import Message
from projects.models.node import Node
from projects.models.pgp import PgpKeyInfo
from projects.models.project import (
    Project,
    ProjectRole,
    ProjectUserRole,
    ProjectUserRoleHistory,
)
from projects.models.task import PeriodicTaskRun
from projects.models.tile import QuickAccessTile
from projects.models.user import Profile, UserNamespace

User = get_user_model()
USER_PASSWORD = "pass"  # nosec


class FeedFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Feed

    label = factory.Faker("random_element", elements=Feed.FeedLabel)
    title = factory.Faker("sentence")
    message = factory.Faker("paragraph")


class FlagFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Flag
        django_get_or_create = ("code",)

    code = "sphn"
    description = (
        "This flag is set for an SPHN project user, "
        "when the user passes SPHN security exam and "
        "all relevant SPHN user agreements are signed."
    )

    class Params:
        aup_scicore = factory.Trait(
            name="aup_scicore",
            description=(
                "This flag is set when AUP for sciCORE is signed by the User."
            ),
        )


class QuickAccessTileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = QuickAccessTile

    title = "GitLab"
    url = "https://gitlab.com/"
    image = factory.django.ImageField()
    flag = factory.SubFactory(FlagFactory)


@factory.django.mute_signals(signals.post_save)
class ProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Profile

    affiliation = factory.Faker("company")


@factory.django.mute_signals(signals.post_save)
class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: f"user_{n}")
    password = factory.PostGenerationMethodCall("set_password", USER_PASSWORD)
    email = factory.Faker("email")
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    profile = factory.RelatedFactory(ProfileFactory, "user")

    class Params:
        basic = factory.Trait(
            username="user-basic",
            email="foo@bar.org",
        )
        staff = factory.Trait(
            username="user-staff",
            is_staff=True,
        )

    @factory.post_generation
    def post_flags(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            for flag in extracted:
                self.flags.add(flag)


class UserNamespaceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UserNamespace

    name = factory.Faker("word")


class GroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Group
        django_get_or_create = ("name",)

    name = factory.Sequence(lambda n: f"Group {n}")


class NodeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Node

    code = factory.Sequence(lambda n: f"node_{n}")
    name = factory.Sequence(lambda n: f"HPC center {n}")
    node_status = Node.STATUS_ONLINE
    ticketing_system_email = factory.Faker("email")


class ProjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Project

    gid = factory.Sequence(lambda n: n)
    code = factory.Sequence(lambda n: f"project_{n}")
    name = factory.Sequence(lambda n: f"Project {n}")
    destination = factory.SubFactory(
        NodeFactory,
        code=factory.Sequence(lambda n: f"project_node_{n}"),
        name=factory.Sequence(lambda n: f"Project Node {n}"),
    )


class ProjectUserRoleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProjectUserRole

    project = factory.SubFactory(ProjectFactory)
    user = factory.SubFactory(UserFactory)
    role = ProjectRole.USER.value


class ProjectUserRoleHistoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProjectUserRoleHistory

    changed_by = factory.SubFactory(UserFactory)
    user = factory.SubFactory(UserFactory)
    project = factory.SubFactory(ProjectFactory)
    project_str = factory.LazyAttribute(lambda o: o.project.code)
    role = ProjectRole.USER.value
    enabled = True


@factory.django.mute_signals(signals.post_save)
class DataProviderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataProvider

    name = factory.Faker("sentence")
    code = factory.Faker("word")
    node = factory.SubFactory(
        NodeFactory,
        code=factory.Sequence(lambda n: f"data_provider_node_{n}"),
        name=factory.Sequence(lambda n: f"Data Provider Node {n}"),
    )


class DataTransferFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataTransfer

    project = factory.SubFactory(ProjectFactory)
    data_provider = factory.SubFactory(DataProviderFactory)
    requestor = factory.SubFactory(UserFactory)
    purpose = DataTransfer.TEST


class DataPackageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataPackage

    metadata = "{}"
    data_transfer = factory.SubFactory(DataTransferFactory)


class DataPackageTraceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataPackageTrace

    data_package = factory.SubFactory(DataPackageFactory)
    node = factory.SubFactory(NodeFactory)


class PeriodicTaskRunFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PeriodicTaskRun

    created_at = factory.Faker("date_time", tzinfo=timezone.utc)
    task = factory.Faker("word")


class MessageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Message

    title = factory.Faker("sentence")
    body = factory.Faker("paragraph", nb_sentences=5)
    user = factory.SubFactory(UserFactory)


class PgpKeyInfoFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PgpKeyInfo

    user = factory.SubFactory(UserFactory)

    @factory.sequence
    def fingerprint(x):  # pylint: disable=no-self-argument
        return f"{'A' * (40 - len(str(x)))}{x}"

    key_user_id = factory.Faker("name")
    key_email = factory.Faker("email")
    status = PgpKeyInfo.Status.PENDING


@factory.django.mute_signals(signals.post_save)
class NodeApprovalFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = NodeApproval

    data_transfer = factory.SubFactory(DataTransferFactory)
    node = factory.SubFactory(NodeFactory)
    type = NodeApproval.Type.HOSTING


@factory.django.mute_signals(signals.post_save)
class DataProviderApprovalFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataProviderApproval

    data_transfer = factory.SubFactory(DataTransferFactory)
    data_provider = factory.SelfAttribute("data_transfer.data_provider")


@factory.django.mute_signals(signals.post_save)
class GroupApprovalFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = GroupApproval

    data_transfer = factory.SubFactory(
        DataTransferFactory, purpose=DataTransfer.PRODUCTION
    )
    group = factory.SubFactory(GroupFactory)

    @factory.post_generation
    def users(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            self.group.user_set.add(*extracted)


def make_node_admin(user: User | None = None, node: Node | None = None) -> User:
    if user is None:
        user = UserFactory()
    if node is None:
        node = NodeFactory()
    group, _ = Group.objects.get_or_create(name=f"Node Admin {node.code}")
    group.user_set.add(user)
    assign_perm(APP_PERM_NODE_ADMIN, group, node)
    return user


def make_node_viewer(user: User | None = None, node: Node | None = None) -> User:
    if user is None:
        user = UserFactory()
    if node is None:
        node = NodeFactory()
    group, _ = Group.objects.get_or_create(name=f"Node Viewer {node.code}")
    group.user_set.add(user)
    assign_perm(APP_PERM_NODE_VIEWER, group, node)
    return user


def make_group_manager(managed_group: Group, user: User | None = None) -> User:
    if user is None:
        user = UserFactory()
    group, _ = Group.objects.get_or_create(name=f"Group Manager {managed_group.name}")
    group.user_set.add(user)
    assign_perm(perm_group_manager, group, managed_group)
    return user


def make_data_provider_admin(
    user: User | None = None, data_provider: DataProvider | None = None
) -> User:
    if user is None:
        user = UserFactory()
    if data_provider is None:
        data_provider = DataProviderFactory()
    group = GroupFactory(name=f"Data Provider Admin {data_provider.code}")
    group.user_set.add(user)
    assign_perm(APP_PERM_DATA_PROVIDER_ADMIN, group, data_provider)
    return user


def make_data_provider_coordinator(
    user: User | None = None, data_provider: DataProvider | None = None
) -> User:
    if user is None:
        user = UserFactory()
    if data_provider is None:
        data_provider = DataProviderFactory()
    coordinator_group = GroupFactory(
        name=f"Data Provider Coordinator {data_provider.code}"
    )
    coordinator_group.user_set.add(user)
    assign_dp_coordinator(coordinator_group, data_provider)
    return user


def make_data_provider_viewer(
    user: User | None = None, data_provider: DataProvider | None = None
) -> User:
    if user is None:
        user = UserFactory()
    if data_provider is None:
        data_provider = DataProviderFactory()
    group = GroupFactory(name=f"Data Provider Viewer {data_provider.code}")
    group.user_set.add(user)
    assign_dp_viewer(group, data_provider)
    return user


def make_data_provider_users(dp: DataProvider) -> Tuple[User, User, User]:
    viewer, technical_admin, coordinator = UserFactory.create_batch(3)
    make_data_provider_viewer(viewer, dp)
    make_data_provider_admin(technical_admin, dp)
    make_data_provider_coordinator(coordinator, dp)
    return viewer, coordinator, technical_admin


def make_project_user(
    project: Project, role=ProjectRole, user: User | None = None
) -> User:
    if user is None:
        user = UserFactory()
    ProjectUserRoleFactory(project=project, user=user, role=role.value)
    return user
