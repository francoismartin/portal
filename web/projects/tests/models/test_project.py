import pytest
from assertpy import assert_that
from django.core.exceptions import ValidationError
from django_drf_utils.tests.models.utils import validate_field

from projects.models.project import (
    Project,
    ProjectIPAddresses,
    ProjectRole,
    Resource,
    has_project_role,
    is_data_manager,
    is_manager,
    is_project_leader,
)

from ..factories import make_project_user


def test_project_role_choices():
    choices = ProjectRole.choices()
    assert len(choices) == 4
    assert choices[0] == (1, "User")


def test_gid_label(project_factory):
    project = project_factory.build()
    assert project._meta.get_field("gid").verbose_name == "gid"


def test_str(project_factory):
    project = project_factory.build()
    assert str(project) == f"{project.name} ({project.code})"


LONGEST_VALID_NAME = (
    "ËÛÄâU(PëÏtjzÜwPÂ93ÛfJÀ2ÄÖKàWdçEoBnÖôwGmg3yÇï5WMw3qkioÀQÈIvRÔVBOÔêëüàtËF"
    "7ÈÇËDkXßOÛÀÔQÈ4cöSa8ÏCAhkßh5m(qBÖcYhàzbÈ QQÇGFs8IËNWil07ÏbKÀR6säq1OÄÈâI"
    "eâûGvEPç0YÎR0päï6HnZ4SWg9éëaOSPÂXF5îY7qM1ÇçQLMêIDïèèxzLôMzërWöïÇo6SÇhGë"
    "7wé7Cöc5ßÊWèäFètpP3Âuî5ÜBLT0P5yjWÎgrAzntDy()îé6g5AÀ96gEËLüjxïxsfRtêa3Â3"
    "tTüAtêb8ÎZ(0cNÜÀJÜhvKge17ÖwqÄÇ9xZ RûsIèAyGeàmhÊ7M99êûliYyixfÖ8êlÄméoz1J"
    "mB9cÊî28ÄqïOXieyiCöSNê9çfNeIdÏPÊ)EGÄZÜlfbaüLyÛDTü9ûj6K6xEbVGkAEwîmndëL "
    "âÔH6qJAßhtarèyGvvÏkVÄ4Q4hdËVxîqj)zßkMÎQÊï0üÂï9ÛÈÜéSÉÜbHsÊ-i5M9öÊöJnIôDk"
    "iwà2ÇöÈVïasO ôa"
)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "test_input, expected",
    (
        # (name, is_valid)
        ("ABCabc - (012)ßàÀéÉèÈäÄëËïÏöÖüÜçÇâÂêÊîÎôÔûÛ", True),
        ("L'Ospitale", True),
        (LONGEST_VALID_NAME, True),  # upper length limit
        (LONGEST_VALID_NAME + " ", False),  # one character over length limit
        ("జ్ఞ‌ా", False),  # unicode characters
        ("<script>alert('Hello')</script>", False),  # XSS prevention
        (".", False),  # special characters
        ("_", False),  # special characters
        ("{", False),  # special characters
        ("[", False),  # special characters
        ("<", False),  # special characters
        ("/", False),  # special characters
        ("&", False),  # special characters
        ("!", False),  # special characters
        ("$", False),  # special characters
    ),
)
def test_validation_project_name(test_input, expected):
    validate_field(Project, "name", test_input, expected)


LONGEST_VALID_ID = "hi8ksu8ey1cd_r82xj6d7ry33gvm5kx7"


@pytest.mark.django_db
@pytest.mark.parametrize(
    "test_input, expected",
    (
        # (name, is_valid)
        ("abc_012", True),
        (LONGEST_VALID_ID, True),  # upper length limit
        (LONGEST_VALID_ID + "a", False),  # one character over length limit
        ("జ్ఞ‌ా", False),  # unicode characters
        ("<script>alert('Hello')</script>", False),  # XSS prevention
        ("ABC", False),  # uppercase characters
        ("abc def", False),  # spaces
        ("abc-def", False),  # dashes
        (".", False),  # special characters
        ("{", False),  # special characters
        ("[", False),  # special characters
        ("<", False),  # special characters
        ("/", False),  # special characters
        ("&", False),  # special characters
        ("!", False),  # special characters
        ("$", False),  # special characters
    ),
)
def test_validation_code(test_input, expected):
    validate_field(Project, "code", test_input, expected)


def test_ip_addresses_validation():
    with pytest.raises(ValidationError):
        project_ip_addr = ProjectIPAddresses(
            project=None, ip_address="127.0.0.1", mask=33
        )
        project_ip_addr.clean_fields(exclude=["project", "ip_address"])


def test_ip_addresses_str():
    project_ip = ProjectIPAddresses(project=None, ip_address="127.0.0.1", mask=24)
    assert str(project_ip) == "127.0.0.1/24"


def test_resource_location_scheme():
    for scheme in ("http", "https", "ftp", "ftps", "ssh"):
        Resource(
            project=None, name="Test resource", location=f"{scheme}://resource.org"
        ).clean_fields(exclude=["project"])
    with pytest.raises(ValidationError):
        Resource(
            project=None, name="Test resource", location="foo://resource.org"
        ).clean_fields(exclude=["project"])


def test_resource_str():
    resource = Resource(
        project=None, name="Test resource", location="https://resource.org"
    )
    assert str(resource) == "https://resource.org"


@pytest.fixture
def project_basic_user_role(user_factory, project_factory, project_user_role_factory):
    user = user_factory(basic=True)
    project = project_factory()

    def create(role: ProjectRole):
        return project_user_role_factory(user=user, project=project, role=role.value)

    return create


@pytest.mark.django_db
@pytest.mark.parametrize(
    "role,manager,true_callable",
    (
        (ProjectRole.PL, True, is_project_leader),
        (ProjectRole.DM, False, is_data_manager),
        (ProjectRole.PM, True, None),
    ),
)
def test_has_any_project_role(
    project_basic_user_role,  # pylint: disable=redefined-outer-name
    role,
    manager,
    true_callable,
):
    project_user_role = project_basic_user_role(role=role)
    assert_that(
        is_manager(project_user_role.user),
    ).is_equal_to(manager)
    if true_callable:
        assert_that(true_callable(project_user_role.user)).is_true()


@pytest.mark.django_db
def test_is_manager_other(user_factory, project_factory, project_user_role_factory):
    user = user_factory(basic=True)
    project = project_factory()
    # No project permission
    assert_that(is_manager(user)).is_false()
    # USER
    project_user_role_factory(user=user, project=project, role=ProjectRole.USER.value)
    assert_that(is_manager(user)).is_false()
    # DM
    make_project_user(project, ProjectRole.DM, user)
    assert_that(is_manager(user)).is_false()


@pytest.mark.django_db
def test_has_project_role(
    project_basic_user_role,
):  # pylint: disable=redefined-outer-name
    pl_role = ProjectRole.PL
    project_user_role = project_basic_user_role(role=pl_role)
    assert_that(
        has_project_role(project_user_role.user, project_user_role.project, pl_role)
    ).is_true()
    for role in (
        ProjectRole.PM,
        ProjectRole.DM,
        ProjectRole.USER,
    ):
        assert_that(
            has_project_role(project_user_role.user, project_user_role.project, role)
        ).is_false()
