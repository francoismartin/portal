import pytest
from assertpy import assert_that
from django_drf_utils.tests.models.utils import validate_field

from projects.models.data_transfer import DataTransfer


@pytest.mark.parametrize(
    "test_input, expected",
    (
        # (requestor_pgp_key_fp, is_valid)
        (
            "A0B1C2D3E4F5A6B7C8D9E0F1A2B3C4D5E6F7A8B9",
            True,
        ),  # uppercase, upper length limit
        ("A0B1C2D3E4F5A6B7C8D9", True),  # uppercase, shorter length
        ("a0b1c2d3e4f5a6b7c8d9e0f1a2b3c4d5e6f7a8b9", False),  # lowercase
        ("A0B1C2D3E4F5A6B7C8D9E0F1A2B3C4D5E6F7A8B9C", False),  # too long
        ("A0B1 C2D3 E4F5 A6B7 C8D9 E0F1 A2B3 C4D5", False),  # spaces
        ("G0B1C2D3E4F5A6B7C8D9E0F1A2B3C4D5E6F7A8B9", False),  # not allowed characters
        ("$0B1C2D3E4F5A6B7C8D9E0F1A2B3C4D5E6F7A8B9", False),  # not allowed characters
    ),
)
def test_validation_requestor_pgp_key_fp(test_input, expected):
    validate_field(DataTransfer, "requestor_pgp_key_fp", test_input, expected)


def test_approvals(node_approval_factory):
    node_approval = node_approval_factory()
    data_transfer = node_approval.data_transfer
    assert_that(data_transfer.approvals).is_length(1).starts_with(node_approval)
