from ..factories import PeriodicTaskRunFactory


def test_str():
    periodic_task_run = PeriodicTaskRunFactory.build()
    assert (
        str(periodic_task_run)
        == f"{periodic_task_run.task} ({periodic_task_run.created_at})"
    )
