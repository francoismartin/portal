import pytest
from assertpy import assert_that
from django.test import TestCase
from guardian.shortcuts import get_groups_with_perms

from projects.models.node import (
    Node,
    assign_node_admin,
    assign_node_viewer,
    get_node_admin_nodes,
    get_node_admins,
    get_node_managers,
    get_node_viewer_nodes,
    get_node_viewers,
    has_node_admin_nodes,
    has_node_admin_permission,
    has_node_viewer_nodes,
    has_node_viewer_permission,
)

from ..factories import NodeFactory, UserFactory, make_node_admin, make_node_viewer


def test_node_str():
    node = Node(code="node_1", name="HPC center 1", node_status=Node.STATUS_ONLINE)
    assert str(node) == f"{node.name} ({node.code})"


class TestNodeViewer(TestCase):
    def setUp(self):
        self.node = NodeFactory.create()
        self.node_viewer = make_node_viewer(node=self.node)
        self.staff = UserFactory(staff=True)

    def test_get_node_viewer_nodes(self):
        assert_that(get_node_viewer_nodes(self.node_viewer).all()).contains_only(
            self.node
        )

        node2 = NodeFactory.create()
        make_node_viewer(self.node_viewer, node2)
        assert_that(get_node_viewer_nodes(self.node_viewer).all()).contains_only(
            self.node, node2
        )

        assert_that(get_node_viewer_nodes(self.staff).all()).is_empty()

    def test_has_node_viewer_nodes(self):
        self.assertTrue(has_node_viewer_nodes(self.node_viewer))
        self.assertFalse(has_node_viewer_nodes(self.staff))
        make_node_viewer(self.staff, self.node)
        self.assertTrue(has_node_viewer_nodes(self.staff))

    def test_has_node_viewer_permission(self):
        # only `node_viewer` has node viewer permission for `node`
        self.assertTrue(has_node_viewer_permission(self.node_viewer, self.node))
        self.assertFalse(has_node_viewer_permission(self.staff, self.node))
        # `node_viewer` does NOT have node viewer permission for all nodes
        self.assertFalse(has_node_viewer_permission(self.node_viewer))


class TestNodeAdmin(TestCase):
    def setUp(self):
        self.node = NodeFactory.create()
        self.node_admin = make_node_admin(node=self.node)
        self.staff = UserFactory(staff=True)

    def test_get_node_admin_nodes(self):
        assert_that(get_node_admin_nodes(self.node_admin).all()).contains_only(
            self.node
        )

        node2 = NodeFactory.create()
        make_node_admin(self.node_admin, node2)
        assert_that(get_node_admin_nodes(self.node_admin).all()).contains_only(
            self.node, node2
        )

        assert_that(get_node_admin_nodes(self.staff).all()).is_empty()

    def test_has_node_admin_nodes(self):
        self.assertTrue(has_node_admin_nodes(self.node_admin))
        self.assertFalse(has_node_admin_nodes(self.staff))
        make_node_admin(self.staff, self.node)
        self.assertTrue(has_node_admin_nodes(self.staff))

    def test_has_node_admin_permission(self):
        # only `node_admin` has node admin permission for `node`
        self.assertTrue(has_node_admin_permission(self.node_admin, self.node))
        self.assertFalse(has_node_admin_permission(self.staff, self.node))
        # `node_admin` does NOT have node admin permission for all nodes
        self.assertFalse(has_node_admin_permission(self.node_admin))


@pytest.mark.parametrize(
    "getter, setter",
    ((get_node_admins, assign_node_admin), (get_node_viewers, assign_node_viewer)),
)
def test_get_node_role_users(getter, setter, node_factory, group_factory, user_factory):
    node = node_factory()
    group = group_factory()
    user1, user2, *_ = user_factory.create_batch(4)
    group.user_set.add(user1)
    assert_that(list(getter(node))).is_length(0)
    setter(group, node)
    assert_that(list(getter(node))).is_length(1).starts_with(user1)
    group.user_set.add(user2)
    assert_that(list(getter(node))).is_length(2).contains(user1, user2)


def test_get_node_managers(node_factory, user_factory):
    """Test that all node managers are returned correctly - if any"""

    node = node_factory()
    node_manager_1, node_manager_2 = user_factory.create_batch(2)
    node_admin_group, node_view_group = get_groups_with_perms(node)
    node_manager_group, *_ = get_groups_with_perms(node_admin_group).intersection(
        get_groups_with_perms(node_view_group)
    )

    assert_that(list(get_node_managers(node))).is_length(0)

    node_manager_group.user_set.add(node_manager_1)
    assert_that(list(get_node_managers(node))).is_length(1).starts_with(node_manager_1)

    node_manager_group.user_set.add(node_manager_2)
    assert_that(list(get_node_managers(node))).is_length(2).contains(
        node_manager_1, node_manager_2
    )
