import pytest
from django_drf_utils.tests.models.utils import validate_field

from projects.models.user import Profile, UserNamespace


def test_user_namespace_str():
    assert str(UserNamespace(name="ch")) == "Namespace (ch)"


@pytest.mark.parametrize(
    "test_input, expected",
    (
        (
            ("cnorris", "Chuck", "Norris", "cnorris@roundhouse.gov", None, None),
            ("Chuck Norris (cnorris@roundhouse.gov)", ""),
        ),
        (
            (
                "CNORRIS@roundhouse.gov",
                None,
                None,
                "cnorris@roundhouse.gov",
                "chuck",
                "ch",
            ),
            ("chuck (ID: CNORRIS) (cnorris@roundhouse.gov)", "ch_chuck"),
        ),
        (
            ("CNORRIS@roundhouse.gov", None, None, None, "chuck", None),
            ("chuck (ID: CNORRIS)", "chuck"),
        ),
        (
            ("CNORRIS@roundhouse.gov", None, None, None, "chuck", "ch"),
            ("chuck (ID: CNORRIS)", "ch_chuck"),
        ),
    ),
)
def test_profile_display(profile_factory, user_factory, test_input, expected):
    namespace = None if test_input[5] is None else UserNamespace(name=test_input[5])
    profile = profile_factory.build(
        user=user_factory.build(
            username=test_input[0],
            first_name=test_input[1],
            last_name=test_input[2],
            email=test_input[3],
        ),
        local_username=test_input[4],
        namespace=namespace,
    )
    assert profile.display_name == expected[0]
    assert profile.display_local_username == expected[1]


@pytest.mark.parametrize(
    "test_input, expected",
    (
        # "local_user" cannot start with a number, not with a capital.
        ("1username", False),
        ("Code", False),
        # Valid "local_user" values.
        ("code", True),
        ("cod3", True),
        # Only "_" is allowed as special character.
        ("bad_u$er_name", False),
        ("bad-user-name", False),
        ("bad user name", False),
        ("bad\tuser\tname", False),
        # "local_username" of less <= 64 characters are allowed.
        ("user_name_with_a_length_of_exactly_than_64_characters_true_story", True),
        # "local_username" with > 64 characters are allowed.
        (
            "this_really_is_a_user_name_that_is_longer_than_64_characters_isnt_it",
            False,
        ),
    ),
)
def test_profile_local_username(test_input, expected):
    validate_field(Profile, "local_username", test_input, expected)


def test_profile_str(profile_factory, user_factory):
    assert (
        str(profile_factory.build(user=user_factory.build(username="bob")))
        == "Profile (bob)"
    )
