from assertpy import assert_that
from django.test import TestCase

from projects.models.data_provider import (
    assign_dp_manager,
    get_data_provider_admin_data_providers,
    get_data_provider_coordinators,
    get_data_provider_managers,
    get_data_provider_viewer_data_providers,
    get_data_provider_viewers,
    has_data_provider_admin_data_providers,
    has_data_provider_admin_permission,
    has_data_provider_viewer_data_providers,
    has_data_provider_viewer_permission,
    assign_dp_coordinator,
    has_data_provider_coordinator_permission,
    assign_dp_viewer,
)
from ..factories import (
    UserFactory,
    DataProviderFactory,
    make_data_provider_admin,
    make_data_provider_coordinator,
    make_data_provider_viewer,
)


class TestDataProviderCoordinator(TestCase):
    def setUp(self):
        self.dp = DataProviderFactory()
        self.coordinator = UserFactory()
        make_data_provider_coordinator(self.coordinator, self.dp)

    def test_get_data_provider_coordinator_data_providers(self):
        assert_that(
            get_data_provider_viewer_data_providers(self.coordinator).all()
        ).contains_only(self.dp)

    def test_has_data_provider_coordinator_permission(self):
        assert_that(
            has_data_provider_coordinator_permission(self.coordinator, self.dp)
        ).is_true()


def test_get_data_provider_coordinators(
    data_provider_factory, group_factory, user_factory
):
    dp = data_provider_factory()
    group = group_factory()
    user1, user2, *_ = user_factory.create_batch(4)
    group.user_set.add(user1)
    assign_dp_coordinator(group, dp)
    assert_that(list(get_data_provider_coordinators(dp))).is_length(1).starts_with(
        user1
    )
    group.user_set.add(user2)
    assert_that(list(get_data_provider_coordinators(dp))).is_length(2).contains(
        user1, user2
    )


def test_get_data_provider_coordinators_different_groups(
    data_provider_factory, group_factory, user_factory
):
    dp1, dp2 = data_provider_factory.create_batch(2)
    coordinators_group, viewers_group = group_factory.create_batch(2)
    user = user_factory()
    coordinators_group.user_set.add(user)
    viewers_group.user_set.add(user)
    assign_dp_coordinator(coordinators_group, dp1)
    assign_dp_viewer(viewers_group, dp2)
    assert_that(list(get_data_provider_coordinators(dp1))).is_length(1)
    assert_that(list(get_data_provider_coordinators(dp2))).is_length(0)
    # Coordinators are viewers as well
    assert_that(list(get_data_provider_viewers(dp1))).is_length(1)
    assert_that(list(get_data_provider_viewers(dp2))).is_length(1)


def test_superuser_dp_viewer_not_coordinator(
    data_provider_factory, group_factory, user_factory
):
    dp = data_provider_factory()
    superuser = user_factory(staff=True, is_superuser=True)
    group = group_factory()
    group.user_set.add(superuser)
    assign_dp_viewer(group, dp)
    assert_that(list(get_data_provider_coordinators(dp))).is_length(0)


class TestDataProviderViewer(TestCase):
    def setUp(self):
        self.dp = DataProviderFactory()
        self.viewer = UserFactory()
        make_data_provider_viewer(self.viewer, self.dp)
        self.staff = UserFactory(staff=True)

    def test_get_dp_viewer_dps(self):
        assert_that(
            get_data_provider_viewer_data_providers(self.viewer).all()
        ).contains_only(self.dp)

        dp2 = DataProviderFactory()
        make_data_provider_viewer(self.viewer, dp2)
        assert_that(
            get_data_provider_viewer_data_providers(self.viewer).all()
        ).contains_only(self.dp, dp2)

        assert_that(
            get_data_provider_viewer_data_providers(self.staff).all()
        ).is_empty()

    def test_has_dp_viewer_dps(self):
        assert_that(has_data_provider_viewer_data_providers(self.viewer)).is_true()
        assert_that(has_data_provider_viewer_data_providers(self.staff)).is_false()
        make_data_provider_viewer(self.staff, self.dp)
        assert_that(has_data_provider_viewer_data_providers(self.staff)).is_true()

    def test_has_dp_viewer_permission(self):
        # only `dataprovider_viewer` has data provider viewer permission for `dp`
        assert_that(has_data_provider_viewer_permission(self.viewer, self.dp)).is_true()
        assert_that(has_data_provider_viewer_permission(self.staff, self.dp)).is_false()
        # `dataprovider_viewer` does NOT have data provider viewer permission
        # for all data providers
        assert_that(has_data_provider_viewer_permission(self.viewer)).is_false()


def test_get_data_provider_viewers(data_provider_factory, user_factory):
    dp = data_provider_factory()
    user1, *_ = user_factory.create_batch(3)
    make_data_provider_viewer(user1, dp)
    assert_that(get_data_provider_viewers(dp)).is_length(1).starts_with(user1)


class TestDataProviderAdmin(TestCase):
    def setUp(self):
        self.dp = DataProviderFactory()
        self.dp_admin = UserFactory()
        make_data_provider_admin(self.dp_admin, self.dp)
        self.staff = UserFactory(staff=True)

    def test_get_dp_admin_dps(self):
        assert_that(
            get_data_provider_admin_data_providers(self.dp_admin).all()
        ).contains_only(self.dp)

        dp2 = DataProviderFactory()
        make_data_provider_admin(self.dp_admin, dp2)
        assert_that(
            get_data_provider_admin_data_providers(self.dp_admin).all()
        ).contains_only(self.dp, dp2)

        assert_that(get_data_provider_admin_data_providers(self.staff).all()).is_empty()

    def test_has_dp_admin_dps(self):
        assert_that(has_data_provider_admin_data_providers(self.dp_admin)).is_true()
        assert_that(has_data_provider_admin_data_providers(self.staff)).is_false()
        make_data_provider_admin(self.staff, self.dp)
        assert_that(has_data_provider_admin_data_providers(self.staff)).is_true()

    def test_has_dp_admin_permission(self):
        # only `dataprovider_admin` has data provider admin permission for `dp`
        assert_that(
            has_data_provider_admin_permission(self.dp_admin, self.dp)
        ).is_true()
        assert_that(has_data_provider_admin_permission(self.staff, self.dp)).is_false()
        # `dataprovider_admin` does NOT have data provider admin permission
        # for all data providers
        assert_that(has_data_provider_admin_permission(self.dp_admin)).is_false()


def test_get_data_provider_managers(data_provider_factory, user_factory, group_factory):
    dp = data_provider_factory()
    manager_group, g1, g2 = group_factory.create_batch(3)
    user, *_ = user_factory.create_batch(3)
    assign_dp_manager(manager_group, dp, g1, g2)
    manager_group.user_set.add(user)
    assert list(get_data_provider_managers(dp)) == [user]
