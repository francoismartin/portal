import pytest
from django_drf_utils.tests.models.utils import validate_field

from projects.models.flag import Flag


@pytest.mark.parametrize(
    "field_name, test_input, expected",
    (
        ("code", "fhnw", True),
        ("code", "FHNW", False),
        ("code", "", False),
        ("description", "", True),
        ("description", None, True),
        ("description", "Lorem ipsum ...", True),
    ),
)
@pytest.mark.django_db
def test_validation_user_flag(field_name, test_input, expected):
    validate_field(Flag, field_name, test_input, expected)
