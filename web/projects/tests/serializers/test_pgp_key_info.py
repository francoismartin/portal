from unittest.mock import DEFAULT, patch
import pytest

from django.contrib.auth import get_user_model

from projects.serializers import pgp
from projects.models.pgp import PgpKeyInfo
from projects.serializers.pgp import PgpKeyInfoSerializer
from projects.utils.pgp import KeyMetadata, KeyStatus


User = get_user_model()


def test_notify_new_pgp_key_approval_request(user_factory) -> None:
    """Test that an email is sent when a new PgpKeyInfo is created"""

    with patch.multiple(
        pgp,
        sendmail=DEFAULT,
        get_request_username=lambda _: user_factory(),
        retrieve_key_metadata=lambda _: KeyMetadata(
            status=KeyStatus.VERIFIED,
            user_id="Chuck Norris",
            email="chuck.norris@roundhouse.kick",
        ),
    ) as mocks:
        PgpKeyInfoSerializer().create({"fingerprint": "0123456789ABCDEF"})

        assert mocks["sendmail"].called


@pytest.mark.parametrize(
    ("old_status", "new_status", "should_send_email"),
    (
        (PgpKeyInfo.Status.PENDING, PgpKeyInfo.Status.APPROVED, True),
        (PgpKeyInfo.Status.PENDING, PgpKeyInfo.Status.REJECTED, True),
        (PgpKeyInfo.Status.PENDING, PgpKeyInfo.Status.DELETED, False),
    ),
)
def test_notify_user_for_approval_or_rejection(
    old_status: PgpKeyInfo.Status,
    new_status: PgpKeyInfo.Status,
    should_send_email: bool,
    pgp_key_info_factory,
) -> None:
    """Test that an email is sent when a PgpKeyInfo is approved or rejected"""

    pgp_key_info: PgpKeyInfo = pgp_key_info_factory(status=old_status)

    with patch.multiple(pgp, sendmail=DEFAULT, retrieve_key_metadata=DEFAULT) as mocks:
        PgpKeyInfoSerializer().update(pgp_key_info, {"status": new_status})

        # Email should be sent upon update only if the request has been approved
        # or rejected
        assert not should_send_email ^ mocks["sendmail"].called
