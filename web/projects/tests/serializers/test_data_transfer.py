from assertpy import assert_that
from django_drf_utils.config import Email

from projects.models.data_transfer import DataTransfer
from projects.models.message import Message
from projects.models.project import ProjectRole
from projects.notifications.data_transfer import (
    create_data_transfer_creation_notification,
)
from projects.serializers.data_transfer import DataTransferSerializer
from ..factories import make_data_provider_coordinator, make_node_admin


def test_notify_on_creation(
    data_transfer_factory, project_user_role_factory, user_factory, settings, mailoutbox
):
    ticketing_system_email = "chuck@example.org"
    settings.CONFIG.notification.ticket_mail = ticketing_system_email
    settings.CONFIG.email = Email.empty()
    dtr: DataTransfer = data_transfer_factory()
    node_destination = dtr.project.destination
    node_transit = dtr.data_provider.node
    project_user_role_factory(project=dtr.project, role=ProjectRole.PL.value)
    (
        coordinator,
        na1_destination,
        na2_destination,
        na_transfer,
    ) = user_factory.create_batch(4)
    make_data_provider_coordinator(coordinator, dtr.data_provider)
    make_node_admin(na1_destination, node_destination)
    make_node_admin(na2_destination, node_destination)
    make_node_admin(na_transfer, node_transit)

    url = "https://portal.test"
    DataTransferSerializer.create_approvals(dtr)
    DataTransferSerializer.notify_on_creation(dtr, url)

    subject, body = create_data_transfer_creation_notification(dtr)
    # Check if messages are generated
    messages = Message.objects.all()
    assert len(messages) == 5
    assert {m.user for m in messages} == {
        coordinator,
        na1_destination,
        na2_destination,
        na_transfer,
        dtr.requestor,
    }
    # Check if all messages are identical
    assert_that({m.title for m in messages}).contains_only(subject)
    assert_that({m.body for m in messages}).is_length(1)
    assert_that(messages[0].body).starts_with(body.replace("\n", "\r\n")).ends_with(
        f"[Go to the DTR](data-transfers/{dtr.id})"
    )
    # Check if emails are being sent
    assert len(mailoutbox) == 1
    m = mailoutbox[0]
    assert m.subject == subject
    assert_that(m.body).starts_with(body).contains(f"{url}/data-transfers/{dtr.id}")
    expected_recipients = {
        u.email
        for u in (
            coordinator,
            na1_destination,
            na2_destination,
            na_transfer,
            dtr.requestor,
        )
    } | {
        node_destination.ticketing_system_email,
        node_transit.ticketing_system_email,
        ticketing_system_email,
    }
    assert set(m.to) == expected_recipients
