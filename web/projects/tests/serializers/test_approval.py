from unittest.mock import patch

import pytest

from projects.models.data_transfer import DataTransfer
from projects.notifications import dtr_notification
from projects.serializers.approval import ApprovalSerializer
from ..factories import (
    DataProviderApprovalFactory,
    NodeApprovalFactory,
)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "approval_factory, status",
    [
        (NodeApprovalFactory, DataTransfer.AUTHORIZED),
        (NodeApprovalFactory, DataTransfer.INITIAL),
        (NodeApprovalFactory, DataTransfer.UNAUTHORIZED),
        (DataProviderApprovalFactory, DataTransfer.AUTHORIZED),
        (DataProviderApprovalFactory, DataTransfer.INITIAL),
        (DataProviderApprovalFactory, DataTransfer.UNAUTHORIZED),
    ],
)
def test_notification_sent_on_approval(
    approval_factory, status, user_factory, data_transfer_factory
):
    """Test that email notification is correctly sent upon approval"""

    dtr_approval = approval_factory(data_transfer=data_transfer_factory(status=status))

    approved = status in [DataTransfer.AUTHORIZED, DataTransfer.INITIAL]
    authorized = status == DataTransfer.AUTHORIZED
    user = user_factory()

    with patch.object(
        dtr_notification,
        "create_final_approval_notification",
        return_value=("subject", "body"),
    ) as mock:
        ApprovalSerializer.notify_on_update(
            dtr_approval,
            "https://example.ch",
            user,
            approved,
        )

        # Test that final email notification is sent if and only if DTRs is authorized
        if authorized:
            mock.assert_called_once()
        else:
            mock.assert_not_called()
