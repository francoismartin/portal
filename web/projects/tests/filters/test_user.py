import pytest
from assertpy import assert_that

from django.contrib.auth import get_user_model

from projects.filters.user import UserLookupFilter

User = get_user_model()


@pytest.mark.django_db
@pytest.mark.parametrize(
    "query_params, expected_length",
    [
        [{}, 7],
        [{"first_name": "Albus"}, 2],
        [{"first_name": "albus"}, 2],
        [{"first_name": "Hermione"}, 1],
        [{"first_name": "Viktor"}, 0],
        [{"last_name": "Weasley"}, 2],
        [{"last_name": "Weasley"}, 2],
        [{"last_name": "Granger"}, 1],
        [{"last_name": "Krum"}, 0],
        [{"first_name": "Harry", "last_name": "Potter"}, 1],
        [{"first_name": "John", "last_name": "Potter"}, 0],
        [{"first_name": "Harry", "last_name": "Houdini"}, 0],
        [{"affiliation": "staff@hogwarts.uk"}, 1],
        [{"affiliation": "hogwarts"}, 5],
        [{"affiliation": "Hogwarts"}, 5],
        [{"affiliation": "durmstrang"}, 0],
    ],
)
def test_user_lookup_filter(user_factory, query_params, expected_length, rf):
    for first_name, last_name, affiliation in (
        ("Harry", "Potter", "member@hogwarts.uk"),
        ("Albus", "Dumbledore", "member@hogwarts.uk,staff@hogwarts.uk"),
        ("Ron", "Weasley", "member@hogwarts.uk"),
        ("Ginny", "Weasley", "member@hogwarts.uk"),
        ("Hermione", "Granger", "member@hogwarts.uk"),
        ("Albus", "Potter", ""),
    ):
        user_factory.create(
            first_name=first_name,
            last_name=last_name,
            profile__affiliation=affiliation,
        )

    request = rf.get("/")
    request.query_params = query_params
    queryset = User.objects.all()
    q = UserLookupFilter().filter_queryset(request, queryset, None)

    assert_that(q).is_length(expected_length)
