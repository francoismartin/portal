import pytest
from assertpy import assert_that

from projects.filters.project import ProjectsLookupFilter
from projects.models.node import Node
from projects.models.project import Project


@pytest.mark.django_db
@pytest.mark.parametrize(
    "query_params, expected_length",
    [
        [{}, 3],
        [{"name": "Dark Arts"}, 1],
        [{"name": "dark arts"}, 1],
        [{"code": "da"}, 1],
        [{"destination": "hogwarts"}, 2],
        [{"destination": "durmstrang"}, 1],
        [{"name": "mathematics"}, 0],
        [{"code": "mt"}, 0],
        [{"destination": "unibas"}, 0],
    ],
)
def test_project_lookup_filter(project_factory, query_params, expected_length, rf):
    for name, code, dest in (
        ("Defence Against the Dark Arts", "dada", "hogwarts"),
        ("Dark Arts", "da", "durmstrang"),
        ("Transfiguration", "tr", "hogwarts"),
    ):
        project_factory.create(
            name=name,
            code=code,
            destination=Node.objects.get_or_create(code=dest, name=dest)[0],
        )

    request = rf.get("/")
    request.query_params = query_params
    queryset = Project.objects.all()
    q = ProjectsLookupFilter().filter_queryset(request, queryset, None)

    assert_that(q).is_length(expected_length)
