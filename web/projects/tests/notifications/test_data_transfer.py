from assertpy import assert_that

from django.conf import settings

from projects.notifications.data_transfer import (
    create_data_transfer_creation_notification,
)
from projects.models.data_transfer import DataTransfer
from projects.models.project import ProjectRole
from projects.serializers.data_transfer import DataTransferSerializer

from ..factories import make_data_provider_coordinator


def test_data_transfer_creation_notification(
    data_transfer_factory, project_user_role_factory, user_factory
):
    dtr: DataTransfer = data_transfer_factory(purpose=DataTransfer.PRODUCTION)
    pl = project_user_role_factory(project=dtr.project, role=ProjectRole.PL.value).user
    coordinator = user_factory()
    make_data_provider_coordinator(coordinator, dtr.data_provider)
    DataTransferSerializer.create_approvals(dtr)
    subject, body = create_data_transfer_creation_notification(dtr)
    assert_that(subject).is_equal_to(
        f"[DTR-{dtr.id}] - Data Transfer Request from "
        f"'{dtr.data_provider.name}' to '{dtr.project.name}' project"
    )
    assert_that(body).contains(
        f"Project Lead: {pl.first_name} {pl.last_name}",
        (
            f"Data Provider: {dtr.data_provider.name}, "
            f"{coordinator.first_name} {coordinator.last_name} ({coordinator.email})"
        ),
        "One time",
        "real patient data: Yes",
        f"There is an existing legal basis for the Data Transfer ('{dtr.legal_basis}')",
        settings.CONFIG.notification.ticket_mail,  # pylint: disable=no-member
    ).does_not_contain("Recurring")

    dtr.purpose = DataTransfer.TEST
    dtr.max_packages = -1
    dtr.save()
    assert_that(create_data_transfer_creation_notification(dtr)[1]).contains(
        "Recurring",
        "real patient data: No",
    ).does_not_contain("legal basis")
