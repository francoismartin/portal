from django.conf import settings
import pytest
from guardian.shortcuts import get_groups_with_perms

from projects.models.approval import BaseApproval
from projects.models.const import PERM_NODE_ADMIN
from projects.models.data_provider import assign_dp_coordinator
from projects.models.data_transfer import DataTransfer
from projects.notifications.dtr_notification import (
    DtrApprovedNotification,
    DtrAuthorizedNotification,
    DtrCreatedNotification,
    DtrRejectedNotification,
)


@pytest.mark.parametrize(
    "approval_status, dtr_status, DtrNotificationClass",
    [
        (
            BaseApproval.ApprovalStatus.WAITING,
            DataTransfer.INITIAL,
            DtrCreatedNotification,
        ),
        (
            BaseApproval.ApprovalStatus.APPROVED,
            DataTransfer.INITIAL,
            DtrApprovedNotification,
        ),
        (
            BaseApproval.ApprovalStatus.APPROVED,
            DataTransfer.AUTHORIZED,
            DtrAuthorizedNotification,
        ),
        (
            BaseApproval.ApprovalStatus.REJECTED,
            DataTransfer.UNAUTHORIZED,
            DtrRejectedNotification,
        ),
    ],
)
def test_email_recipients(
    approval_status,
    dtr_status,
    DtrNotificationClass,
    user_factory,
    node_factory,
    data_transfer_factory,
    project_factory,
    data_provider_factory,
    group_factory,
    node_approval_factory,
):
    """Test that the list of email recipients is correct"""

    local, remote = nodes = node_factory.create_batch(2)
    approver, *_ = node_admins = user_factory.create_batch(2)
    for node, node_admin in zip(nodes, node_admins):
        groups = get_groups_with_perms(node, attach_perms=True)
        for group, permission in groups.items():
            if permission == [PERM_NODE_ADMIN]:
                group.user_set.add(node_admin)

    data_provider = data_provider_factory(node=local)
    dp_coordinator = user_factory()
    data_provider_coordinator_group = group_factory()
    assign_dp_coordinator(data_provider_coordinator_group, data_provider)
    data_provider_coordinator_group.user_set.add(dp_coordinator)

    project = project_factory(destination=remote)

    requestor = user_factory()
    dtr = data_transfer_factory(
        project=project,
        data_provider=data_provider,
        requestor=requestor,
        status=dtr_status,
    )

    approval, *_ = (
        node_approval_factory(node=node, data_transfer=dtr, status=approval_status)
        for node in nodes
    )

    default_recipients = set(node_admins + [dp_coordinator, requestor])

    approved = approval_status == BaseApproval.ApprovalStatus.APPROVED
    authorized = dtr_status == DataTransfer.AUTHORIZED
    partially_approved = approved and not authorized

    dtr_notification = (
        DtrNotificationClass(
            "http://example.ch",
            approval,
            approver,
        )
        if DtrNotificationClass is DtrApprovedNotification
        or DtrNotificationClass is DtrRejectedNotification
        else DtrNotificationClass(dtr, "http://example.ch")
    )
    recipients = dtr_notification.email_recipients()

    # Test that all default recipients are included
    assert all(list(user.email in recipients for user in default_recipients))

    # Test that DCC ticketing system is included except for partial approvals
    # pylint: disable=no-member
    assert (settings.CONFIG.notification.ticket_mail in recipients) ^ partially_approved

    # Test that nodes ticketing systems are NOT included if the DTR is approved but not yet authorized
    assert all(
        (node.ticketing_system_email in recipients) ^ partially_approved
        for node in nodes
    )
