from random import sample
import pytest
from assertpy import assert_that

from django.conf import settings
from django.contrib.auth import get_user_model

from projects.notifications.project_user_notification import (
    pretty_print_permission_changes,
    project_user_notification,
    review_user_roles_notification,
)
from projects.models.project import ProjectRole

User = get_user_model()
PROJECT_NAME = "Doghouse"
TEST_PROJECT_NAME = f"Project name: {PROJECT_NAME}\n"


@pytest.fixture(name="permissions_list")
def permissions_list_fixture(user_factory):
    ww = user_factory(
        first_name="Walter",
        last_name="White",
        email="walter.white@doghouse.com",
        profile__affiliation="lab1",
    )
    jp = user_factory(
        first_name="Jesse",
        last_name="Pinkman",
        email="jesse.pinkman@doghouse.com",
        profile__affiliation="lab2",
    )
    gb = user_factory(
        first_name="Gale",
        last_name="Boetticher",
        email="gale.boetticher@doghouse.com",
        profile__affiliation="",
    )
    return [
        (ww, ProjectRole.PL),
        (jp, ProjectRole.DM),
        (jp, ProjectRole.USER),
        (gb, ProjectRole.USER),
    ]


pretty_printed_permission_changes = (
    "User: Walter White (walter.white@doghouse.com) (lab1)\tPermissions: Project Leader",
    "User: Jesse Pinkman (jesse.pinkman@doghouse.com) (lab2)\tPermissions: Data Manager, User",
    "User: Gale Boetticher (gale.boetticher@doghouse.com) (no affiliation)\tPermissions: User",
)


def test_pretty_print_permission_changes(permissions_list):
    lines = pretty_printed_permission_changes
    assert pretty_print_permission_changes(permissions_list) == "\n".join(lines)


def test_remove_project_user_notification(permissions_list):
    lines = (
        TEST_PROJECT_NAME,
        "Removed users:",
        *pretty_printed_permission_changes,
    )
    assert project_user_notification(PROJECT_NAME, [], permissions_list) == "\n".join(
        lines
    )


def test_add_project_user_notification(permissions_list):
    lines = (
        TEST_PROJECT_NAME,
        "Added users:",
        *pretty_printed_permission_changes,
    )
    assert project_user_notification(PROJECT_NAME, permissions_list, []) == "\n".join(
        lines
    )


def test_project_user_notification(permissions_list):
    lines = (
        TEST_PROJECT_NAME,
        "Added users:",
        *pretty_printed_permission_changes,
        "\n\nRemoved users:",
        *pretty_printed_permission_changes,
    )
    assert project_user_notification(
        PROJECT_NAME, permissions_list, permissions_list
    ) == "\n".join(lines)


def test_review_user_roles_body(permissions_list):
    subject, body = review_user_roles_notification(
        PROJECT_NAME, sample(permissions_list, len(permissions_list))
    )

    assert subject == f"Periodic review of user roles for the project {PROJECT_NAME}"
    # pylint: disable=no-member
    assert_that(body).contains(
        PROJECT_NAME,
        settings.CONFIG.notification.ticket_mail,
        ("Project Leader\nWalter White (walter.white@doghouse.com)"),
        ("Data Manager\nJesse Pinkman (jesse.pinkman@doghouse.com)"),
        (
            "User\n"
            "Gale Boetticher (gale.boetticher@doghouse.com)\n"
            "Jesse Pinkman (jesse.pinkman@doghouse.com)"
        ),
    )
