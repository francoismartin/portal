from assertpy import assert_that
import pytest

from django.contrib.auth import get_user_model

from projects.notifications.pgp_key_info import (
    create_pgp_key_info_new_approval_request_notification,
    create_pgp_key_info_approved_or_rejected_notification,
)
from projects.models.pgp import PgpKeyInfo

User = get_user_model()


def test_create_pgp_key_info_new_approval_request_notification(
    pgp_key_info_factory,
) -> None:
    """Test that subject and body are generated correctly"""

    pgp_key_info = pgp_key_info_factory()
    subject, body = create_pgp_key_info_new_approval_request_notification(pgp_key_info)

    assert_that(subject).contains(pgp_key_info.user.profile.display_name)
    assert_that(body).contains(
        pgp_key_info.user.profile.display_name,
        pgp_key_info.key_user_id,
        pgp_key_info.key_email,
        pgp_key_info.fingerprint,
    )


@pytest.mark.parametrize(
    "status, expected, should_raise_exception",
    (
        (PgpKeyInfo.Status.APPROVED, "approved", False),
        (PgpKeyInfo.Status.REJECTED, "rejected", False),
        (PgpKeyInfo.Status.PENDING, "rejected", True),
    ),
)
def test_create_pgp_key_info_approved_or_rejected_notification(
    status: PgpKeyInfo.Status,
    expected: str,
    should_raise_exception: bool,
    pgp_key_info_factory,
) -> None:
    """Test that subject and body are generated correctly"""

    pgp_key_info = pgp_key_info_factory(status=status)

    try:
        subject, body = create_pgp_key_info_approved_or_rejected_notification(
            pgp_key_info
        )

        assert_that(subject).contains(expected)
        assert_that(body).contains(
            expected,
            pgp_key_info.key_user_id,
            pgp_key_info.key_email,
            pgp_key_info.fingerprint,
        )
    except ValueError:
        assert should_raise_exception
