from unittest import mock
from assertpy import assert_that
from django.conf import settings
import pytest

from projects.notifications import approval
from projects.notifications.approval import (
    create_reject_notification,
    create_approve_notification,
    create_final_approval_notification,
)
from projects.models.project import ProjectRole
from projects.models.approval import DataProviderApproval, NodeApproval
from projects.models.data_transfer import DataTransfer

from ..factories import (
    make_project_user,
    make_data_provider_coordinator,
)


@pytest.mark.parametrize(
    "node_approval_type, dtr_purpose, project_space_ready, legal_basis",
    (
        (
            NodeApproval.Type.HOSTING,
            DataTransfer.PRODUCTION,
            True,
            True,
        ),
        (
            NodeApproval.Type.TRANSFER,
            DataTransfer.TEST,
            False,
            False,
        ),
    ),
)
def test_create_approve_notification(
    node_approval_type,
    dtr_purpose,
    project_space_ready,
    legal_basis,
    data_transfer_factory,
    data_provider_approval_factory,
    node_approval_factory,
    user_factory,
):
    dtr = data_transfer_factory(purpose=dtr_purpose)
    data_provider_approval = data_provider_approval_factory(data_transfer=dtr)
    node_approval = node_approval_factory(data_transfer=dtr, type=node_approval_type)
    coordinator = user_factory()
    make_data_provider_coordinator(coordinator, data_provider_approval.data_provider)
    legal_basis_text = "There is a legal basis for the Data Transfer:"
    project_space_ready_text = (
        "A project space in the BioMedIT environment has been setup: Yes"
    )

    with mock.patch.object(approval, "project_metadata") as mock_method:
        dp_subject, dp_body = create_approve_notification(
            data_provider_approval, coordinator
        )
        node_subject, node_body = create_approve_notification(
            node_approval, coordinator
        )

        # project_metadata is always called
        assert mock_method.call_count == 2

    for subject in (dp_subject, node_subject):
        # Subject is always the same
        assert_that(subject).starts_with(
            f"[DTR-{data_provider_approval.data_transfer.id}] - Data Transfer Request "
            f"from '{data_provider_approval.data_provider.name}'"
        )

    for body, institution in zip(
        (dp_body, node_body), (data_provider_approval.data_provider, node_approval.node)
    ):
        # Body always contains the institution
        assert_that(body).contains(str(institution))

        # Body always contains the user
        assert_that(body).contains(str(coordinator))

        # Legal basis is Yes only if purpose is PRODUCTION
        assert_that(body).contains(
            f"{legal_basis_text} Yes" if legal_basis else f"{legal_basis_text} No"
        )

    # Project space ready is present only for the hosting node
    assert_that(dp_body).does_not_contain(project_space_ready_text)
    if project_space_ready:
        assert_that(node_body).contains(project_space_ready_text)
    else:
        assert_that(node_body).does_not_contain(project_space_ready_text)


def test_create_reject_notification(node_approval_factory, user_factory):
    node_approval = node_approval_factory()
    user = user_factory()
    subject, body = create_reject_notification(node_approval, user)
    assert_that(subject).starts_with(
        f"[DTR-{node_approval.data_transfer.id}] - Data Transfer Request "
        f"from '{node_approval.data_transfer.data_provider.name}'"
    )
    assert_that(body).contains(
        f"Please be informed that the DTR 1 has been rejected by {user}."
    )


def test_create_final_approval_notification(
    data_transfer_factory,
    data_provider_factory,
    project_factory,
    node_factory,
    user_factory,
):
    data_provider = data_provider_factory()
    project = project_factory()
    data_manager = make_project_user(
        project,
        ProjectRole.DM,
    )
    dtr = data_transfer_factory(data_provider=data_provider, project=project)

    # NOTE: we can't use approval factories here, they don't produce
    # historical records
    node = node_factory()
    user1, user2 = user_factory.create_batch(2)
    na = NodeApproval.objects.create(
        data_transfer=dtr,
        node=node,
        type=NodeApproval.Type.HOSTING,
        status=NodeApproval.ApprovalStatus.APPROVED,
    )
    na_history_latest = na.history.latest()
    na_history_latest.history_user = user1
    na_history_latest.save()
    dpa = DataProviderApproval.objects.create(
        data_transfer=dtr,
        data_provider=data_provider,
        status=DataProviderApproval.ApprovalStatus.APPROVED,
    )
    dpa_history_latest = dpa.history.latest()
    dpa_history_latest.history_user = user2
    dpa_history_latest.save()

    subject, body = create_final_approval_notification(dtr)

    assert (
        subject
        == f"[DTR-{dtr.id}] - Approval of Data Transfer Request from {data_provider.name} to {project.name} Project"
    )
    assert_that(body).contains(
        f"DTR {dtr.id}",
        f"project {project.name} ({project.code})",
        f"{data_provider.name}",
        f"{data_provider.node.name}",
        f"{data_manager.profile.display_name}",
        f"{settings.CONFIG.notification.ticket_mail}",  # pylint: disable=no-member
        f"{node.name} by {user1.profile.display_name} at {na.change_date:%Y-%m-%d %H:%M:%S}",
        f"{data_provider.name} by {user2.profile.display_name} at {dpa.change_date:%Y-%m-%d %H:%M:%S}",
    )
