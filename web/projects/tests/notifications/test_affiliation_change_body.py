import pytest
from django.contrib.auth import get_user_model

from projects.notifications import affiliation_change_body

User = get_user_model()

OLD_AFFILIATION = "member@fhnw.ch,staff@fhnw.ch"
NEW_AFFILIATION = "member@chuv.ch,staff@chuv.ch"

FLAG_1 = "myflag1"
FLAG_2 = "myflag2"


def get_expected_body(flags: str = ""):
    def format_affiliation(affiliations: str):
        return affiliations.replace(",", ", ")

    return (
        "Display name: Peter Müller (ID: 102715327534) (peter@mueller.ch)\n"
        f"Old affiliation: {format_affiliation(OLD_AFFILIATION)}\n"
        f"New affiliation: {format_affiliation(NEW_AFFILIATION)}\n"
        f"Flags: {flags}"
    )


@pytest.fixture(name="user_without_flags")
def user_without_flags_fixture(user_factory):
    return user_factory(
        first_name="Peter",
        last_name="Müller",
        username="102715327534@eduid.ch",
        email="peter@mueller.ch",
    )


@pytest.fixture(name="user_with_flag")
def user_with_flag_fixture(user_without_flags, flag_factory):
    user_without_flags.flags.add(flag_factory(code=FLAG_1))
    return user_without_flags


@pytest.fixture(name="user_with_flags")
def user_with_flags_fixture(user_with_flag, flag_factory):
    user_with_flag.flags.add(flag_factory(code=FLAG_2))
    return user_with_flag


def test_affiliation_change_body_without_flags(user_without_flags):
    assert (
        affiliation_change_body(
            user_without_flags,
            OLD_AFFILIATION,
            NEW_AFFILIATION,
        )
        == get_expected_body()
    )


def test_affiliation_change_body_with_flag(user_with_flag):
    assert affiliation_change_body(
        user_with_flag,
        OLD_AFFILIATION,
        NEW_AFFILIATION,
    ) == get_expected_body(FLAG_1)


def test_affiliation_change_body_with_flags(user_with_flags):
    assert affiliation_change_body(
        user_with_flags,
        OLD_AFFILIATION,
        NEW_AFFILIATION,
    ) == get_expected_body(f"{FLAG_1}, {FLAG_2}")
