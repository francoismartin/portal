from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser, Group
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from guardian.mixins import GuardianUserMixin
from simple_history.models import HistoricalRecords


class User(AbstractUser, GuardianUserMixin):
    email = models.EmailField(
        blank=True, max_length=254, unique=True, verbose_name="email address"
    )
    history = HistoricalRecords()

    def __str__(self) -> str:
        return f"{self.first_name} {self.last_name} ({self.username})"


def get_anonymous_user_instance(UserModel):
    return UserModel(username="AnonymousUser", email="anonymous_user@localhost")


class AuthenticationLog(models.Model):
    class Action(models.TextChoices):
        LOGIN = "I"
        LOGOUT = "O"
        FAILED = "F"

    timestamp = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(
        get_user_model(), on_delete=models.SET_NULL, null=True, related_name="+"
    )
    username = models.CharField(
        max_length=150,  # the same value as in `AbstractUser`
        validators=(UnicodeUsernameValidator(),),
        null=False,
        blank=True,
    )
    sender = models.CharField(max_length=64, null=False, blank=False)
    action = models.CharField(max_length=1, choices=Action.choices)

    def __str__(self) -> str:
        return f"{self.Action(self.action).label}({self.timestamp}): {self.username}"


class GroupProfile(models.Model):
    description = models.TextField(
        null=True,
        blank=True,
        help_text="Group description",
    )
    group = models.OneToOneField(
        Group,
        related_name="profile",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"GroupProfile ({self.group})"
