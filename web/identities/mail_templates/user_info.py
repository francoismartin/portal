from itertools import groupby

from django.contrib.auth import get_user_model

from projects.models.project import ProjectUserRole, ProjectRole

User = get_user_model()


def user_info_body(user: User) -> str:
    body = [
        f"User ID: {user.id}",
        f"Display Name: {user.profile.display_name}",
        f"Last Login: {user.last_login}",
        f"Affiliation: {user.profile.affiliation}",
        f"LinkedAffiliationUniqueID: {user.profile.affiliation_id}",
        f"Flags: {', '.join([flag.code for flag in user.flags.all()]) or '-'}",
        "",
        "Projects and Roles:",
    ]

    purs = ProjectUserRole.objects.filter(user=user).order_by("project__code")
    for code, pur in groupby(purs, lambda pur: pur.project.code):
        body += (f"{code}: {', '.join([ProjectRole(p.role).name for p in pur])}",)

    return "\n".join(body)
