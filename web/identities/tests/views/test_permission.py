import pytest
from assertpy import assert_that
from django.contrib.auth.models import Permission
from django.urls import reverse
from rest_framework import status

from identities.apps import APP_NAME
from identities.models import User
from projects.models.project import Project


class TestPermission:
    view_name = f"{APP_NAME}:permission"
    url_list = reverse(f"{view_name}-list")

    def test_anonymous(self, client):
        assert client.get(self.url_list).status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.usefixtures("basic_user_auth")
    def test_authenticated(self, client):
        assert client.get(self.url_list).status_code == status.HTTP_403_FORBIDDEN

    def test_get_all_permissions(self, admin_client):
        assert admin_client.get(self.url_list).status_code == status.HTTP_200_OK

    def test_get_objects_for_permission(self, admin_client):
        perm_obj = Permission.objects.get(codename="change_user")
        url = reverse(f"{self.view_name}-objects", kwargs={"pk": perm_obj.id})
        response = admin_client.get(url)
        assert response.status_code == status.HTTP_200_OK
        users = [{"id": x.id, "name": str(x)} for x in User.objects.all()]
        assert_that(response.json()).is_length(len(users)).contains(*users)


class TestObjectByPermission:
    url_list = reverse(f"{APP_NAME}:object_by_permission-list")

    def test_anonymous(self, client):
        assert client.get(self.url_list).status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.usefixtures("basic_user_auth")
    def test_authenticated(self, client):
        assert client.get(self.url_list).status_code == status.HTTP_403_FORBIDDEN

    def test_get_empty(self, admin_client):
        response = admin_client.get(self.url_list)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {}

    def test_get_objects_by_permission(self, admin_client, project_factory):
        perm_change_node = Permission.objects.get(codename="add_node")
        perm_change_project = Permission.objects.get(codename="change_project")
        perm_change_user = Permission.objects.get(codename="change_user")
        # Non-existing permissions
        response = admin_client.get(self.url_list, {"perm_id": 1_000_000})
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {}
        # Existing permissions (without associated objects)
        response = admin_client.get(
            self.url_list, {"perm_id": [perm_change_node.id, perm_change_project.id]}
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            str(perm_change_node.id): [],
            str(perm_change_project.id): [],
        }
        # Existing permsissions (with associated objects)
        project_factory.create_batch(5)
        response = admin_client.get(
            self.url_list,
            {
                "perm_id": [
                    perm_change_user.id,
                    perm_change_project.id,
                ]
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert sorted(response.json().keys()) == sorted(
            map(str, [perm_change_user.id, perm_change_project.id])
        )
        assert_that(response.json()[str(perm_change_user.id)]).contains_only(
            *[{"id": x.id, "name": str(x)} for x in User.objects.all()]
        )
        assert_that(response.json()[str(perm_change_project.id)]).contains_only(
            *[{"id": x.id, "name": str(x)} for x in Project.objects.all()]
        )
