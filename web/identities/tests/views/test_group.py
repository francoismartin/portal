import logging

import pytest
from assertpy import assert_that
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from guardian.models import GroupObjectPermission
from rest_framework import status

from identities.apps import APP_NAME
from identities.serializers.group import VALIDATION_ERROR_NON_STAFF_FIELDS
from identities.tests import APPLICATION_JSON
from projects.models.const import PERM_NODE_ADMIN
from projects.models.node import Node
from projects.models.project import Project
from projects.tests.factories import (
    NodeFactory,
    UserFactory,
    USER_PASSWORD,
    make_group_manager,
)

VIEW_NAME = f"{APP_NAME}:group"
URL_LIST = reverse(f"{VIEW_NAME}-list")
GROUP_NAME = "New Group"


def test_get_groups_anonymous(client):
    assert client.get(URL_LIST).status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.usefixtures("basic_user_auth")
def test_get_groups_authenticated(client):
    assert client.get(URL_LIST).status_code == status.HTTP_403_FORBIDDEN


def test_get_groups_permissions(admin_client, group_factory):
    response = admin_client.get(URL_LIST)
    assert response.status_code == status.HTTP_200_OK
    assert_that(response.json()).is_empty()

    group_factory.create()
    response = admin_client.get(URL_LIST)
    assert response.status_code == status.HTTP_200_OK
    assert_that(response.json()).is_length(1)


def test_get_groups_by_permission(admin_client, client, group_factory):
    managed_group_1, _ = group_factory.create_batch(2)

    # staff can see all groups
    response = admin_client.get(URL_LIST)
    assert response.status_code == status.HTTP_200_OK
    assert_that(response.json()).is_length(2)

    # group manager can only see their managed groups
    group_manager = make_group_manager(managed_group_1)
    client.login(username=group_manager.username, password=USER_PASSWORD)
    response = client.get(URL_LIST)
    assert response.status_code == status.HTTP_200_OK
    assert_that(response.json()).is_length(1).extracting("name").is_equal_to(
        [managed_group_1.name]
    )
    client.logout()


def test_get_groups_sorted(admin_client, group_factory):
    group_factory(name="Test Node 1 Node Admin")
    group_factory(name="dataprovider 1 DP Manager")
    group_factory(name="dataprovider 1 DP Coordinator")
    response = admin_client.get(URL_LIST)
    assert response.status_code == status.HTTP_200_OK
    assert_that(response.json()).is_length(3)
    original_list = [group["name"].lower() for group in response.json()]
    sorted_list = sorted(original_list)
    assert_that(sorted_list).is_equal_to(original_list)


def test_create_group(admin_client, admin_user, user_factory, project_factory, caplog):
    caplog.set_level(logging.INFO)
    user1, user2 = user_factory.create_batch(2)
    project1, project2 = project_factory.create_batch(2)
    perm_add_node = Permission.objects.get(codename="add_node")
    perm_change_project = Permission.objects.get(codename="change_project")
    perm_delete_project = Permission.objects.get(codename="delete_project")
    group_name = GROUP_NAME

    data = {
        "name": group_name,
        "profile": {"description": "This is the group description"},
        "users": [user1.id, user2.id],
        "permissions": [perm_add_node.id],
        "permissions_object": [
            {
                "permission": perm_change_project.id,
                "objects": [project1.id, project2.id],
            },
            {"permission": perm_delete_project.id, "objects": [project2.id]},
        ],
    }
    response = admin_client.post(URL_LIST, data, content_type=APPLICATION_JSON)
    assert response.status_code == status.HTTP_201_CREATED
    assert_that(caplog.record_tuples).is_length(2).extracting(1).contains_only(
        logging.INFO
    )
    assert_that(caplog.record_tuples[1][2]).starts_with(
        f"{str(admin_user)} created"
    ).contains(
        "change_project",
        "delete_project",
        GROUP_NAME,
        project1.code,
        project2.code,
    )
    group = Group.objects.get(name=group_name)
    assert_that(group.profile.description).is_equal_to(data["profile"]["description"])
    object_permissions = GroupObjectPermission.objects.filter(group=group)
    assert_that(object_permissions).is_length(
        sum(len(x["objects"]) for x in data["permissions_object"])
    )
    assert_that(
        set((x.group, x.permission, x.content_object) for x in object_permissions)
    ).is_equal_to(
        {
            (group, perm, proj)
            for perm, proj in (
                (perm_change_project, project1),
                (perm_change_project, project2),
                (perm_delete_project, project2),
            )
        }
    )


def test_create_group_target_does_not_exist(admin_client, project_factory):
    project = project_factory()
    perm_change_project = Permission.objects.get(codename="change_project")
    non_existing_pk = 1_000_000
    initial_group_count = Group.objects.count()
    # Non-existing object
    response = admin_client.post(
        URL_LIST,
        {
            "name": GROUP_NAME,
            "users": [],
            "permissions_object": [
                {
                    "permission": perm_change_project.id,
                    "objects": [non_existing_pk],
                },
            ],
        },
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["permissions_object"] == [
        {
            "non_field_errors": [
                f"Object {non_existing_pk} does not exist for model "
                f"{project._meta.model.__name__}"
            ]
        }
    ]
    # Non-existing permission
    response = admin_client.post(
        URL_LIST,
        {
            "name": GROUP_NAME,
            "users": [],
            "permissions_object": [
                {
                    "permission": non_existing_pk,
                    "objects": [project.id],
                },
            ],
        },
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["permissions_object"] == [
        {"permission": [f"{Permission.__name__} {non_existing_pk} does not exist"]}
    ]
    assert Group.objects.count() == initial_group_count


def test_update_group(admin_client, project_factory, caplog, group_factory):
    caplog.set_level(logging.INFO)
    project1, project2, project3 = project_factory.create_batch(3)
    perm_change_project = Permission.objects.get(codename="change_project")
    perm_delete_project = Permission.objects.get(codename="delete_project")
    group = group_factory.create()
    GroupObjectPermission.objects.bulk_create(
        GroupObjectPermission(
            group=group,
            content_type=ContentType.objects.get_for_model(Project),
            object_pk=str(proj.pk),
            permission=perm,
        )
        for perm, proj in (
            (perm_change_project, project1),
            (perm_change_project, project2),
            (perm_delete_project, project3),
        )
    )
    initial_group_perms = GroupObjectPermission.objects.filter(
        permission=perm_change_project
    )
    initial_group_perm_count = GroupObjectPermission.objects.count()

    url = reverse(f"{VIEW_NAME}-detail", kwargs={"pk": group.pk})
    data = {
        "name": group.name,
        "profile": {"description": "This is a changed description!"},
        "users": [],
        "permissions": [],
        "permissions_object": [
            {
                "permission": perm_change_project.id,
                "objects": [project3.id, project2.id],
            },
        ],
    }
    response = admin_client.put(url, data, content_type=APPLICATION_JSON)
    assert response.status_code == status.HTTP_200_OK
    # one log message for created, another for deleted
    assert_that(caplog.record_tuples).is_length(3).extracting(1).contains_only(
        logging.INFO
    )
    group = Group.objects.get(name=group.name)
    assert_that(group.profile.description).is_equal_to(data["profile"]["description"])
    object_permissions = GroupObjectPermission.objects.filter(
        permission=perm_change_project
    )
    assert_that(object_permissions.difference(initial_group_perms).count()).is_equal_to(
        0
    )
    assert_that(
        set((x.group, x.permission, x.content_object) for x in object_permissions)
    ).is_equal_to(
        {
            (group, perm, proj)
            for perm, proj in (
                (perm_change_project, project2),
                (perm_change_project, project3),
            )
        }
    )

    # Add new permission without deleting anything
    caplog.clear()
    admin_client.patch(
        url,
        {
            "permissions_object": [
                {
                    "permission": perm_change_project.id,
                    "objects": [project3.id, project2.id],
                },
                {
                    "permission": perm_delete_project.id,
                    "objects": [project1.id],
                },
            ]
        },
        content_type=APPLICATION_JSON,
    )
    assert_that(caplog.record_tuples).is_length(2).extracting(1).contains_only(
        logging.INFO
    )
    assert GroupObjectPermission.objects.count() == initial_group_perm_count


def test_delete_group(admin_client, project_factory, group_factory):
    group = group_factory.create()
    GroupObjectPermission.objects.create(
        group=group,
        content_type=ContentType.objects.get_for_model(Project),
        object_pk=str(project_factory().pk),
        permission=Permission.objects.get(codename="change_project"),
    )
    initial_group_perm_count = GroupObjectPermission.objects.count()
    initial_group_count = Group.objects.count()
    response = admin_client.delete(
        reverse(f"{VIEW_NAME}-detail", kwargs={"pk": group.pk})
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert GroupObjectPermission.objects.count() == initial_group_perm_count - 1
    assert Group.objects.count() == initial_group_count - 1


@pytest.mark.django_db
class TestGroupManagerUpdate:
    @staticmethod
    def get_url(group: Group):
        return reverse(f"{VIEW_NAME}-detail", kwargs={"pk": group.pk})

    @pytest.fixture(autouse=True)
    def setup(self, client, group_factory):
        # pylint: disable=attribute-defined-outside-init
        self.managed_group, self.unmanaged_group = group_factory.create_batch(2)
        self.managed_node, self.unmanaged_node = NodeFactory.create_batch(2)
        self.group_manager = make_group_manager(self.managed_group)
        self.user = UserFactory()
        self.perm_node_admin = Permission.objects.get(codename=PERM_NODE_ADMIN)
        self.perm_change_node = Permission.objects.get(codename="change_node")
        GroupObjectPermission.objects.bulk_create(
            GroupObjectPermission(
                group=group,
                content_type=ContentType.objects.get_for_model(Node),
                object_pk=str(node.pk),
                permission=self.perm_node_admin,
            )
            for group, node in (
                (self.managed_group, self.managed_node),
                (self.unmanaged_group, self.unmanaged_node),
            )
        )
        self.data = {
            "name": self.managed_group.name,
            "users": [self.user.id],
            "permissions": [],
            "permissions_object": [
                {
                    "permission": self.perm_node_admin.id,
                    "objects": [self.managed_node.id],
                },
            ],
        }
        self.url = self.get_url(self.managed_group)
        self.client = client
        # pylint: enable=attribute-defined-outside-init

        self.client.login(username=self.group_manager.username, password=USER_PASSWORD)
        yield
        # teardown
        self.client.logout()

    def assert_error_response(self):
        error_response = self.client.put(
            self.url, self.data, content_type=APPLICATION_JSON
        )
        assert error_response.status_code == status.HTTP_400_BAD_REQUEST
        assert error_response.json() == [VALIDATION_ERROR_NON_STAFF_FIELDS]

    def test_can_update_users(self):
        # group manager can update `users` on groups they manage
        response = self.client.put(self.url, self.data, content_type=APPLICATION_JSON)
        assert response.status_code == status.HTTP_200_OK
        assert response.json()["users"] == [self.user.id]

    def test_cannot_update_name(self):
        # group manager CANNOT update `name`
        self.data["name"] = "different"
        self.assert_error_response()

    def test_cannot_update_permissions(self):
        # group manager CANNOT update `permissions`
        self.data["permissions"] = [self.perm_change_node.id]
        self.assert_error_response()

    def test_cannot_update_permission_in_permissions_object(self):
        # group manager CANNOT update `permission` on an existing object in `permissions_object`
        self.data["permissions_object"][0]["permission"] = self.perm_change_node.id
        self.assert_error_response()

    def test_cannot_update_objects_in_permissions_object(self):
        # group manager CANNOT update `objects` on an existing object in `permissions_object`
        self.data["permissions_object"][0]["objects"] = [self.unmanaged_node.id]
        self.assert_error_response()

    def test_cannot_add_object_to_objects_in_permissions_object(self):
        # group manager CANNOT add an object to `objects` on existing object in `permissions_object`
        self.data["permissions_object"][0]["objects"].append(self.unmanaged_node.id)
        self.assert_error_response()

    def test_cannot_add_new_object_to_permissions_object(self):
        # group manager CANNOT add a new object to `permissions_object`
        self.data["permissions_object"].append(
            {
                "permission": self.perm_node_admin.id,
                "objects": [self.unmanaged_node.id],
            }
        )
        self.assert_error_response()

    def test_cannot_update_unmanaged_group_users(self):
        # group manager CANNOT update `users` on groups they DO NOT manage
        url = self.get_url(self.unmanaged_group)
        self.data["name"] = self.unmanaged_group.name
        response = self.client.put(url, self.data, content_type=APPLICATION_JSON)
        assert response.status_code == status.HTTP_404_NOT_FOUND
