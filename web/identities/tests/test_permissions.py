import pytest
from assertpy import assert_that
from django.contrib.auth.models import AnonymousUser
from django.test import TestCase, RequestFactory

from identities.permissions import (
    get_group_manager_groups,
    has_group_manager_permission,
    has_group_manager_groups,
    IsGroupManagerRead,
    IsGroupManagerWrite,
)
from projects.tests.factories import GroupFactory, UserFactory, make_group_manager


class TestGroupManager(TestCase):
    def setUp(self):
        self.managed_group = GroupFactory.create()
        self.group_manager = make_group_manager(self.managed_group)
        self.staff = UserFactory(staff=True)

    def test_get_group_manager_groups(self):
        assert_that(get_group_manager_groups(self.group_manager).all()).contains_only(
            self.managed_group
        )

        managed_group2 = GroupFactory.create()
        make_group_manager(managed_group2, self.group_manager)
        assert_that(get_group_manager_groups(self.group_manager).all()).contains_only(
            self.managed_group, managed_group2
        )

        assert_that(get_group_manager_groups(self.staff).all()).is_empty()

    def test_has_group_manager_groups(self):
        self.assertTrue(has_group_manager_groups(self.group_manager))
        self.assertFalse(has_group_manager_groups(self.staff))
        make_group_manager(self.managed_group, self.staff)
        self.assertTrue(has_group_manager_groups(self.staff))

    def test_has_group_manager_permission(self):
        # only `group_manager` has group manager permission for `managed_group`
        self.assertTrue(
            has_group_manager_permission(self.group_manager, self.managed_group)
        )
        self.assertFalse(has_group_manager_permission(self.staff, self.managed_group))
        # `group_manager` does NOT have group manager permission for all groups
        self.assertFalse(has_group_manager_permission(self.group_manager))


@pytest.mark.django_db
@pytest.mark.parametrize(
    "permission_obj, allowed_methods, forbidden_methods",
    (
        (IsGroupManagerRead(), ("GET", "HEAD"), ("PUT", "PATCH", "POST", "DELETE")),
        (IsGroupManagerWrite(), ("PUT", "PATCH"), ("GET", "HEAD", "POST", "DELETE")),
    ),
)
class TestIsGroupManager:
    @pytest.fixture(autouse=True)
    def before_each(self):
        # pylint: disable=attribute-defined-outside-init
        self.factory = RequestFactory()
        self.user = UserFactory()
        self.staff = UserFactory(staff=True)
        self.managed_group_1, self.managed_group_2 = GroupFactory.create_batch(2)
        self.group_manager_1 = make_group_manager(self.managed_group_1)
        self.group_manager_2 = make_group_manager(self.managed_group_2)
        self.request = self.factory.request()
        # pylint: enable=attribute-defined-outside-init

    # pylint: disable=unused-argument
    def test_anonymous(self, permission_obj, allowed_methods, forbidden_methods):
        for method in allowed_methods:
            self.request.user = AnonymousUser()
            self.request.method = method
            assert permission_obj.has_permission(self.request, None) is False

    def test_user(self, permission_obj, allowed_methods, forbidden_methods):
        for method in allowed_methods:
            self.request.user = self.user
            self.request.method = method
            assert permission_obj.has_permission(self.request, None) is False

    def test_group_manager_own_group(
        self, permission_obj, allowed_methods, forbidden_methods
    ):
        for user, group in (
            (self.group_manager_1, self.managed_group_1),
            (self.group_manager_2, self.managed_group_2),
        ):
            for method in allowed_methods:
                self.request.user = user
                self.request.method = method
                assert permission_obj.has_permission(self.request, None) is True
                assert (
                    permission_obj.has_object_permission(self.request, None, group)
                    is True
                )

    def test_group_manager_other_group(
        self, permission_obj, allowed_methods, forbidden_methods
    ):
        for user, group in (
            (self.group_manager_1, self.managed_group_2),
            (self.group_manager_2, self.managed_group_1),
        ):
            for method in allowed_methods:
                self.request.user = user
                self.request.method = method
                assert permission_obj.has_permission(self.request, None) is True
                assert (
                    permission_obj.has_object_permission(self.request, None, group)
                    is False
                )

    def test_group_manager_forbidden_methods(
        self, permission_obj, allowed_methods, forbidden_methods
    ):
        for method in forbidden_methods:
            self.request.user = self.group_manager_1
            self.request.method = method
            assert permission_obj.has_permission(self.request, None) is False

    def test_staff(self, permission_obj, allowed_methods, forbidden_methods):
        self.request.user = self.staff
        for method in allowed_methods:
            self.request.method = method
            assert permission_obj.has_permission(self.request, None) is False

    # pylint: enable=unused-argument
