from time import sleep

import pytest

from identities.models import AuthenticationLog
from projects.tests.factories import USER_PASSWORD


def test_login_logout_events(client, user_factory):
    # `logout` can be triggered by anonymous users.
    # In pratice it can happen when a user tries to logout,
    # but the session is already expired.
    client.logout()
    assert not AuthenticationLog.objects.exists()
    # User login and logout (while session is valid)
    user = user_factory()
    assert not AuthenticationLog.objects.exists()
    client.login(username=user.username, password=USER_PASSWORD)
    assert AuthenticationLog.objects.count() == 1
    sleep(0.1)
    client.logout()
    logs = AuthenticationLog.objects.all()
    assert len(logs) == 2
    login, logout = logs
    assert login.user == logout.user == user
    assert login.username == logout.username == user.username
    assert login.action == AuthenticationLog.Action.LOGIN.value
    assert logout.action == AuthenticationLog.Action.LOGOUT.value
    assert login.timestamp < logout.timestamp


@pytest.mark.django_db
def test_failed_login(client):
    username = "brian"
    assert not AuthenticationLog.objects.exists()
    client.login(username=username, password="wrong")
    failed = AuthenticationLog.objects.get()
    assert failed.user is None
    assert failed.username == username
    assert failed.action == AuthenticationLog.Action.FAILED.value
