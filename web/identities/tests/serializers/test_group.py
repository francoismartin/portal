from identities.serializers import AnyObjectSerializer


def test_generic_object_serializer(group_factory):
    group = group_factory(name="New Group")
    assert AnyObjectSerializer(group).data == {"id": group.id, "name": str(group)}
