from datetime import datetime, timezone

import pytest

from identities.mail_templates.user_info import user_info_body
from projects.models.project import ProjectRole


@pytest.mark.django_db
def test_user_info_body_with_flags(user_factory, flag_factory):
    flag = flag_factory()
    user = user_factory(post_flags=(flag,))
    assert f"Flags: {flag.code}" in user_info_body(user)


@pytest.mark.parametrize(
    # Test 2 different scenarios where the project code alphabetical order
    # is reversed.
    "p1_code, p2_code",
    [("proj_AA", "proj_BB"), ("proj_BB", "proj_AA")],
)
def test_user_info_body(
    project_factory, user_factory, project_user_role_factory, p1_code, p2_code
):
    """Verify that that user info summary is correctly generated."""
    # Create 2 test projects and 2 test users.
    p1 = project_factory(code=p1_code)
    p2 = project_factory(code=p2_code)
    u1 = user_factory(
        last_login=datetime(2018, 12, 7, 3, 0, 0, 0, timezone.utc),
        profile__affiliation="staff@example.org,member@example.org",
        profile__affiliation_id="123456@example.org,987654@example.org",
    )
    u2 = user_factory(staff=True)

    # When adding users roles to the database, we alternate between project 1
    # and project 2, so that we can check if the function does perform the
    # sorting properly.
    project_user_role_factory(project=p2, user=u1, role=ProjectRole.USER.value)
    project_user_role_factory(project=p1, user=u1, role=ProjectRole.DM.value)
    project_user_role_factory(project=p1, user=u1, role=ProjectRole.PM.value)
    project_user_role_factory(project=p1, user=u2, role=ProjectRole.PM.value)
    project_user_role_factory(project=p2, user=u1, role=ProjectRole.PM.value)

    # Sort projects in alphabetic order, so the asserting below works correctly
    # depending on whether the randomly generated code for p2 is alphabetically
    # greater or smaller than the code for p1.
    project_and_roles = (
        f"{p1.code}: DM, PM\n{p2.code}: USER, PM"
        if p1.code < p2.code
        else f"{p2.code}: USER, PM\n{p1.code}: DM, PM"
    )
    assert user_info_body(u1) == (
        f"User ID: {u1.id}\n"
        f"Display Name: {u1.profile.display_name}\n"
        f"Last Login: {u1.last_login}\n"
        f"Affiliation: {u1.profile.affiliation}\n"
        f"LinkedAffiliationUniqueID: {u1.profile.affiliation_id}\n"
        "Flags: -\n"
        "\n"
        f"Projects and Roles:\n{project_and_roles}"
    )
