from datetime import datetime, timezone
from unittest.mock import patch, Mock

import pytest

import identities
from identities.tasks import user_last_logged_in_notification


@patch.object(identities.tasks, "datetime", Mock(wraps=datetime))
@pytest.mark.usefixtures("apply_notification_settings")
def test_user_last_logged_in_notification(user_factory, mailoutbox):
    subject = "User {} has last logged in 60 days"

    day1 = datetime(2020, 2, 1, 3, 0, 0, 0, timezone.utc)
    day2 = datetime(2020, 2, 2, 3, 0, 0, 0, timezone.utc)
    day3 = datetime(2020, 2, 3, 3, 0, 0, 0, timezone.utc)

    (
        day1_user1,
        day1_user2,
        day1_inactive,
        day2_user1,
        day2_user2,
        day3_user1,
    ) = user_factory.create_batch(6)

    # setup day 1
    day1_user1.last_login = datetime(2000, 12, 3, 13, 0, 0, 0, timezone.utc)

    day1_user2.last_login = datetime(2019, 12, 3, 3, 0, 0, 0, timezone.utc)

    day1_inactive.last_login = day1_user1.last_login
    day1_inactive.is_active = False

    # setup day 2
    day2_user1.last_login = datetime(2019, 12, 3, 3, 0, 0, 1, timezone.utc)

    day2_user2.last_login = datetime(2019, 12, 4, 3, 0, 0, 0, timezone.utc)

    # setup day 3
    day3_user1.last_login = datetime(2019, 12, 5, 3, 0, 0, 1, timezone.utc)

    for user in (
        day1_user1,
        day1_user2,
        day1_inactive,
        day2_user1,
        day2_user2,
        day3_user1,
    ):
        user.save()

    # test day 1
    identities.tasks.datetime.now.return_value = day1

    # pylint: disable=no-value-for-parameter
    user_last_logged_in_notification()

    assert len(mailoutbox) == 2, "Sent two emails"
    email1 = mailoutbox[0]
    assert email1.subject == subject.format(day1_user1.username)
    assert f"Display Name: {day1_user1.profile.display_name}" in email1.body

    email2 = mailoutbox[1]
    assert email2.subject == subject.format(day1_user2.username)

    # test day 2
    identities.tasks.datetime.now.return_value = day2

    # pylint: disable=no-value-for-parameter
    user_last_logged_in_notification()

    assert len(mailoutbox) == 4, "Sent two more emails"
    email3 = mailoutbox[2]
    assert email3.subject == subject.format(day2_user1.username)

    email4 = mailoutbox[3]
    assert email4.subject == subject.format(day2_user2.username)

    # test day 3
    identities.tasks.datetime.now.return_value = day3

    # pylint: disable=no-value-for-parameter
    user_last_logged_in_notification()

    assert len(mailoutbox) == 4, "No emails are sent"
