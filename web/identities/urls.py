from django.conf.urls import include
from django.urls import re_path
from rest_framework.routers import DefaultRouter

from identities.apps import APP_NAME
from identities import views


router = DefaultRouter()
router.register(r"permission", views.PermissionViewSet, "permission")
router.register(
    r"object_by_permission", views.ObjectByPermissionViewSet, "object_by_permission"
)
router.register(r"group", views.GroupViewSet, "group")

app_name = APP_NAME
urlpatterns = [
    re_path(r"^", include(router.urls)),
]
