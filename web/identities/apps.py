from django.apps import AppConfig

APP_NAME = "identities"


class IdentitiesConfig(AppConfig):
    name = APP_NAME

    def ready(self):
        from . import signals  # pylint: disable=import-outside-toplevel,unused-import
