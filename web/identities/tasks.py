import logging
from datetime import timedelta, datetime, timezone

from celery import shared_task
from django.contrib.auth import get_user_model
from django.conf import settings
from django_drf_utils.email import sendmail

from identities.mail_templates.user_info import user_info_body
from projects.models.task import PeriodicTaskRun
from projects.utils.task import get_last_run

User = get_user_model()

logger = logging.getLogger(__name__)


# Remember to migrate `PeriodicTask` and `PeriodicTaskRun` when you move / rename this method!
@shared_task(bind=True, ignore_result=True)
def user_last_logged_in_notification(self, days: int = 60):
    """Notify about inactive users.

    :param self: the task instance
    :param days: number of days after which user's inactivity is reported
    """
    now = datetime.now(timezone.utc)
    threshold_difference = timedelta(days=days)
    kwargs = {"is_active": True}

    last_run = get_last_run(self)
    if last_run:
        kwargs["last_login__gt"] = last_run - threshold_difference
    kwargs["last_login__lte"] = now - threshold_difference
    users = User.objects.filter(**kwargs)
    user: User
    for user in users:
        sendmail(
            subject=f"User {user.username} has last logged in {days} days",
            body=user_info_body(user),
            recipients=(settings.CONFIG.notification.ticket_mail,),
            email_cfg=settings.CONFIG.email,
        )

    PeriodicTaskRun.objects.create(task=self.name, created_at=now)
    logger.info(
        "Successfully sent notification emails for %s users which have last logged in %s days "
        "since the last run on %s.",
        len(users),
        days,
        last_run,
    )
