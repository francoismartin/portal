from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin as BaseGroupAdmin
from django.contrib.auth.models import Group

from .models import AuthenticationLog, GroupProfile


@admin.register(AuthenticationLog)
class AuthenticationLogAdmin(admin.ModelAdmin):
    list_display = ("user", "username", "action", "timestamp")
    list_filter = ("action",)

    def has_add_permission(self, request) -> bool:
        return False

    def has_change_permission(self, request, obj=None) -> bool:
        return False

    def has_delete_permission(self, request, obj=None) -> bool:
        return False


class GroupInline(admin.StackedInline):
    model = GroupProfile
    can_delete = False
    verbose_name_plural = "group profiles"
    fields = ["description"]


class GroupAdmin(BaseGroupAdmin):
    inlines = (GroupInline,)


# Re-register GroupAdmin
admin.site.unregister(Group)
admin.site.register(Group, GroupAdmin)
