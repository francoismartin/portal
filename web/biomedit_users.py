#!/usr/bin/env python

"""Get users for BioMedIT projects.

Run without any arguments to see the list of projects.

User credentials can be stored in the .netrc file or passed with the --user and
--password arguments.
"""

import argparse
from urllib.parse import urljoin

import requests


def parse_options():
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument("-p", "--project", help="Project name")
    parser.add_argument("-u", "--user", help="User name")
    parser.add_argument("-P", "--password", help="User password")
    parser.add_argument(
        "-H", "--host", help="API host URL", default="http://localhost:8000/"
    )
    return parser.parse_args()


def main(options):
    base_url = urljoin(options.host, "backend/projects/")
    if options.user and options.password:
        credentials = (options.user, options.password)
    else:
        credentials = None
    if options.project:
        payload = {"project_id": options.project}
        r = requests.get(base_url, auth=credentials, params=payload)
        if r.ok and r.json():
            for user_url in r.json()[0]["users"]:
                user_response = requests.get(user_url)
                if user_response.ok and r.json():
                    user_data = user_response.json()
                    print(user_data["username"], user_data["email"])
                else:
                    print(user_data, r.json())
        else:
            print(r, r.json())
    else:
        r = requests.get(base_url, auth=credentials)
        if r.ok and r.json():
            project_names = [project["project_id"] for project in r.json()]
            print("\n".join(project_names))
        else:
            print(r, r.json())


if __name__ == "__main__":
    options = parse_options()
    main(options)
