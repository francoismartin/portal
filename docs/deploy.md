# Deployment

## Production

The instructions below describe a containerized deployment with `docker compose`.
If you want to deploy to on Kubernetes see [k8s deployment](../k8s/README.md).

- Create `.env`, `frontend/.env`, `web/.config.json` (check `.env.sample`,
  `frontend/.env.sample`, `web/.config.json.sample`) files, which match your
  service configuration.
- Run:

  ```bash
  docker compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.prod.yml build
  docker compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.prod.yml up -d
  docker compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.prod.yml exec -T web python manage.py migrate
  docker compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.prod.yml exec -T web python user_init.py
  ```

- Access service at your domain name.

Note: the build command creates two images, one for `frontend` and one for `web`.
Their registry prefixes are defined by the `REGISTRY` environment variable
(see `.env.sample`).

### Running without traefik / filebeat

If you have a system with pre deployed traefik, like
[here](https://framagit.org/oxyta.net/proxyta.net), then just omit
`-f compose/traefik.yml` in all `docker compose` invocations.

Similarly, if you don't need filebeat, simply omit the `compose/filebeat.yml` file.

## Production-like local deployment

You can set up a local environment resembling production by using
`docker-compose.local-prod.yml` instead of `docker-compose.prod.yml`.
The `local-prod` deployment runs over plain HTTP and exposes the service
monitoring system at <http://localhost:8080>

```bash
docker compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.local-prod.yml build
docker compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.local-prod.yml up -d
docker compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.local-prod.yml exec -T web python manage.py migrate
docker compose -f docker-compose.yml -f compose/traefik.yml -f compose/filebeat.yml -f docker-compose.local-prod.yml exec -T web python user_init.py
```
