#!/usr/bin/env bash

set -e

PG_VERSION=${PG_VERSION:-13}

volume="$1"
[[ -z "$volume" ]] && echo "💥 No volume specified" >&2 && exit 1
dump="$2"
! [[ -f .env ]] && echo "💥 No .env file found in the current directory" >&2 && exit 1
! grep -q "^POSTGRES_PASSWORD=" .env && echo "💥 .env must set a POSTGRES_PASSWORD variable" >&2 && exit 1

dir=$(dirname $(realpath $0))
! docker run -d --rm --name pg_temp \
       -v "${dir}/init-db.sh":/docker-entrypoint-initdb.d/00-init-db.sh:ro \
       -v "$volume":/var/lib/postgresql/data:rw \
       --env-file .env \
       postgres:$PG_VERSION \
    && echo "💥 failed to start temp docker container" >&2 && exit 1

echo "ℹ️ Waiting for docker process to initialize db:"
# grep -q not working (stops immediately after first match:
docker logs -f pg_temp 2>&1 | grep --max-count=2 'database system was shut down'
echo "ℹ️  --- Output of temp container --- "
docker logs pg_temp
echo "ℹ️  --- End of output of temp container --- "

return_value=$?
if [[ $return_value -eq 0 ]] && [[ -n "$dump" ]] ; then
    echo "ℹ️ Restore $dump"
    "$dir"/db-restore.sh pg_temp < "$dump"
    return_value=$?
fi

echo "ℹ️ Stopping / removing temp container"
docker stop pg_temp

exit $return_value
