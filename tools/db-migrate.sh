#!/usr/bin/env bash

set -euo pipefail

export PG_VERSION=${PG_VERSION:-13}
DB_VOLUME=${DB_VOLUME:-portal_db_data}

TIMESTAMP="$(date +'%Y_%m_%dT%H_%M_%S')"
sql_dump="/tmp/backup_${TIMESTAMP}_db.sql.gz"
dir=$(dirname $(realpath $0))

! "$dir"/db-dump.sh > "$sql_dump" && echo "💥 Could not backup db" >&2 && exit 1
docker stop portal_db_1 || exit 1
! "$dir"/volume-dump.sh "$DB_VOLUME" > /tmp/${DB_VOLUME}-${TIMESTAMP}.tar && echo "💥 Could not backup volume" >&2 && exit 1
docker rm portal_db_1 && docker volume rm "$DB_VOLUME" || exit 1
"$dir"/init-db-volume.sh "$DB_VOLUME" "$sql_dump"

echo "🎊 Migration successful"
